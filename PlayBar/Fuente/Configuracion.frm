VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form Configuracion 
   Caption         =   "Configuraci�n"
   ClientHeight    =   6360
   ClientLeft      =   90
   ClientTop       =   450
   ClientWidth     =   10665
   Icon            =   "Configuracion.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6360
   ScaleWidth      =   10665
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   9360
      Top             =   4560
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "par_rubros_venta"
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame Frame6 
      Caption         =   "Happy Hours"
      Height          =   2655
      Left            =   4440
      TabIndex        =   38
      Top             =   3600
      Width           =   4815
      Begin VB.TextBox TxtH2 
         Height          =   285
         Left            =   2280
         TabIndex        =   46
         Text            =   "22:30"
         Top             =   1920
         Width           =   1095
      End
      Begin VB.TextBox TxtH1 
         Height          =   285
         Left            =   2280
         TabIndex        =   45
         Text            =   "18:30"
         Top             =   1560
         Width           =   1095
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Intervales especificados"
         Height          =   375
         Left            =   720
         TabIndex        =   42
         Top             =   1200
         Width           =   2055
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Disponible Todo horario"
         Height          =   375
         Left            =   720
         TabIndex        =   41
         Top             =   840
         Value           =   -1  'True
         Width           =   2175
      End
      Begin VB.ComboBox CboHappy 
         Height          =   315
         Left            =   2280
         Style           =   2  'Dropdown List
         TabIndex        =   40
         Top             =   360
         Width           =   2295
      End
      Begin VB.Label Label6 
         Caption         =   "(HH:MM)"
         Height          =   255
         Left            =   3480
         TabIndex        =   47
         Top             =   1800
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "Hora Fin     :"
         Height          =   375
         Left            =   1200
         TabIndex        =   44
         Top             =   1920
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Hora Inicio  :"
         Height          =   255
         Left            =   1200
         TabIndex        =   43
         Top             =   1560
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Rubro asocioado a Happy Hours"
         Height          =   495
         Left            =   720
         TabIndex        =   39
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Impresora Fiscal"
      Height          =   735
      Left            =   240
      TabIndex        =   28
      Top             =   5880
      Width           =   3735
      Begin VB.CommandButton CmdCimpresion 
         Caption         =   "Configurar Impresi�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1680
         TabIndex        =   35
         Top             =   360
         Width           =   1575
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Imprime boleta"
         Height          =   375
         Index           =   15
         Left            =   120
         TabIndex        =   30
         Top             =   240
         Width           =   1455
      End
      Begin VB.CheckBox Check5 
         Caption         =   "Activado"
         DataField       =   "ImprimeFiscal"
         DataSource      =   "AdoConfig"
         Height          =   255
         Left            =   1680
         TabIndex        =   29
         Top             =   120
         Width           =   1215
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Ventas"
      Height          =   2730
      Left            =   225
      TabIndex        =   2
      Top             =   1275
      Width           =   3732
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Documentos de Pago"
         Height          =   375
         Index           =   18
         Left            =   150
         TabIndex        =   36
         Top             =   2160
         Width           =   2400
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Activado"
         DataField       =   "CuadroGarzones"
         DataSource      =   "AdoConfig"
         Enabled         =   0   'False
         Height          =   372
         Left            =   2640
         TabIndex        =   14
         Top             =   1800
         Width           =   972
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Muestra Lista de Garzones"
         Height          =   372
         Index           =   6
         Left            =   120
         TabIndex        =   13
         Top             =   1800
         Width           =   2412
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Maestro de Rubros"
         Height          =   372
         Index           =   5
         Left            =   120
         TabIndex        =   12
         Top             =   1440
         Width           =   2412
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Activado"
         DataField       =   "EliminarVenta"
         DataSource      =   "AdoConfig"
         Enabled         =   0   'False
         Height          =   252
         Left            =   2640
         TabIndex        =   8
         Top             =   1200
         Width           =   972
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Permite eliminar Venta"
         Height          =   372
         Index           =   4
         Left            =   120
         TabIndex        =   5
         Top             =   1080
         Width           =   2412
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "N�mero de Mesas (de 11 en 11)"
         Height          =   372
         Index           =   3
         Left            =   120
         TabIndex        =   4
         Top             =   720
         Width           =   2412
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Opci�n Descuento"
         Height          =   372
         Index           =   2
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   2412
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Activado"
         DataField       =   "Descuento"
         DataSource      =   "AdoConfig"
         Enabled         =   0   'False
         Height          =   192
         Left            =   2640
         TabIndex        =   6
         Top             =   480
         Width           =   972
      End
      Begin VB.Label LBmesas 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         DataField       =   "NumeroMesas"
         DataSource      =   "AdoConfig"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   252
         Left            =   2640
         TabIndex        =   7
         Top             =   840
         Width           =   732
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "General"
      Height          =   3375
      Left            =   4320
      TabIndex        =   16
      Top             =   120
      Width           =   4935
      Begin VB.CheckBox Check7 
         Caption         =   "Activado"
         DataField       =   "CargaConMouse"
         DataSource      =   "AdoConfig"
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   34
         Top             =   3000
         Width           =   1095
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Permite cargar con mouse"
         Height          =   375
         Index           =   17
         Left            =   240
         TabIndex        =   33
         Top             =   2880
         Width           =   2535
      End
      Begin VB.CheckBox Check6 
         Caption         =   "Activado"
         DataField       =   "ImpCentroProduccion"
         DataSource      =   "AdoConfig"
         Enabled         =   0   'False
         Height          =   255
         Left            =   2880
         TabIndex        =   32
         Top             =   2640
         Width           =   1455
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Imprime-centros de produccion"
         Height          =   375
         Index           =   16
         Left            =   240
         TabIndex        =   31
         Top             =   2520
         Width           =   2535
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Imprimir en Columna"
         Height          =   372
         Index           =   14
         Left            =   240
         TabIndex        =   25
         Top             =   2160
         Width           =   2532
      End
      Begin VB.CheckBox Check4 
         Caption         =   "Activado"
         DataField       =   "SacaTicket"
         DataSource      =   "AdoConfig"
         Enabled         =   0   'False
         Height          =   252
         Left            =   2880
         TabIndex        =   23
         Top             =   1560
         Width           =   972
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Nivel del sistema"
         Height          =   372
         Index           =   13
         Left            =   240
         TabIndex        =   22
         Top             =   1800
         Width           =   2532
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Imprime Ticket"
         Height          =   372
         Index           =   12
         Left            =   240
         TabIndex        =   21
         Top             =   1440
         Width           =   2532
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Porcentaje Desc Card"
         Height          =   372
         Index           =   11
         Left            =   240
         TabIndex        =   19
         Top             =   1080
         Width           =   2532
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Formas de Pago"
         Height          =   372
         Index           =   10
         Left            =   240
         TabIndex        =   18
         Top             =   720
         Width           =   2532
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Comentario en Ticket"
         Height          =   372
         Index           =   9
         Left            =   240
         TabIndex        =   17
         Top             =   360
         Width           =   2532
      End
      Begin VB.Label Label2 
         Caption         =   "No puede ser < 4"
         Height          =   255
         Left            =   3480
         TabIndex        =   27
         Top             =   2280
         Width           =   1215
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "ColumnasTicket"
         DataSource      =   "AdoConfig"
         Height          =   255
         Left            =   2880
         TabIndex        =   26
         Top             =   2280
         Width           =   495
      End
      Begin VB.Label Lblevel 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "NivelSistema"
         DataSource      =   "AdoConfig"
         Height          =   252
         Left            =   2880
         TabIndex        =   24
         Top             =   1920
         Width           =   492
      End
   End
   Begin MSAdodcLib.Adodc AdoConfig 
      Height          =   312
      Left            =   480
      Top             =   6720
      Width           =   2172
      _ExtentX        =   3836
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Config"
      Caption         =   "Configuracion"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame Frame3 
      Caption         =   "Equipo (Este Computador)"
      Height          =   1815
      Left            =   195
      TabIndex        =   9
      Top             =   4080
      Width           =   3735
      Begin VB.CommandButton CmdIdEquipo 
         Caption         =   "Id Equipo"
         Height          =   375
         Left            =   120
         TabIndex        =   37
         Top             =   1320
         Width           =   2535
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Designar Impresoras"
         Height          =   372
         Index           =   8
         Left            =   120
         TabIndex        =   15
         Top             =   960
         Width           =   2532
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Unidad y Directorio de Trabajo"
         Height          =   372
         Index           =   7
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   2532
      End
      Begin VB.Label LbRuta 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Directorio"
         DataSource      =   "AdoConfig"
         Height          =   255
         Left            =   600
         TabIndex        =   20
         Top             =   600
         Width           =   1695
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Licencia"
      Height          =   1092
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   2772
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Niveles de Seguridad"
         Height          =   372
         Index           =   1
         Left            =   120
         TabIndex        =   11
         Top             =   600
         Width           =   2172
      End
      Begin VB.CommandButton CmdConfig 
         Caption         =   "Empresa"
         Height          =   372
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   2172
      End
   End
   Begin VB.Label Label7 
      Caption         =   "Label7"
      DataField       =   "NombreRubro"
      DataSource      =   "Adodc1"
      Height          =   255
      Left            =   9480
      TabIndex        =   48
      Top             =   5040
      Width           =   615
   End
End
Attribute VB_Name = "Configuracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Nmesas As Integer

Private Sub CmdCimpresion_Click()
 '   MiBoleta.Show
End Sub

Private Sub CmdConfig_Click(Index As Integer)
    Dim nm As String
    Dim MDesc As String
    Dim listaGar As Integer
    If Index = 1 Then
        Configuracion.Enabled = False
        Security.Show
    End If
    If Index = 2 Then
        MDesc = MsgBox("�Mostrar opci�n de Descuento?", vbInformation + vbYesNo, "Descuento a Clientes")
        If MDesc = 6 Then
            Check1.Value = 1
        ElseIf MDesc = 7 Then
            Check1.Value = 0
        End If
     End If
    
    If Index = 3 Then
        nm = Nmesas
        nm = InputBox("El n�mero de Mesas actual es =" & Nmesas & Chr(13) & " Ingrese el nuevo n�mero de Mesas", "Cantidad de Mesas", Nmesas)
        nm = Val(nm)
        If Nmesas <> nm And nm > 0 Then
            Nmesas = nm
            LBmesas.Caption = Nmesas
            AdoConfig.Recordset.MoveFirst
        End If
    End If
    
    If Index = 4 Then
        MDesc = MsgBox("�Permitir eliminar venta?", vbInformation + vbYesNo, "Eliminacion de Venta")
        If MDesc = 6 Then
            Check2.Value = 1
        ElseIf MDesc = 7 Then
            Check2.Value = 0
        End If
    End If
    
    If Index = 5 Then
        Configuracion.Visible = False
        Rubro.Show
    End If
    
    If Index = 6 Then
        listaGar = MsgBox("Desea que se muestre un cuadro de " & Chr(13) & _
                                         "los garzones Activos al momento de " & Chr(13) & _
                                         "Seleccionar el Garz�n", vbInformation + vbYesNo, "Garzones")
        If listaGar = 6 Then Check3.Value = 1
        If listaGar = 7 Then Check3.Value = 0
    End If
    
    If Index = 7 Then ''' Directorio de trabajo"
        miruta = InputBox("Escriba la ubicaci�n completa" & Chr(13) & _
                                         "de los archivos del programa " & Chr(13) & _
                                         "U:\ruta  ...", "Ruta de Archivos")
        If Len(miruta) > 0 Then LbRuta.Caption = miruta
    End If
    If Index = 8 Then
        Configuracion.Visible = False
        DImpresoras.Show
    End If
    If Index = 9 Then
        comenTicket = InputBox("Escriba el comentario que ir� impreso " & Chr(13) & _
        "en la parte final del Ticket", "Comentario en Ticket")
        If Len(comenTicket) > 0 Then
            AdoConfig.Recordset.Fields(19) = comenTicket
        End If
    End If
    If Index = 10 Then
        'Configuracion.Visible = False   '''   Archivo de Formas de Pago
        'FormasDePago.Show
        LasFormasdPago.Show
    End If
    If Index = 11 Then
        desctarj = InputBox("Porcentaje a Descontar en Tarjeta ...", "Tarjeta de Cr�dito")
        If IsNumeric(desctarj) Then
            AdoConfig.Recordset.Fields(20) = Val(desctarj)
        End If
    End If
     If Index = 12 Then
        MDesc = MsgBox("�Saca ticket impreso ... ?", vbQuestion + vbYesNo, "Tickets")
        If MDesc = 6 Then
            Check4.Value = 1
        ElseIf MDesc = 7 Then
            Check4.Value = 0
        End If
    End If
      If Index = 13 Then
        desctarj = InputBox("Digite el nivel de seguridad:" & Chr(13) & _
        "1.- Total (Adici�n y Caja)" & Chr(13) & _
        "2.- Parcial (Solo Adici�n, caja aparte)", "Nivel de control")
        If IsNumeric(desctarj) Then
            Lblevel.Caption = Val(desctarj)
        End If
    End If
    
    If Index = 14 Then
        printColumn = InputBox("Columna en la que se imprime ...", "Tarjeta de Cr�dito")
        If IsNumeric(desctarj) Then
            AdoConfig.Recordset.Fields(22) = Val(printColumn)
        End If
    End If
    
    If Index = 15 Then
        MDesc = MsgBox("Si tiene instalada una impresora fiscal" & Chr(13) & _
        "puede activar esta casilla para imprimir la boleta...", vbQuestion + vbYesNo, "Impresora Fiscal")
        
        If MDesc = 6 Then
            Check5.Value = 1
        ElseIf MDesc = 7 Then
            Check5.Value = 0
        End If
    End If
     If Index = 16 Then  'Imprime centro produccion
        MDesc = MsgBox("Si tiene instaladas impresora en los" & Chr(13) & _
        "centros de produccion, Puede activar esta casilla ...", vbQuestion + vbYesNo, "Centros de Producci�n")
        
        If MDesc = 6 Then
            Check6.Value = 1
        ElseIf MDesc = 7 Then
            Check6.Value = 0
        End If
    End If
    If Index = 17 Then  'Imprime centro produccion
        MDesc = MsgBox("Si desea cargar los productos con el mouse" & Chr(13) & _
        "centros de Active esta casilla presionando Aceptar...", vbQuestion + vbYesNo, "Centros de Producci�n")
        
        If MDesc = 6 Then
            Check7.Value = 1
        ElseIf MDesc = 7 Then
            Check7.Value = 0
        End If
    End If
    If Index = 18 Then DocumentosPago.Show 1
End Sub

Private Sub CmdIdEquipo_Click()
    LbIdEquipo.Caption = InputBox("Describa el Id de este PC", "Identificaci�n de equipo")
End Sub

Private Sub Form_Load()
    Dim Hr As Integer: Hr = FreeFile
    Dim HaRu As String, Horario As String
    Open LaRutadeArchivos & "exactfood\RHappy.aso" For Input As Hr
        Line Input #Hr, HaRu
        Line Input #Hr, Horario
    Close Hr
   
    'Me.CboHappy.Text = HaRu
    
    
    
    With Adodc1.Recordset
        .MoveFirst
        Do While Not .EOF
            CboHappy.AddItem .Fields(1)
            .MoveNext
        Loop
    End With
    Nmesas = AdoConfig.Recordset.Fields(5)
    Unload Main
    Unload ListaGarzones
    If Option1.Value = True Then
        TxtH1.Enabled = False
        TxtH2.Enabled = False
    End If
    For i = 0 To CboHappy.ListCount - 1
        
        If CboHappy.List(i) = HaRu Then
            CboHappy.ListIndex = i
            Exit For
        End If
    Next
    If Horario = "Todo" Then
        Option1.Value = True
        Option2.Value = False
    Else
        Option2.Value = True
        Option1.Value = False
        TxtH1.Text = Mid(Horario, 1, 5)
        TxtH2.Text = Mid(Horario, 6)
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    AdoConfig.Recordset.MoveFirst
    Hr = FreeFile
    Open LaRutadeArchivos & "exactfood\RHappy.aso" For Output As Hr
        Print #Hr, Me.CboHappy.Text
        If Option1.Value = True Then
            Print #Hr, "Todo"
        Else
            Print #Hr, TxtH1.Text & TxtH2.Text
        End If
    Close Hr
        
        
    Unload Pago
    Load Main
End Sub


Private Sub Option1_Click()
    If Option1.Value = True Then
        TxtH1.Enabled = False
        TxtH2.Enabled = False
    Else
        TxtH1.Enabled = True
        TxtH2.Enabled = True
    End If
End Sub

Private Sub Option2_Click()
    If Option2.Value = True Then
        TxtH1.Enabled = True
        TxtH2.Enabled = True
    Else
        TxtH1.Enabled = False
        TxtH2.Enabled = False
    End If
    
End Sub
