VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Pago 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pago de Ticket"
   ClientHeight    =   6450
   ClientLeft      =   2040
   ClientTop       =   1320
   ClientWidth     =   8325
   ControlBox      =   0   'False
   FillColor       =   &H00800000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6450
   ScaleWidth      =   8325
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox ListaFpagos 
      Height          =   1035
      Left            =   6120
      TabIndex        =   13
      Top             =   3240
      Visible         =   0   'False
      Width           =   975
   End
   Begin MSAdodcLib.Adodc AdoFpagos 
      Height          =   330
      Left            =   6480
      Top             =   6120
      Visible         =   0   'False
      Width           =   2160
      _ExtentX        =   3810
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "FormasDePago"
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "Pago.frx":0000
      Height          =   1575
      Left            =   3960
      TabIndex        =   12
      Top             =   4440
      Visible         =   0   'False
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   2778
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      FormatLocked    =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   3
      BeginProperty Column00 
         DataField       =   "FormaDePago"
         Caption         =   "FormaDePago"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   "Rendible"
         Caption         =   "Rendible"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column02 
         DataField       =   "Contable"
         Caption         =   "Contable"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
         BeginProperty Column02 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc AdoMonedas 
      Height          =   330
      Left            =   360
      Top             =   6120
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Extranjeras"
      Caption         =   "Monedas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MshMonedas 
      Bindings        =   "Pago.frx":0018
      Height          =   2295
      Left            =   240
      TabIndex        =   10
      Top             =   3600
      Width           =   2775
      _ExtentX        =   4895
      _ExtentY        =   4048
      _Version        =   393216
      FixedCols       =   0
      HighLight       =   2
      FillStyle       =   1
      ScrollBars      =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial CE"
         Size            =   8.25
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
      _Band(0)._NumMapCols=   2
      _Band(0)._MapCol(0)._Name=   "Nombre"
      _Band(0)._MapCol(0)._RSIndex=   0
      _Band(0)._MapCol(1)._Name=   "Valor Nacional"
      _Band(0)._MapCol(1)._RSIndex=   1
      _Band(0)._MapCol(1)._Alignment=   7
   End
   Begin VB.CommandButton CmdFpago 
      BackColor       =   &H0080FF80&
      Caption         =   "Command1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   0
      Left            =   4080
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   240
      Width           =   3975
   End
   Begin MSAdodcLib.Adodc AdoTickets 
      Height          =   315
      Left            =   4320
      Top             =   6120
      Visible         =   0   'False
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   582
      ConnectMode     =   8
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   2
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Tickets"
      Caption         =   "AdoTickets"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label LbDoc 
      Height          =   255
      Left            =   840
      TabIndex        =   14
      Top             =   1080
      Width           =   1815
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      DataField       =   "Nombre"
      DataSource      =   "AdoMonedas"
      Height          =   135
      Left            =   3120
      TabIndex        =   11
      Top             =   6240
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Total Monedas extranjeras"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   600
      TabIndex        =   9
      Top             =   3000
      Width           =   1815
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H80000002&
      FillStyle       =   0  'Solid
      Height          =   3255
      Index           =   1
      Left            =   120
      Top             =   2880
      Width           =   3015
   End
   Begin VB.Label LBPagoNDoc 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1200
      TabIndex        =   8
      Top             =   360
      Width           =   1215
   End
   Begin VB.Label LBMonto 
      BackColor       =   &H00000000&
      Caption         =   "N�"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000014&
      Height          =   495
      Index           =   2
      Left            =   960
      TabIndex        =   7
      Top             =   240
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      DataField       =   "Nticket"
      DataSource      =   "AdoTickets"
      Height          =   255
      Left            =   7800
      TabIndex        =   6
      Top             =   4560
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label LBmesa 
      Caption         =   "Mesa"
      Height          =   255
      Left            =   7425
      TabIndex        =   5
      Top             =   3735
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label LBgarzon 
      Caption         =   "garzon"
      Height          =   255
      Left            =   7425
      TabIndex        =   4
      Top             =   3495
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label LBpedido 
      Caption         =   "npedido"
      Height          =   255
      Left            =   3120
      TabIndex        =   3
      Top             =   6000
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H80000002&
      FillStyle       =   0  'Solid
      Height          =   495
      Index           =   0
      Left            =   600
      Top             =   960
      Width           =   2295
   End
   Begin VB.Label LBFinal 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   19.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      TabIndex        =   2
      Top             =   2040
      Width           =   2535
   End
   Begin VB.Label LBMonto 
      Alignment       =   2  'Center
      BackColor       =   &H00000000&
      Caption         =   "MONTO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000014&
      Height          =   975
      Index           =   1
      Left            =   240
      TabIndex        =   1
      Top             =   1680
      Width           =   3255
   End
End
Attribute VB_Name = "Pago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public TipoDoc As String
Private Sub CmdFpago_Click(Index As Integer)
    
    Dim Fpago As String
    Dim Monto As Double
    Dim TotalFinal As Double
    Dim PagoEs As String
    Dim PorsiTotal As Double
    Dim StMesa As String
    
    TipoDoc = LbDoc.Caption
    TotalFinal = 0
    Monto = LBFinal.Caption
    PorsiTotal = 0
    
    
    If CmdFpago.Count > 1 Then
        If Index = CmdFpago.Count - 1 And CmdFpago(Index).Caption <> "Credito." Then GoTo RutAnula
    End If
    For F = 0 To CmdFpago.Count - 2
        CmdFpago(F).Enabled = False
    Next
    CmdFpago(Index).Enabled = False
    TotalFinal = Val(LBFinal.Caption)
    Monto = Val(Me.LBFinal.Caption)
   
    If Monto < 0 Then Exit Sub  '' Ignora si monto es 0 (cero)
        Dim aplDesc As Variant
        aplDesc = MesaDescuento(lbmesa.Caption)
        If aplDesc <> "" Or aplDesc = "0" Then '' Si es que hay descuento
            If PorsiTotal > 0 Then antTotal = PorsiTotal Else antTotal = TotalFinal
            If Mid(aplDesc, 1, 1) = "$" Then
                valordesc = Mid(aplDesc, 2)
                porcentaje = (valordesc * 100) / (antTotal + valordesc)
                porcentaje = Val(Mid(Str(porcentaje), 1, 5))
            Else
                porcentaje = Val(MesaDescuento(lbmesa.Caption))
            End If
            If RubroDef1(lbmesa.Caption) > 0 Then
                desct1 = (RubroDef1(lbmesa.Caption) / 100) * porcentaje
                RubroDef1(lbmesa.Caption) = RubroDef1(lbmesa.Caption) - desct1
            End If
            If RubroDef2(lbmesa.Caption) > 0 Then
                desct2 = (RubroDef2(lbmesa.Caption) / 100) * porcentaje
                RubroDef2(lbmesa.Caption) = RubroDef2(lbmesa.Caption) - desct2
            End If
        End If
    Fpago = CmdFpago(Index).Caption
   
    If Monto < TotalFinal Then   '' se paga en mas de una forma de pago
        PorsiTotal = TotalFinal
        LBFinal.Caption = TotalFinal - Monto
        TxtMonto.Text = LBFinal.Caption
        TxtMonto.Text = LBFinal.Caption
        With AdoTickets.Recordset
            .AddNew
            .Fields(0) = LBpedido.Caption
            .Fields(1) = lbmesa.Caption
            .Fields(2) = Monto
            .Fields(3) = CmdFpago(Index).Caption
            If LBGARZON.Caption = "" Then LBGARZON.Caption = "N"
            .Fields(4) = LBGARZON.Caption
            .Fields(5) = Time
            .Fields(6) = Main.TurnoFecha.Caption
            .Fields(7) = TipoDoc
            If TipoDoc <> "N" Then .Fields(8) = Val(LBPagoNDoc.Caption) Else .Fields(8) = 0
            .Fields(9) = Main.TurnoTurno.Caption
            .Fields(12) = Val(Main.TxtNpersonas.Text)
            .MoveFirst
            CmdFpago(CmdFpago.Count - 1).Enabled = False
            CmdFpago(CmdFpago.Count - 2).Enabled = False
            
                
        End With
        TxtMonto.SelLength = Len(TxtMonto.Text)
        TxtMonto.SetFocus
        Exit Sub
    
    
    
    ElseIf Monto = TotalFinal Then '' GRABAR TICKET
        
    
        With AdoTickets.Recordset
             '.MoveLast
            .AddNew
            .Fields(0) = LBpedido.Caption
            .Fields(1) = Main.TxtMesaSel.Text
            .Fields(2) = Monto
            .Fields(3) = CmdFpago(Index).Caption
            If LBGARZON.Caption = "" Then LBGARZON.Caption = "N"
            .Fields(4) = LBGARZON.Caption
            .Fields(5) = Time
            .Fields(6) = Main.TurnoFecha.Caption
            .Fields(7) = TipoDoc
            If TipoDoc <> "N" Then .Fields(8) = Val(LBPagoNDoc.Caption) Else .Fields(8) = 0
            .Fields(9) = Main.TurnoTurno.Caption
            
        
            
            
            .Fields(10) = RubroDef1(lbmesa.Caption)
            .Fields(11) = RubroDef2(lbmesa.Caption)
            .Fields(12) = Val(Main.TxtNpersonas.Text)
            If Saldillo > 0 Then
                .AddNew
                .Fields(0) = LBpedido.Caption
                .Fields(1) = Main.TxtMesaSel.Text
                .Fields(2) = Saldillo
                .Fields(3) = "Efectivo."
                If LBGARZON.Caption = "" Then LBGARZON.Caption = "N"
                .Fields(4) = LBGARZON.Caption
                .Fields(5) = Time
                .Fields(6) = Main.TurnoFecha.Caption
                .Fields(7) = "Boleta"
                .Fields(8) = Boletilla
                .Fields(9) = Main.TurnoTurno.Caption
                .Fields(10) = 0
                .Fields(11) = 0
                .Fields(12) = Val(Main.TxtNpersonas.Text)
                Boletilla = 0
                Saldillo = 0
            End If
            .UpdateBatch adAffectCurrent
            .MoveFirst
        End With
        Q = FreeFile
        StMesa = "StatusTable" & lbmesa.Caption & ".tab"
        
        Open PathMesas & StMesa For Output As Q
            Print #Q, "Si-" & IdEquipo & "-" & Now
            Print #Q, "Nothing"
            Print #Q, Time
            Print #Q, 0
            Print #Q, 1
            Print #Q, 0
            Print #Q, 0
            Print #Q, 0
            Print #Q, 0
        Close #Q
       
   
        
        GarzonMesa(lbmesa.Caption) = ""
        MesaStatus(lbmesa.Caption) = 1
        MesaUpedido(lbmesa.Caption) = 0
        MesaDescuento(lbmesa.Caption) = 0
      
        RubroDef1(lbmesa.Caption) = 0
        RubroDef2(lbmesa.Caption) = 0
        With Main
            If MesaSelectItem <> lbmesa.Caption Then
                .lbmesa(lbmesa.Caption - 1).BackColor = MainColores(1)
      
                
            End If
            .TxtMesaSel.Text = "0"
            '.txtGarzonSel.Text = ""
            If MesaSelectItem = lbmesa.Caption Then
                'Si solo son parciales
                For BO = .List1(MesaSelectItem).ListCount - 1 To 0 Step -1
                    .List1(MesaSelectItem).RemoveItem (BO)
                Next
            Else
                'Si es mesa completa
                For BO = .List1(lbmesa.Caption - 1).ListCount - 1 To 0 Step -1
                    .List1(lbmesa.Caption - 1).RemoveItem (BO)
                Next
                Main.MesaAcero (lbmesa.Caption)
            End If
            .LBTOTAL.Caption = "0"
        End With
    '************************
' Esta info es para el robot, al
        'cazar esta informacion debe avisar al resto
        'que la mesa esta desocupada
    End If
    ''' Despues de los procedimientos anteriores
    '' del cobro del ticket
    
    Unload Me
RutAnula:
    Unload Me
    'FrmFondo.Enabled = True
    'Main.Enabled = True
    'Main.Show
    'Main.WindowState = 2
End Sub


Private Sub CmdFpago_GotFocus(Index As Integer)
     For cc = 0 To CmdFpago.Count - 1
        CmdFpago(cc).BackColor = &H80FF80
    Next
    CmdFpago(Index).BackColor = &HC000&
  
End Sub




Private Sub Form_Unload(Cancel As Integer)
ElMainActivo = True
End Sub
