VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form ParALternativas 
   Caption         =   "Mantenedor de Alternativas"
   ClientHeight    =   5130
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   8955
   LinkTopic       =   "Form1"
   ScaleHeight     =   5130
   ScaleWidth      =   8955
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer 
      Interval        =   5
      Left            =   870
      Top             =   4800
   End
   Begin ACTIVESKINLibCtl.Skin Skin 
      Left            =   240
      OleObjectBlob   =   "ParAlternativas.frx":0000
      Top             =   4845
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   300
      Left            =   7440
      TabIndex        =   3
      Top             =   4185
      Width           =   1095
   End
   Begin VB.CommandButton cmdEditar 
      Caption         =   "Editar"
      Height          =   300
      Left            =   1515
      TabIndex        =   2
      Top             =   4200
      Width           =   1095
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "Nueva"
      Height          =   300
      Left            =   375
      TabIndex        =   1
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Frame Frame 
      Caption         =   "Alernativas"
      Height          =   3540
      Left            =   375
      TabIndex        =   0
      Top             =   585
      Width           =   8175
      Begin MSComctlLib.ListView LvAlternativas 
         Height          =   2970
         Left            =   150
         TabIndex        =   4
         Top             =   360
         Width           =   7695
         _ExtentX        =   13573
         _ExtentY        =   5239
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "ID Alt."
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Descripcion"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Text            =   "Precio"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Text            =   "Cant. Alt."
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Activo"
            Object.Width           =   1411
         EndProperty
      End
   End
End
Attribute VB_Name = "ParALternativas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdEditar_Click()
    If LvAlternativas.SelectedItem Is Nothing Then Exit Sub
    IM_Id_Alternativa = LvAlternativas.SelectedItem.Text
    BM_NuevaAlternativa = False
    OpcionesAlternativas.Show 1
    CargaAlt
End Sub

Private Sub cmdNuevo_Click()
    BM_NuevaAlternativa = True
    OpcionesAlternativas.Show 1
    CargaAlt
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
      Aplicar_skin Me
      CargaAlt
End Sub
Private Sub CargaAlt()
    Sql = "SELECT pro_alternativa_id,pro_alternativa_nombre,pro_alternativa_p_venta,pro_alternativa_cantidad,pro_alternativa_activo " & _
          "FROM productos_alternativas "
    Call Consulta(Rst_tmp, Sql)
    
    LLenar_Grilla Rst_tmp, Me, LvAlternativas, False, True, True, False

End Sub

Private Sub Timer_Timer()
  
    LvAlternativas.SetFocus
    Timer.Enabled = False
   
End Sub
