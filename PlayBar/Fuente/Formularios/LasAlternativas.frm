VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form LasAlternativas 
   Caption         =   "Alternativas"
   ClientHeight    =   8850
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   13065
   LinkTopic       =   "Form1"
   ScaleHeight     =   8850
   ScaleWidth      =   13065
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame 
      Caption         =   "Produc"
      Height          =   8130
      Index           =   1
      Left            =   5475
      TabIndex        =   2
      Top             =   360
      Width           =   7980
      Begin VB.Frame Frame 
         Caption         =   "Alternativas Seleccionadas"
         Height          =   1815
         Index           =   3
         Left            =   330
         TabIndex        =   6
         Top             =   6045
         Width           =   6135
      End
      Begin MSComctlLib.ListView LvProductos 
         Height          =   5535
         Left            =   300
         TabIndex        =   5
         Top             =   405
         Width           =   6795
         _ExtentX        =   11986
         _ExtentY        =   9763
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Lista de Alternativas"
      Height          =   8175
      Index           =   0
      Left            =   270
      TabIndex        =   0
      Top             =   360
      Width           =   5130
      Begin VB.Frame Frame 
         Caption         =   "Productos de la ALTERNATIVA"
         Height          =   3210
         Index           =   2
         Left            =   240
         TabIndex        =   3
         Top             =   4740
         Width           =   4425
         Begin MSComctlLib.ListView lvProAlt 
            Height          =   2700
            Left            =   75
            TabIndex        =   4
            Top             =   330
            Width           =   4170
            _ExtentX        =   7355
            _ExtentY        =   4763
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
      End
      Begin MSComctlLib.ListView LvAlternativas 
         Height          =   4065
         Left            =   225
         TabIndex        =   1
         Top             =   435
         Width           =   4470
         _ExtentX        =   7885
         _ExtentY        =   7170
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Timer Timer 
      Interval        =   5
      Left            =   645
      Top             =   5190
   End
   Begin ACTIVESKINLibCtl.Skin Skin 
      Left            =   15
      OleObjectBlob   =   "LasAlternativas.frx":0000
      Top             =   5160
   End
End
Attribute VB_Name = "LasAlternativas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Timer_Timer()
    Aplicar_skin Me
    Timer.Enabled = False
End Sub
Private Sub CargaDatos()
    Sql = "SELECT pro_alternativa_id as ID,pro_alternativa_nombre AS 'Nombre Alternativa'," & _
          "pro_alternativa_p_venta AS Precio,pro_alternativa_cantidad as Cantidad " & _
          "FROM productos_alternativas " & _
          "WHERE productos_alternativa_activo='SI'"
    Call Consulta(Rst_tmp, Sql)
    LLenar_Grilla Rst_tmp, Me, LvAlternativas, True, True, True, False
          
    'select
End Sub
