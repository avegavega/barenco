VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form OpcionesAlternativas 
   Caption         =   "Opciones Alternativas"
   ClientHeight    =   5490
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   6480
   LinkTopic       =   "Form1"
   ScaleHeight     =   5490
   ScaleWidth      =   6480
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   1365
      Top             =   5070
   End
   Begin VB.Timer Timer 
      Interval        =   10
      Left            =   660
      Top             =   5025
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   45
      OleObjectBlob   =   "OpcionesAlternativas.frx":0000
      Top             =   4950
   End
   Begin VB.CommandButton cmdOut 
      Caption         =   "&Cerrar "
      Height          =   375
      Left            =   5100
      TabIndex        =   21
      Top             =   4965
      Width           =   1200
   End
   Begin VB.CommandButton cmdGraba 
      Caption         =   "&Grabar"
      Height          =   375
      Left            =   3690
      TabIndex        =   20
      Top             =   4965
      Width           =   1350
   End
   Begin VB.Frame FrmAlternativas 
      Caption         =   "Opciones Alternativa"
      Height          =   3375
      Left            =   255
      TabIndex        =   13
      Top             =   1485
      Width           =   6045
      Begin VB.CommandButton CmdBuscaAlternativa 
         Caption         =   "Buscar"
         Height          =   285
         Left            =   1200
         TabIndex        =   5
         Top             =   585
         Width           =   720
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   4
         Left            =   1950
         OleObjectBlob   =   "OpcionesAlternativas.frx":0234
         TabIndex        =   16
         Top             =   315
         Width           =   2175
      End
      Begin VB.TextBox txtDescipcionAlternativa 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1950
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   585
         Width           =   3465
      End
      Begin VB.TextBox txtCodigoAlternativa 
         Height          =   285
         Left            =   180
         TabIndex        =   4
         Tag             =   "N"
         Top             =   585
         Width           =   1050
      End
      Begin VB.CommandButton CmdOkAlternativa 
         Caption         =   "Ok"
         Height          =   285
         Left            =   5400
         TabIndex        =   6
         Top             =   585
         Width           =   510
      End
      Begin MSComctlLib.ListView LvAlternativas 
         Height          =   1815
         Left            =   165
         TabIndex        =   14
         Top             =   1005
         Width           =   5790
         _ExtentX        =   10213
         _ExtentY        =   3201
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Cod."
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Alternativa"
            Object.Width           =   7056
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   5
         Left            =   180
         OleObjectBlob   =   "OpcionesAlternativas.frx":029E
         TabIndex        =   17
         Top             =   300
         Width           =   735
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkAF 
         Height          =   255
         Left            =   255
         OleObjectBlob   =   "OpcionesAlternativas.frx":0300
         TabIndex        =   18
         Top             =   2910
         Visible         =   0   'False
         Width           =   3015
      End
   End
   Begin VB.Frame Alternativa 
      Caption         =   "Alternativa"
      Height          =   1035
      Left            =   285
      TabIndex        =   7
      Top             =   225
      Width           =   6045
      Begin VB.TextBox txtCantAlternativa 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4155
         TabIndex        =   2
         Tag             =   "N"
         Text            =   "1"
         Top             =   585
         Width           =   915
      End
      Begin VB.ComboBox ComActivo 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "OpcionesAlternativas.frx":035E
         Left            =   5100
         List            =   "OpcionesAlternativas.frx":0368
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   585
         Width           =   885
      End
      Begin VB.TextBox txtPventa 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3255
         TabIndex        =   1
         Tag             =   "N"
         Text            =   "0"
         Top             =   585
         Width           =   930
      End
      Begin VB.TextBox txtNombre 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1200
         TabIndex        =   0
         Tag             =   "T"
         Top             =   585
         Width           =   2070
      End
      Begin VB.TextBox txtId 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   180
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Text            =   "auto"
         Top             =   585
         Width           =   990
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   225
         Index           =   0
         Left            =   195
         OleObjectBlob   =   "OpcionesAlternativas.frx":0374
         TabIndex        =   8
         Top             =   345
         Width           =   615
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   225
         Index           =   1
         Left            =   1170
         OleObjectBlob   =   "OpcionesAlternativas.frx":03D6
         TabIndex        =   9
         Top             =   360
         Width           =   1770
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   225
         Index           =   2
         Left            =   2970
         OleObjectBlob   =   "OpcionesAlternativas.frx":0458
         TabIndex        =   10
         Top             =   375
         Width           =   1170
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   225
         Index           =   3
         Left            =   5175
         OleObjectBlob   =   "OpcionesAlternativas.frx":04C6
         TabIndex        =   11
         Top             =   375
         Width           =   630
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   10
         Left            =   4350
         OleObjectBlob   =   "OpcionesAlternativas.frx":0530
         TabIndex        =   19
         Top             =   360
         Width           =   795
      End
   End
End
Attribute VB_Name = "OpcionesAlternativas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdGraba_Click()
    Dim Ip_Nuevo_Alt As Integer
    Dim ip_Temp As Integer
    ip_Temp = 0
    If Len(txtNombre) = 0 Then
        Call MsgBox("DEBE INGRESAR NOMBRE DE ALTERNATIVA...", vbExclamation Or vbSystemModal, "Grabar Alternativa")
        txtNombre.SetFocus
        Exit Sub
    ElseIf Val(txtCantAlternativa) = 0 Then
        Call MsgBox("DEBE INGRESAR CANTIDAD DE OPCIONES", vbExclamation Or vbSystemModal, "Grabar Alternativa")
        txtCantAlternativa.SetFocus
        Exit Sub
    End If
    
    If txtId <> "auto" Then
        Sql = "DELETE FROM productos_alternativas " & _
              "WHERE pro_alternativa_id=" & txtId
        Call Consulta(Rst_tmp, Sql)
        If Val(txtId) > 0 Then
            Sql = "DELETE FROM productos_alternativas_mov " & _
                  "WHERE pro_alternativa_id=" & txtId
            Call Consulta(Rst_tmp, Sql)
        End If
    End If
    If LvAlternativas.ListItems.Count = 0 Then Exit Sub
    
    With LvAlternativas
        If BM_NuevaAlternativa Then
            Sql = "SELECT MAX(pro_alternativa_id)+1 AS NuevoNro FROM productos_alternativas"
            Call Consulta(Rst_tmp, Sql)
            If Rst_tmp.RecordCount > 0 Then
                Ip_Nuevo_Alt = IIf(IsNull(Rst_tmp!NuevoNro), 1, Rst_tmp!NuevoNro)
            Else
                Ip_Nuevo_Alt = 1
            End If
        Else
            Ip_Nuevo_Alt = txtId
        End If
        
        Sql = "INSERT INTO productos_alternativas (pro_alternativa_id,pro_alternativa_nombre,pro_codigo,pro_alternativa_p_venta,pro_alternativa_cantidad,pro_alternativa_activo) " & _
               "VALUES(" & Ip_Nuevo_Alt & ",'" & txtNombre & "'," & ip_Temp & "," & Val(txtPventa) & "," & Val(txtCantAlternativa) & ",'" & ComActivo.Text & "')"
        Call Consulta(Rst_tmp, Sql)
        
        
        Sql = "INSERT INTO productos_alternativas_mov (pro_alternativa_id,pro_codigo) " & _
              "VALUES "
        
        For i = 1 To .ListItems.Count
            Sql = Sql & "(" & Ip_Nuevo_Alt & "," & .ListItems(i).Text & "),"
        Next
        Sql = Mid(Sql, 1, Len(Sql) - 1)
        Call Consulta(Rst_tmp, Sql)
        Unload Me
    End With
    
    
End Sub

Private Sub CmdOkAlternativa_Click()
Dim YaEsta As Boolean
    YaEsta = False
10        If Len(txtCodigoAlternativa) = 0 Then Exit Sub
20        Sql = "SELECT cod,descripcion " & _
                "FROM productos " & _
                "WHERE cod=" & txtCodigoAlternativa
30        Call Consulta(Rst_tmp, Sql)
40        If Rst_tmp.RecordCount > 0 Then
                For i = 1 To LvAlternativas.ListItems.Count
                    If Val(LvAlternativas.ListItems(i).Text) = (txtCodigoAlternativa) Then
                        LvAlternativas.ListItems(i).SubItems(1) = Rst_tmp!descripcion
                        YaEsta = True
                        Exit For
                    End If
            
                Next
                If Not YaEsta Then
                    Set itmx = LvAlternativas.ListItems.Add(, , txtCodigoAlternativa)
                    LvAlternativas.ListItems(LvAlternativas.ListItems.Count).SubItems(1) = Rst_tmp!descripcion
                End If
            Else
                Call MsgBox("El c�digo ingresado no fue encontrado...", vbExclamation Or vbSystemModal, "Codigo no encontrado")
                
50        End If
                
End Sub

Private Sub cmdOut_Click()
    Unload Me
End Sub

Private Sub CmdBuscaAlternativa_Click()
        Tipo_Producto_Busqueda = 4
        Tipo_Producto_Busqueda = " tpr_id<>3"
        BusquedaProducto.Show 1
        txtCodigoAlternativa = Codigo_Devuelto
        
        Call BuscaCodigo(Val(txtCodigoAlternativa))
        Me.CmdOkAlternativa.SetFocus
        
        
End Sub

Private Sub ComActivo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtCodigoAlternativa.SetFocus
End Sub

Private Sub Form_Load()
    
    
    If BM_NuevaAlternativa Then
        '
    Else
        txtId = IM_Id_Alternativa
        Sql = "SELECT pro_alternativa_nombre,pro_alternativa_p_venta,pro_alternativa_cantidad " & _
              "FROM productos_alternativas " & _
              "WHERE pro_alternativa_id=" & txtId
        Call Consulta(Rst_tmp, Sql)
        If Rst_tmp.RecordCount > 0 Then
            txtNombre = Rst_tmp!pro_alternativa_nombre
            txtPventa = Rst_tmp!pro_alternativa_p_venta
            txtCantAlternativa = Rst_tmp!pro_alternativa_cantidad
        Else
            Unload Me
        End If
        
        
        txtId = IM_Id_Alternativa
        Sql = "SELECT pro_codigo,p.descripcion " & _
              "FROM productos_alternativas_mov m,productos p " & _
              "WHERE m.pro_codigo=p.cod AND pro_alternativa_id=" & txtId
        Call Consulta(Rst_tmp, Sql)
        LLenar_Grilla Rst_tmp, Me, LvAlternativas, False, True, True, False
        
    End If
    
    Me.ComActivo.ListIndex = 0
End Sub

Private Sub Timer_Timer()
    'txtNombre.SetFocus
    Aplicar_skin Me
    Timer.Enabled = False
End Sub
Private Sub BuscaCodigo(Codigo As Double)
    Sql = "SELECT descripcion,pro_ultimo_costo as costo " & _
          "FROM productos " & _
          "WHERE cod=" & Codigo & " AND tpr_id<>3 "
    Call Consulta(Rst_tmp, Sql)
    If Rst_tmp.RecordCount > 0 Then
        txtDescipcionAlternativa = Rst_tmp!descripcion
       
      
        Cancel = False
    Else
        MsgBox "Codigo no encontrado ... ", vbOKOnly + vbInformation
        Me.txtDescipcionAlternativa = ""
        Me.txtCodigoAlternativa = ""
        Cancel = True
    End If
End Sub

Private Sub Timer1_Timer()
    Dim txtS As Control
    For Each txtS In Controls
        If (TypeOf txtS Is TextBox) Then
            If Me.ActiveControl.Name = txtS.Name Then 'Foco activo
                txtS.BackColor = IIf(txtS.Locked, ClrDesha, ClrCfoco)
            Else
                txtS.BackColor = IIf(txtS.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
End Sub
Private Sub txtCantAlternativa_KeyPress(KeyAscii As Integer)
     KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtCodigoAlternativa_KeyPress(KeyAscii As Integer)
     KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtCodigoAlternativa_Validate(Cancel As Boolean)
    Call BuscaCodigo(Val(txtCodigoAlternativa))
    If Len(txtDescipcionAlternativa) > 0 Then CmdOkAlternativa.SetFocus
End Sub

Private Sub txtNombre_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 13 Then txtPventa.SetFocus
End Sub

Private Sub TxtPventa_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    
End Sub
