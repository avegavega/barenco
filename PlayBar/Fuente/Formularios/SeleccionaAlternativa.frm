VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form SeleccionaAlternativa 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecciona Alternativa"
   ClientHeight    =   3390
   ClientLeft      =   45
   ClientTop       =   405
   ClientWidth     =   6120
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3390
   ScaleWidth      =   6120
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FrmInfo 
      Height          =   795
      Left            =   315
      TabIndex        =   6
      Top             =   2355
      Width           =   3675
      Begin VB.TextBox txtSeleccionadas 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1875
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Text            =   "txtDisponibles"
         Top             =   405
         Width           =   1515
      End
      Begin VB.TextBox txtDisponibles 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   180
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Text            =   "txtDisponibles"
         Top             =   420
         Width           =   1515
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   315
         Index           =   0
         Left            =   165
         OleObjectBlob   =   "SeleccionaAlternativa.frx":0000
         TabIndex        =   7
         Top             =   150
         Width           =   1230
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   315
         Index           =   1
         Left            =   1890
         OleObjectBlob   =   "SeleccionaAlternativa.frx":006D
         TabIndex        =   8
         Top             =   165
         Width           =   1470
      End
   End
   Begin VB.CommandButton cmdSeguir 
      Caption         =   "CONTINUAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   690
      Left            =   4050
      TabIndex        =   5
      Top             =   2430
      Width           =   1785
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkIdUnico 
      Height          =   225
      Left            =   3510
      OleObjectBlob   =   "SeleccionaAlternativa.frx":00DE
      TabIndex        =   4
      Top             =   30
      Width           =   2370
   End
   Begin VB.Frame Frame 
      Caption         =   "PRODUCTO"
      Height          =   930
      Left            =   255
      TabIndex        =   2
      Top             =   225
      Width           =   5580
      Begin ACTIVESKINLibCtl.SkinLabel skProducto 
         Height          =   345
         Left            =   150
         OleObjectBlob   =   "SeleccionaAlternativa.frx":013C
         TabIndex        =   3
         Top             =   465
         Width           =   5295
      End
   End
   Begin VB.Frame FrmAlt 
      Height          =   855
      Left            =   285
      TabIndex        =   0
      Top             =   1365
      Width           =   5535
      Begin VB.CommandButton cmdAlt 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Index           =   0
         Left            =   270
         TabIndex        =   1
         Top             =   -135
         Visible         =   0   'False
         Width           =   5010
      End
   End
   Begin VB.Timer Timer 
      Interval        =   5
      Left            =   660
      Top             =   4260
   End
   Begin ACTIVESKINLibCtl.Skin Skin 
      Left            =   75
      OleObjectBlob   =   "SeleccionaAlternativa.frx":0193
      Top             =   4245
   End
End
Attribute VB_Name = "SeleccionaAlternativa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAlt_Click(Index As Integer)
    Dim ip_Seleccionadas As Integer
    ip_Seleccionadas = 0
    If Mid(cmdAlt(Index).Caption, 1, 4) = "*** " Then
        cmdAlt(Index).Caption = Mid(cmdAlt(Index).Caption, 4)
    Else
        cmdAlt(Index).Caption = "*** " & cmdAlt(Index).Caption
    End If
    For i = 1 To cmdAlt.Count - 1
        If Mid(cmdAlt(i).Caption, 1, 4) = "*** " Then ip_Seleccionadas = ip_Seleccionadas + 1
    Next
    txtSeleccionadas = ip_Seleccionadas
   
End Sub

Private Sub cmdSeguir_Click()
    Dim rs_Inserta As Recordset
    'GRABA
    
    If Me.txtDisponibles <> Me.txtSeleccionadas Then
        Call MsgBox("Debe seleccionar la cantidad de opciones disponibles...", vbInformation, "IMPOSIBLE CONTINUAR...")
        Exit Sub
    End If
    
    Sql = "INSERT INTO alternativas_mov (id_unico,pro_alt_id) " & _
          "VALUES "
    'If Me.txtDisponibles > 1 Then
        For i = 1 To cmdAlt.Count - 1
            If Mid(cmdAlt(i).Caption, 1, 4) = "*** " Then Sql = Sql & "('" & Me.SkIdUnico & "'," & cmdAlt(i).Tag & "),"
        Next
        Sql = Mid(Sql, 1, Len(Sql) - 1)
    'Else
        
    Call Consulta(rs_Inserta, Sql)
    'End If
    
    
    Unload Me
End Sub

Private Sub Timer_Timer()
    Aplicar_skin Me
    Timer.Enabled = False
End Sub
