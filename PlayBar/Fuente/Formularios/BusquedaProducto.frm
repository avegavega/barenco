VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form BusquedaProducto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar Producto"
   ClientHeight    =   5850
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   6495
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5850
   ScaleWidth      =   6495
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Seleccionar"
      Height          =   375
      Left            =   480
      TabIndex        =   3
      Top             =   5400
      Width           =   1215
   End
   Begin VB.Frame Frame2 
      Caption         =   "Listado"
      Height          =   3615
      Left            =   240
      TabIndex        =   2
      Top             =   1680
      Width           =   6135
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MshListado 
         Height          =   3135
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   5655
         _ExtentX        =   9975
         _ExtentY        =   5530
         _Version        =   393216
         FixedCols       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
   End
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar"
      Height          =   375
      Left            =   5160
      TabIndex        =   1
      Top             =   5400
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1095
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   6135
      Begin VB.TextBox txtDescripcion 
         Height          =   285
         Left            =   240
         TabIndex        =   5
         Top             =   600
         Width           =   5655
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   240
         OleObjectBlob   =   "BusquedaProducto.frx":0000
         TabIndex        =   4
         Top             =   360
         Width           =   1455
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   120
      OleObjectBlob   =   "BusquedaProducto.frx":0074
      Top             =   5400
   End
   Begin VB.Timer Timer1 
      Interval        =   40
      Left            =   120
      Top             =   4800
   End
End
Attribute VB_Name = "BusquedaProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCerrar_Click()
    Codigo_Devuelto = 0
    Unload Me
End Sub

Private Sub Form_Load()
    Sql = "SELECT cod as CODIGO,DESCRIPCION " & _
          "FROM productos " & _
          "WHERE " & Tipo_Producto_Busqueda
          
    
End Sub
Private Sub MshListado_DblClick()
    If MshListado.Row = 0 Then Exit Sub
    Codigo_Devuelto = MshListado.TextMatrix(MshListado.Row, 0)
   
    Unload Me
End Sub

Private Sub Timer1_Timer()
    txtDescripcion.SetFocus
    Aplicar_skin Me
    Timer1.Enabled = False
    LLenaGrilla
End Sub
Private Sub LLenaGrilla()
    Call Consulta(Rst_tmp, Sql)
    MshListado.FixedCols = 0
    Set MshListado.DataSource = Rst_tmp
    MshListado.ColWidth(1) = 4000
    MshListado.FixedCols = 1
End Sub

Private Sub txtDescripcion_KeyPress(KeyAscii As Integer)
    If Len(txtDescripcion) < 2 Then
        'sigue
    Else
        Sql = "SELECT cod as CODIGO,DESCRIPCION " & _
              "FROM productos " & _
              "WHERE tpr_id=" & Tipo_Producto_Busqueda & " AND " & _
              "descripcion LIKE '%" & txtDescripcion & "%'"
        LLenaGrilla
    End If
    
End Sub
