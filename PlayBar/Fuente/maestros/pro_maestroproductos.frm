VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form pro_maestroproductos 
   Caption         =   "Maestro de Productos"
   ClientHeight    =   8535
   ClientLeft      =   165
   ClientTop       =   1125
   ClientWidth     =   11400
   LinkTopic       =   "Form1"
   ScaleHeight     =   8535
   ScaleWidth      =   11400
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Cerrar &Ventana"
      Height          =   405
      Left            =   13620
      TabIndex        =   11
      Top             =   9285
      Width           =   1485
   End
   Begin VB.CommandButton cmdEdita 
      Caption         =   "Editar Producto Seleccionado"
      Height          =   390
      Left            =   2520
      TabIndex        =   10
      Top             =   9240
      Width           =   2265
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "Crear Producto"
      Height          =   390
      Left            =   120
      TabIndex        =   9
      Top             =   9240
      Width           =   2265
   End
   Begin VB.Frame framel 
      Caption         =   "Listado"
      Height          =   8010
      Left            =   90
      TabIndex        =   8
      Top             =   1170
      Width           =   15015
      Begin MSComctlLib.ListView LvProductos 
         Height          =   7485
         Left            =   90
         TabIndex        =   12
         Top             =   270
         Width           =   14760
         _ExtentX        =   26035
         _ExtentY        =   13203
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   9
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Codigo"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T2000"
            Text            =   "Descripcion"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Precio Venta"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T1500"
            Text            =   "Rubro (Venta)"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "Impresora"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1000"
            Text            =   "Rubro Principal"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Tag             =   "T1500"
            Text            =   "Depto"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1500"
            Text            =   "Linea (Inventario)"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "T1500"
            Text            =   "Tipo de Producto"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Filtros"
      Height          =   975
      Index           =   0
      Left            =   45
      TabIndex        =   0
      Top             =   90
      Width           =   15060
      Begin VB.CommandButton cmdTodos 
         Caption         =   "&Todos"
         Height          =   315
         Left            =   13650
         TabIndex        =   15
         Top             =   585
         Width           =   975
      End
      Begin VB.ComboBox ComRubro 
         Height          =   315
         Left            =   9720
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   600
         Width           =   2850
      End
      Begin VB.CommandButton cmdFiltro 
         Caption         =   "&Filtrar"
         Height          =   315
         Left            =   12600
         TabIndex        =   7
         Top             =   585
         Width           =   975
      End
      Begin VB.ComboBox ComTpr 
         Height          =   315
         Left            =   6600
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   600
         Width           =   3135
      End
      Begin VB.ComboBox ComLineas 
         Height          =   315
         Left            =   3360
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   600
         Width           =   3240
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   0
         Left            =   120
         OleObjectBlob   =   "pro_maestroproductos.frx":0000
         TabIndex        =   2
         Top             =   345
         Width           =   1335
      End
      Begin VB.ComboBox ComDeptos 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   600
         Width           =   3255
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   1
         Left            =   3360
         OleObjectBlob   =   "pro_maestroproductos.frx":0076
         TabIndex        =   4
         Top             =   360
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   2
         Left            =   6600
         OleObjectBlob   =   "pro_maestroproductos.frx":00F8
         TabIndex        =   6
         Top             =   360
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   3
         Left            =   9720
         OleObjectBlob   =   "pro_maestroproductos.frx":0170
         TabIndex        =   14
         Top             =   360
         Width           =   1335
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin 
      Left            =   3045
      OleObjectBlob   =   "pro_maestroproductos.frx":01EC
      Top             =   7590
   End
   Begin VB.Menu mnmaestro 
      Caption         =   "Maestros"
      Begin VB.Menu mnDep 
         Caption         =   "Departamentos"
      End
      Begin VB.Menu mnlineas 
         Caption         =   "Lineas (inventario)"
      End
      Begin VB.Menu mn_pv 
         Caption         =   "Punto de Venta"
         Begin VB.Menu mnRubros 
            Caption         =   "Rubros (Punto Venta)"
         End
         Begin VB.Menu mnMensajesRubros 
            Caption         =   "Mensajes en Rubros de venta"
         End
      End
      Begin VB.Menu mnTipo 
         Caption         =   "Tipos de Productos"
      End
      Begin VB.Menu sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mn_alternativas 
         Caption         =   "Alternativas"
      End
   End
End
Attribute VB_Name = "pro_maestroproductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sql_todos As String
Dim Sql_Productos As String
Dim SP_Filtro As String

Private Sub cmdEdita_Click()
    Dim filtro_Actual As String
    
    If LvProductos.SelectedItem Is Nothing Then Exit Sub
    filtro_Actual = SP_Filtro
    Nuevo_Visor = False
    ID_Visor = LvProductos.SelectedItem.Text
    par_Ficha_Productos.Show 1
    SP_Filtro = filtro_Actual
    If Len(filtro_Actual) > 0 Then
        CargaDatos
        'cmdFiltro_Click
    Else
        cmdTodos_Click
    End If
    
End Sub

Private Sub cmdFiltro_Click()
    SP_Filtro = ""
    If ComDeptos.ListIndex > -1 Then SP_Filtro = SP_Filtro & " AND d.dep_id=" & ComDeptos.ItemData(ComDeptos.ListIndex)
    If ComLineas.ListIndex > -1 Then SP_Filtro = SP_Filtro & " AND l.lin_id=" & ComLineas.ItemData(ComLineas.ListIndex)
    If ComTpr.ListIndex > -1 Then SP_Filtro = SP_Filtro & " AND s.trp_id=" & ComTpr.ItemData(ComTpr.ListIndex)
    If ComRubro.ListIndex > -1 Then SP_Filtro = SP_Filtro & " AND p.Rubro=" & ComRubro.ItemData(ComRubro.ListIndex)
       
    CargaDatos
End Sub

Private Sub cmdNuevo_Click()
    Nuevo_Visor = True
    par_Ficha_Productos.Show 1
    If Len(SP_Filtro) > 0 Then cmdFiltro_Click Else cmdTodos_Click
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdTodos_Click()
    SP_Filtro = ""
    Sql_Productos = Sql_todos
    CargaDatos
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    SP_Filtro = ""
    Sql_todos = "SELECT cod,descripcion,precioventa,rub_nombre,cen_impresora,IF(bebidacomestible=1,'BEBESTIBLE','ALIMENTOS') AS Rubro_Principal,dep_nombre,lin_nombre,trp_nombre " & _
          "FROM productos p,par_rubros_venta r,par_departamentos d,par_lineas l,par_tipos_productos s,centros_produccion c " & _
          "WHERE p.rubro=r.rub_id AND p.dep_id=d.dep_id AND p.lin_id=l.lin_id AND p.tpr_id=s.trp_id AND p.dirigida=cen_id "
    Sql_Productos = Sql_todos
    
    CargaDatos
End Sub
Private Sub CargaDatos()
    Sql_Productos = Sql_todos & SP_Filtro
    Call Consulta(Rst_tmp, Sql_Productos)
    
    LLenar_Grilla Rst_tmp, Me, LvProductos, False, True, True, False
    
    LLenarCombo ComDeptos, "dep_nombre", "dep_id", "par_departamentos", "dep_activo='SI'"
    LLenarCombo ComLineas, "lin_nombre", "lin_id", "par_lineas", "lin_activo='SI'"
    LLenarCombo ComTpr, "trp_nombre", "trp_id", "par_tipos_productos", "trp_activo='SI'"
    LLenarCombo ComRubro, "rub_nombre", "rub_id", "par_rubros_venta", "rub_activo='SI'"
    
End Sub


Private Sub LvProductos_DblClick()
    cmdEdita_Click
End Sub

Private Sub mn_alternativas_Click()
    ParALternativas.Show 1
End Sub

Private Sub mnDep_Click()
    Sql_Visor = "SELECT dep_id as ID,dep_nombre as DESCRIPCION,dep_activo as ACTIVO " & _
                "FROM par_departamentos " & _
                "WHERE dep_activo='SI' "
'Set Formulario_Visor = par_departementos
    par_visor.Caption = "DEPARTAMENTOS"
    par_visor.Frame(0).Caption = "Departamentos"
    par_visor.Show 1
    CargaDatos
End Sub

Private Sub mnlineas_Click()
    Sql_Visor = "SELECT lin_id as ID,lin_nombre as DESCRIPCION,lin_activo as ACTIVO " & _
                "FROM par_lineas " & _
                "WHERE lin_activo='SI' "
    par_visor.Caption = "LINEAS"
    par_visor.Frame(0).Caption = "Departamentos"
    par_visor.Show 1
    CargaDatos
End Sub

Private Sub mnMensajesRubros_Click()
    ped_Mensajes.Show 1
End Sub

Private Sub mnRubros_Click()
    Sql_Visor = "SELECT rub_id as ID,rub_nombre as DESCRIPCION,rub_activo as ACTIVO " & _
                "FROM par_rubros_venta " & _
                "WHERE rub_activo='SI' "
    par_visor.Caption = "RUBROS"
    par_visor.Frame(0).Caption = "RUBROS (VENTA)"
    par_visor.Show 1
    CargaDatos
End Sub
