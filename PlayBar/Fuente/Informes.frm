VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Informes 
   BackColor       =   &H00008080&
   Caption         =   "Hoja de Informes"
   ClientHeight    =   8760
   ClientLeft      =   45
   ClientTop       =   405
   ClientWidth     =   13980
   Icon            =   "Informes.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8760
   ScaleWidth      =   13980
   StartUpPosition =   3  'Windows Default
   Begin MSAdodcLib.Adodc AdoFormPagos 
      Height          =   330
      Left            =   5760
      Top             =   7440
      Visible         =   0   'False
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "FormasDePago"
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton CmdCierra 
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   12240
      TabIndex        =   30
      Top             =   8160
      Width           =   1455
   End
   Begin VB.Timer Timer1 
      Left            =   0
      Top             =   -120
   End
   Begin MSAdodcLib.Adodc AdoTurnoPedido 
      Height          =   315
      Left            =   360
      Top             =   7680
      Visible         =   0   'False
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Select myrub, codproducto, detalle, turno, fecha, sum(cantidad) from pedidos  group by myrub, codproducto, detalle, turno, fecha"
      Caption         =   "Pedidos"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc AdoOut 
      Height          =   315
      Left            =   2040
      Top             =   0
      Visible         =   0   'False
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Egresos"
      Caption         =   "Egreso"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton CmdPrint 
      Caption         =   "I&mprimir"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   2160
      TabIndex        =   9
      Top             =   8160
      Width           =   3132
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7695
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   13665
      _ExtentX        =   24104
      _ExtentY        =   13573
      _Version        =   393216
      Tabs            =   4
      Tab             =   1
      TabsPerRow      =   4
      TabHeight       =   420
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Estado de Caja"
      TabPicture(0)   =   "Informes.frx":030A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame8(0)"
      Tab(0).Control(1)=   "Frame7"
      Tab(0).Control(2)=   "Frame6"
      Tab(0).Control(3)=   "Frame1"
      Tab(0).Control(4)=   "Label1"
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Ticket y Boletas"
      TabPicture(1)   =   "Informes.frx":0326
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame2"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "CmdVerDetalle"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Garzon/Mesa/Eliminados/Desc."
      TabPicture(2)   =   "Informes.frx":0342
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame3"
      Tab(2).Control(1)=   "Frame4"
      Tab(2).Control(2)=   "Frame10"
      Tab(2).Control(3)=   "Frame11"
      Tab(2).ControlCount=   4
      TabCaption(3)   =   "Productos e Insumos"
      TabPicture(3)   =   "Informes.frx":035E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "MSHCostos"
      Tab(3).Control(1)=   "Frame9"
      Tab(3).Control(2)=   "CommonDialog1"
      Tab(3).Control(3)=   "Frame5"
      Tab(3).Control(4)=   "LbMercValorada"
      Tab(3).Control(5)=   "LbVtaPersonal"
      Tab(3).Control(6)=   "LbAtenciones"
      Tab(3).Control(7)=   "LbGerencias"
      Tab(3).Control(8)=   "LBVentas"
      Tab(3).Control(9)=   "Label8"
      Tab(3).Control(10)=   "Label15"
      Tab(3).Control(11)=   "Label20"
      Tab(3).Control(12)=   "Label5"
      Tab(3).ControlCount=   13
      Begin VB.CommandButton CmdVerDetalle 
         Appearance      =   0  'Flat
         BackColor       =   &H00404040&
         Caption         =   "Ver detalle del ticket"
         Height          =   375
         Left            =   3600
         TabIndex        =   51
         Top             =   360
         Width           =   2055
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHCostos 
         Height          =   5295
         Left            =   -74760
         TabIndex        =   48
         Top             =   840
         Width           =   5535
         _ExtentX        =   9763
         _ExtentY        =   9340
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         HighLight       =   2
         FillStyle       =   1
         ScrollBars      =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   6
      End
      Begin VB.Frame Frame11 
         Caption         =   "Descuentos Efectuados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2895
         Left            =   -69480
         TabIndex        =   34
         Top             =   360
         Width           =   5775
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHdescuentos 
            Height          =   2295
            Left            =   240
            TabIndex        =   36
            Top             =   360
            Width           =   5295
            _ExtentX        =   9340
            _ExtentY        =   4048
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            HighLight       =   2
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   10.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   6
            _Band(0).GridLinesBand=   1
            _Band(0).TextStyleBand=   0
            _Band(0).TextStyleHeader=   0
         End
      End
      Begin VB.Frame Frame10 
         Caption         =   "Ticket Eliminados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2895
         Left            =   -74760
         TabIndex        =   33
         Top             =   360
         Width           =   5055
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHeliminados 
            Height          =   2295
            Left            =   600
            TabIndex        =   35
            Top             =   360
            Width           =   4215
            _ExtentX        =   7435
            _ExtentY        =   4048
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            HighLight       =   2
            ScrollBars      =   2
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   10.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   3
         End
      End
      Begin VB.Frame Frame9 
         Caption         =   "Ticket Emitidos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5415
         Left            =   -74880
         TabIndex        =   31
         Top             =   720
         Width           =   615
         Begin VB.CommandButton CmdReImprime 
            Caption         =   "R&e-Imprime seleccionado"
            Height          =   252
            Left            =   2280
            TabIndex        =   45
            Top             =   120
            Width           =   2052
         End
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHvalesEmitidos 
            Height          =   4812
            Left            =   120
            TabIndex        =   32
            Top             =   480
            Width           =   7452
            _ExtentX        =   13150
            _ExtentY        =   8493
            _Version        =   393216
            Cols            =   8
            FixedCols       =   0
            FocusRect       =   2
            HighLight       =   2
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   8
         End
         Begin VB.Label Label6 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Doble Click para anular ticket."
            Height          =   252
            Left            =   4560
            TabIndex        =   46
            Top             =   120
            Width           =   2412
         End
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   -74879
         Top             =   0
         _ExtentX        =   688
         _ExtentY        =   688
         _Version        =   393216
      End
      Begin VB.Frame Frame5 
         Caption         =   "Ventas ordenadas x Rubro"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5415
         Left            =   -68640
         TabIndex        =   26
         Top             =   720
         Width           =   6975
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHxRubros 
            Height          =   4815
            Left            =   120
            TabIndex        =   27
            Top             =   480
            Width           =   6615
            _ExtentX        =   11668
            _ExtentY        =   8493
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            FocusRect       =   2
            HighLight       =   2
            ScrollBars      =   2
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   3
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Platos Quitados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2655
         Left            =   -69480
         TabIndex        =   24
         Top             =   3480
         Width           =   5775
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHquitados 
            Height          =   2175
            Left            =   120
            TabIndex        =   25
            Top             =   240
            Width           =   5415
            _ExtentX        =   9551
            _ExtentY        =   3836
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            HighLight       =   2
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   6
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "No Rendibles en el Turno"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4815
         Index           =   0
         Left            =   -70320
         TabIndex        =   21
         Top             =   1320
         Width           =   4455
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHnoRinde 
            Height          =   3615
            Left            =   120
            TabIndex        =   22
            Top             =   480
            Width           =   3975
            _ExtentX        =   7011
            _ExtentY        =   6376
            _Version        =   393216
            Rows            =   1
            FixedRows       =   0
            FixedCols       =   0
            HighLight       =   2
            ScrollBars      =   2
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   2
         End
         Begin VB.Label LBtotNoRinde 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1800
            TabIndex        =   23
            Top             =   4200
            Width           =   2295
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Totales"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4212
         Left            =   -64920
         TabIndex        =   7
         Top             =   1800
         Width           =   2412
         Begin VB.Label LBtotEgresos 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Total Egresos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   612
            Left            =   120
            TabIndex        =   17
            Top             =   2760
            Width           =   2172
         End
         Begin VB.Label LbaRendir 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Total a Rendir"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   612
            Left            =   120
            TabIndex        =   16
            Top             =   3480
            Width           =   2172
         End
         Begin VB.Label LBInicial 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Efectivo Inicial"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   612
            Left            =   120
            TabIndex        =   15
            Top             =   1320
            Width           =   2172
         End
         Begin VB.Label LBTotEfectivos 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "TotalEfectivos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   612
            Left            =   120
            TabIndex        =   14
            Top             =   2160
            Width           =   2172
         End
         Begin VB.Label LBTotIngresos 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "TotalIngresos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   612
            Left            =   120
            TabIndex        =   13
            Top             =   600
            Width           =   2172
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Egresos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4212
         Left            =   -66000
         TabIndex        =   5
         Top             =   1320
         Visible         =   0   'False
         Width           =   495
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHEgresos 
            Height          =   3255
            Left            =   120
            TabIndex        =   6
            Top             =   360
            Width           =   3135
            _ExtentX        =   5530
            _ExtentY        =   5741
            _Version        =   393216
            Rows            =   1
            FixedRows       =   0
            FixedCols       =   0
            ScrollBars      =   2
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   7.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   2
         End
         Begin VB.Label LBTegresos 
            Alignment       =   1  'Right Justify
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1200
            TabIndex        =   11
            Top             =   3720
            Width           =   2055
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Acumulado Garzones-Mesas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2655
         Left            =   -74640
         TabIndex        =   3
         Top             =   3480
         Width           =   4932
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHGarzones 
            Height          =   2175
            Left            =   480
            TabIndex        =   19
            Top             =   240
            Width           =   4215
            _ExtentX        =   7435
            _ExtentY        =   3836
            _Version        =   393216
            FixedCols       =   0
            HighLight       =   2
            ScrollBars      =   2
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   10.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   2
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Todos los tickets del Turno"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6135
         Left            =   240
         TabIndex        =   2
         Top             =   840
         Width           =   13095
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHallEmitidos 
            Height          =   4935
            Left            =   360
            TabIndex        =   8
            Top             =   480
            Width           =   12735
            _ExtentX        =   22463
            _ExtentY        =   8705
            _Version        =   393216
            Cols            =   10
            FixedCols       =   0
            ForeColorSel    =   -2147483639
            AllowBigSelection=   0   'False
            TextStyleFixed  =   2
            HighLight       =   2
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   10
         End
         Begin VB.Label LbTotAll 
            Alignment       =   1  'Right Justify
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   2520
            TabIndex        =   18
            Top             =   5640
            Width           =   2175
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Ingresos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4815
         Left            =   -74760
         TabIndex        =   1
         Top             =   1320
         Width           =   4215
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHingresos 
            Height          =   3615
            Left            =   240
            TabIndex        =   4
            Top             =   480
            Width           =   3735
            _ExtentX        =   6588
            _ExtentY        =   6376
            _Version        =   393216
            Rows            =   1
            FixedRows       =   0
            FixedCols       =   0
            HighLight       =   2
            ScrollBars      =   2
            SelectionMode   =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _NumberOfBands  =   1
            _Band(0).Cols   =   2
         End
         Begin VB.Label LbTIngresos 
            Alignment       =   1  'Right Justify
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   480
            TabIndex        =   10
            Top             =   4200
            Width           =   3375
         End
      End
      Begin VB.Label LbMercValorada 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -72240
         TabIndex        =   49
         Top             =   5640
         Width           =   4695
      End
      Begin VB.Label Label1 
         Caption         =   "Aqui sumare el total en mesas ocupadas."
         Height          =   1095
         Left            =   -74760
         TabIndex        =   47
         Top             =   5160
         Width           =   2415
      End
      Begin VB.Label LbVtaPersonal 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   -69120
         TabIndex        =   44
         Top             =   6600
         Visible         =   0   'False
         Width           =   1572
      End
      Begin VB.Label LbAtenciones 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   -70920
         TabIndex        =   43
         Top             =   6600
         Visible         =   0   'False
         Width           =   1332
      End
      Begin VB.Label LbGerencias 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   -72840
         TabIndex        =   42
         Top             =   6600
         Visible         =   0   'False
         Width           =   1332
      End
      Begin VB.Label LBVentas 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   -74760
         TabIndex        =   41
         Top             =   6600
         Visible         =   0   'False
         Width           =   1572
      End
      Begin VB.Label Label8 
         Caption         =   "Ventas al personal"
         Height          =   252
         Left            =   -69000
         TabIndex        =   40
         Top             =   6360
         Visible         =   0   'False
         Width           =   1452
      End
      Begin VB.Label Label15 
         Caption         =   "Atenciones"
         Height          =   252
         Left            =   -70680
         TabIndex        =   39
         Top             =   6360
         Width           =   972
      End
      Begin VB.Label Label20 
         Caption         =   "Gerencias"
         Height          =   252
         Left            =   -72600
         TabIndex        =   38
         Top             =   6360
         Width           =   972
      End
      Begin VB.Label Label5 
         Caption         =   "Total Ventas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   252
         Left            =   -74640
         TabIndex        =   37
         Top             =   6360
         Width           =   1332
      End
   End
   Begin VB.Label Label4 
      Caption         =   "Label4"
      DataField       =   "FormaDePago"
      DataSource      =   "AdoFormPagos"
      Height          =   255
      Left            =   6240
      TabIndex        =   50
      Top             =   8160
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label LBbebestibles 
      Caption         =   "0"
      Height          =   253
      Left            =   9196
      TabIndex        =   29
      Top             =   7381
      Visible         =   0   'False
      Width           =   1221
   End
   Begin VB.Label LBalimentos 
      Caption         =   "0"
      Height          =   253
      Left            =   7502
      TabIndex        =   28
      Top             =   7381
      Visible         =   0   'False
      Width           =   1463
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      DataField       =   "codproducto"
      DataSource      =   "AdoTurnoPedido"
      Height          =   252
      Left            =   2520
      TabIndex        =   20
      Top             =   6720
      Visible         =   0   'False
      Width           =   852
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      DataField       =   "Concepto"
      DataSource      =   "AdoOut"
      Height          =   252
      Left            =   4800
      TabIndex        =   12
      Top             =   120
      Visible         =   0   'False
      Width           =   732
   End
End
Attribute VB_Name = "Informes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public myIngresos As Double
Public myEgresos As Double
Public myNorindes As Double
Public myInicial As Double
Public myArendir As Double
Public myEfectivos As Double

Private Sub cmdCierra_Click()
    Unload Me
End Sub

Private Sub CmdPrint_Click()
    If SSTab1.Tab = 0 Then '' Estado de Caja
        NuevoTXTFile (LaRutadeArchivos & "StatusBox.txt")
        PrintTXTFile (LaRutadeArchivos & "StatusBox.txt")
    End If
    If SSTab1.Tab = 3 Then '' Ventas x Rubro
        Nuevo3XTFile (LaRutadeArchivos & "VtaRubro.txt")
        Print3XTFile (LaRutadeArchivos & "VtaRubro.txt")
    End If
    
End Sub

Private Sub Form_Load()
    If Main.LBlevelControl.Caption = 2 Then
        Control2
        Exit Sub
    End If
    Frame9.Visible = False
    Label15.Visible = False
    Label20.Visible = False
    Label5.Visible = False
    Label8.Visible = False
    Dim VentasTicket As ADODB.Recordset
    Dim VentasXgarzon As ADODB.Recordset
    Dim ventasXmesa As ADODB.Recordset
    Dim EgresosTurno As ADODB.Recordset
    Dim VentasXrubro As ADODB.Recordset
    Dim VentasXquitados As ADODB.Recordset
    Dim EsteTurno As String
    Dim EstaFecha As String
    Dim cu As Integer
    Dim Fpagos(20) As String
    Dim ValoresFP(20) As Double
    Dim Lgarzones(30) As String
    Dim AcumGar(30) As Double
    Dim LMesa(150) As Integer
    Dim AcumMesa(150) As Double
    Timer1.Interval = 700
    myIngresos = 0
    myEgresos = 0
    myNorindes = 0
    myInicial = 0
    myArendir = 0
    myEfectivos = 0
    
        
    LBInicial.Caption = "Efectivo Inicial  $ " & Main.TurnoEfeInicial.Caption
    myInicial = Main.TurnoEfeInicial.Caption
    ctfp = Me.AdoFormPagos.Recordset.RecordCount
    EsteTurno = Main.TurnoTurno.Caption    ' ' Datos del Turno Actual
    EstaFecha = Main.TurnoFecha.Caption
  '  ttt = Pago.CmdFpago.Count
    fp = 1
    With Me.AdoFormPagos.Recordset
        .MoveFirst
        Do While Not .EOF
        
            Fpagos(fp) = .Fields(0)
            
            If .Fields(1) = 1 Then Fpagos(fp) = Fpagos(fp) & "."
            If .Fields(2) = 0 Then Fpagos(fp) = Fpagos(fp) & "_"
            fp = fp + 1
            .MoveNext
        Loop
        
        'For fp = 1 To ctfp
        '    Fpagos(fp) = Pago.CmdFpago(fp - 1).Caption
        'Next
        Fpagos(fp + 1) = "Nulo_"
    End With
    Dim AdoTickets As Recordset
    Sql = "SELECT * FROM tickets WHERE turno=" & EsteTurno & " AND fecha='" & Format(EstaFecha, "YYYY-MM-DD") & "')"
    'cu = Pago.AdoTickets.Recordset.RecordCount
    Set VentasXdescuentos = FilterField(Main.AdoDescuentos.Recordset, "Turno", EsteTurno, "Fecha", EstaFecha)
    Set VentasTicket = FilterField(Pago.AdoTickets.Recordset, "Turno", EsteTurno, "Fecha", EstaFecha)
    Set VentasXquitados = FilterField(Main.AdoQuitados.Recordset, "Turno", EsteTurno, "Fecha", EstaFecha)

    If VentasTicket.RecordCount > 0 Then
        
        
        Set Me.MSHdescuentos.DataSource = VentasXdescuentos
        'Me.MSHdescuentos.Refresh
       
        
        'With Me.AdoDsctos.Recordset   '*** Filtro Descuentos
        '    Criterio = "Turno = " & EsteTurno & " And Fecha = " & EstaFecha
       '
       '     .Filter = Criterio
        
      '  End With
        
        
        
        
        
        With VentasTicket
        
          If .RecordCount = 0 Then Exit Sub
            .MoveFirst
            For ko = 1 To .RecordCount
                For cc = 1 To ctfp + 1
              
                    If .Fields(3) = Fpagos(cc) Then  '' Sumando los valores de cada forma de pago
                        ValoresFP(cc) = ValoresFP(cc) + .Fields(2)
                        Exit For
                    End If
                Next
                .MoveNext
            Next
        End With
        MSHnoRinde.ColWidth(0) = 1500
        MSHnoRinde.ColWidth(1) = 700
        MSHingresos.ColWidth(0) = 1500
        MSHingresos.ColWidth(1) = 700
        For cc = 1 To ctfp + 1     '' Llena cuadricula de ingresos
            If ValoresFP(cc) > 0 Then   '' Por forma de pago
                If Right(Fpagos(cc), 1) = "_" Then
                    
                    MSHnoRinde.Col = 0       ''  LLena cuadricula de
                    MSHnoRinde.Text = Fpagos(cc) '' no rendibles
                    MSHnoRinde.Col = 1
                    MSHnoRinde.Text = ValoresFP(cc)
                    myNorindes = myNorindes + ValoresFP(cc)
                    LBtotNoRinde.Caption = LBtotNoRinde.Caption + ValoresFP(cc)
                    MSHnoRinde.Rows = MSHnoRinde.Rows + 1
                    MSHnoRinde.Row = MSHnoRinde.Row + 1
                Else
                    MSHingresos.Col = 0
                    MSHingresos.Text = Fpagos(cc)
                    MSHingresos.Col = 1
                    MSHingresos.Text = ValoresFP(cc)
                    myIngresos = myIngresos + ValoresFP(cc)
                    LbTIngresos.Caption = LbTIngresos.Caption + ValoresFP(cc)
                    MSHingresos.Rows = MSHingresos.Rows + 1
                    MSHingresos.Row = MSHingresos.Row + 1
                End If
                
            End If
        Next
        LBTotIngresos.Caption = "Total Ingresos  $ " & LbTIngresos.Caption
        LbTIngresos.Caption = "Total  $ " & LbTIngresos.Caption
        MSHingresos.Rows = MSHingresos.Rows - 1   '' Hasta aqui cuadricula de ingresos
        MSHnoRinde.Rows = MSHnoRinde.Rows - 1
        If Fpagos(1) = "Efectivo." Then
            LBTotEfectivos.Caption = "Total Efectivos  $ " & ValoresFP(1) + Main.TurnoEfeInicial.Caption
            myEfectivos = ValoresFP(1)
        Else
            LBTotEfectivos.Caption = "Total Efectivos  $  0"
        End If
    End If
    cu = AdoOut.Recordset.RecordCount
    If cu > 0 Then
        With AdoOut.Recordset
            Criterio = "Fecha = " & EstaFecha     '''  Procesando Archivo de Egresos
            .MoveFirst
            .Find Criterio
            If Not .EOF Then
                .MoveFirst
                Criterio = "Turno = " & EsteTurno
               .Find Criterio
                If Not .EOF Then
                    Set EgresosTurno = FilterField(AdoOut.Recordset, "Fecha", EstaFecha, "Turno", EsteTurno)
                    MSHEgresos.ColWidth(0) = 1500
                    MSHEgresos.ColWidth(1) = 700
                    For cc = 1 To EgresosTurno.RecordCount       '' Llena cuadricula de Egresos
                            MSHEgresos.Col = 0
                            MSHEgresos.Text = .Fields(1)
                            MSHEgresos.Col = 1
                            MSHEgresos.Text = .Fields(2)
                            LBTegresos.Caption = LBTegresos.Caption + .Fields(2)
                            MSHEgresos.Rows = MSHEgresos.Rows + 1
                            MSHEgresos.Row = MSHEgresos.Row + 1
                            EgresosTurno.MoveNext
                    Next
                    myEgresos = myEgresos + LBTegresos.Caption
                    LBtotEgresos.Caption = "Total Egresos  $ " & LBTegresos.Caption
                    LBTegresos.Caption = "Total  $ " & LBTegresos.Caption
                    MSHEgresos.Rows = MSHEgresos.Rows - 1   '' Hasta aqui cuadricula de ingresos
    
                Else
                    LBTegresos.Caption = "Total  $ 0"
                End If
            Else
       '         MsgBox "No hay egresos"
                LBTegresos.Caption = "Total  $ 0"
            End If
        End With
    End If
    mie = Val(Mid(LBTotEfectivos.Caption, 19))
    mic = Val(Mid(LBtotEgresos.Caption, 17))
    LbaRendir.Caption = "Efectivo a Rendir  $ " & mie - mic
    
    
    ''Seccion de todos los Tickets emitidos
    If VentasTicket.RecordCount > 0 Then
         LbTotAll.Caption = Format(LbTIngresos.Caption, "##,###")
         With MSHallEmitidos
             .Row = 0
             .MergeCol(0) = True
             .Col = 0
             .ColWidth(0) = 700
             .Text = "Ticket"
             .Col = 1
             .ColWidth(1) = 600
             .Text = "Mesa"
             .Col = 2
             .ColWidth(2) = 1900
             .Text = "Forma Pago"
             .Col = 3
             .ColWidth(3) = 850
             .Text = "Valor"
             .ColAlignment(4) = 0
             .ColWidth(4) = 2100
             .Col = 4
             .Text = "Garz�n"
             .Col = 5
             .ColWidth(5) = 1400
             .Text = "Hora"
             .Col = 6
             .ColWidth(6) = 1500
             .Text = "Documento"
             .Col = 7
             .ColWidth(7) = 800
             .Text = "N� Doc"
             .Col = 8
             .ColWidth(8) = 700
             .Text = "Alimentos"
             .Col = 9
             .ColWidth(9) = 700
             .Text = "Bebidas"
             .Row = .Row + 1
             
             VentasTicket.MoveFirst
             For tr = 1 To VentasTicket.RecordCount   '' Llena tickets emitidos en el turno
                 .Col = 0
                 .Text = VentasTicket.Fields(0)
                 .Col = 1
                 .Text = VentasTicket.Fields(1)
                 .Col = 2
                 .Text = VentasTicket.Fields(3)
                 .Col = 3
                 .Text = VentasTicket.Fields(2)
                 .Col = 4
                 .Text = VentasTicket.Fields(4)
                 .Col = 5
                 .Text = VentasTicket.Fields(5)
                 .Col = 6
                 .Text = VentasTicket.Fields(7)
                 .Col = 7
                 .Text = VentasTicket.Fields(8)
                 .Col = 8
                 LBalimentos.Caption = LBalimentos.Caption + VentasTicket.Fields(10)
                 .Text = VentasTicket.Fields(10)
                 .Col = 9
                 LBbebestibles.Caption = LBbebestibles.Caption + VentasTicket.Fields(11)
                 .Text = VentasTicket.Fields(11)
                 .Rows = .Rows + 1
                 .Row = .Row + 1
                 VentasTicket.MoveNext
             Next
             .Rows = .Rows - 1
             .Col = 0     ''  Ordena los tickets emitidos en el turno actual
             .Sort = flexSortGenericAscending
         End With
         
         
         '''  Seccion de Ventas por Garz�n"
         For Lg = 1 To 30
             AcumGar(Lg) = 0
         Next
         For Lg = 0 To ListaGarzones.CmdGarzones.Count - 1
             Lgarzones(Lg + 1) = ListaGarzones.CmdGarzones(Lg).Caption
         Next
         VentasTicket.MoveFirst
         For Lg = 1 To VentasTicket.RecordCount
             For cg = 1 To ListaGarzones.CmdGarzones.Count
                 If Lgarzones(cg) = VentasTicket.Fields(4) Then
                     AcumGar(cg) = AcumGar(cg) + VentasTicket.Fields(2)
                     Exit For
                 End If
             Next
             VentasTicket.MoveNext
         Next
        ' MsgBox "acumular"
         With MSHGarzones
             .Row = 0
             .ColWidth(0) = 2500
             .Col = 0
             .Text = "Garz�n"
             .ColWidth(1) = 1300
             .Col = 1
             .Text = "Acumulado"
             For Lg = 1 To 30
                 If AcumGar(Lg) > 0 Then
                     .Row = .Row + 1
                     .Col = 0
                     .Text = Lgarzones(Lg)
                     .Col = 1
                     .Text = AcumGar(Lg)
                     .Rows = .Rows + 1
                 End If
             Next
             .Rows = .Rows - 1
             .ColAlignment(0) = 0
             .Col = 0
             .Sort = flexSortGenericAscending
         End With
         
         
         
         '' Aqui procesamos para Acumulaci�n de Cada Mesa
         For Lg = 1 To 99
             AcumMesa(Lg) = 0
         Next
         For Lg = 1 To Main.LBmesas.Caption
             LMesa(Lg) = Lg
         Next
         VentasTicket.MoveFirst
         For Lg = 1 To VentasTicket.RecordCount
             For cg = 1 To Main.LBmesas.Caption
                 If LMesa(cg) = VentasTicket.Fields(1) Then
                     AcumMesa(cg) = AcumMesa(cg) + VentasTicket.Fields(2)
                     Exit For
                 End If
             Next
             VentasTicket.MoveNext
         Next
                 With MSHGarzones
            ' .Row = 0
            ' .ColWidth(0) = 1000
            .Rows = .Rows + 3
            .Row = .Rows - 2
             .Col = 0
             .Text = "N� Mesa"
             .Col = 1
             .Text = "Acumulado"
             For Lg = 1 To 99
                   If AcumMesa(Lg) > 0 Then
                     .Row = .Row + 1
                     .Col = 0
                     .Text = LMesa(Lg)
                     .Col = 1
                     .Text = AcumMesa(Lg)
                     .Rows = .Rows + 1
               End If
             Next
            .Rows = .Rows - 1
             .ColAlignment(0) = 0
           '  .Col = 0
           '  .Sort = flexSortGenericAscending
         End With
    End If

    If VentasXquitados.RecordCount > 0 Then
        VentasXquitados.MoveFirst
        With MSHquitados
            .Row = 0
            .Col = 0
            .Text = "C�d"
            .Col = 1
            .Text = "Producto"
            .Col = 2
            .Text = "Mesa"
            .Col = 3
            .Text = "Hora"
            .Col = 4: .Text = "Cantidad"
            .Col = 5: .Text = "Autoriz�"
            
            .ColWidth(0) = 500
            .ColWidth(1) = 1700
            .ColWidth(2) = 600
            .ColWidth(3) = 1150
            .ColWidth(5) = 2000
            For b = 1 To VentasXquitados.RecordCount
                
                .Row = b
                .Col = 0
                .Text = VentasXquitados.Fields(0)
                .Col = 1
                .Text = VentasXquitados.Fields(1)
                .Col = 2
                .Text = VentasXquitados.Fields(2)
                .Col = 3
                .Text = VentasXquitados.Fields(3)
                .Col = 4
                .Text = VentasXquitados.Fields(7)
                .Col = 5
                .Text = VentasXquitados.Fields(6)
                VentasXquitados.MoveNext
                .Rows = .Rows + 1
            Next b
        End With
    End If
    
    '''' Seccion de venta separada por RUBROS
    conru = 0
    cu = 0
    'cu = AdoTurnoPedido.Recordset.RecordCount
    Sql = "SELECT myrub, codproducto, descripcion, turno, fecha, sum(cantidad) " & _
          "FROM pedidos l,productos p  " & _
           "WHERE l.codproducto=p.cod AND turno=" & EsteTurno & " AND fecha='" & Format(EstaFecha, "YYYY-MM-DD") & "' " & _
           "GROUP BY myrub, codproducto, detalle, turno, fecha "

    Call Consulta(VentasXrubro, Sql)
    
          
  '  Set VentasXrubro = FilterField(AdoTurnoPedido.Recordset, "Turno", EsteTurno, "Fecha", EstaFecha)
    If VentasXrubro.RecordCount > 0 Then
        With VentasXrubro
            .MoveFirst
            If .EOF Then Exit Sub
            MSHxRubros.ColWidth(1) = 4000
            MSHxRubros.ColWidth(0) = 600
            MSHxRubros.ColWidth(2) = 900
            
            MSHxRubros.Row = 0
            MSHxRubros.Col = 0
            MSHxRubros.Text = "C�digo"
            MSHxRubros.Col = 2
            MSHxRubros.Text = "Cantidad"
            conru = .Fields(0)
            MSHxRubros.Row = 1
            MSHxRubros.Col = 1
            MSHxRubros.Text = UCase(Main.CMDrubros(conru - 1).Caption)
            MSHxRubros.Rows = MSHxRubros.Rows + 1
            MSHxRubros.Row = MSHxRubros.Row + 1
            For rr = 1 To .RecordCount
                If .Fields(0) <> conru Then
                    conru = .Fields(0)
                    MSHxRubros.Rows = MSHxRubros.Rows + 3
                    MSHxRubros.Row = MSHxRubros.Row + 2
                    MSHxRubros.Col = 1
                    MSHxRubros.Text = UCase(Main.CMDrubros(conru - 1).Caption)
                    MSHxRubros.Rows = MSHxRubros.Rows + 1
                    'MSHxRubros.Row = MSHxRubros.Row + 1
                    rr = rr - 1
                    If rr = .RecordCount Then
                        MSHxRubros.Row = MSHxRubros.Row + 1
                        MSHxRubros.Col = 0
                        MSHxRubros.Text = .Fields(1)
                        MSHxRubros.Col = 1
                        MSHxRubros.Text = .Fields(2)
                        MSHxRubros.Col = 2
                        MSHxRubros.Text = .Fields(5)
                    End If
                Else
                    MSHxRubros.Rows = MSHxRubros.Rows + 1
                    MSHxRubros.Row = MSHxRubros.Row + 1
                    MSHxRubros.Col = 0
                    MSHxRubros.Text = .Fields(1)
                    MSHxRubros.Col = 1
                    MSHxRubros.Text = .Fields(2)
                    MSHxRubros.Col = 2
                    MSHxRubros.Text = .Fields(5)
                    MSHxRubros.Rows = MSHxRubros.Rows + 1
                    .MoveNext
                End If
            Next
            For sl = MSHxRubros.Rows To 1 Step -1
                MSHxRubros.Row = sl - 1
                MSHxRubros.Col = 0
                If MSHxRubros.Text <> "" Then
                    MSHxRubros.Rows = MSHxRubros.Rows - (MSHxRubros.Rows - sl - 1)
                    Exit For
                End If
            Next
        End With
        With MSHCostos
            
            .ColWidth(2) = 2100: .ColWidth(0) = 1300
            .ColWidth(3) = 450
            .Row = 0: .Col = 0
            .Text = "Familia": .Col = 1
            .Text = "Codigo": .Col = 2
            .Text = "Insumo": .Col = 3
            .Text = "U/M": .Col = 4
            .Text = "Cantidad": .Col = 5
            .Text = "Valorado"
            
            For rr = 0 To MSHxRubros.Rows - 1
                MSHxRubros.Row = rr: MSHxRubros.Col = 0
                Dim MinVendidos As Double, EstosCostos As Double
                If Val(MSHxRubros.Text) > 0 Then
                    MSHxRubros.Col = 2: MinVendidos = Val(MSHxRubros.Text)
                    MSHxRubros.Col = 0
                    Dim LaMinuta As MinutaForma, ind As Integer, MinRuta As String
                    
                    MinRuta = LaRutadeArchivos & "Minutas\" & MSHxRubros.Text & ".min"
                    mivalor = 0
                    X = FreeFile
                    LargoR = Len(LaMinuta)
                    Open MinRuta For Random As #X Len = LargoR
                    If LOF(X) > 0 Then
                        For ind = 1 To LOF(X) / LargoR
                            Get #X, , LaMinuta
                            For ll = 1 To .Rows - 1
                                .Row = ll: .Col = 1
                                rastreado = 1
                                If Val(.Text) = Val(LaMinuta.MinCodInsumo) Then
                                    .Col = 4
                                    mivalor = Val(.Text) + (Val(LaMinuta.MinCantInsumo) * MinVendidos)
                                    .Text = mivalor 'Format(mivalor, "##,###.##")
                                    .Col = 5
                                    
                                    EstosCostos = Val(.Text) + (Val(LaMinuta.MinCostoInsumo)) * MinVendidos
                                    .Text = EstosCostos
                                    rastreado = 2
                                    Exit For
                                End If
                            Next
                            If rastreado = 1 Then
                                .Rows = .Rows + 1
                                .Row = .Rows - 1: .Col = 0
                                .Text = LaMinuta.MinFamiliaInsumo: .Col = 1
                                .Text = LaMinuta.MinCodInsumo: .Col = 2
                                .Text = LaMinuta.MinElInsumo: .Col = 3
                                .Text = LaMinuta.MinUMInsumo: .Col = 4
                                .Text = Val(LaMinuta.MinCantInsumo) * MinVendidos: .Col = 5
                                EstosCostos = LaMinuta.MinCostoInsumo * MinVendidos
                                .Text = EstosCostos
                            End If
                        Next
                        Close #X
                    Else
                        Close #X
                    End If
                
                End If
            Next
            For rr = 0 To .Rows - 1
                .Row = rr: .Col = 5
                If Val(.Text) > 0 Then
                    Me.LbMercValorada.Caption = Val(LbMercValorada.Caption) + .Text
                End If
            Next
            
            .MergeCol(0) = True
            .MergeCells = flexMergeFree
            .Col = 0
            .ColSel = 0
            .Sort = flexSortGenericAscending
            .MergeCol(0) = True
            
            Me.LbMercValorada.Caption = "Mercaderia Usada $ " & Me.LbMercValorada.Caption
        End With
        
    End If
    myArendir = (myEfectivos + myInicial) - myEgresos
    
End Sub
Public Function FilterField(rstTemp As ADODB.Recordset, _
   strField As String, strFilter As String, strCampo As String, strFiltro As String) As ADODB.Recordset
   Dim Cadena1 As String
   Dim Cadena2 As String
   Dim CadenaD As String
   Cadena1 = strField & "=" & strFilter
   Cadena2 = strCampo & "=" & strFiltro
   CadenaD = Cadena1 & " And " & Cadena2
   rstTemp.Filter = CadenaD
   Set FilterField = rstTemp
End Function

Private Sub Form_Unload(Cancel As Integer)
    FrmFondo.Enabled = True
    'Main.Show
    Unload Informes
End Sub
Public Sub NuevoTXTFile(FileName As String)
    Dim X As Integer
    Dim s As String
    Dim TextoConc As String * 18
    Dim TextoValor As String * 7
    X = FreeFile
   ' On Error GoTo HandleError
    Open FileName For Output As X
   ' Do While Not EOF(x)
    With MSHingresos
        Print #X, "ESTADO DE CAJA"
        Print #X, "  "
        Print #X, "INGRESOS "
        For pr = 0 To .Rows - 1
            .Row = pr
            .Col = 0
            TextoConc = .Text
            .Col = 1
            TextoValor = .Text
            Print #X, TextoConc; "    "; TextoValor
        Next
        Print #X, Spc(21); myIngresos
    End With
    With MSHEgresos
        Print #X, "  "
        Print #X, "EGRESOS"
        For pr = 0 To .Rows - 1
            .Row = pr
            .Col = 0
            TextoConc = .Text
            .Col = 1
            TextoValor = .Text
            Print #X, TextoConc; "    "; TextoValor
        Next
        Print #X, Spc(21); myEgresos
    End With
    With MSHnoRinde
        Print #X, "  "
        Print #X, "No RENDIBLES"
        For pr = 0 To .Rows - 1
            .Row = pr
            .Col = 0
            TextoConc = .Text
            .Col = 1
            TextoValor = .Text
            Print #X, TextoConc; "    "; TextoValor
        Next
        Print #X, Spc(21); myNorindes
        Print #X, " "
    End With
    Print #X, "TOTALES"
    Print #X, "  "
    Print #X, "INGRESOS"; Spc(13); myIngresos
    Print #X, "EGRESOS"; Spc(14); myEgresos
    Print #X, "NO RENDIBLES"; Spc(9); myNorindes
    Print #X, "  "
    Print #X, "Total Efectivo"; Spc(7); myEfectivos
    Print #X, "Menos Egresos"; Spc(8); myEgresos
    Print #X, "Efectivo Rendir"; Spc(6); myEfectivos - myEgresos
    Print #X, "Efectivo Inicial"; Spc(5); myInicial
    Close #X
End Sub
Public Sub PrintTXTFile(FileName As String)
    Dim X As Integer
    Dim s As String
    
    Printer.EndDoc
    paso = TipoLetra("Arial", 10, True)
    X = FreeFile
   ' On Error GoTo HandleError
    Open FileName For Input As X
    Line Input #X, s
    Printer.Print s
   paso = TipoLetra("Courier New", 8, False)
    Do While Not EOF(X)
        Line Input #X, s
        Printer.Print s
    Loop
    Printer.EndDoc
    Close #X
End Sub
Public Sub Print3XTFile(FileName As String)
    Dim X As Integer
    Dim s As String
    Printer.EndDoc
    paso = TipoLetra("Arial", 10, True)
    X = FreeFile
   ' On Error GoTo HandleError
    Open FileName For Input As X
    Line Input #X, s
    Printer.Print s
   paso = TipoLetra("Courier New", 8, False)
    Do While Not EOF(X)
        Line Input #X, s
        Printer.Print s
    Loop
    Printer.EndDoc
    Close #X
End Sub
Public Sub Nuevo3XTFile(FileName As String)
    Dim X As Integer
    Dim s As String
    Dim TextoCod As String * 5
    Dim TextoDet As String * 16
    Dim TextoCant As String * 8
    X = FreeFile
   ' On Error GoTo HandleError
    Open FileName For Output As X
   ' Do While Not EOF(x)
    With MSHxRubros
        Print #X, "VENTAS POR RUBROS"
        Print #X, "   "
        Print #X, "   "
        Print #X, "C�d.                      Cant."
        For pr = 1 To .Rows - 1
            .Row = pr
            .Col = 0
            TextoCod = .Text
            .Col = 1
            TextoDet = .Text
            .Col = 2
            TextoCant = .Text
            Print #X, TextoCod & " " & TextoDet & "  " & TextoCant
        Next
    End With
    Close #X
End Sub

Public Sub Nuevo5XTFile(FileName As String)
    Dim X As Integer
    Dim s As String
    Dim TextoCod As String * 4
    Dim TextoDet As String * 16
    Dim TextoCant As String * 2
    Dim Horario As String * 5
    Dim TxtResponde As String * 15
    X = FreeFile
   ' On Error GoTo HandleError
    Open FileName For Output As X
   ' Do While Not EOF(x)
    With MSHquitados
        Print #X, "PLATOS QUITADOS"
        Print #X, "   "
        Print #X, "   "
        Print #X, "C�d. Detalle        Cant. Hora Autoriza"
        For pr = 1 To .Rows - 1
            .Row = pr
            .Col = 0
            TextoCod = .Text
            .Col = 1
            TextoDet = .Text
            .Col = 4
            TextoCant = .Text
            .Col = 3
            Horario = .Text
            .Col = 5
            TxtResponde = .Text
            Print #X, TextoCod & " " & TextoDet & " " & TextoCant _
            & " " & Horario & " " & TxtResponde
        Next
    End With
    Close #X
End Sub

Private Sub MSHallEmitidos_DblClick()
    Dim Ticket As String
    Dim Valor As String
    MSHallEmitidos.Col = 0
    Ticket = MSHallEmitidos.Text
    MSHallEmitidos.Col = 3
    If Val(Ticket) = 0 Then
        Beep
        Exit Sub
    End If
    Valor = MSHallEmitidos.Text
    Supervisor.LbN.Caption = Supervisor.LbN.Caption & Ticket
    Supervisor.LbValor.Caption = Supervisor.LbValor.Caption & Valor
    Informes.Enabled = False
    Supervisor.Show 1
        
End Sub

Private Sub MSHvalesEmitidos_DblClick()
    Dim Ticket As String
    Dim Valor As String
    MSHvalesEmitidos.Col = 0
    Ticket = MSHvalesEmitidos.Text
    If Val(Ticket) = O Then
        Beep
        Exit Sub
    End If
    MSHvalesEmitidos.Col = 2
    Valor = MSHvalesEmitidos.Text
    Supervisor.LbN.Caption = Ticket
    Supervisor.LbValor.Caption = Valor
    
    Informes.Enabled = False
    Supervisor.Show
End Sub

Private Sub Timer1_Timer()
    If SSTab1.Tab = 0 Or SSTab1.Tab = 3 Then
        CmdPrint.Enabled = True
    Else
        CmdPrint.Enabled = False
    End If
End Sub
Public Function TipoLetra(Letra As String, Porte As Long, Raya As Boolean)
    Printer.FontName = Letra
    Printer.FontSize = Porte
    Printer.FontUnderline = Raya
End Function

Public Sub Nuevo2XTFile(FileName As String)
    Dim X As Integer
    Dim s As String
    Dim TextoTicket As String * 7
    Dim TextoMesa  As String * 3
    Dim TextoHora As String * 8
    Dim TextoValor As String * 7
    X = FreeFile
   ' On Error GoTo HandleError
    Open FileName For Output As X
   ' Do While Not EOF(x)
    With MSHallEmitidos
        Print #X, "TICKET EMITIDOS"
        Print #X, "   "
        Print #X, "Ticket  Mesa   Hora  Valor"
        For pr = 1 To .Rows - 1
            .Row = pr
            .Col = 0
            TextoTicket = .Text
            .Col = 1
            TextoMesa = .Text
            .Col = 5
            TextoHora = .Text
            .Col = 3
            TextoValor = .Text
            Print #X, TextoTicket & " " & TextoMesa & " " & TextoHora & " " & TextoValor
        Next
    End With
    Close #X
End Sub

Public Sub Nuevo4XTFile(FileName As String)
    Dim X As Integer
    Dim s As String
    Dim TextoConc As String * 15
    Dim TextoValor As String * 10
    X = FreeFile
   ' On Error GoTo HandleError
    Open FileName For Output As X
   ' Do While Not EOF(x)
    With MSHGarzones
        Print #X, " "
        Print #X, " "
        Print #X, "ACUMULADO GARZONES"
        Print #X, "   "
        For pr = 0 To .Rows - 1
            .Row = pr
            .Col = 0
            TextoConc = .Text
            .Col = 1
            TextoValor = .Text
            Print #X, TextoConc; "    "; TextoValor
        Next
    End With
    Close #X
End Sub

Public Sub Control2()
    Dim VentasTicket As ADODB.Recordset
    Dim VentasXgarzon As ADODB.Recordset
    Dim ventasXmesa As ADODB.Recordset
    Dim EgresosTurno As ADODB.Recordset
    Dim VentasXrubro As ADODB.Recordset
    Dim VentasXdescuentos As ADODB.Recordset
    Dim VentasXeliminados As ADODB.Recordset
    Dim VentasXquitados As ADODB.Recordset
    Dim EsteTurno As String
    Dim EstaFecha As String
    Dim cu As Integer
    Dim Fpagos(20) As String
    Dim ValoresFP(20) As Double
    Dim Lgarzones(30) As String
    Dim AcumGar(30) As Double
    Dim LMesa(99) As Integer
    Dim AcumMesa(99) As Double
    Dim Alimentos As Long
    Dim Bebestibles As Long
    Dim TVentas As Double
    Dim TGerencias As Double
    Dim TAtencion As Double
    Dim TVtaPersonal As Double
    Alimentos = 0
    Bebestibles = 0
    Timer1.Interval = 700
    myIngresos = 0
    myEgresos = 0
    myNorindes = 0
    myInicial = 0
    myArendir = 0
    myEfectivos = 0
    SSTab1.TabEnabled(0) = False
    SSTab1.TabEnabled(1) = False
    MsgBox "control2"
    EsteTurno = Main.TurnoTurno.Caption    ' ' Datos del Turno Actual
    EstaFecha = Main.TurnoFecha.Caption
    'cu = Main.AdoVales.Recordset.RecordCount
    'Set VentasTicket = FilterField(Main.AdoVales.Recordset, "Turno", EsteTurno, "Fecha", EstaFecha)
       
        
  
    Set VentasXdescuentos = FilterField(Main.AdoDescuentos.Recordset, "Turno", EsteTurno, "Fecha", EstaFecha)
    Set VentasXquitados = FilterField(Main.AdoQuitados.Recordset, "Turno", EsteTurno, "Fecha", EstaFecha)
    Set VentasXeliminados = FilterField(Supervisor.AdoEliminados.Recordset, "Turno", EsteTurno, "Fecha", EstaFecha)
    
    If VentasXdescuentos.RecordCount > 0 Then
    '    VentasXdescuentos.MoveFirst
        With MSHdescuentos
            Set .DataSource = VentasXdescuentos
            .Refresh
    '        .Row = 0
    '        .Col = 0
    '        .Text=
        End With
    End If
    If VentasXquitados.RecordCount > 0 Then
        VentasXquitados.MoveFirst
        With MSHquitados
            .Row = 0
            .Col = 0
            .Text = "C�d"
            .Col = 1
            .Text = "Producto"
            .Col = 2
            .Text = "Mesa"
            .Col = 3
            .Text = "Hora"
            .Col = 4
            .Text = "Autoriz�"
            .ColWidth(0) = 500
            .ColWidth(1) = 1700
            .ColWidth(2) = 600
            .ColWidth(3) = 1150
            For b = 1 To VentasXquitados.RecordCount
                
                .Row = b
                .Col = 0
                .Text = VentasXquitados.Fields(0)
                .Col = 1
                .Text = VentasXquitados.Fields(1)
                .Col = 2
                .Text = VentasXquitados.Fields(2)
                .Col = 3
                .Text = VentasXquitados.Fields(3)
                .Col = 4
                .Text = VentasXquitados.Fields(6)
                VentasXquitados.MoveNext
                .Rows = .Rows + 1
            Next b
        End With
    End If
    If VentasXeliminados.RecordCount > 0 Then
        VentasXeliminados.MoveFirst
        With MSHeliminados
            .Row = 0
            .Col = 0
            .Text = "N�Ticket"
            .Col = 1
            .Text = "Autoriz� Sr."
            .Col = 2
            .Text = "Monto"
            .ColWidth(0) = 850
            .ColWidth(1) = 1700
            .ColWidth(2) = 850
            For b = 1 To VentasXeliminados.RecordCount
                .Row = b
                .Col = 0
                .Text = VentasXeliminados.Fields(0)
                .Col = 1
                .Text = VentasXeliminados.Fields(1)
                .Col = 2
                .Text = VentasXeliminados.Fields(2)
                VentasXeliminados.MoveNext
                .Rows = .Rows + 1
            Next b
        End With
    End If
    ''Seccion de todos los Tickets emitidos
    If VentasTicket.RecordCount > 0 Then
         With MSHvalesEmitidos
             .Row = 0
             .MergeCol(0) = True
             .Col = 0
             .ColWidth(0) = 800
             .Text = "Ticket"
             .Col = 1
             .ColWidth(1) = 700
             .Text = "Mesa"
             .Col = 2
             .ColWidth(2) = 850
             .ColAlignment = 0
             .Text = "Monto"
             .Col = 3
             .ColWidth(3) = 1800
             .Text = "Garz�n"
             .ColAlignment = 1
             .ColWidth(4) = 1100
             .Col = 4
             .Text = "Hora"
             .ColWidth(5) = 650
             .Col = 5
             .Text = "Orden"
             .Col = 6
             .Text = "Alimentos"
             .ColWidth(6) = 650
             .Col = 7
             .Text = "Bebidas"
             .ColWidth(7) = 650
             .Row = .Row + 1
             
             VentasTicket.MoveFirst
             For tr = 1 To VentasTicket.RecordCount   '' Llena tickets emitidos en el turno
                If VentasTicket.Fields(10) = 1 Then
                    LBVentas.Caption = LBVentas.Caption + VentasTicket.Fields(2)
                End If
                If VentasTicket.Fields(10) = 2 Then
                    LbGerencias.Caption = LbGerencias.Caption + VentasTicket.Fields(2)
                End If
                If VentasTicket.Fields(10) = 3 Then
                    LbAtenciones.Caption = LbAtenciones.Caption + VentasTicket.Fields(2)
                End If
                If VentasTicket.Fields(10) = 4 Then
                    LbVtaPersonal.Caption = LbVtaPersonal.Caption + VentasTicket.Fields(2)
                End If
                .Col = 0
                 .Text = VentasTicket.Fields(0)
                 .Col = 1
                 .Text = VentasTicket.Fields(1)
                 .Col = 2
                 .Text = VentasTicket.Fields(2)
                 .Col = 3
                 .Text = VentasTicket.Fields(4)
                 .Col = 4
                 .Text = VentasTicket.Fields(3)
                 .Col = 5
                 .Text = VentasTicket.Fields(7)
                 .Col = 6
                 .Text = VentasTicket.Fields(8)
                 .Col = 7
                 .Text = VentasTicket.Fields(9)
                 myIngresos = myIngresos + VentasTicket.Fields(2)
                 Alimentos = Alimentos + VentasTicket.Fields(8)
                 Bebestibles = Bebestibles + VentasTicket.Fields(9)
                 .Rows = .Rows + 1
                 .Row = .Row + 1
                 VentasTicket.MoveNext
             Next
             .Rows = .Rows - 1
             .Col = 5     ''  Ordena los tickets emitidos en el turno actual
             
             .Sort = flexSortGenericAscending
             .Rows = .Rows + 2
             .Row = .Rows - 1
             .Col = 1
             .Text = "TOTAL"
             .Col = 2
             .Text = myIngresos
             .Col = 6
             .Text = Alimentos
             .Col = 7
             .Text = Bebestibles
         End With
         
         
         '''  Seccion de Ventas por Garz�n"
         For Lg = 1 To 30
             AcumGar(Lg) = 0
         Next
         For Lg = 0 To ListaGarzones.CmdGarzones.Count - 1
             Lgarzones(Lg + 1) = ListaGarzones.CmdGarzones(Lg).Caption
         Next
         VentasTicket.MoveFirst
         For Lg = 1 To VentasTicket.RecordCount
             For cg = 1 To ListaGarzones.CmdGarzones.Count
                 If Lgarzones(cg) = VentasTicket.Fields(4) Then
                     AcumGar(cg) = AcumGar(cg) + VentasTicket.Fields(2)
                     Exit For
                 End If
             Next
             VentasTicket.MoveNext
         Next
         With MSHGarzones
             .Row = 0
             .ColWidth(0) = 2500
             .Col = 0
             .Text = "Garz�n"
             .ColWidth(1) = 1300
             .Col = 1
             .Text = "Acumulado"
             For Lg = 1 To 30
                 If AcumGar(Lg) > 0 Then
                     .Row = .Row + 1
                     .Col = 0
                     .Text = Lgarzones(Lg)
                     .Col = 1
                     .Text = AcumGar(Lg)
                     .Rows = .Rows + 1
                 End If
             Next
             .Rows = .Rows - 1
             .ColAlignment(0) = 0
             .Col = 0
             .Sort = flexSortGenericAscending
         End With
         
         
         
         '' Aqui procesamos para Acumulaci�n de Cada Mesa
         For Lg = 1 To 99
             AcumMesa(Lg) = 0
         Next
         For Lg = 1 To Main.LBmesas.Caption
             LMesa(Lg) = Lg
         Next
         VentasTicket.MoveFirst
         For Lg = 1 To VentasTicket.RecordCount
             For cg = 1 To Main.LBmesas.Caption
                 If LMesa(cg) = VentasTicket.Fields(1) Then
                     AcumMesa(cg) = AcumMesa(cg) + VentasTicket.Fields(2)
                     Exit For
                 End If
             Next
             VentasTicket.MoveNext
         Next
        
         With MSHGarzones
            ' .Row = 0
            ' .ColWidth(0) = 1000
            .Rows = .Rows + 3
            .Row = .Rows - 2
             .Col = 0
             .Text = "N� Mesa"
             .Col = 1
             .Text = "Acumulado"
             For Lg = 1 To 99
                   If AcumMesa(Lg) > 0 Then
                     .Row = .Row + 1
                     .Col = 0
                     .Text = LMesa(Lg)
                     .Col = 1
                     .Text = AcumMesa(Lg)
                     .Rows = .Rows + 1
               End If
             Next
            .Rows = .Rows - 1
             .ColAlignment(0) = 0
           '  .Col = 0
           '  .Sort = flexSortGenericAscending
         End With
    End If
    
    '''' Seccion de venta separada por RUBROS
    conru = 0
    cu = 0
    cu = AdoTurnoPedido.Recordset.RecordCount
    Set VentasXrubro = FilterField(AdoTurnoPedido.Recordset, "Turno", EsteTurno, "Fecha", EstaFecha)
    If VentasXrubro.RecordCount > 0 Then
    With VentasXrubro
        .MoveFirst
        If .EOF Then Exit Sub
        MSHxRubros.ColWidth(1) = 1850
        MSHxRubros.ColWidth(0) = 600
        MSHxRubros.ColWidth(2) = 600
        
        MSHxRubros.Row = 0
        MSHxRubros.Col = 0
        MSHxRubros.Text = "C�digo"
        MSHxRubros.Col = 2
        MSHxRubros.Text = "Cant."
        conru = .Fields(0)
        MSHxRubros.Row = 1
        MSHxRubros.Col = 1
        MSHxRubros.Text = UCase(Main.CMDrubros(conru - 1).Caption)
        MSHxRubros.Rows = MSHxRubros.Rows + 1
        MSHxRubros.Row = MSHxRubros.Row + 1
        For rr = 1 To .RecordCount
            If .Fields(0) <> conru Then
                conru = .Fields(0)
                MSHxRubros.Rows = MSHxRubros.Rows + 3
                MSHxRubros.Row = MSHxRubros.Row + 2
                MSHxRubros.Col = 1
                MSHxRubros.Text = UCase(Main.CMDrubros(conru - 1).Caption)
                MSHxRubros.Rows = MSHxRubros.Rows + 1
                'MSHxRubros.Row = MSHxRubros.Row + 1
                rr = rr - 1
                If rr = .RecordCount Then
                    MSHxRubros.Row = MSHxRubros.Row + 1
                    MSHxRubros.Col = 0
                    MSHxRubros.Text = .Fields(1)
                    MSHxRubros.Col = 1
                    MSHxRubros.Text = .Fields(2)
                    MSHxRubros.Col = 2
                    MSHxRubros.Text = .Fields(5)
                End If
            Else
                MSHxRubros.Rows = MSHxRubros.Rows + 1
                MSHxRubros.Row = MSHxRubros.Row + 1
                MSHxRubros.Col = 0
                MSHxRubros.Text = .Fields(1)
                MSHxRubros.Col = 1
                MSHxRubros.Text = .Fields(2)
                MSHxRubros.Col = 2
                MSHxRubros.Text = .Fields(5)
                MSHxRubros.Rows = MSHxRubros.Rows + 1
                .MoveNext
            End If
        Next
        For sl = MSHxRubros.Rows To 1 Step -1
            MSHxRubros.Row = sl - 1
            MSHxRubros.Col = 0
            If MSHxRubros.Text <> "" Then
                MSHxRubros.Rows = MSHxRubros.Rows - (MSHxRubros.Rows - sl - 1)
                Exit For
            End If
        Next
    End With
    End If
End Sub
Public Sub Nuevo6XTFile(FileName As String)
    Dim X As Integer
    Dim s As String
    Dim TextoTicket As String * 7
    Dim TextoMesa  As String * 2
    Dim TextoHora As String * 8
    Dim TextoValor As String * 7
    Dim TextoGarzon As String * 2
    X = FreeFile
   ' On Error GoTo HandleError
    Open FileName For Output As X
   ' Do While Not EOF(x)
    With MSHvalesEmitidos
        Print #X, "TICKET EMITIDOS"
        Print #X, "   "
        Print #X, "Ticket Mes   Hora  Valor   Garz�n"
        For pr = 1 To .Rows - 1
            .Row = pr
            .Col = 0
            TextoTicket = .Text
            .Col = 1
            TextoMesa = .Text
            .Col = 4
            TextoHora = .Text
            .Col = 2
            TextoValor = .Text
            .Col = 3
            TextoGarzon = Mid(.Text, 1, 2)
            Print #X, TextoTicket & " " & TextoMesa & " " & TextoHora & " " & TextoValor & " " & TextoGarzon
        Next
        .Col = 6
        Print #X, "Alimentos   $" & .Text
        .Col = 7
        Print #X, "Bebestibles $" & .Text
        Print #X, "______________"
        Print #X, "Ventas         $" & LBVentas.Caption
        Print #X, "Gerencias      $" & LbGerencias.Caption
        Print #X, "Atenciones     $" & LbAtenciones.Caption
        Print #X, "Vtas. Personal $" & LbVtaPersonal.Caption
    End With
    Close #X
End Sub

Public Sub DescuentosEfectuados(FileName As String)
    X = FreeFile
    Open FileName For Output As X
        Print #X, "DESCUENTOS EFECTUADOS"
        
        Print #X, "- - - - - - - - - - -------------"
        Print #X, "Mesa Hora    Dscto   Real   Autorizo"
        If Me.MSHdescuentos.Rows > 1 Then
            For i = 1 To Me.MSHdescuentos.Rows - 1
                Print #X, Me.MSHdescuentos.TextMatrix(i, 0) & "     "; _
                          Mid(Me.MSHdescuentos.TextMatrix(i, 1), 1, 5) & "   "; _
                          Me.MSHdescuentos.TextMatrix(i, 2) & "   "; _
                          Me.MSHdescuentos.TextMatrix(i, 3) & "   "; _
                          Me.MSHdescuentos.TextMatrix(i, 7)
                          
                          
            Next
        End If
        Print #X, "----------------------"
    Close X
        
    
End Sub
