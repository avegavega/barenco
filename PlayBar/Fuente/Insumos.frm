VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form Insumos 
   Caption         =   "Insumos"
   ClientHeight    =   7215
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7215
   ScaleWidth      =   10680
   WindowState     =   2  'Maximized
   Begin MSAdodcLib.Adodc AdoInsumos 
      Height          =   330
      Left            =   7920
      Top             =   5760
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=Euro"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "Euro"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "Select codigo,insumo,umedida,familia,precio,stockminimo,duracion from losinsumos order by codigo"
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8640
      TabIndex        =   34
      Top             =   6480
      Width           =   2055
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Familias"
      Height          =   2175
      Left            =   2640
      TabIndex        =   27
      Top             =   5520
      Width           =   5055
      Begin VB.CommandButton CmdActFamilia 
         Caption         =   "Actualiza"
         Height          =   615
         Left            =   120
         TabIndex        =   32
         Top             =   960
         Width           =   1335
      End
      Begin VB.ListBox ListaFamily 
         Height          =   1815
         Left            =   1680
         TabIndex        =   31
         Top             =   240
         Width           =   2055
      End
      Begin VB.CommandButton CmdAgree 
         Caption         =   "Agrega"
         Height          =   375
         Left            =   3960
         TabIndex        =   30
         Top             =   480
         Width           =   855
      End
      Begin VB.CommandButton CmdBorraFamily 
         Caption         =   "Borra"
         Height          =   375
         Left            =   3960
         TabIndex        =   29
         Top             =   960
         Width           =   855
      End
      Begin VB.CommandButton CmdEditaFamilia 
         Caption         =   "Edita"
         Height          =   375
         Left            =   3960
         TabIndex        =   28
         Top             =   1440
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00000006&
      Caption         =   "Insumos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5055
      Left            =   480
      TabIndex        =   0
      Top             =   360
      Width           =   9255
      Begin VB.CommandButton CmdInsumo 
         Caption         =   "Busca"
         Height          =   375
         Index           =   5
         Left            =   7680
         TabIndex        =   12
         Top             =   4320
         Width           =   975
      End
      Begin VB.ListBox ListaInsumos 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   480
         TabIndex        =   18
         Top             =   360
         Width           =   2535
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Actualiza Listado"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   480
         TabIndex        =   17
         Top             =   4440
         Width           =   2535
      End
      Begin VB.CommandButton CmdInsumo 
         Caption         =   "Agrega"
         Height          =   375
         Index           =   0
         Left            =   3720
         TabIndex        =   7
         Top             =   4320
         Width           =   855
      End
      Begin VB.CommandButton CmdInsumo 
         Caption         =   "Edita"
         Height          =   375
         Index           =   1
         Left            =   4560
         TabIndex        =   8
         Top             =   4320
         Width           =   735
      End
      Begin VB.CommandButton CmdInsumo 
         Caption         =   "Graba"
         Height          =   375
         Index           =   2
         Left            =   5280
         TabIndex        =   9
         Top             =   4320
         Width           =   855
      End
      Begin VB.CommandButton CmdInsumo 
         Caption         =   "Cancela "
         Height          =   375
         Index           =   3
         Left            =   6120
         TabIndex        =   10
         Top             =   4320
         Width           =   855
      End
      Begin VB.CommandButton CmdInsumo 
         Caption         =   "Borra"
         Height          =   375
         Index           =   4
         Left            =   6960
         TabIndex        =   11
         Top             =   4320
         Width           =   735
      End
      Begin VB.TextBox TxtInsumo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   5520
         TabIndex        =   1
         Top             =   840
         Width           =   2535
      End
      Begin VB.ComboBox ComboUM 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5520
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1440
         Width           =   2055
      End
      Begin VB.ComboBox ComboFamilia 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5520
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   2040
         Width           =   2415
      End
      Begin VB.TextBox TxtPrecio 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "#.##0,0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   5520
         TabIndex        =   4
         Top             =   2640
         Width           =   1455
      End
      Begin VB.CommandButton CmdAvance 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   6360
         TabIndex        =   16
         Top             =   360
         Width           =   495
      End
      Begin VB.CommandButton CmdAvance 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   6840
         TabIndex        =   15
         Top             =   360
         Width           =   375
      End
      Begin VB.CommandButton CmdAvance 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   7200
         TabIndex        =   14
         Top             =   360
         Width           =   375
      End
      Begin VB.CommandButton CmdAvance 
         Caption         =   ">>"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   3
         Left            =   7560
         TabIndex        =   13
         Top             =   360
         Width           =   495
      End
      Begin VB.TextBox TxtMinimo 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   5520
         TabIndex        =   5
         Top             =   3240
         Width           =   1335
      End
      Begin VB.TextBox TxtDuracion 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   5520
         TabIndex        =   6
         Top             =   3840
         Width           =   1335
      End
      Begin VB.Line Line4 
         BorderColor     =   &H80000004&
         X1              =   9240
         X2              =   7920
         Y1              =   2160
         Y2              =   2160
      End
      Begin VB.Label Label4 
         BackColor       =   &H00000000&
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   3720
         TabIndex        =   26
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label5 
         BackColor       =   &H00000000&
         Caption         =   "Insumo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   3720
         TabIndex        =   25
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label6 
         BackColor       =   &H00000000&
         Caption         =   "UM"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   3720
         TabIndex        =   24
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label7 
         BackColor       =   &H00000000&
         Caption         =   "Familia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   3720
         TabIndex        =   23
         Top             =   2040
         Width           =   975
      End
      Begin VB.Label Label8 
         BackColor       =   &H00000000&
         Caption         =   "Precio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   0
         Left            =   3720
         TabIndex        =   22
         Top             =   2640
         Width           =   735
      End
      Begin VB.Label LbCodigo 
         Alignment       =   2  'Center
         Caption         =   "Label9"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5280
         TabIndex        =   21
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label8 
         BackColor       =   &H00000000&
         Caption         =   "Stock M�nimo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   1
         Left            =   3720
         TabIndex        =   20
         Top             =   3240
         Width           =   1815
      End
      Begin VB.Label Label8 
         BackColor       =   &H00000000&
         Caption         =   "Duraci�n (d�as)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   2
         Left            =   3720
         TabIndex        =   19
         Top             =   3840
         Width           =   2055
      End
   End
   Begin VB.Line Line3 
      X1              =   10080
      X2              =   9720
      Y1              =   2520
      Y2              =   2520
   End
   Begin VB.Line Line2 
      X1              =   10080
      X2              =   10080
      Y1              =   6360
      Y2              =   2520
   End
   Begin VB.Line Line1 
      X1              =   7680
      X2              =   10080
      Y1              =   6360
      Y2              =   6360
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      DataField       =   "Insumo"
      DataSource      =   "AdoInsumos"
      Height          =   375
      Left            =   10200
      TabIndex        =   33
      Top             =   5160
      Visible         =   0   'False
      Width           =   1095
   End
End
Attribute VB_Name = "Insumos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CodFree As Integer

Private Sub CmdActFamilia_Click()
    Dim ll As Integer
    ComboFamilia.Clear
    For ll = 0 To ListaFamily.ListCount - 1
        ComboFamilia.AddItem (ListaFamily.List(ll))
    Next
End Sub

Private Sub CmdAgree_Click()
    Dim NuevaFam As String * 15 '' Agrega familia
    Dim F As Integer, i As Integer
    F = FreeFile
    NuevaFam = InputBox("Nombre de la nueva familia", "Nueva Familia")
    If Len(Trim(NuevaFam)) > 0 Then
        If ListaFamily.ListCount = 1 And Len(ListaFamily.List(0)) = 0 Then
            ListaFamily.List(0) = NuevaFam
        Else
            ListaFamily.AddItem (NuevaFam)
        End If
    End If
    LargoR = Len(rFamilia)
    Kill LaRutadeArchivos & "familia.txt"
    Open LaRutadeArchivos & "familia.txt" For Random As #F Len = LargoR
    For i = 0 To ListaFamily.ListCount - 1
        rFamilia.NomFamilia = Mid(ListaFamily.List(i), 1, 15)
        Put #F, , rFamilia
    Next
    Close #F
End Sub

Private Sub CmdAvance_Click(Index As Integer)
    Dim ii As Integer
    With AdoInsumos.Recordset
        If Index = 0 Then .MoveFirst
        If Index = 1 Then .MovePrevious
        If Index = 2 Then .MoveNext
        If Index = 3 Then .MoveLast
        If .EOF Then .MovePrevious
        If .BOF Then .MoveNext
        LbCodigo.Caption = .Fields(0)
        TxtInsumo.Text = .Fields(1)
        ComboUM.Text = .Fields(2)
        For ii = 0 To ComboFamilia.ListCount - 1
            If ComboFamilia.List(ii) = .Fields(3) Then
                ComboFamilia.ListIndex = ii
                Exit For
            End If
        Next
        'ComboFamilia.Text = .Fields(3)
        TxtPrecio.Text = .Fields(4)
        TxtMinimo.Text = .Fields(5)
        TxtDuracion.Text = .Fields(6)
    End With
End Sub

Private Sub CmdBorraFamily_Click() ''Borra Familia
    If ListaFamily.ListIndex = -1 Then Exit Sub
    ListaFamily.RemoveItem (ListaFamily.ListIndex)
    Dim F As Integer, i As Integer
    F = FreeFile
    LargoR = Len(rFamilia)
    Kill LaRutadeArchivos & "familia.txt"
    Open LaRutadeArchivos & "familia.txt" For Random As #F Len = LargoR
    For i = 0 To ListaFamily.ListCount - 1
        rFamilia.NomFamilia = Mid(ListaFamily.List(i), 1, 15)
        Put #F, , rFamilia
    Next
    Close #F
    
End Sub

Private Sub CmdEditaFamilia_Click()
    Dim hola As String
    hola = InputBox(ListaFamily.List(ListaFamily.ListIndex), "Editando familia")
    If Len(hola) > 0 Then
        ListaFamily.List(ListaFamily.ListIndex) = hola
    End If
End Sub


Private Sub CmdInsumo_Click(Index As Integer)
    If Index = 0 Then 'Agree Insumo
        LiberarCajas (True)
        LiberaBotones (False)
        CmdInsumo(2).Enabled = True
        CmdInsumo(3).Enabled = True
        LimpiaCajas
        BuscaCodigoLibre
        LbCodigo.Caption = CodFree
        TxtInsumo.SetFocus
    End If
    
    If Index = 1 Then ' Edita el registro
        LiberarCajas (True)
        LiberaBotones (False)
        CmdInsumo(2).Caption = "Actualiza"
        CmdInsumo(2).Enabled = True
        CmdInsumo(3).Enabled = True
    End If
    
    If Index = 2 Then 'Graba registro actual
        If Len(TxtInsumo.Text) = 0 Or _
            Val(TxtPrecio.Text) = 0 Or _
            Val(TxtMinimo.Text) = 0 Or _
            Val(TxtDuracion.Text) = 0 Then
            MsgBox "Faltan datos o hay datos mal ingresados"
            Beep
            Exit Sub
        End If
        With AdoInsumos.Recordset
            If CmdInsumo(2).Caption = "Graba" Then
                .AddNew
                .Fields(0) = LbCodigo.Caption
            End If
            .Fields(1) = TxtInsumo.Text
            .Fields(2) = ComboUM.List(ComboUM.ListIndex)
            .Fields(3) = ComboFamilia.List(ComboFamilia.ListIndex)
            .Fields(4) = TxtPrecio.Text
            .Fields(5) = TxtMinimo.Text
            .Fields(6) = TxtDuracion.Text
            .Update
            CmdInsumo(2).Caption = "Graba"
        End With
        LiberarCajas (False)
        LiberaBotones (True)
        CmdInsumo(2).Enabled = False
        CmdInsumo(3).Enabled = False
        CmdAvance_Click (0)
    End If
    If Index = 3 Then '' Cancela ingreso actual
        LiberarCajas (False)
        LiberaBotones (True)
        CmdInsumo(2).Enabled = False
        CmdInsumo(3).Enabled = False
        CmdAvance_Click (0)
        'AdoInsumos.Recordset.MovePrevious
    End If
    If Index = 4 Then 'Borrar registro
        Dim i As Integer
        i = MsgBox("Est� seguro de eliminar este registro ...", vbQuestion + vbOKCancel)
        If i = 1 Then
            AdoInsumos.Recordset.Delete
            CmdAvance_Click (0)
        End If
    End If
    If Index = 5 Then 'buscar registro
        Dim Buscado As String, Criterio As String
        Buscado = InputBox("Descripci�n del insumo ... ")
        If Buscado = "" Then Exit Sub
        Criterio = "Insumo Like '*" & Buscado & "*'"
        ' Buscar desde el siguiente registro a la posici�n actual
        With AdoInsumos.Recordset
            .MoveNext
            If Not .EOF Then
                .Find Criterio
            End If
            If .EOF Then
                .MoveFirst
                ' Buscar desde el principio
                .Find Criterio
                If .EOF Then
                    .MoveLast
                    MsgBox ("Producto no encontrado")
                End If
            End If
            LbCodigo.Caption = .Fields(0)
            TxtInsumo.Text = .Fields(1)
            ComboUM.Text = .Fields(2)
            ComboFamilia.Text = .Fields(3)
            TxtPrecio.Text = .Fields(4)
            TxtMinimo.Text = .Fields(5)
            TxtDuracion.Text = .Fields(6)
        End With
    End If
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Command2_Click()
    Dim ll As Integer
    ListaInsumos.Clear
    With AdoInsumos.Recordset
        .MoveFirst
        For ll = 1 To .RecordCount
            If .Fields(0) > 0 Then _
            ListaInsumos.AddItem (.Fields(1))
            .MoveNext
        Next
        .MoveFirst
    End With
    
End Sub

Private Sub Form_Load()
    ComboUM.AddItem ("UN")
    ComboUM.AddItem ("KL")
    ComboUM.AddItem ("LT")
    ComboUM.AddItem ("LB")
    ComboUM.ListIndex = 0
    Dim F As Integer, ll As Integer, Contador As Integer
    F = FreeFile
    ListaFamily.Clear
    LargoR = Len(rFamilia)
    Open "c:\cafeuro\Familia.txt" For Random As #F Len = LargoR
    While Not EOF(F)
        Get #F, , rFamilia
        If EOF(F) = False Then
            If Len(Trim(rFamilia.NomFamilia)) > 0 Then ListaFamily.AddItem (rFamilia.NomFamilia)
        End If
    Wend
    Close #F
    For ll = 0 To ListaFamily.ListCount - 1
        ComboFamilia.AddItem (ListaFamily.List(ll))
    Next
    If AdoInsumos.Recordset.RecordCount = 1 Then
        LiberaBotones (False)
        LiberarCajas (False)
        CmdInsumo(0).Enabled = True
    Else
        LiberarCajas (False)
        LiberaBotones (True)
        CmdInsumo(2).Enabled = False
        CmdInsumo(3).Enabled = False
        With AdoInsumos.Recordset
            
            For ll = 1 To .RecordCount
                If .Fields(0) > 0 Then _
                ListaInsumos.AddItem (.Fields(1))
                .MoveNext
            Next
            .MoveFirst
            LbCodigo.Caption = .Fields(0)
            TxtInsumo.Text = .Fields(1)
            ComboUM.Text = .Fields(2)
            For ll = 0 To ListaFamily.ListCount - 1
                If ListaFamily.List(ll) = .Fields(3) Then
                    ComboFamilia.Text = ListaFamily.List(ll)
                    Exit For
                End If
            Next
            TxtPrecio.Text = .Fields(4)
            TxtMinimo.Text = .Fields(5)
            TxtDuracion.Text = .Fields(6)
        End With
        
    End If
 '   ComboFamilia.ListIndex = 0

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main.Enabled = True
End Sub

Public Sub LiberaBotones(oSi As Boolean)
    Dim i As Integer
    For i = 0 To 5
        If oSi = True Then CmdInsumo(i).Enabled = True
        If oSi = False Then CmdInsumo(i).Enabled = False
    Next
    For i = 0 To 3
        If oSi = True Then CmdAvance(i).Enabled = True
        If oSi = False Then CmdAvance(i).Enabled = False
    Next
End Sub

Public Sub LiberarCajas(Oyes As Boolean)
    If Oyes = True Then
        TxtInsumo.Enabled = True
        Me.ComboUM.Enabled = True
        Me.ComboFamilia.Enabled = True
        TxtPrecio.Enabled = True
        Me.TxtMinimo.Enabled = True
        Me.TxtDuracion.Enabled = True
    End If
    If Oyes = False Then
        TxtInsumo.Enabled = False
        Me.ComboUM.Enabled = False
        Me.ComboFamilia.Enabled = False
        TxtPrecio.Enabled = False
        Me.TxtMinimo.Enabled = False
        Me.TxtDuracion.Enabled = False
    End If

End Sub
Public Sub LimpiaCajas()
        TxtInsumo.Text = ""
        Me.ComboUM.ListIndex = 0
        Me.ComboFamilia.ListIndex = 0
        TxtPrecio.Text = ""
        Me.TxtMinimo.Text = ""
        Me.TxtDuracion.Text = ""
End Sub

Public Sub BuscaCodigoLibre()
    Dim cl As Double
    For cl = 1 To 9999
        CodigosLibres(cl) = False
    Next
    With AdoInsumos.Recordset
        .MoveFirst
        For cl = 1 To 9999
            If .Fields(0) = cl Then
                CodigosLibres(cl) = True
            Else
                CodigosLibres(cl) = False
            End If
            .MoveNext
            If .EOF Then Exit For
        Next
        .MoveFirst
    End With
    For cl = 1 To 9999
        If CodigosLibres(cl) = False Then
            CodFree = cl
            Exit For
        End If
    Next
End Sub

