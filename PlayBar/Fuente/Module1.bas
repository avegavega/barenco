Attribute VB_Name = "Module1"
Public esCaja As Boolean
Public IdEquipo    As String
Public PathMesas As String
Public CantidadRubros As Integer
Public contGarzones As Integer
Public LaRutadeArchivos As String
Public GarzonMesa(150) As String
Public Imprimir As Boolean
Public MesaStatus(150) As Integer
Public MainColores(4) As Variant
Public Autorizo    As String
Public CodigosLibres(9999) As Boolean
Public MesaUpedido(150) As Double
Public MesaDescuento(150) As String
Public RubroDef1(150) As Double
Public RubroDef2(150) As Double
Public Ccomenzales(150) As Integer
Public MesaSelectItem As Integer
Public Distinto As String * 3
Public Producto_Comanda As String
Public Type EstrucPedido  'estructura de archivo
    Npedido As String * 6 'de pedido (comandas)
    Hora As String * 9
    Nmesa As String * 3
    Codigo As String * 5
    Cantidad As String * 3
    Detalle As String * 20
    PrecioU As String * 6
    SubTotal As String * 7
    Bebestible As String * 3
    Rubro As String * 2
    NComenzales As String * 3
End Type
Public rPedido As EstrucPedido
Public Type Familia
    NomFamilia As String * 15
End Type
Public rFamilia As Familia
Public Actualiza As Byte
Public TotalItems As Integer
Public LargoR As Long
Public Desde As Integer
Public PorParte As Boolean
Public DescuentoActual(150) As Double
Public NoRindes As Byte
Public NombreGar(150) As String
Public Lg As Integer
Public PermiteSacarItem As Boolean
Public permiteDescuento As Boolean
Public Autorizador As String
Public Type MinutaForma  'estructura de archivo
    MinCodInsumo As String * 5
    MinElInsumo As String * 20
    MinUMInsumo As String * 2
    MinCantInsumo As String * 5
    MinCostoInsumo As String * 7
    MinFamiliaInsumo As String * 15
End Type
Public ClaveSi As Boolean
Public OpcionesDePago(20) As String
Public Saldillo As Variant
Public Boletilla As Variant
Public SaldilloMesa(150) As Variant
Public BoletillaMesa(150) As Variant
Public PassWordGarzon(20) As Integer
Public HappyHours As String
Public HorarioIniHappy As String
Public HorarioFinHappy As String
Public ElE As Boolean
Public ElMainActivo As Boolean
Public S_Servidor As String
Public Rst_tmp As Recordset
Public Sql  As String
Public S_Empresa As String
Public Sql_Visor As String
Public Formulario_Visor As Form
Public Nuevo_Visor As Boolean
Public ID_Visor As Double
Public Tipo_Producto_Busqueda As Variant
Public Codigo_Devuelto As Double
Public I_Men_Rubro As Integer
Public S_Men_Producto As String
Public S_Hora_Unica As String * 9

Public ClrCfoco As String
Public ClrSfoco As String
Public ClrDesha As String

Public BM_NuevaAlternativa As Boolean
Public IM_Id_Alternativa As Integer
