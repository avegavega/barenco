VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form IngresoInsumos 
   BackColor       =   &H00C0C0C0&
   Caption         =   "Ingreso de Mercader�as"
   ClientHeight    =   8355
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   ScaleHeight     =   8355
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.ComboBox CboBusqueda 
      Height          =   315
      Left            =   360
      TabIndex        =   53
      Text            =   "CboBusqueda"
      Top             =   120
      Width           =   3015
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   7680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "IngresoInsumos.frx":0000
            Key             =   "chile"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "IngresoInsumos.frx":119A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "IngresoInsumos.frx":274C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "IngresoInsumos.frx":2B9E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "IngresoInsumos.frx":6CDE
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "IngresoInsumos.frx":7130
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "IngresoInsumos.frx":7582
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "IngresoInsumos.frx":79D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "IngresoInsumos.frx":7CEE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton CmdRevisaCompras 
      Appearance      =   0  'Flat
      Caption         =   "&Revisar Compras"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1440
      TabIndex        =   41
      Top             =   7320
      Width           =   1935
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9360
      TabIndex        =   40
      Top             =   7320
      Width           =   2055
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHPaso 
      Height          =   255
      Left            =   3840
      TabIndex        =   37
      Top             =   8040
      Visible         =   0   'False
      Width           =   375
      _ExtentX        =   661
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   7
      FixedRows       =   0
      FixedCols       =   0
      _NumberOfBands  =   1
      _Band(0).Cols   =   7
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos del proveedor"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   360
      TabIndex        =   6
      Top             =   480
      Width           =   11295
      Begin VB.TextBox TxtProveedor 
         DataField       =   "NombreFantasia"
         DataSource      =   "AdoProveedores"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         TabIndex        =   1
         Top             =   480
         Width           =   3975
      End
      Begin VB.TextBox Text5 
         DataField       =   "FormaPago"
         DataSource      =   "AdoProveedores"
         Height          =   285
         Left            =   5640
         TabIndex        =   5
         Top             =   1080
         Width           =   1695
      End
      Begin VB.TextBox Text4 
         DataField       =   "Ciudad"
         DataSource      =   "AdoProveedores"
         Height          =   285
         Left            =   3600
         TabIndex        =   4
         Top             =   1080
         Width           =   1575
      End
      Begin VB.TextBox Text3 
         DataField       =   "Direccion"
         DataSource      =   "AdoProveedores"
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   1080
         Width           =   3375
      End
      Begin VB.TextBox Text2 
         DataField       =   "fonofax"
         DataSource      =   "AdoProveedores"
         Height          =   285
         Left            =   6720
         TabIndex        =   2
         Top             =   480
         Width           =   2895
      End
      Begin VB.TextBox TxtRutP 
         DataField       =   "Rut"
         DataSource      =   "AdoProveedores"
         Height          =   285
         Left            =   720
         TabIndex        =   0
         Top             =   480
         Width           =   1215
      End
      Begin MSAdodcLib.Adodc AdoProveedores 
         Height          =   330
         Left            =   8280
         Top             =   1080
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   2
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   3
         Appearance      =   1
         BackColor       =   8421376
         ForeColor       =   65280
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "DSN=Euro"
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "Euro"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   "Proveedores"
         Caption         =   "Avance Proveedor"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.Label Label6 
         Caption         =   "Forma de Pago"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5640
         TabIndex        =   12
         Top             =   840
         Width           =   1935
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         Caption         =   "Ciudad"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3720
         TabIndex        =   11
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Direcci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Fono y Fax"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6960
         TabIndex        =   9
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2880
         TabIndex        =   8
         Top             =   240
         Width           =   2295
      End
      Begin VB.Label Label1 
         Caption         =   "R.U.T."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   480
         Width           =   735
      End
   End
   Begin VB.Frame Frame4 
      Height          =   5415
      Left            =   360
      TabIndex        =   43
      Top             =   2040
      Visible         =   0   'False
      Width           =   10815
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHComprasMensuales 
         Height          =   3615
         Left            =   2040
         TabIndex        =   50
         Top             =   840
         Width           =   8055
         _ExtentX        =   14208
         _ExtentY        =   6376
         _Version        =   393216
         Cols            =   5
         FixedCols       =   0
         HighLight       =   2
         FillStyle       =   1
         MergeCells      =   2
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   5
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin VB.ComboBox Combo1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   48
         Top             =   600
         Width           =   1095
      End
      Begin VB.Frame Frame3 
         Caption         =   "Total Mes"
         Height          =   735
         Left            =   7800
         TabIndex        =   45
         Top             =   4440
         Width           =   2175
         Begin VB.Label LbTMes 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            BeginProperty DataFormat 
               Type            =   1
               Format          =   """$"" #.##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   2
            EndProperty
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   240
            TabIndex        =   46
            Top             =   240
            Width           =   1695
         End
      End
      Begin MSComctlLib.TabStrip TabCompras 
         Height          =   4935
         Left            =   2040
         TabIndex        =   44
         Top             =   360
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   8705
         HotTracking     =   -1  'True
         Separators      =   -1  'True
         _Version        =   393216
         BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
            NumTabs         =   12
            BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Enero-01"
               Key             =   "ene"
               Object.Tag             =   "1"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Febreo-02"
               Key             =   "feb"
               ImageVarType    =   2
               ImageIndex      =   1
            EndProperty
            BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Marzo-03"
               Key             =   "mar"
               ImageVarType    =   2
               ImageIndex      =   3
            EndProperty
            BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Abril-04"
               Key             =   "Abr"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Mayo-05"
               Key             =   "May"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab6 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Junio-06"
               Key             =   "Jun"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab7 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Julio-07"
               Key             =   "Jul"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab8 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Agosto-08"
               Key             =   "Ago"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab9 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Septiembre-09"
               Key             =   "Sep"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab10 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Octubre-10"
               Key             =   "Oct"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab11 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Noviembre-11"
               Key             =   "Nov"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab12 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Diciembre-12"
               Key             =   "Dic"
               ImageVarType    =   2
            EndProperty
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label21 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Para eliminar alguna compra haga Doble Click en el N� de Documento."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   240
         TabIndex        =   51
         Top             =   2400
         Width           =   1455
      End
      Begin VB.Label Label20 
         Caption         =   "A�o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   49
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Caption         =   "Datos del Documento"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   5295
      Left            =   720
      TabIndex        =   13
      Top             =   2040
      Width           =   10815
      Begin MSAdodcLib.Adodc AdoListadoCompras 
         Height          =   330
         Left            =   3240
         Top             =   4920
         Visible         =   0   'False
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   2
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   3
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "DSN=Euro"
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "Euro"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   "ListaCompras"
         Caption         =   "Listado"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.ComboBox ComboFpago 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "IngresoInsumos.frx":B29B
         Left            =   8640
         List            =   "IngresoInsumos.frx":B2A5
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   360
         Width           =   1935
      End
      Begin MSAdodcLib.Adodc AdoCompras 
         Height          =   330
         Left            =   1440
         Top             =   4560
         Visible         =   0   'False
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   2
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   3
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "DSN=Euro"
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "Euro"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   "Compras"
         Caption         =   "Compras"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.CommandButton CmdGuardar 
         Caption         =   "&Guardar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   9360
         Picture         =   "IngresoInsumos.frx":B2BD
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   4200
         Width           =   1335
      End
      Begin MSAdodcLib.Adodc AdoInsumos 
         Height          =   330
         Left            =   2880
         Top             =   4680
         Visible         =   0   'False
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   2
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   3
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "DSN=Euro"
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "Euro"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   "LosInsumos"
         Caption         =   "Insumos"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.CommandButton CmdAgrega 
         Caption         =   "&Agrega"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9480
         TabIndex        =   33
         Top             =   1080
         Width           =   1215
      End
      Begin VB.TextBox TxtValor 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   5880
         TabIndex        =   28
         Text            =   "0"
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   3840
         TabIndex        =   26
         Text            =   "0"
         Top             =   1320
         Width           =   855
      End
      Begin VB.ComboBox ComboInsumo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   480
         TabIndex        =   24
         Text            =   "Seleccione Insumo"
         Top             =   1320
         Width           =   3015
      End
      Begin VB.ComboBox ComboDatInsumos 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   0
         TabIndex        =   23
         Text            =   "Seleccione Insumo"
         Top             =   840
         Visible         =   0   'False
         Width           =   390
      End
      Begin VB.ComboBox ComboTipoDoc 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "IngresoInsumos.frx":B6FF
         Left            =   6360
         List            =   "IngresoInsumos.frx":B70F
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   360
         Width           =   1695
      End
      Begin VB.TextBox TxtNdoc 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         TabIndex        =   18
         Top             =   360
         Width           =   1335
      End
      Begin VB.TextBox TxtFecha 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   840
         TabIndex        =   16
         Top             =   360
         Width           =   1215
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHDocumento 
         Height          =   2895
         Left            =   240
         TabIndex        =   14
         Top             =   1800
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   5106
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         HighLight       =   2
         FillStyle       =   1
         ScrollBars      =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   7
      End
      Begin VB.Label Label18 
         Caption         =   "Label18"
         DataField       =   "TipDoc"
         DataSource      =   "AdoListadoCompras"
         Height          =   255
         Left            =   5160
         TabIndex        =   42
         Top             =   4920
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000000&
         BorderWidth     =   2
         X1              =   0
         X2              =   10800
         Y1              =   720
         Y2              =   720
      End
      Begin VB.Label LbTdoc 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7320
         TabIndex        =   36
         Top             =   4800
         Width           =   1815
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "TOTAL  $"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6000
         TabIndex        =   35
         Top             =   4920
         Width           =   1215
      End
      Begin VB.Label Label16 
         Caption         =   "(Con doble click saca item)"
         Height          =   255
         Left            =   480
         TabIndex        =   34
         Top             =   4920
         Width           =   2775
      End
      Begin VB.Label Label15 
         Caption         =   "Cantidad"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   3840
         TabIndex        =   32
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label14 
         Caption         =   "Valor U."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6000
         TabIndex        =   31
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label13 
         Alignment       =   2  'Center
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7560
         TabIndex        =   30
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label LbSTotal 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7560
         TabIndex        =   29
         Top             =   1320
         Width           =   1575
      End
      Begin VB.Label Lbum 
         Caption         =   "UM"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4800
         TabIndex        =   27
         Top             =   1320
         Width           =   375
      End
      Begin VB.Label Label10 
         Caption         =   "Seleccione Insumo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   480
         TabIndex        =   25
         Top             =   1080
         Width           =   2175
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000004&
         BorderWidth     =   3
         X1              =   0
         X2              =   10800
         Y1              =   720
         Y2              =   720
      End
      Begin VB.Label Label9 
         Caption         =   "Tipo Documento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   4800
         TabIndex        =   20
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label Label8 
         Caption         =   "N�mero"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2400
         TabIndex        =   19
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label7 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   855
      End
   End
   Begin MSComctlLib.ImageCombo ImageCombo1 
      Height          =   330
      Left            =   3840
      TabIndex        =   52
      Top             =   7560
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   582
      _Version        =   393216
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Text            =   "ImageCombo1"
      ImageList       =   "ImageList1"
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "REGISTRO DE COMPRAS"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      TabIndex        =   47
      Top             =   1200
      Width           =   5055
   End
   Begin VB.Label Label17 
      Caption         =   "Label17"
      DataField       =   "TipoDocumento"
      DataSource      =   "AdoCompras"
      Height          =   135
      Left            =   5640
      TabIndex        =   39
      Top             =   6960
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label12 
      Caption         =   "Label12"
      DataField       =   "Insumo"
      DataSource      =   "AdoInsumos"
      Height          =   135
      Left            =   5640
      TabIndex        =   15
      Top             =   6720
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "IngresoInsumos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Icod As String * 5, Ium As String * 2, Ifamilia As String * 15, Ipu As String * 7, IStock As String * 7, Iub As Integer, Iitem As String
Dim ComprasMes As ADODB.Recordset

Private Sub cmdClose_Click()
    Main.Enabled = True
    Unload Me
End Sub


Private Sub CboBusqueda_Click()
    Criterio = "RazonSocial = '" & CboBusqueda.Text & "'"
    Me.AdoProveedores.Recordset.Find Criterio, , , 1
End Sub

Private Sub CmdAgrega_Click()
    Iub = ComboInsumo.ListIndex
    Iitem = ComboDatInsumos.List(Iub)
    If Iub = -1 Or Val(TxtCantidad.Text) = 0 Or Val(TxtValor.Text) = 0 Then Exit Sub
    With MSHDocumento
        .Row = .Rows - 1: .Col = 0
        .Text = Val(Mid(Iitem, 1, 5)): .Col = 1
        .Text = ComboInsumo.Text: .Col = 2
        .Text = Val(TxtCantidad.Text): .Col = 3
        .Text = Lbum.Caption: .Col = 4
        .Text = Mid(Iitem, 10, 15): .Col = 5
        .Text = TxtValor.Text: .Col = 6
        .Text = LbSTotal.Caption
        LbTdoc.Caption = "0"
        For i = 1 To .Rows - 1
            .Row = i: .Col = 6
            LbTdoc.Caption = Val(LbTdoc.Caption) + Val(.Text)
        Next
        .Rows = .Rows + 1
    End With
End Sub
Private Sub CmdGuardar_Click()
    Dim tInsu As Integer, Tcod As String
    Dim iBox As Variant
    If Val(TxtNdoc.Text) < 1 Then
        MsgBox "Falta N� Documento ...", vbExclamation + vbOKOnly, "No hay forma de identificar el documento"
        Exit Sub: End If
    If Val(LbTdoc.Caption) = 0 Then Exit Sub
    tInsu = MSHDocumento.Rows - 2
    iBox = InputBox("Nombre de responsable de esta compra", "Responsabilidad")
    If Len(iBox) = 0 Then Exit Sub
    With AdoInsumos.Recordset
        For i = 1 To tInsu  ' Actualiza Insumos
            .MoveFirst
            MSHDocumento.Col = 0: MSHDocumento.Row = i
            Tcod = MSHDocumento.Text
            Criterio = "Codigo = " & Tcod
            .Find Criterio
            If .EOF Then MsgBox "no se encontr�"
            MSHDocumento.Col = 5
            .Fields(4) = Val(MSHDocumento.Text)
            MSHDocumento.Col = 2
            .Fields(7) = .Fields(7) + Val(MSHDocumento.Text)
        Next
    End With
    With AdoCompras.Recordset
        For i = 1 To tInsu
            .AddNew   '' Carga archivo de compras
            .Fields(0) = Val(TxtNdoc.Text)
            .Fields(1) = TxtProveedor.Text
            .Fields(2) = ComboTipoDoc.Text
            .Fields(3) = Right(TxtFecha.Text, 7)
            MSHDocumento.Col = 0: MSHDocumento.Row = i
            .Fields(4) = MSHDocumento.Text
            MSHDocumento.Col = 1: MSHDocumento.Row = i
            .Fields(5) = MSHDocumento.Text
            MSHDocumento.Col = 2: MSHDocumento.Row = i
            .Fields(6) = Val(MSHDocumento.Text)
            MSHDocumento.Col = 3: MSHDocumento.Row = i
            .Fields(7) = MSHDocumento.Text
            MSHDocumento.Col = 4: MSHDocumento.Row = i
            .Fields(8) = MSHDocumento.Text
            MSHDocumento.Col = 5: MSHDocumento.Row = i
            .Fields(9) = Val(MSHDocumento.Text)
            MSHDocumento.Col = 6: MSHDocumento.Row = i
            .Fields(10) = Val(MSHDocumento.Text)
            .Fields(11) = iBox
            .Fields(12) = Mid(TxtFecha.Text, 1, 2)
            .Update
        Next
    End With
    With AdoListadoCompras.Recordset
        .AddNew
        .Fields(0) = TxtNdoc.Text
        .Fields(1) = ComboTipoDoc.Text
        .Fields(2) = TxtProveedor.Text
        .Fields(3) = Mid(TxtFecha.Text, 1, 2)
        .Fields(4) = Right(TxtFecha.Text, 7)
        .Fields(5) = LbTdoc.Caption
        .Update
    End With
    LbTdoc.Caption = "0": MsgBox "Los ingresos han sido grabados y se" & Chr(13) & _
    "han actualizado los archivos de inventarios...", vbInformation
    MSHDocumento.Rows = 1: MSHDocumento.Rows = 2: MSHDocumento.FixedRows = 1
    CreaListados
End Sub
Private Sub CmdRevisaCompras_Click()
    If Frame2.Visible = True Then
        Frame2.Visible = False
        Frame1.Visible = False
        Frame4.Visible = True
        CmdRevisaCompras.Caption = "Volver"
    Else
        Frame2.Visible = True
        Frame1.Visible = True
        Frame4.Visible = False
        CmdRevisaCompras.Caption = "Revisar Compras"
    End If
End Sub
Private Sub ComboInsumo_Click()
    Iub = ComboInsumo.ListIndex
    If Iub > -1 Then
        Iitem = ComboDatInsumos.List(Iub)
        Lbum.Caption = Mid(Iitem, 7, 2)
        TxtValor.Text = Mid(Iitem, 26, 7)
    End If
End Sub
Private Sub Command1_Click()
    Unload Me
    Main.Enabled = True
End Sub
Private Sub Form_Load()
    ImageCombo1.ComboItems.Add 1, "chile", "Chile", 1
    Dim ano As String, l As Integer
    For i = 1 To 20
        ano = 1999 + i
        If Val(ano) = Year(Date) Then l = i
        Combo1.AddItem (ano)
    Next
    Combo1.ListIndex = Val(Right(Str(Year(Date)), 2))
    ComboFpago.ListIndex = 0
    With MSHComprasMensuales
        .Row = 0
        .Col = 0: .Text = "Proveedor"
        .Col = 1: .Text = "D�a"
        .Col = 2: .Text = "Documento"
        .Col = 3: .Text = "N�mero"
        .Col = 4: .Text = "Total"
        .ColWidth(0) = 2500
        .ColWidth(1) = 500
        .ColWidth(2) = 2000
        .ColWidth(3) = 1200
        .ColWidth(4) = 1200
    End With
    TxtFecha.Text = Date
    TxtNdoc.Text = 0
    ComboTipoDoc.ListIndex = 0
    With MSHDocumento
        .ColWidth(1) = 2400
        .ColWidth(3) = 600
        .ColWidth(4) = 1600
        .Row = 0: .Col = 0
        .Text = "Codigo": .Col = 1
        .Text = "Insumo": .Col = 2
        .Text = "Cantidad": .Col = 3
        .Text = "UM": .Col = 4
        .Text = "Familia": .Col = 5
        .Text = "P/u": .Col = 6
        .Text = "Totales"
    End With
    
    Me.AdoProveedores.Recordset.MoveFirst
    
    Do While Not Me.AdoProveedores.Recordset.EOF
        CboBusqueda.AddItem Me.AdoProveedores.Recordset.Fields(1)
        AdoProveedores.Recordset.MoveNext
    Loop
    
    
    'With CboBusqueda
        
    'End With
    
    CreaListados
    
End Sub
Private Sub MSHComprasMensuales_DblClick()
    Dim CompraEliminar As ADODB.Recordset, myDatos As String
    Dim DocEliminar As ADODB.Recordset
    With MSHComprasMensuales
        If .Row = 0 Then Exit Sub
        Dim MyProv As String, MyTdoc As String, MyNumD As String
        .Col = 3: MyNumD = .Text
        .Col = 0: MyProv = .Text
        .Col = 2: MyTdoc = .Text
    End With
    paso = MsgBox("Est� a punto de iliminar la compra con" & Chr(13) & _
                MyTdoc & " de " & MyProv & " N� " & MyNumD, vbQuestion + vbOKCancel, "Confirme eliminaci�n")
    If paso = 1 Then
        myDatos = "NumDoc = " & MyNumD '
        Set CompraEliminar = FiltraMes(ComprasMes, myDatos)
        For i = 1 To CompraEliminar.RecordCount
            If CompraEliminar.Fields(1) = MyTdoc And CompraEliminar.Fields(2) = MyProv Then
                CompraEliminar.Delete
            End If
        Next
    End If
    myDatos = "FechaMesAno = " & Right(TabCompras.SelectedItem, 2) & "-" & Combo1.Text
    Set DocEliminar = FiltraMes(AdoCompras.Recordset, myDatos)
    myDatos = "NDoc = " & MyNumD
    Set CompraEliminar = FiltraMes(DocEliminar, myDatos)
    CompraEliminar.MoveFirst
    For i = 1 To CompraEliminar.RecordCount
        If CompraEliminar.Fields(2) = MyTdoc And CompraEliminar.Fields(1) = MyProv Then
            CompraEliminar.Delete
        End If
        CompraEliminar.MoveNext
    Next
    TabCompras_Click
End Sub

Private Sub MSHDocumento_DblClick()
    Dim LargoMsh As Integer
    Dim IdenSaca As Integer
    With MSHDocumento
        IdenSaca = .Row
        If IdenSaca = 0 Or IdenSaca = .Rows - 1 Then Exit Sub
        MSHPaso.Rows = 1
        LargoMsh = .Rows - 2
        Contador = 0
        For i = 1 To LargoMsh
            If IdenSaca <> i Then
                .Row = i: .Col = 0
                MSHPaso.Row = Contador: MSHPaso.Col = 0
                MSHPaso.Text = .Text
                .Row = i: .Col = 1
                MSHPaso.Row = Contador: MSHPaso.Col = 1
                MSHPaso.Text = .Text
                .Row = i: .Col = 2
                MSHPaso.Row = Contador: MSHPaso.Col = 2
                MSHPaso.Text = .Text
                .Row = i: .Col = 3
                MSHPaso.Row = Contador: MSHPaso.Col = 3
                MSHPaso.Text = .Text
                .Row = i: .Col = 4
                MSHPaso.Row = Contador: MSHPaso.Col = 4
                MSHPaso.Text = .Text
                .Row = i: .Col = 5
                MSHPaso.Row = Contador: MSHPaso.Col = 5
                MSHPaso.Text = .Text
                .Row = i: .Col = 6
                MSHPaso.Row = Contador: MSHPaso.Col = 6
                MSHPaso.Text = .Text
                Contador = Contador + 1
                MSHPaso.Rows = MSHPaso.Rows + 1
            End If
        Next
        .Rows = 1: .Rows = 2: .FixedRows = 1
        For i = 1 To LargoMsh - 1
            .Row = i: .Col = 0
            MSHPaso.Row = i - 1: MSHPaso.Col = 0
            .Text = MSHPaso.Text
            .Row = i: .Col = 1
            MSHPaso.Row = i - 1: MSHPaso.Col = 1
            .Text = MSHPaso.Text
            .Row = i: .Col = 2
            MSHPaso.Row = i - 1: MSHPaso.Col = 2
            .Text = MSHPaso.Text
            .Row = i: .Col = 3
            MSHPaso.Row = i - 1: MSHPaso.Col = 3
            .Text = MSHPaso.Text
            .Row = i: .Col = 4
            MSHPaso.Row = i - 1: MSHPaso.Col = 4
            .Text = MSHPaso.Text
            .Row = i: .Col = 5
            MSHPaso.Row = i - 1: MSHPaso.Col = 5
            .Text = MSHPaso.Text
            .Row = i: .Col = 6
            MSHPaso.Row = i - 1: MSHPaso.Col = 6
            .Text = MSHPaso.Text
            .Rows = .Rows + 1
        Next
        LbTdoc.Caption = "0"
        For i = 1 To .Rows - 1
            .Row = i: .Col = 6
            LbTdoc.Caption = Val(LbTdoc.Caption) + Val(.Text)
        Next
    End With
End Sub

Private Sub TabCompras_Click()
    Dim MesAno As String
    MesAno = "AnoMesDoc = " & Right(TabCompras.SelectedItem, 2) & "-" & Combo1.Text
    Set ComprasMes = FiltraMes(AdoListadoCompras.Recordset, MesAno)
    MSHComprasMensuales.Rows = 2: MSHComprasMensuales.Row = 1: LbTMes.Caption = "0"
    For i = 0 To MSHComprasMensuales.Cols - 1: MSHComprasMensuales.Col = i: MSHComprasMensuales.Text = " ": Next

    If ComprasMes.RecordCount > 0 Then
        With MSHComprasMensuales
            For i = 1 To ComprasMes.RecordCount
                .Row = i: .Col = 0: .Text = ComprasMes.Fields(2)
                .Col = 1: .Text = ComprasMes.Fields(3)
                .Col = 2: .Text = ComprasMes.Fields(1)
                .Col = 3: .Text = ComprasMes.Fields(0)
                .Col = 4: .Text = ComprasMes.Fields(5)
                LbTMes.Caption = Val(LbTMes.Caption) + Val(ComprasMes.Fields(5))
                ComprasMes.MoveNext
                .Rows = .Rows + 1
            Next
            .Rows = .Rows - 1
            .Col = 0
            .ColSel = .Cols - 1
            .Sort = flexSortGenericAscending
            .MergeCol(0) = True
        End With
    End If
End Sub

Private Sub TxtCantidad_Change()
    LbSTotal.Caption = Val(TxtCantidad.Text) * Val(TxtValor.Text)
End Sub
Private Sub TxtValor_Change()
    LbSTotal.Caption = Val(TxtCantidad.Text) * Val(TxtValor.Text)
End Sub
Public Sub CreaListados()
    ComboInsumo.Clear
    ComboDatInsumos.Clear
    With AdoInsumos.Recordset
        .MoveFirst
        For i = 1 To .RecordCount
            ComboInsumo.AddItem (.Fields(1))
            Icod = .Fields(0)
            Ium = .Fields(2)
            Ifamilia = .Fields(3)
            Ipu = .Fields(4)
            IStock = .Fields(7)
            ComboDatInsumos.AddItem (Icod & "|" & Ium & "|" & Ifamilia & "|" & Ipu & "|" & IStock)
            .MoveNext
        Next
    End With
End Sub
Public Function FiltraMes(rstTemp As ADODB.Recordset, _
   MAno As String) As ADODB.Recordset
   rstTemp.Filter = MAno: Set FiltraMes = rstTemp
End Function
Public Function FiltraCompraDel(rstTempDel As ADODB.Recordset, _
   MDel As String) As ADODB.Recordset
   rstTempDel.Filter = MDel
   Set FiltraCompraDel = rstTempDel
End Function
