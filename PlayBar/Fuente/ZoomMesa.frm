VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form ZoomMesa 
   Caption         =   "Vista Ampliada"
   ClientHeight    =   8760
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   14160
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8760
   ScaleWidth      =   14160
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "Cerrar Vista Ampliada"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   11325
      TabIndex        =   11
      Top             =   7695
      Width           =   2715
   End
   Begin VB.CommandButton cmdImprimeComanda 
      Caption         =   "IMPRIME COMANDA(S)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   6030
      TabIndex        =   10
      Top             =   7710
      Width           =   5235
   End
   Begin VB.Timer Timer 
      Interval        =   25
      Left            =   60
      Top             =   7395
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   15
      OleObjectBlob   =   "ZoomMesa.frx":0000
      Top             =   7020
   End
   Begin VB.Frame Frame2 
      Caption         =   "Productos"
      Height          =   6000
      Left            =   240
      TabIndex        =   1
      Top             =   1545
      Width           =   13830
      Begin VB.CommandButton cmdMenAlt 
         Caption         =   "MENSAJES Y ALTERNATIVAS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   255
         TabIndex        =   9
         Top             =   5235
         Width           =   3420
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MshProductos 
         Height          =   4740
         Left            =   225
         TabIndex        =   2
         Top             =   375
         Width           =   13560
         _ExtentX        =   23918
         _ExtentY        =   8361
         _Version        =   393216
         Rows            =   1
         Cols            =   9
         FixedRows       =   0
         RowHeightMin    =   400
         FocusRect       =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   9
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Informacion Mesa"
      Height          =   975
      Left            =   270
      TabIndex        =   0
      Top             =   420
      Width           =   13815
      Begin VB.TextBox LBTOTAL 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   9705
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   375
         Width           =   3525
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   330
         Index           =   2
         Left            =   8355
         OleObjectBlob   =   "ZoomMesa.frx":0234
         TabIndex        =   7
         Top             =   450
         Width           =   1305
      End
      Begin VB.TextBox LBGARZON 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   3765
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   405
         Width           =   4125
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   330
         Index           =   1
         Left            =   2640
         OleObjectBlob   =   "ZoomMesa.frx":029F
         TabIndex        =   5
         Top             =   450
         Width           =   1080
      End
      Begin VB.TextBox lbmesa 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   945
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   405
         Width           =   1560
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   330
         Index           =   0
         Left            =   180
         OleObjectBlob   =   "ZoomMesa.frx":0302
         TabIndex        =   3
         Top             =   450
         Width           =   675
      End
   End
End
Attribute VB_Name = "ZoomMesa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdImprimeComanda_Click()
    Me.Hide
    Call Main.EnviaCproduccion
    Unload Me
End Sub

Private Sub cmdMenAlt_Click()
  '  MsgBox Me.MshProductos.TextMatrix(MshProductos.Row, 6)
    If MshProductos.TextMatrix(MshProductos.Row, 8) = "SI" Or _
       Val(MshProductos.TextMatrix(MshProductos.Row, 1)) = 0 Then Exit Sub
    
    S_Men_Producto = Me.MshProductos.TextMatrix(MshProductos.Row, 2) & " - " & Me.MshProductos.TextMatrix(MshProductos.Row, 3)
    I_Men_Rubro = Val(MshProductos.TextMatrix(MshProductos.Row, 7))
    S_Hora_Unica = MshProductos.TextMatrix(MshProductos.Row, 6)
    ped_mensaje_alternativa.Show 1
    CargaZoom
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub



Private Sub Form_Load()
   CargaZoom
End Sub
Private Sub CargaZoom()
    Dim Sql2 As String, RstTmp2 As Recordset
    MshProductos.ClearStructure
    MshProductos.FixedRows = 0
    MshProductos.Rows = 1
    mesa = Val(Trim(Main.TxtMesaSel))
    StrMesa = PathMesas & "mesa" & mesa & ".ped"
    Y = FreeFile
    LargoR = Len(rPedido)
    ZoomMesa.MshProductos.TextMatrix(0, 0) = "Cod."
    ZoomMesa.MshProductos.TextMatrix(0, 1) = "Cant."
    ZoomMesa.MshProductos.TextMatrix(0, 2) = "Linea"
    ZoomMesa.MshProductos.TextMatrix(0, 3) = "Producto"
    ZoomMesa.MshProductos.TextMatrix(0, 4) = "P.Uni."
    ZoomMesa.MshProductos.TextMatrix(0, 5) = "Total"
    ZoomMesa.MshProductos.TextMatrix(0, 6) = "Hora"
    ZoomMesa.MshProductos.TextMatrix(0, 7) = "rubro"
    ZoomMesa.MshProductos.TextMatrix(0, 8) = "Enviado"
    ZoomMesa.lbmesa = mesa
    ZoomMesa.LBGARZON = Main.lbgarcia
    ZoomMesa.LBTOTAL = Format(Main.LBTOTAL, "$##,###")
    Open StrMesa For Random As Y Len = LargoR
    For act = 1 To LOF(Y) / LargoR
        Get #Y, , rPedido
        ProductoCodigo = Trim(rPedido.Codigo)
        Sql = "SELECT p.Descripcion,r.rub_nombre,rubro FROM productos p,par_rubros_venta r " & _
              "WHERE p.cod=" & rPedido.Codigo & " AND p.rubro=r.rub_id"
        Call Consulta(Rst_tmp, Sql)
        If Rst_tmp.RecordCount > 0 Then
            With ZoomMesa.MshProductos
                .AddItem rPedido.Codigo & _
                vbTab & rPedido.Cantidad & _
                vbTab & Rst_tmp!rub_nombre & _
                vbTab & Rst_tmp!descripcion & _
                vbTab & Format(Val(rPedido.PrecioU), "##,###") & _
                vbTab & Format(Val(rPedido.SubTotal), "##,###") & _
                vbTab & Mid(rPedido.Hora, 1, 9) & _
                vbTab & Rst_tmp!Rubro & _
                vbTab & IIf(Mid(rPedido.Detalle, 20, 1) = "*", "SI", "NO")
                
                
                'AQUI AGREGAMOS LOS MENSAJES
                Sql2 = "SELECT m.men_detalle " & _
                        "FROM tmp_mensajes t, ped_mensajes m " & _
                        "WHERE tmp_hora_unica='" & Mid(rPedido.Hora, 1, 9) & "' AND tmp_mesa=" & Val(Main.TxtMesaSel) & " AND " & _
                        "t.men_id=m.men_id"
                Call Consulta(RstTmp2, Sql2)
                If RstTmp2.RecordCount > 0 Then
                    RstTmp2.MoveFirst
                   ' .AddItem "" & vbTab & "" & vbTab & "Mensajes"
                    Do While Not RstTmp2.EOF
                        .AddItem "" & vbTab & "" & vbTab & "" & vbTab & ">" & LCase(RstTmp2!men_detalle)
                        RstTmp2.MoveNext
                    Loop
                End If
                
                'AQUI AGREGAMOS LAS ALTERNATIVAS
                Sql2 = "SELECT a.pro_codigo,p.descripcion " & _
                       "FROM alternativas_mov m,productos_alternativas_mov a,productos p " & _
                       "WHERE m.id_unico='" & Mid(rPedido.Hora, 1, 9) & "' AND m.pro_alt_id=a.pro_alt_id AND a.pro_codigo=p.cod "
                Call Consulta(RstTmp2, Sql2)
                If RstTmp2.RecordCount > 0 Then
                    RstTmp2.MoveFirst
                   ' .AddItem "" & vbTab & "" & vbTab & "Mensajes"
                    Do While Not RstTmp2.EOF
                        .AddItem "" & vbTab & "" & vbTab & "" & vbTab & "con->" & RstTmp2!descripcion
                        RstTmp2.MoveNext
                    Loop
                End If

                
                
            End With
        End If
        
    Next
    Close #Y ' Datos extraidos
    ZoomMesa.MshProductos.ColWidth(0) = 600
    ZoomMesa.MshProductos.ColWidth(1) = 600
    ZoomMesa.MshProductos.ColWidth(2) = 2500
    ZoomMesa.MshProductos.ColWidth(3) = 5000
    ZoomMesa.MshProductos.ColWidth(4) = 1200
    ZoomMesa.MshProductos.ColWidth(5) = 1000 'Rubro
    ZoomMesa.MshProductos.ColWidth(6) = 1200 '000'hora
    ZoomMesa.MshProductos.ColWidth(7) = 10 '200 '000'hora
'    ZoomMesa.MshProductos.FixedRows = 1
    ZoomMesa.MshProductos.ColAlignment(4) = 7
    ZoomMesa.MshProductos.ColAlignment(5) = 7
    ZoomMesa.MshProductos.ColAlignment(8) = 4
    
End Sub
Private Sub Timer_Timer()
    Aplicar_skin Me
    Timer.Enabled = False
End Sub
