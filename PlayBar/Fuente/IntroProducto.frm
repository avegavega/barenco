VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form IntroProducto 
   BackColor       =   &H0080C0FF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Maestro de Productos"
   ClientHeight    =   6705
   ClientLeft      =   825
   ClientTop       =   1320
   ClientWidth     =   14715
   Icon            =   "IntroProducto.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6705
   ScaleWidth      =   14715
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CmboPrint1er 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   2520
      Style           =   2  'Dropdown List
      TabIndex        =   26
      Top             =   3960
      Width           =   2895
   End
   Begin MSDataListLib.DataList DataList1 
      Bindings        =   "IntroProducto.frx":030A
      Height          =   4905
      Left            =   10080
      TabIndex        =   24
      Top             =   1320
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   8652
      _Version        =   393216
      ListField       =   "Descripcion"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton Final 
      Caption         =   ">>"
      Height          =   252
      Left            =   3600
      TabIndex        =   23
      Top             =   1800
      Width           =   372
   End
   Begin VB.CommandButton Siguiente 
      Caption         =   ">"
      Height          =   252
      Left            =   3240
      TabIndex        =   22
      Top             =   1800
      Width           =   372
   End
   Begin VB.CommandButton Anterior 
      Caption         =   "<"
      Height          =   252
      Left            =   2880
      TabIndex        =   21
      Top             =   1800
      Width           =   372
   End
   Begin VB.CommandButton Inicio 
      Caption         =   "<<"
      Height          =   252
      Left            =   2520
      TabIndex        =   20
      Top             =   1800
      Width           =   372
   End
   Begin VB.ComboBox CmboRubro 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      ItemData        =   "IntroProducto.frx":0325
      Left            =   2520
      List            =   "IntroProducto.frx":0327
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   3360
      Width           =   3012
   End
   Begin VB.ComboBox CmboRmain 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      ItemData        =   "IntroProducto.frx":0329
      Left            =   2520
      List            =   "IntroProducto.frx":032B
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   5160
      Width           =   2532
   End
   Begin VB.ComboBox CmboPrint 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      ItemData        =   "IntroProducto.frx":032D
      Left            =   2520
      List            =   "IntroProducto.frx":032F
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   4560
      Width           =   2895
   End
   Begin MSAdodcLib.Adodc AdoRubritos 
      Height          =   315
      Left            =   1680
      Top             =   6120
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "Rubros"
      Caption         =   "AdoRubritos"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox TxtPventa 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   2520
      TabIndex        =   8
      Top             =   2760
      Width           =   1812
   End
   Begin VB.TextBox TxtDetalle 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   2520
      MaxLength       =   50
      TabIndex        =   7
      Top             =   2160
      Width           =   5415
   End
   Begin MSAdodcLib.Adodc AdoProductos 
      Height          =   330
      Left            =   2880
      Top             =   240
      Visible         =   0   'False
      Width           =   2400
      _ExtentX        =   4233
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   1
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "select Cod,Descripcion,PrecioVenta,Rubro,Dirigida,BebidaComestible, Dirigida1er from Productos Order by Cod"
      Caption         =   "Productos"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton Buscar 
      Caption         =   "Buscar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   8400
      TabIndex        =   6
      Top             =   5640
      Width           =   1332
   End
   Begin VB.CommandButton Refrescar 
      Caption         =   "Refrescar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   8400
      TabIndex        =   5
      Top             =   5040
      Width           =   1332
   End
   Begin VB.CommandButton Cancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   348
      Left            =   8400
      TabIndex        =   4
      Top             =   3840
      Width           =   1332
   End
   Begin VB.CommandButton Borrar 
      Caption         =   "Borrar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   348
      Left            =   8400
      TabIndex        =   3
      Top             =   3240
      Width           =   1332
   End
   Begin VB.CommandButton Grabar 
      Caption         =   "Grabar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   348
      Left            =   8400
      TabIndex        =   2
      Top             =   2640
      Width           =   1332
   End
   Begin VB.CommandButton Editar 
      Caption         =   "Editar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   348
      Left            =   8400
      TabIndex        =   1
      Top             =   2040
      Width           =   1332
   End
   Begin VB.CommandButton Nuevo 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Nuevo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   348
      Left            =   8400
      TabIndex        =   0
      Top             =   1440
      Width           =   1332
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Impresora Nivel 1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   6
      Left            =   240
      TabIndex        =   25
      Top             =   3960
      Width           =   2175
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      DataField       =   "NombreRubro"
      DataSource      =   "AdoRubritos"
      Height          =   255
      Left            =   4320
      TabIndex        =   19
      Top             =   6120
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label LBcodigo 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      DataField       =   "Cod"
      DataSource      =   "AdoProductos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2520
      TabIndex        =   18
      Top             =   1320
      Width           =   975
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Rubro Principal"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   5
      Left            =   240
      TabIndex        =   17
      Top             =   5160
      Width           =   2175
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Impresora Nivel 2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   4
      Left            =   240
      TabIndex        =   16
      Top             =   4560
      Width           =   2175
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Rubro"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   600
      TabIndex        =   15
      Top             =   3360
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Precio Venta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   600
      TabIndex        =   14
      Top             =   2760
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Descripci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   600
      TabIndex        =   13
      Top             =   2160
      Width           =   1815
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   840
      TabIndex        =   12
      Top             =   1320
      Width           =   1815
   End
End
Attribute VB_Name = "IntroProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim CodFree As Double
Dim RubroStr(30) As String
Dim RubroMain(3) As String
Dim manda(4) As String
Option Explicit

Private Sub InhabilitarBotones()
  Dim n As Integer
  For n = 0 To Controls.Count - 1
    If TypeOf Controls(n) Is CommandButton Then
      Controls(n).Enabled = False
    End If
  Next n
  AdoProductos.Enabled = False
End Sub

Private Sub HabilitarBotones()
  Dim n As Integer
  For n = 0 To Controls.Count - 1
    If TypeOf Controls(n) Is CommandButton Then
      Controls(n).Enabled = True
    End If
  Next n
  AdoProductos.Enabled = True
End Sub

Private Sub InhabilitarCajas()
  Dim n As Integer
  For n = 0 To Controls.Count - 1
    If TypeOf Controls(n) Is TextBox Or TypeOf Controls(n) Is ComboBox Then
      Controls(n).Enabled = False
    End If
  Next n
  AdoProductos.Enabled = True
End Sub

Private Sub HabilitarCajas()
  Dim n As Integer
  For n = 0 To Controls.Count - 1
    If TypeOf Controls(n) Is TextBox Or TypeOf Controls(n) Is ComboBox Then
      Controls(n).Enabled = True
    End If
  Next n
  AdoProductos.Enabled = False
End Sub


Private Sub DataList1_DblClick()
    Dim irAa As Long
    irAa = DataList1.SelectedItem - 1
    AdoProductos.Recordset.Move irAa, adBookmarkFirst
    LlenaControles
End Sub
Private Sub Form_Load()
    Dim ru As Integer
    Dim cl As Double
    Grabar.Enabled = False
    InhabilitarCajas
    ChDir App.Path
    For ru = 0 To AdoRubritos.Recordset.RecordCount - 1
        CmboRubro.AddItem (AdoRubritos.Recordset.Fields(1))
        RubroStr(ru + 1) = AdoRubritos.Recordset.Fields(1)
        AdoRubritos.Recordset.MoveNext
    Next
    RubroMain(1) = "Alimentos"
    RubroMain(2) = "Bebestibles"
    RubroMain(3) = "Cafes"
    
    CmboRmain.AddItem RubroMain(1)
    CmboRmain.AddItem RubroMain(2)
    CmboRmain.AddItem RubroMain(3)
    
   
    CmboRubro.ListIndex = 0
    
   
    CmboRmain.ListIndex = 0
    
    Sql = "SELECT id,nombre_centro FROM centros_produccion ORDER BY id"
    Call Consulta(Rst_tmp, Sql)
    With Rst_tmp
        If .RecordCount > 0 Then
            .MoveFirst 'Llenamos combo centros
            Do While Not .EOF
                CmboPrint.AddItem !nombre_centro
                CmboPrint.ItemData(CmboPrint.NewIndex) = !id
                .MoveNext
            Loop
            .MoveFirst 'Llenamos combo centros
            Do While Not .EOF
                CmboPrint1er.AddItem !nombre_centro
                CmboPrint1er.ItemData(CmboPrint1er.NewIndex) = !id
                .MoveNext
            Loop
            CmboPrint1er.ListIndex = 0
            CmboPrint.ListIndex = 0
        End If
    End With
        
    BuscaCodigoFree
    LlenaControles
 End Sub

Private Sub Form_Unload(Cancel As Integer)
  '  FrmFondo.Enabled = True
    Unload ListaGarzones
    Unload Configuracion
    Unload Pago
    Unload FormasDePago
    Unload AccionMesa
    Unload DImpresoras
    Main.Visible = True
    Main.WindowState = 2
    ElMainActivo = True
End Sub

Private Sub Inicio_Click()
  AdoProductos.Recordset.MoveFirst
  LlenaControles
End Sub

Private Sub Anterior_Click()
  AdoProductos.Recordset.MovePrevious
  If AdoProductos.Recordset.BOF Then
    AdoProductos.Recordset.MoveFirst
  End If
  LlenaControles
End Sub




Private Sub Siguiente_Click()
  AdoProductos.Recordset.MoveNext
  If AdoProductos.Recordset.EOF Then
    AdoProductos.Recordset.MoveLast
  End If
  LlenaControles
End Sub
Private Sub Final_Click()
  AdoProductos.Recordset.MoveLast
  LlenaControles
End Sub

Private Sub Nuevo_Click()
  HabilitarCajas
  InhabilitarBotones
  Grabar.Enabled = True
  Cancelar.Enabled = True
  BuscaCodigoFree
  AdoProductos.Recordset.AddNew 'a�adir un nuevo registro
  LbCodigo.Caption = CodFree
  LimpiaControles
  TxtDetalle.SetFocus         'poner el cursor en la caja del "Nombre"
End Sub

Private Sub Editar_Click()
  HabilitarCajas
  InhabilitarBotones
  Grabar.Enabled = True
  Cancelar.Enabled = True
  TxtDetalle.SetFocus       'poner el cursor en la caja del "Nombre"
End Sub

Private Sub Grabar_Click()
    With AdoProductos.Recordset
        .Fields(1) = TxtDetalle.Text
        .Fields(2) = TxtPventa.Text
        .Fields(3) = CmboRubro.ListIndex + 1
        .Fields(4) = CmboPrint.ItemData(CmboPrint.ListIndex)
        .Fields(6) = CmboPrint1er.ItemData(CmboPrint1er.ListIndex)
        .Fields(5) = CmboRmain.ListIndex + 1
        .Update
        .Requery
    End With
    HabilitarBotones
    Grabar.Enabled = False
    InhabilitarCajas
    LlenaControles
End Sub

Private Sub Cancelar_Click()
  AdoProductos.Recordset.CancelUpdate
  HabilitarBotones
  Grabar.Enabled = False
  InhabilitarCajas
  LlenaControles
End Sub

Private Sub Borrar_Click()
  Dim r As Integer
  On Error GoTo RutinaDeError
  r = MsgBox("�Desea borrar el registro?", vbYesNo, "Atenci�n")
  If r <> vbYes Then Exit Sub
  AdoProductos.Recordset.Delete   'borrar el registro actual
  AdoProductos.Recordset.MoveNext 'situarse en el registro siguiente
  If AdoProductos.Recordset.EOF Then
    AdoProductos.Recordset.MoveLast
  End If
  LlenaControles
  BuscaCodigoFree
  Exit Sub
RutinaDeError:
  r = MsgBox(Error, vbOKOnly, "Se ha producido un error:")
  AdoProductos.Recordset.CancelUpdate
  AdoProductos.Recordset.Requery
  LlenaControles
End Sub

Private Sub Refrescar_Click()
  AdoProductos.Recordset.Requery
  HabilitarBotones
  Grabar.Enabled = False
  LlenaControles
End Sub

Private Sub Buscar_Click()
  Dim Buscado As String, Criterio As String
  Buscado = InputBox("Escriba el Producto a Buscar")
  If Buscado = "" Then Exit Sub
  Criterio = "Descripcion Like '*" & Buscado & "*'"
  ' Buscar desde el siguiente registro a la posici�n actual
  AdoProductos.Recordset.MoveNext
  If Not AdoProductos.Recordset.EOF Then
    AdoProductos.Recordset.Find Criterio
  End If
  If AdoProductos.Recordset.EOF Then
    AdoProductos.Recordset.MoveFirst
    ' Buscar desde el principio
    AdoProductos.Recordset.Find Criterio
    If AdoProductos.Recordset.EOF Then
      AdoProductos.Recordset.MoveLast
      MsgBox ("Producto no encontrado")
    End If
  End If
  LlenaControles
End Sub


Public Sub BuscaCodigoFree()
    Dim cl As Double
    For cl = 1 To 9999
        CodigosLibres(cl) = False
    Next
    With AdoProductos.Recordset
        .MoveFirst
        For cl = 1 To 9999
            If .Fields(0) = cl Then
                CodigosLibres(cl) = True
            Else
                CodigosLibres(cl) = False
            End If
            .MoveNext
            If .EOF Then Exit For
        Next
        .MoveFirst
    End With
    For cl = 1 To 9999
        If CodigosLibres(cl) = False Then
            CodFree = cl
            Exit For
        End If
    Next
End Sub

Public Sub LlenaControles()
    Dim nl As Double
    With AdoProductos.Recordset
        TxtDetalle.Text = .Fields(1)
        TxtPventa.Text = .Fields(2)
        nl = .Fields(3) - 1
        CmboRubro.ListIndex = nl
        CmboPrint.ListIndex = BuscaIndice(CmboPrint, CmboPrint.ItemData(.Fields("dirigida"))) - 1
        CmboPrint1er.ListIndex = BuscaIndice(CmboPrint, CmboPrint.ItemData(.Fields("dirigida1er"))) - 1
        CmboRmain.ListIndex = .Fields(5) - 1
    End With
     
End Sub

Public Sub LimpiaControles()
    TxtDetalle.Text = ""
    TxtPventa.Text = ""
    CmboRubro.ListIndex = 0
    CmboPrint.ListIndex = 0
    CmboPrint1er.ListIndex = 0
    CmboRmain.ListIndex = 0
End Sub

Private Sub TxtDetalle_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub TxtPventa_KeyPress(KeyAscii As Integer)
    If IsNumeric(Chr(KeyAscii)) Or KeyAscii = 12 Or KeyAscii = 32 Then
        'siga
    Else
        KeyAscii = 0
    End If
End Sub
