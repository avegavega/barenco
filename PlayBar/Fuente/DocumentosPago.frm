VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form DocumentosPago 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Documentos de Pago"
   ClientHeight    =   6450
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10770
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6450
   ScaleWidth      =   10770
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdSelecionar 
      Caption         =   "Selecciona ->"
      Height          =   375
      Left            =   600
      TabIndex        =   9
      Top             =   4560
      Width           =   1575
   End
   Begin VB.CommandButton CmdCerra 
      Caption         =   "&Cerrar"
      DownPicture     =   "DocumentosPago.frx":0000
      Height          =   780
      Left            =   10080
      Picture         =   "DocumentosPago.frx":04CD
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   5160
      Width           =   555
   End
   Begin VB.FileListBox File1 
      Height          =   1260
      Left            =   3480
      TabIndex        =   6
      Top             =   5280
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Frame Frame2 
      Caption         =   "Formas de pago asosciadas a "
      Height          =   3855
      Left            =   3480
      TabIndex        =   3
      Top             =   960
      Width           =   3615
      Begin VB.CommandButton CmdGrabalistaAsosciados 
         BackColor       =   &H00004040&
         Height          =   495
         Left            =   2280
         Picture         =   "DocumentosPago.frx":099D
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Graba minuta"
         Top             =   3240
         Width           =   1095
      End
      Begin VB.CommandButton CmdSaca 
         Caption         =   "x      Quitar"
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   3360
         Width           =   1215
      End
      Begin VB.ListBox ListaDasociados 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2400
         Left            =   360
         TabIndex        =   8
         Top             =   600
         Width           =   2895
      End
      Begin VB.Label lbdocumento 
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   240
         Width           =   2175
      End
   End
   Begin MSAdodcLib.Adodc adoFpagos 
      Height          =   330
      Left            =   7680
      Top             =   5160
      Visible         =   0   'False
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "FormasDePago"
      Caption         =   "Formas pago"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      Caption         =   "Formas de Pago Disponibles"
      Height          =   4455
      Left            =   7320
      TabIndex        =   1
      Top             =   600
      Width           =   3255
      Begin VB.CommandButton CmdAddFpago 
         Caption         =   "<- Agregar Forma de pago"
         Height          =   375
         Left            =   600
         TabIndex        =   5
         Top             =   360
         Width           =   2175
      End
      Begin VB.ListBox ListaFormasDpago 
         Height          =   3180
         ItemData        =   "DocumentosPago.frx":0DDF
         Left            =   120
         List            =   "DocumentosPago.frx":0DE6
         TabIndex        =   4
         Top             =   960
         Width           =   2775
      End
   End
   Begin VB.ListBox listaDocPago 
      DataField       =   "FormaDePago"
      DataSource      =   "adoFpagos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3180
      Left            =   360
      TabIndex        =   0
      Top             =   1320
      Width           =   2895
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      DataField       =   "FormaDePago"
      DataSource      =   "adoFpagos"
      Height          =   255
      Left            =   7080
      TabIndex        =   11
      Top             =   5400
      Width           =   615
   End
   Begin VB.Line Line2 
      BorderColor     =   &H8000000E&
      X1              =   3240
      X2              =   3480
      Y1              =   2660
      Y2              =   2660
   End
   Begin VB.Line Line1 
      BorderColor     =   &H8000000B&
      X1              =   3240
      X2              =   3480
      Y1              =   2640
      Y2              =   2640
   End
   Begin VB.Label Label2 
      Caption         =   "Seleccione Documento de pago con doble click para ver opciones de pago"
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Top             =   720
      Width           =   2895
   End
End
Attribute VB_Name = "DocumentosPago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdAddFpago_Click()
    ListaFormasDpago_DblClick
End Sub

Private Sub CmdCerra_Click()
    Unload Me
End Sub


Private Sub CmdGrabalistaAsosciados_Click()
    If Me.lbdocumento.Caption <> "" And ListaDasociados.ListCount > 0 Then
        X = FreeFile
        Open LaRutadeArchivos & lbdocumento.Caption For Output As X
            For i = 0 To ListaDasociados.ListCount - 1
                Print #X, ListaDasociados.List(i)
            Next
        Close #X
        MsgBox "Se ha crado un archivo asociado al documento:" & vbCr & lbdocumento.Caption
        ListaDasociados.Clear
        lbdocumento.Caption = ""
    End If
End Sub

Private Sub CmdSaca_Click()
    If ListaDasociados.ListIndex > -1 Then ListaDasociados.RemoveItem (ListaDasociados.ListIndex)
End Sub

Private Sub CmdSelecionar_Click()
    listaDocPago_DblClick
End Sub

Private Sub Form_Load()
    Dim X As Integer, s As String
    X = FreeFile
    Open LaRutadeArchivos & "pagodoc.opc" For Input As X
        Do While Not EOF(X)
            Line Input #X, s
            listaDocPago.AddItem (s)
        Loop
    Close #X
    File1.Path = LaRutadeArchivos
    With Me.AdoFpagos.Recordset
        ListaFormasDpago.Clear
        .MoveFirst
        Do While Not .EOF()
            ListaFormasDpago.AddItem (.Fields(0))
            .MoveNext
        Loop
    End With
    File1.Path = LaRutadeArchivos
End Sub




Private Sub ListaDasociados_DblClick()
    If ListaDasociados.ListIndex > -1 Then ListaDasociados.RemoveItem (ListaDasociados.ListIndex)
End Sub

Private Sub listaDocPago_DblClick()
    Dim Existe As Boolean
    Dim s As String
    X = FreeFile
    Existe = False
    lbdocumento.Caption = listaDocPago.List(listaDocPago.ListIndex)
    ListaDasociados.Clear
    File1.Path = "c:\cafeuro"
    For i = 0 To File1.ListCount
        If File1.List(i) = Me.lbdocumento.Caption Then
        
            Open File1.Path & "\" & File1.List(i) For Input As X
                Do While Not EOF(X)
                    Line Input #X, s
                    ListaDasociados.AddItem (s)
                Loop
            Close X
                'MsgBox "si existe"
            Existe = True
            Exit For
        End If
    Next
    If Not Existe Then
        'MsgBox "no existe"
    End If
End Sub

Private Sub ListaFormasDpago_DblClick()
    If Len(Trim(lbdocumento.Caption)) = 0 Then Exit Sub
    If ListaFormasDpago.ListIndex > -1 Then
        For i = 0 To ListaDasociados.ListCount - 1
            If ListaFormasDpago.List(ListaFormasDpago.ListIndex) = ListaDasociados.List(i) Then
                MsgBox "Este documento ya contiene esta forma de pago", vbExclamation + vbOKOnly
                Exit Sub
            End If
        Next
        ListaDasociados.AddItem (ListaFormasDpago.List(ListaFormasDpago.ListIndex))
    End If
End Sub
