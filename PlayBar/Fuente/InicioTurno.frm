VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form InicioTurno 
   BackColor       =   &H0080C0FF&
   Caption         =   "Turno"
   ClientHeight    =   7200
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   11040
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   Moveable        =   0   'False
   ScaleHeight     =   7200
   ScaleWidth      =   11040
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin MSComCtl2.DTPicker Calendar1 
      Height          =   495
      Left            =   5760
      TabIndex        =   15
      Top             =   2040
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   873
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   100073473
      CurrentDate     =   40291
   End
   Begin MSAdodcLib.Adodc AdoInformes 
      Height          =   330
      Left            =   6240
      Top             =   7200
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "InfoVentas"
      Caption         =   "InfoVentas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc AdoTurnos 
      Height          =   330
      Left            =   7560
      Top             =   600
      Visible         =   0   'False
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Turnos"
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox Text1 
      DataField       =   "Turno"
      DataSource      =   "AdoTurnos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      Left            =   5520
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   120
      Width           =   1332
   End
   Begin VB.TextBox Text2 
      DataField       =   "Nombre"
      DataSource      =   "AdoTurnos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      Left            =   5520
      TabIndex        =   1
      Text            =   "Text2"
      Top             =   720
      Width           =   3972
   End
   Begin VB.TextBox Text4 
      DataField       =   "Inicio"
      DataSource      =   "AdoTurnos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      Left            =   5640
      TabIndex        =   2
      Text            =   "Text4"
      Top             =   4560
      Width           =   2412
   End
   Begin VB.CommandButton CmdTurno 
      Caption         =   "Iniciar Nuevo Turno"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   3480
      TabIndex        =   5
      Top             =   6360
      Width           =   4935
   End
   Begin VB.TextBox Text5 
      DataField       =   "Entrega"
      DataSource      =   "AdoTurnos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5640
      TabIndex        =   3
      Text            =   "Text5"
      Top             =   5160
      Width           =   2409
   End
   Begin VB.TextBox Text6 
      Alignment       =   1  'Right Justify
      DataField       =   "EfectivoInicial"
      DataSource      =   "AdoTurnos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      Left            =   5640
      TabIndex        =   4
      Text            =   "0"
      Top             =   5760
      Width           =   2412
   End
   Begin VB.CommandButton CmdEntrar 
      Caption         =   "&Entrar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4920
      TabIndex        =   9
      Top             =   6480
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label lbTurno 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Responsable"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   3
      Left            =   2640
      TabIndex        =   14
      Top             =   720
      Width           =   2535
   End
   Begin VB.Label lbTurno 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Fecha"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   1
      Left            =   3360
      TabIndex        =   13
      Top             =   1440
      Width           =   1815
   End
   Begin VB.Label lbTurno 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Turno Activo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   2640
      TabIndex        =   12
      Top             =   120
      Width           =   2535
   End
   Begin VB.Label text3 
      Alignment       =   2  'Center
      BackColor       =   &H0080C0FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      DataField       =   "Fecha"
      DataSource      =   "AdoTurnos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2400
      TabIndex        =   11
      Top             =   2040
      Width           =   2895
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      DataField       =   "Fecha"
      DataSource      =   "AdoInformes"
      Height          =   135
      Left            =   4560
      TabIndex        =   10
      Top             =   6720
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lbTurno 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Hora de Entrega"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   5
      Left            =   2520
      TabIndex        =   8
      Top             =   5160
      Width           =   3015
   End
   Begin VB.Label lbTurno 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Efectivo Inicial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   4
      Left            =   3000
      TabIndex        =   7
      Top             =   5760
      Width           =   2535
   End
   Begin VB.Label lbTurno 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Hora de Inicio"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   2
      Left            =   3000
      TabIndex        =   6
      Top             =   4560
      Width           =   2535
   End
End
Attribute VB_Name = "InicioTurno"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Calendar1_AfterUpdate()
Text3.Caption = Calendar1.Value
End Sub



Private Sub Calendar1_Validate(Cancel As Boolean)
Text3 = Calendar1.Value
End Sub

Private Sub CmdEntrar_Click()
    Dim NuevaFecha As String
    If Len(Text1.Text) = 0 Or Val(Text1.Text) = 0 Then
        MsgBox "El turno ingresado no es v�lido" & Chr(13) _
        & "Por favor vuelva a ingresar su turno", vbInformation + vbOKOnly
        Text1.SetFocus
        Exit Sub
    ElseIf Len(Text2.Text) = 0 Then
        Text2.SetFocus
        Exit Sub
    'ElseIf Len(Text3.Text) = 0 Then
    '    Text3.SetFocus
    '    Exit Sub
    End If
    NuevaFecha = Format(Text3, "YYYY-MM-DD")
    Sql = "SELECT turno " & _
          "FROM tickets  " & _
          "WHERE turno=" & Text1.Text & _
          " AND fecha BETWEEN '" & NuevaFecha & "' and '" & NuevaFecha & "' LIMIT 1"
    Call Consulta(Rst_tmp, Sql)
    If Rst_tmp.RecordCount > 0 Then
        MsgBox " � Este turno ya existe ... !", vbExclamation
        Text1.SetFocus
        Exit Sub
    End If
            
    'Unload All
    'Unload Main
    'Unload FrmFondo
    With Main.AdoTurnos.Recordset
        .Fields(0) = Text1.Text
        .Fields(1) = "" & Text2.Text
        .Fields(2) = Text3.Caption
        .Fields(3) = Text4.Text
        .Fields(4) = Text5.Text
        .Fields(5) = Text6.Text
        .Update
        Main.TurnoTurno.Caption = Text1.Text
        Main.TurnoNombre.Caption = Text2.Text
        Main.TurnoFecha.Caption = Text3.Caption
        Main.TurnoInicio.Caption = Text4.Text
        Main.TurnoEfeInicial.Caption = Text6.Text
    End With
    Me.Hide
    Main.Visible = True
    FrmFondo.Show
End Sub


Private Sub CmdTurno_Click()
    
    If Mid(CmdTurno.Caption, 1, 7) = "Iniciar" Then
        
        Text1.Text = ""
        Text2.Text = ""
        'Text3.Text = ""
        Text4.Text = Time
        Text5.Text = "Activo"
        Text1.Text = 0
        Text1.Enabled = True
        Text2.Enabled = True
        Text3.Enabled = True
        CmdTurno.Visible = False
        CmdEntrar.Visible = True
        Calendar1.Visible = True
        Text3.Caption = Calendar1.Value
        Exit Sub
        
    End If
    
    Main.AdoTurnos.Recordset.Update
  '  tamaancho = Image1.Width
  '  tamalargo = Image1.Height
  '  For i = 1 To 134
  '      Image1.Width = Image1.Width + i
  '      Image1.Height = Image1.Height + i
  '      Image1.Top = Image1.Top - 1
  '      For O = 1 To 350
  '      Next
  '  Next

    Me.Hide
    ElMainActivo = True
    Main.Visible = True
    FrmFondo.Show

End Sub

Private Sub Form_Load()
    Unload Acceso
    
    With Main.AdoTurnos.Recordset
        .MoveLast
        If Text5.Text = "Activo" Then
            CmdTurno.Caption = "Continuar Turno Activo"
        Else
            CmdTurno.Caption = "Iniciar Nuevo Turno"
        End If
    End With
    Text1.Enabled = False
    Text2.Enabled = False
    'Text3.Enabled = False
    Text4.Enabled = False
    Text5.Enabled = False
    Calendar1.Value = Date
    Calendar1.Visible = False
    Text3.Caption = Date
End Sub
