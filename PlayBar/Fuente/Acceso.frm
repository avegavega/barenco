VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form Acceso 
   BackColor       =   &H00000080&
   BorderStyle     =   0  'None
   Caption         =   "Contrase�a                                   Acceso EURO"
   ClientHeight    =   4695
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   6105
   FillStyle       =   0  'Solid
   Icon            =   "Acceso.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   4695
   ScaleWidth      =   6105
   StartUpPosition =   2  'CenterScreen
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   3090
      OleObjectBlob   =   "Acceso.frx":0442
      Top             =   3915
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      DrawMode        =   6  'Mask Pen Not
      DrawStyle       =   5  'Transparent
      Height          =   1500
      Left            =   1320
      Picture         =   "Acceso.frx":0676
      ScaleHeight     =   1500
      ScaleWidth      =   1575
      TabIndex        =   5
      Top             =   1560
      Width           =   1575
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1800
      TabIndex        =   1
      Top             =   3240
      Width           =   1140
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3360
      TabIndex        =   2
      Top             =   3240
      Width           =   1020
   End
   Begin VB.TextBox txtPassword 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   390
      IMEMode         =   3  'DISABLE
      Left            =   2040
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   1080
      Width           =   2055
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00000080&
      Caption         =   " aveag@fadmin.cl  997826874"
      ForeColor       =   &H0000FFFF&
      Height          =   495
      Left            =   2880
      TabIndex        =   4
      Top             =   2160
      Width           =   1815
   End
   Begin VB.Label lblLabels 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Contrase�a"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      TabIndex        =   3
      Top             =   600
      Width           =   1695
   End
End
Attribute VB_Name = "Acceso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Declare Function SetWindowRgn Lib "user32" (ByVal hwnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long
Private Declare Function CreateEllipticRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Option Explicit
Public LoginSucceeded As Boolean

Private Sub cmdCancel_Click()
    'establecer la variable global a false
    'para indicar un inicio de sesi�n fallido
    LoginSucceeded = False
    Me.Hide
 '   Shell "rundll32.exe user.exe,ExitWindows"
    End
End Sub

Private Sub cmdOK_Click()
    Dim C As Integer
    ClaveSi = False
     'colocar c�digo aqu� para pasar al sub
     'que llama si la contrase�a es correcta
     'lo m�s f�cil es establecer una variable global
    If txtPassword.Text = Security.Text1 Then
        ' CLAVE caja
        Main.manconfig.Enabled = False
        Main.manager.Enabled = False
        Main.manProductos.Enabled = True
        ClaveSi = True
    End If
    If txtPassword.Text = Security.Text2 Then
        ' CLAVE caja
        Main.manconfig.Enabled = False
        Main.manager.Enabled = False
        Main.manProductos.Enabled = True
        ClaveSi = True
    End If
    If txtPassword.Text = Security.Text3 Then
        ' CLAVE caja
        Main.manconfig.Enabled = False
        Main.manager.Enabled = False
        Main.manProductos.Enabled = True
        ClaveSi = True
    End If
    If txtPassword.Text = Security.Text4 Then
        ' CLAVE caja
        
        ClaveSi = True
    End If
    If Not ClaveSi Then
        MsgBox "La contrase�a no es v�lida. Vuelva a intentarlo", vbInformation, "Inicio de sesi�n"
        txtPassword.SetFocus
        SendKeys "{Home}+{End}"
    Else
        Main.Visible = False
        Unload InicioTurno
        LoginSucceeded = True
        InicioTurno.Show
        'Me.Hide
        '
        'FrmFondo.Show
    End If
End Sub

Private Sub Form_Load()
    ClrCfoco = &H80C0FF
    ClrSfoco = &H80000014
    ClrDesha = &HE0E0E0
    Dim Xs As Long, Ys As Long, archivo As Integer, cajja As String
    archivo = FreeFile
    Xs = Me.Width / Screen.TwipsPerPixelX
    Ys = Me.Height / Screen.TwipsPerPixelY
    SetWindowRgn hwnd, CreateEllipticRgn(0, 0, Xs, Ys), True
    ElMainActivo = False
    Open "c:\cafeuro\sv.ip" For Input As #archivo
    Line Input #archivo, S_Servidor
    Close #archivo
    archivo = FreeFile
    Open "c:\cafeuro\iscaja.if" For Input As #archivo
    Line Input #archivo, cajja
    If cajja = "NO" Then
        esCaja = False
    Else
        esCaja = True
    End If
    Close #archivo
End Sub

