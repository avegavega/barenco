VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form Personal 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00C0C000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Maestro de Garzones"
   ClientHeight    =   5295
   ClientLeft      =   825
   ClientTop       =   1320
   ClientWidth     =   6300
   Icon            =   "Personal.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5295
   ScaleWidth      =   6300
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox CboCargos 
      DataField       =   "Cargo"
      DataSource      =   "Adodc1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   348
      ItemData        =   "Personal.frx":0442
      Left            =   2040
      List            =   "Personal.frx":046A
      TabIndex        =   22
      Text            =   "Combo1"
      Top             =   1680
      Width           =   2772
   End
   Begin VB.TextBox Text5 
      DataField       =   "Codigo"
      DataSource      =   "Adodc1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   288
      Left            =   2040
      TabIndex        =   3
      Top             =   720
      Width           =   1212
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   312
      Left            =   600
      Top             =   4800
      Visible         =   0   'False
      Width           =   4272
      _ExtentX        =   7541
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   1
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Personal"
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox Text4 
      DataField       =   "Telefono Celular"
      DataSource      =   "Adodc1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   288
      Left            =   2040
      ScrollBars      =   2  'Vertical
      TabIndex        =   9
      Top             =   3096
      Width           =   1860
   End
   Begin VB.CommandButton Final 
      Caption         =   "Final"
      Height          =   255
      Left            =   4080
      TabIndex        =   19
      Top             =   4080
      Width           =   975
   End
   Begin VB.CommandButton Siguiente 
      Caption         =   "Siguiente"
      Height          =   255
      Left            =   2880
      TabIndex        =   18
      Top             =   4080
      Width           =   975
   End
   Begin VB.CommandButton Anterior 
      Caption         =   "Anterior"
      Height          =   255
      Left            =   1680
      TabIndex        =   17
      Top             =   4080
      Width           =   975
   End
   Begin VB.CommandButton Inicio 
      Caption         =   "Inicio"
      Height          =   255
      Left            =   360
      TabIndex        =   16
      Top             =   4080
      Width           =   975
   End
   Begin VB.CommandButton Buscar 
      Caption         =   "Buscar"
      Height          =   255
      Left            =   5040
      TabIndex        =   15
      Top             =   3228
      Width           =   975
   End
   Begin VB.CommandButton Refrescar 
      Caption         =   "Refrescar"
      Height          =   255
      Left            =   5040
      TabIndex        =   14
      Top             =   2868
      Width           =   975
   End
   Begin VB.CommandButton Cancelar 
      Caption         =   "Cancelar"
      Height          =   255
      Left            =   5040
      TabIndex        =   13
      Top             =   2508
      Width           =   975
   End
   Begin VB.CommandButton Borrar 
      Caption         =   "Borrar"
      Height          =   255
      Left            =   5040
      TabIndex        =   12
      Top             =   2148
      Width           =   975
   End
   Begin VB.CommandButton Grabar 
      Caption         =   "Grabar"
      Height          =   255
      Left            =   5040
      TabIndex        =   11
      Top             =   1788
      Width           =   975
   End
   Begin VB.CommandButton Editar 
      Caption         =   "Editar"
      Height          =   255
      Left            =   5040
      TabIndex        =   10
      Top             =   1428
      Width           =   975
   End
   Begin VB.CommandButton Nuevo 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Nuevo"
      Height          =   255
      Left            =   5040
      TabIndex        =   6
      Top             =   1068
      Width           =   975
   End
   Begin VB.TextBox Text3 
      DataField       =   "Telefono Red Fija"
      DataSource      =   "Adodc1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2040
      TabIndex        =   8
      Top             =   2592
      Width           =   1812
   End
   Begin VB.TextBox Text2 
      DataField       =   "Direccion"
      DataSource      =   "Adodc1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2040
      TabIndex        =   7
      Top             =   2148
      Width           =   2850
   End
   Begin VB.TextBox text1 
      DataField       =   "Nombre"
      DataSource      =   "Adodc1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   2040
      TabIndex        =   5
      Top             =   1200
      Width           =   2865
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Cargo"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   372
      Index           =   2
      Left            =   360
      TabIndex        =   21
      Top             =   1680
      Width           =   1008
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Codigo"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Index           =   1
      Left            =   360
      TabIndex        =   20
      Top             =   720
      Width           =   1125
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "PASSWORD"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   390
      TabIndex        =   4
      Top             =   3045
      Width           =   1770
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Tel�fono Fijo:"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   396
      TabIndex        =   2
      Top             =   2592
      Width           =   1632
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Direcci�n:"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Index           =   0
      Left            =   390
      TabIndex        =   1
      Top             =   2145
      Width           =   1365
   End
   Begin VB.Label Label1 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre:"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   360
      TabIndex        =   0
      Top             =   1200
      Width           =   1215
   End
End
Attribute VB_Name = "Personal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub InhabilitarBotones()
  Dim n As Integer
  For n = 0 To Controls.Count - 1
    If TypeOf Controls(n) Is CommandButton Then
      Controls(n).Enabled = False
    End If
  Next n
End Sub

Private Sub HabilitarBotones()
  Dim n As Integer
  For n = 0 To Controls.Count - 1
    If TypeOf Controls(n) Is CommandButton Then
      Controls(n).Enabled = True
    End If
  Next n
End Sub

Private Sub InhabilitarCajas()
  Dim n As Integer
  For n = 0 To Controls.Count - 1
    If TypeOf Controls(n) Is TextBox Then
      Controls(n).Enabled = False
    End If
  Next n
  CboCargos.Enabled = False
End Sub

Private Sub HabilitarCajas()
  Dim n As Integer
  For n = 0 To Controls.Count - 1
    If TypeOf Controls(n) Is TextBox Then
      Controls(n).Enabled = True
    End If
  Next n
  CboCargos.Enabled = True
End Sub

Private Sub Form_Load()
    Main.Visible = False
  Grabar.Enabled = False
  InhabilitarCajas
  ChDir App.Path
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main.Visible = True
End Sub

Private Sub Inicio_Click()
  Adodc1.Recordset.MoveFirst
End Sub

Private Sub Anterior_Click()
  Adodc1.Recordset.MovePrevious
  If Adodc1.Recordset.BOF Then
    Adodc1.Recordset.MoveFirst
  End If
End Sub

Private Sub Siguiente_Click()
  Adodc1.Recordset.MoveNext
  If Adodc1.Recordset.EOF Then
    Adodc1.Recordset.MoveLast
  End If
End Sub

Private Sub Final_Click()
  Adodc1.Recordset.MoveLast
End Sub

Private Sub Nuevo_Click()
  HabilitarCajas
  InhabilitarBotones
  Grabar.Enabled = True
  Cancelar.Enabled = True
  Adodc1.Recordset.AddNew 'a�adir un nuevo registro
  Text1.SetFocus         'poner el cursor en la caja del "Nombre"
End Sub

Private Sub Editar_Click()
  HabilitarCajas
  InhabilitarBotones
  Grabar.Enabled = True
  Cancelar.Enabled = True
  Text1.SetFocus       'poner el cursor en la caja del "Nombre"
End Sub

Private Sub Grabar_Click()
  Adodc1.Recordset.Update
  HabilitarBotones
  Grabar.Enabled = False
  InhabilitarCajas
End Sub

Private Sub Cancelar_Click()
  Adodc1.Recordset.CancelUpdate
  HabilitarBotones
  Grabar.Enabled = False
  InhabilitarCajas
End Sub

Private Sub Borrar_Click()
  Dim r As Integer
  On Error GoTo RutinaDeError
  r = MsgBox("�Desea borrar el registro?", vbYesNo, "Atenci�n")
  If r <> vbYes Then Exit Sub
  Adodc1.Recordset.Delete   'borrar el registro actual
  Adodc1.Recordset.MoveNext 'situarse en el registro siguiente
  If Adodc1.Recordset.EOF Then
    Adodc1.Recordset.MoveLast
  End If
  Exit Sub
RutinaDeError:
  r = MsgBox(Error, vbOKOnly, "Se ha producido un error:")
  Adodc1.Recordset.CancelUpdate
End Sub

Private Sub Refrescar_Click()
  Adodc1.Recordset.Requery
  HabilitarBotones
  Grabar.Enabled = False
End Sub

Private Sub Buscar_Click()
  Dim Buscado As String, Criterio As String
  Buscado = InputBox("�Qu� nombre quieres buscar?")
  If Buscado = "" Then Exit Sub
  Criterio = "Nombre Like '*" & Buscado & "*'"
  ' Buscar desde el siguiente registro a la posici�n actual
  Adodc1.Recordset.MoveNext
  If Not Adodc1.Recordset.EOF Then
    Adodc1.Recordset.Find Criterio
  End If
  If Adodc1.Recordset.EOF Then
    Adodc1.Recordset.MoveFirst
    ' Buscar desde el principio
    Adodc1.Recordset.Find Criterio
    If Adodc1.Recordset.EOF Then
      Adodc1.Recordset.MoveLast
      MsgBox ("No encuentro ese nombre")
    End If
  End If
End Sub
