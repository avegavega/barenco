VERSION 5.00
Begin VB.Form AccionMesa 
   BackColor       =   &H00000000&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Accion para la Mesa"
   ClientHeight    =   4935
   ClientLeft      =   1050
   ClientTop       =   2400
   ClientWidth     =   4200
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   4200
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.CommandButton CmdMesa 
      BackColor       =   &H0080C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   4
      Left            =   0
      Picture         =   "AccionMesa.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   4200
      Width           =   4212
   End
   Begin VB.CommandButton CmdMesa 
      BackColor       =   &H0080C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   732
      Index           =   3
      Left            =   0
      Picture         =   "AccionMesa.frx":0D1E
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3480
      Width           =   4212
   End
   Begin VB.OptionButton Interno 
      BackColor       =   &H00000000&
      Caption         =   "Otro"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   2280
      TabIndex        =   6
      Top             =   960
      Width           =   1095
   End
   Begin VB.OptionButton Normal 
      BackColor       =   &H00000000&
      Caption         =   "Boleta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   840
      TabIndex        =   5
      Top             =   960
      Value           =   -1  'True
      Width           =   1455
   End
   Begin VB.CommandButton CmdMesa 
      BackColor       =   &H0080C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   732
      Index           =   2
      Left            =   0
      Picture         =   "AccionMesa.frx":1A49
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2760
      Width           =   4212
   End
   Begin VB.CommandButton CmdMesa 
      BackColor       =   &H000080FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   19.5
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   0
      Left            =   0
      MaskColor       =   &H00404040&
      Picture         =   "AccionMesa.frx":2742
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1320
      UseMaskColor    =   -1  'True
      Width           =   4215
   End
   Begin VB.CommandButton CmdMesa 
      BackColor       =   &H0080C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   732
      Index           =   1
      Left            =   0
      Picture         =   "AccionMesa.frx":34FE
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2040
      Width           =   4212
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H80000007&
      BorderStyle     =   0  'None
      Height          =   1575
      Left            =   120
      TabIndex        =   11
      Top             =   1320
      Visible         =   0   'False
      Width           =   3855
      Begin VB.TextBox TxtInterno 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   1320
         TabIndex        =   13
         Text            =   "0"
         Top             =   510
         Width           =   2415
      End
      Begin VB.ComboBox CboOtros 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "AccionMesa.frx":41FF
         Left            =   1320
         List            =   "AccionMesa.frx":4201
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   120
         Width           =   2415
      End
      Begin VB.Label lbSaldo 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   375
         Left            =   1320
         TabIndex        =   17
         Top             =   960
         Width           =   2415
      End
      Begin VB.Label lbmonto 
         BackColor       =   &H80000008&
         Caption         =   "Monto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   375
         Left            =   240
         TabIndex        =   16
         Top             =   600
         Width           =   975
      End
      Begin VB.Label lbtxtsaldo 
         BackColor       =   &H80000008&
         Caption         =   "Saldo Boleta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   375
         Left            =   0
         TabIndex        =   15
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label3 
         BackColor       =   &H00000000&
         Caption         =   "Tipo Doc."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   0
         TabIndex        =   14
         Top             =   120
         Width           =   1335
      End
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H80000008&
      BackStyle       =   0  'Transparent
      Caption         =   "Mesa"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   240
      Width           =   495
   End
   Begin VB.Label LBIden 
      Alignment       =   2  'Center
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   480
      Width           =   735
   End
   Begin VB.Label LBTotal 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   21.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Left            =   960
      TabIndex        =   8
      Top             =   360
      Width           =   3015
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H80000012&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "TOTAL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   1325
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   4215
   End
End
Attribute VB_Name = "AccionMesa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdMesa_Click(Index As Integer)
    Dim xoMesa As Variant
    Dim NuevaMesa As Integer
    Dim MesaActual As Integer
    Dim Ota As Integer
    Dim ElDescuento As Variant
    Dim DescMonto As String
    Dim NUticket As Double
    Dim StrMesa As String
    Dim docPago As String
    Dim Xualq As Variant
    Dim Instruccion As String
    Dim MiMesita(9) As Variant
    Dim StrPmesa As String
    Dim EnDescMesa As String
    Dim XmesaD As Integer
    docPago = "Boleta"
    Desde = 36
    StrPmesa = PathMesas & "Mesa" & Trim(Main.TxtMesaSel.Text) & ".ped"
    LBIden.Caption = Main.TxtMesaSel.Text
    
    If Index = 1 Then  '' Realizar Descuento
        If Main.Dscto.Value = 0 Then
            permiteDescuento = False
            AutDescuento.Show 1
        Else
            permiteDescuento = True
        End If
        
        
        
        If permiteDescuento = False Then
            
        
        Else
            ElDescuento = InputBox("Si es porcentaje digite el Tanto %" & Chr(13) & _
                                                             "Si monto  es en dinero anteponga $", "Descuento a Mesa", "10", 4500, 5500)
                                                             DescMonto = ElDescuento
                                                             
            
            
            
            If Mid(DescMonto, 1, 1) = "$" Then '' Descuento en $ pesos
                If Val(Mid(DescMonto, 2)) > (Val(LBTotal.Caption) / 2) - 1 Then
                    MsgBox "El descuento no puede superar el 50% del Total", vbInformation + vbOKOnly
                    Exit Sub
                End If
                
                X = FreeFile ' Actualizando la mesa
                StrMesa = "StatusTable" & LBIden.Caption & ".tab"
                Open PathMesas & StrMesa For Input As X
                    For i = 1 To 9
                        Line Input #X, MiMesita(i)
                    Next
                Close #X
                
                MiMesita(1) = "Si-" & IdEquipo & "-" & Now
                Q = FreeFile
                MiMesita(8) = ElDescuento
                Open PathMesas & StrMesa For Output As Q
                    For i = 1 To 9
                        Print #Q, MiMesita(i)
                    Next
                Close #X ' Mesa actualizada
                
                
                MesaDescuento(LBIden.Caption) = ElDescuento
                ds = FreeFile
                If Main.Dscto.Value = 0 Then
                    If permiteDescuento = True Then
                        Open LaRutadeArchivos & Me.LBIden.Caption & ".dct" For Output As ds
                            Print #ds, Autorizador
                        Close ds
                        'MsgBox LaRutadeArchivos & Me.LBIden.Caption & ".dct"
                    End If
                End If
                
                'Main.ActualizarEstadoMesas
               ' Main.LBmesa_Click (LBIden.Caption)
                CmdMesa_Click (4)
            End If
            
            'quitar descuento
            If DescMonto = "0" Then
                XmesaD = FreeFile ' Obteniendo datos
                EnDescMesa = "StatusTable" & LBIden.Caption & ".tab"
                Open PathMesas & EnDescMesa For Input As XmesaD
                    For i = 1 To 9
                        Line Input #XmesaD, MiMesita(i)
                    Next
                Close #XmesaD
                XmesaD = FreeFile
                MiMesita(8) = "0" 'Modificando dscto a "0"
                Open PathMesas & EnDescMesa For Output As XmesaD
                    MiMesita(1) = "Si-" & IdEquipo & "-" & Now
                    For i = 1 To 9
                        Print #XmesaD, MiMesita(i)
                    Next
                Close #XmesaD ' Mesa actualizada
                MesaDescuento(LBIden.Caption) = ElDescuento
                
                    
            
                
                ''  Aqui borramos del descueneto de una mesa
                'Main.LBmesa_Click (LBIden.Caption)
               'Main.ActualizarEstadoMesas
                CmdMesa_Click (4)
            End If
            
            
            If IsNumeric(DescMonto) And Val(DescMonto) < 100 And Val(DescMonto) > 0 Then
                If Val(DescMonto) > 49 Then
                    MsgBox "El descuento no puede superar el 50% del Total", vbInformation + vbOKOnly
                    Exit Sub
                End If
                If Val(DescMonto) < 5 Then
                    MsgBox "El descuento debe ser mayor al 4%"
                    Exit Sub
                End If
                X = FreeFile ' Actualizando la mesa
                StrMesa = "StatusTable" & LBIden.Caption & ".tab"
                Open PathMesas & StrMesa For Input As X
                    For i = 1 To 9
                        Line Input #X, MiMesita(i)
                    Next
                Close #X
                Q = FreeFile
                MiMesita(8) = ElDescuento
                Open PathMesas & StrMesa For Output As Q
                    MiMesita(1) = "Si-" & IdEquipo & "-" & Now
                    For i = 1 To 9
                        Print #Q, MiMesita(i)
                    Next
                Close #X ' Mesa actualizada
                MesaDescuento(LBIden.Caption) = ElDescuento
                
                ds = FreeFile
                If Main.Dscto.Value = 0 Then
                    If permiteDescuento = True Then
                        Open LaRutadeArchivos & LBIden.Caption & ".dct" For Output As ds
                            Print #ds, Autorizador
                        Close ds
                      ''  MsgBox LaRutadeArchivos & LBIden.Caption & ".dct"
                    End If
                End If
                
                'Main.ActualizarEstadoMesas
               ' Main.LBmesa_Click (LBIden.Caption)
                CmdMesa_Click (4)
            End If
        End If
    End If
    If Index = 2 Then  '' Cambiar a otra mesa
        MesaActual = Val(LBIden.Caption)
        xoMesa = InputBox("La mesa actual es " & LBIden.Caption _
        & Chr(13) & "Ingrese la mesa a la cual se va ha cambiar." & Chr(13) & _
        "Recuerde que la mesa debe estar desocupada", "Cambiar a otra mesa")
        If IsNumeric(NuevaMesa) Then
            NuevaMesa = Val(xoMesa)
        
            If MesaStatus(NuevaMesa) <> 1 Then
                    MsgBox "� Error de ingreso ... !", vbExclamation, "La mesa est� ocupada"
            Else
                    C = Main.CambiaMesa(MesaActual, NuevaMesa)
            End If
            CmdMesa_Click (4)
        Else
            MsgBox "N� de mesa no V�lido ...", vbExclamation, "Error de ingreso"
            CmdMesa_Click (4)
        End If
    End If
    
    
    If Index = 3 Then   ' ' ' '  Unir con otra mesa
        MesaActual = Val(LBIden.Caption)
        xoMesa = InputBox("La mesa actual es " & LBIden.Caption _
        & Chr(13) & "Ingrese la mesa con la cual " & Chr(13) & _
        "Desea unir esta . . .  ", "Unir con otra mesa")
        If Val(xoMesa) < 1 Then Index = 4
        Ota = Val(xoMesa)
        If MesaStatus(Ota) = 3 Then
            paso200 = MsgBox("La mesa est� al cobro ... ", vbExclamation)
            CmdMesa_Click (4)
        Else
            If MesaStatus(Ota) = 1 Or MesaStatus(Ota) = 3 Then Index = 4
            j200 = Main.JuntaMesas(MesaActual, Ota)
        End If
    End If
    
    
    
    '  *****************************************************
    If Index = 0 Then   'E M I T I R    V A L E *****************
     ' *************************************************
        LargoR = Len(rPedido)
        If Normal.Value = True Then NoRindes = 1
        itemselec = 0
        '' Si hay items marcados, crear un pedido nuevo con estos.
        '' Asignar mesa 1 mas del total de las mesa
        '' la N� de lista de item ser� 1 mas del total de las mesas
        mselec = Main.List1(LBIden.Caption - 1).ListCount
        tselec = 0
        Main.List1(MesaSelectItem).Clear
        For K = 0 To mselec - 1
            If Mid(Main.ImpTotal.Caption, 1, 3) = "Con" Then
                If Right(Mid(Main.List1(LBIden.Caption - 1).List(K), 15, 20), 1) <> "*" Then
                    MsgBox "Por favor, env�e los productos" & Chr(13) & "a centros de produccion, Presione F5 en la mesa correspondiente", vbInformation + vbOKOnly, "Atenci�n"
                    Index = 4
                    GoTo fuera
                End If
            End If
            If Main.List1(LBIden.Caption - 1).Selected(K) = True Then
                Main.List1(MesaSelectItem).AddItem Main.List1(LBIden.Caption - 1).List(K)
                tselec = tselec + 1
            End If
        Next '
        
        If Main.List1(MesaSelectItem).ListCount > 0 Then ''Si hay items marcados
            PorParte = True
            For K = 0 To tselec
                For l = 0 To Main.List1(LBIden.Caption - 1).ListCount - 1
                    If Main.List1(LBIden.Caption - 1).Selected(l) = True Then
                        Main.List1(LBIden.Caption - 1).RemoveItem l
                        Exit For
                    End If
                Next
            Next
            RubroDef1(Main.TxtMesaSel.Text) = 0
            RubroDef2(Main.TxtMesaSel.Text) = 0
            valorX = 0
            
            For K = 0 To Main.List1(MesaSelectItem).ListCount - 1
                selecItem = Main.List1(MesaSelectItem).List(K)                     ''  Se agregan productos a la mesa
                CantRetira = Val(Left(selecItem, 3))
                valorX = Val(Mid(selecItem, 6, 6)) * CantRetira
                If Val(Right(selecItem, 2)) = 1 Then RubroDef1(Main.TxtMesaSel.Text) = RubroDef1(Main.TxtMesaSel.Text) + valorX
                If Val(Right(selecItem, 2)) = 2 Then RubroDef2(Main.TxtMesaSel.Text) = RubroDef2(Main.TxtMesaSel.Text) + valorX
                itemselec = itemselec + valorX
            Next
            
            'Aqui: si corresponde, sacaremos boleta Fiscal
            ' ****  B O L E T A     F I S C A L  *****
            ' ****************************************
            If Main.fiscalprint.Caption = "Si - Imprime boleta" Then
            
                If Normal.Value = True Then
                    '"Definitavamente" & vbCr & "hay que imprimir boleta"
                    Me.Caption = "Imprimiendo Boleta...Espere"
                    portNum = 1
                    On Error GoTo port_error
                    
                    'Aqui bixolon
                    'Boleta por item marcados
                    CmdMesa(Index).Enabled = False
                    EmiteBoletaBixolon
                    Me.Caption = "Acci�n para la mesa"
                    CmdMesa(Index).Enabled = True
                    GoTo fiscalIBM
                    
                  
                    
                    'Cerrar puerto
                    Main.Tf6.PortOpen = False
                    On Error GoTo 0
fiscalIBM:
                    'Me.Hide
                    
                ElseIf Interno = True Then
                    If lbSaldo.Caption > 0 Then ''''AKI BOLETA POR SALDO
                            Saldillo = lbSaldo.Caption
                            Me.Caption = "Imprimiendo Boleta...Espere"
                            portNum = 1
                            On Error GoTo port_error
                            
                            If Not Main.Tf6.PortOpen Then
                               Main.Tf6.CommPort = portNum
                               Main.Tf6.PortOpen = True
                            End If
                            error_26h = False
                            If Not error_26h Then
                                Me.Normal.Visible = False
                                Me.Interno.Visible = False
                                Dim InsFiscalSaldillo As String * 9
                                InsFiscalSaldillo = "000000000"
                                For i = 0 To 4
                                    Me.CmdMesa(i).Enabled = False
                                Next
                                Call Main.evaluaCommand("001")
                                Instruccion = "111CAJERO(A):" & Main.TurnoNombre.Caption
                                Call Main.evaluaCommand(Instruccion)
                                Instruccion = "111TURNO    :" & Main.TurnoTurno.Caption & "       MESA:" & Me.LBIden.Caption
                                Call Main.evaluaCommand(Instruccion)
                                Call Main.evaluaCommand("12")
                                Mid(InsFiscalSaldillo, 10 - Len(Trim(Me.lbSaldo.Caption)), Len(Trim(Me.lbSaldo.Caption))) = Trim(Me.lbSaldo.Caption)
                                Instruccion = "13112COD_SALDO0000000001000"
                                Instruccion = Instruccion & InsFiscalSaldillo & InsFiscalSaldillo & "Saldo " & CboOtros.List(CboOtros.ListIndex)
                                '             '13fiocccccccccccccqqqqqqqqqppppppppptttttttttxxxxxx...
                                Call Main.evaluaCommand(Instruccion)
                                'Call Principal.evaluaCommand("131127801610001295000001000" & Detalle)
                                Instruccion = "200" & InsFiscalSaldillo
                                Call Main.evaluaCommand(Instruccion)
                                                           '  "200000003600"
                                Instruccion = "2631010" & InsFiscalSaldillo ' Efectivo
                                Call Main.evaluaCommand(Instruccion) 'DESCRIPCION ADICIONAL FP      ")
                                Call Main.evaluaCommand("271000000000")
                                'Cerrar boleta
                                Call Main.evaluaCommand("99")
                                Boletilla = Main.NboletaFiscal.Caption
                                Main.NboletaFiscal.Caption = Main.NboletaFiscal.Caption + 1
                                Q = FreeFile
                                Open LaRutadeArchivos & "boletafiscal.cor" For Output As Q
                                    Print #Q, Trim(Main.NboletaFiscal.Caption)
                                Close #Q
               
                            End If
                    End If  '' FIN BOLETA POR SALDO
                End If ' fIN DE LA BOLETA FISCAL
            Else
'                MsgBox "Imprimir Ticket"
            
            End If
            
            
            Main.LBBpedido.Caption = Correlativos.txtFields(0) + 1
            UPedido = Main.LBBpedido.Caption
            NUticket = UPedido
            Correlativos.txtFields(0) = UPedido
            Correlativos.datPrimaryRS.Recordset.UpdateBatch adAffectCurrent
            
            
            StrMesa = "StatusTable" & MesaSelectItem & ".tab"
            X = FreeFile
            Open PathMesas & StrMesa For Output As X
                Print #X, "Si-" & IdEquipo & "-" & Now
                Print #X, Main.txtGarzonSel.Text
                Print #X, Time
                Print #X, UPedido
                Print #X, 3
                Print #X, Main.List1(MesaSelectItem).ListCount
                Print #X, itemselect
                Print #X, "0"
                Print #X, "2"
                Print #X, miDoc
                Print #X, IIf(miDoc = "Boleta", Str(Main.NboletaFiscal.Caption), "0")
                Print #X, "nada"
                Print #X, "nada"
            Close X
                
            If miDoc = "Boleta" Then
                Main.NboletaFiscal.Caption = Main.NboletaFiscal.Caption + 1
                Q = FreeFile
                Open LaRutadeArchivos & "boletafiscal.cor" For Output As Q
                    Print #Q, Trim(Main.NboletaFiscal.Caption)
                Close #Q
            End If
                        
            
            With Main.AdoPedidos.Recordset
                For j = 1 To Main.List1(MesaSelectItem).ListCount
                    esteitem = Main.List1(MesaSelectItem).List(j - 1)
                    MyHora = Mid(esteitem, Desde, 9)
                    ProdCodi = Val(Mid(esteitem, 50, 5))
                    CantRetira = Val(Left(esteitem, 3))
                    .AddNew
                    .Fields(0) = UPedido 'Npedido
                    .Fields(1) = Main.TurnoFecha.Caption
                    .Fields(2) = MyHora
                    .Fields(3) = MesaSelectItem
                    .Fields(4) = ProdCodi
                    .Fields(5) = CantRetira
                    .Fields(6) = Mid(esteitem, 15, 20) ' detalle
                    .Fields(7) = Val(Mid(esteitem, 6, 6))
                    .Fields(8) = .Fields(5) * .Fields(7)
                    .Fields(9) = Val(Right(esteitem, 3))
                    .Fields(10) = Val(Trim(Mid(esteitem, 55, 2)))
                    .Fields(11) = Main.TurnoTurno.Caption
                    .MoveNext
                Next
                
            End With
            
            
            LBIden.Caption = MesaSelectItem
            'LbTotal.Caption = itemselec
            MesaStatus(MesaSelectItem) = 3
            MesaUpedido(MesaSelectItem) = UPedido
            Main.WindowState = 2
            Imprimir = True
            Kill StrPmesa
            'PathMesas & StrPmesa
            X = FreeFile
            LargoR = Len(rPedido)
            Open StrPmesa For Random As #X Len = LargoR
                For ind = 1 To Main.List1(Val(Main.TxtMesaSel.Text) - 1).ListCount
                    restaItem = Main.List1(Val(Main.TxtMesaSel.Text) - 1).List(ind - 1)
                    rPedido.Npedido = UPedido
                    rPedido.Hora = Mid(restaItem, Desde, 9)
                    rPedido.Nmesa = Main.lbmesas.Caption + 1
                    rPedido.Codigo = Str(Val(Mid(restaItem, 50, 5)))
                    rPedido.Cantidad = Left(restaItem, 3)
                    rPedido.Detalle = Mid(restaItem, 15, 20)
                    rPedido.PrecioU = Mid(restaItem, 6, 6)
                    rPedido.SubTotal = Str(rPedido.Cantidad * rPedido.PrecioU)
                    rPedido.Bebestible = Right(restaItem, 1)
                    rPedido.Rubro = Mid(restaItem, 55, 2)
                    Put #X, , rPedido
                Next
            Close #X
            
            ''Aqui verifico si se pago con otro decumento que no sea boleta
            ' en el caso que sea cheque o ticket restaurant verificar el sado a favor o en contra
            If Interno.Value = True Then docPago = CboOtros.List(CboOtros.ListIndex)
                        
            Xualq = Main.ImprimeVale(docPago, TxtInterno.Text, lbSaldo.Caption) ' Hasta aqui es cuando hay
                             '  Item seleccionados
            If Main.LBlevelControl.Caption = 1 Then
                
                'FrmFondo.Enabled = False  '' Nivel de control total
                ''' Aqui colocar los datos para la formas de pago
                ''' Todavia estoy sacando de un tiket
                'Main.Enabled = False
                If Interno.Value = True Then
                
                End If
                
                Me.Normal.Visible = True
                Me.Interno.Visible = True
                For i = 0 To 4
                    Me.CmdMesa(i).Enabled = True
                Next
                
                
                X = FreeFile
                Pago.ListaFpagos.Clear
                Open LaRutadeArchivos & docPago For Input As X
                    Do While Not EOF(X)
                        Line Input #X, s
                        Pago.ListaFpagos.AddItem s
                    Loop
                Close #X
                Pago.LBpedido.Caption = NUticket
                Pago.LBFinal.Caption = IIf(lbSaldo.Caption > 0, LBTotal.Caption - lbSaldo.Caption, LBTotal.Caption)
                Pago.LBGARZON = Main.lbgarcia.Caption
                Pago.LBmesa.Caption = MesaSelectItem
                altito = 5500
                altito = altito - (10 * Pago.ListaFpagos.ListCount)
                altoboton = altito / (Pago.ListaFpagos.ListCount)
                Pago.CmdFpago(0).Height = altoboton
                Pago.CmdFpago(0).Top = 240
                
                 
                For i = 1 To Pago.ListaFpagos.ListCount - 1
                
                    Load Pago.CmdFpago(i)
                    'Pago.CmdFpago(i).Top = Pago.CmdFpago(i).Top + 100
                    Pago.CmdFpago(i).Visible = True
                    Pago.CmdFpago(i).Height = altoboton
                    Pago.CmdFpago(i).Top = Pago.CmdFpago(i - 1).Top + altoboton + 10
                Next
                
                For i = 0 To Pago.ListaFpagos.ListCount - 1
                    Pago.CmdFpago(i).Caption = Pago.ListaFpagos.List(i)
                    Pago.CmdFpago(i).TabIndex = i
                Next
                 
        
               
                Me.Normal.Visible = True
                Me.Interno.Visible = True
                
                Unload AccionMesa
                
                With Pago
                    
                     For i = 0 To .ListaFpagos.ListCount - 1
                        .AdoFpagos.Recordset.MoveFirst
                        Do While Not .AdoFpagos.Recordset.EOF
                            If .CmdFpago(i).Caption = .AdoFpagos.Recordset.Fields(0) Then
                                If .AdoFpagos.Recordset.Fields(1) Then .CmdFpago(i).Caption = .CmdFpago(i).Caption & "."
                                If .AdoFpagos.Recordset.Fields(2) = 0 Then .CmdFpago(i).Caption = .CmdFpago(i).Caption & "_"
                            End If
                            .AdoFpagos.Recordset.MoveNext
                        Loop
                    Next
                    .LBPagoNDoc.Caption = IIf(docPago = "Boleta", Main.NboletaFiscal.Caption - 1, 0)
                    .LbDoc.Caption = docPago
                End With
                ElMainActivo = True
                
                With Pago.MshMonedas
                   'Set .DataSource = Me.ad
                    .Cols = 3
                    .Row = 0: .Col = 0
                    .Text = "Moneda": .ColWidth(0) = 950: .Col = 1
                    .Text = "Nacional": .ColWidth(1) = 700
                    .ColWidth(2) = 700
                    For hh = 1 To .Rows - 1
                        .Row = hh: .Col = 1
                        vme = .Text: .Col = 2
                        .Text = Format(Pago.LBFinal.Caption / vme, "##,##0.00")
                    Next
                End With
                
                Pago.Show 1
                For i = 1 To Pago.ListaFpagos.ListCount - 1
                    Unload Pago.CmdFpago(i)
                Next

            Else
                '''
                Main.Enabled = True
                'With Main.AdoVales.Recordset
                '    .AddNew
                '    .Fields(0) = UPedido
                '    .Fields(1) = Main.TxtMesaSel.Text
                '    .Fields(2) = LBTotal.Caption
                '    .Fields(3) = Time
                '    .Fields(4) = Main.txtGarzonSel.Text
                '    .Fields(5) = Main.TurnoFecha.Caption
                '    .Fields(6) = Main.TurnoTurno.Caption
                '    .Update
                'End With
                FrmFondo.Enabled = True
              '  Main.Enabled = True
              '  Main.TxtMesaSel.Text = ""
              
              '  Unload AccionMesa
                Main.Enabled = True
               ' Main.TxtMesaSel.Text
                Index = 4
                ElMainActivo = True
                Unload Me
            End If
            
            
            
        Else ' Ticket normal sin item seleccionado
           
            Imprimir = True
            Pago.Visible = False
            ''Aqui verifico si se pago con otro decumento que no sea boleta
            ' en el caso que sea cheque o ticket restaurant verificar el sado a favor o en contra
            If Interno.Value = True Then docPago = CboOtros.List(CboOtros.ListIndex)
            i = Main.ImprimeVale(docPago, TxtInterno.Text, lbSaldo.Caption)
            ElMainActivo = True
            Unload Me
            'CmdMesa_Click (4)
        End If
    End If
fuera:
    If Index = 4 Then   '''  Cancela la acci�n
        If Main.List1(MesaSelectItem).ListCount > O Then
            For K = Main.List1(MesaSelectItem).ListCount - 1 To 0 Step -1
                Main.List1(MesaSelectItem).RemoveItem K
            Next
        End If
        ElMainActivo = True
        Unload AccionMesa
        'AccionMesa.Visible = False
        'FrmFondo.Enabled = True
        'Unload AccionMesa
        'Main.SetFocus
       ' Main.LBmesa_Click (Val(LBIden.Caption))
        'Main.TxtMesaSel = "0"
    End If
    Exit Sub
port_error:
    MsgBox "Error...Vale Parcial", vbCritical
End Sub

Private Sub CmdMesa_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    For cmd = 0 To CmdMesa.Count - 1
        CmdMesa(cmd).FontSize = 18
    Next
    CmdMesa(Index).FontSize = 22
End Sub



Private Sub Form_Load()
   ' With Main
   '     LBIden.Caption = .TxtMesaSel.Text
   '     LBTotal.Caption = Main.LBTotal.Caption
   '     If .Dscto = 0 Then CmdMesa(0).Enabled = False Else CmdMesa(0).Enabled = True
   ' End With
    If Main.LBlevelControl.Caption = 1 Then
      '  Gerencia.Visible = False
      '  Atencion = False
      '  VtaPersonal.Visible = False
    Else
      '  Gerencia.Visible = True
      '  Atencion.Visible = True
      '  VtaPersonal.Visible = True
    End If
    Main.WindowState = 2
    'Unload DImpresoras
    'FrmFondo.Enabled = False

End Sub
Private Sub Form_LostFocus()
    FrmFondo.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    ElMainActivo = True
         '************************
              '  Y = FreeFile
              '  Open PathMesas & "Tmp\" & Me.LBIden.Caption & ".tmp" For Output As Y
              '      Print #Y, "Si-" & IdEquipo & "-" & Now
              '      Print #Y, "2"
              '  Close #Y ' Esta in
End Sub

Private Sub Interno_Click()
    TxtInterno.Visible = True: TxtInterno.Text = Me.LBTotal
    For i = 0 To 4
        CmdMesa(i).Top = CmdMesa(i).Top + 1555
    
    Next
    AccionMesa.Height = AccionMesa.Height + 1555
    For i = 1 To 1000 Step 200
        AccionMesa.Top = AccionMesa.Top - 200
    Next
    'AccionMesa.Top = AccionMesa.Top - 1000
    Frame1.Visible = True

End Sub

Private Sub Normal_Click()
    Frame1.Visible = False
    For i = 0 To 4
        CmdMesa(i).Top = CmdMesa(i).Top - 1555
    Next
    AccionMesa.Height = AccionMesa.Height - 1555
    For i = 1 To 1000 Step 100
        AccionMesa.Top = AccionMesa.Top + 100
    Next
End Sub
Private Sub TxtInterno_Change()
    lbSaldo.Caption = LBTotal.Caption - Val(TxtInterno.Text)
End Sub
Private Sub EmiteBoletaBixolon()

  Dim Lp_CuantoPaga As Long, m As Integer
  'Emitimos boleta
  Dim dato As String, dato2 As String * 3
  Dim Bp_NoFiscal As Boolean
  Dim xCant As Single
  Bp_NoFiscal = False
  On Error GoTo errorIF
                   ' MsgBox "inciado EmiteBoleta"
                    Lp_CuantoPaga = 0
                    'MsgBox "Puerto imp fiscal " & IG_Puerto_Impresora_Fiscal
                    'Vr = OcxFiscal.Init(IG_Puerto_Impresora_Fiscal)
                    VR = Main.OCXFiscal.init(1)
                    'MsgBox "iniciada la conexion con if " & Vr
                    If VR = 0 Then
                        'cn.RollbackTrans
                        MsgBox "No se pudo completar la boleta..." & vbNewLine & Err.Number & " " & Err.Description, vbInformation
                        Exit Sub
                    End If
                    VR = Main.OCXFiscal.conflineacola(1, "=MESA:" & Main.TxtMesaSel & " - UD FUE ATENDIDO POR ===")
                    VR = Main.OCXFiscal.conflineacola(2, "===" & Main.lbgarcia.Caption & " ===")
                    VR = Main.OCXFiscal.abrirboleta(1, 1)
                    'Vr = OcxFiscal.abrirboleta(0, 1)
                    VR = Main.OCXFiscal.obtenernumboleta()
                    Lp_Nro_Boleta_Obtenida_IF = Main.OCXFiscal.boleta
                    
                    
                    For j = 1 To Main.List1(MesaSelectItem).ListCount
                            esteitem = Main.List1(MesaSelectItem).List(j - 1)
                            ProdCodi = Trim(Str(Val(Mid(esteitem, 50, 5))))
                            CantRetira = Trim(Str(Val(Left(esteitem, 3))))
                            productoX = Mid(esteitem, 15, 20) ' detalle
                            PrecioX = Trim(Str(Val(Mid(esteitem, 6, 6))))
                            totalLinea = Trim(Str(Val(CantRetira) * Val(PrecioX)))
                            xCant = CantRetira
                    
                            
                            'MsgBox productoX & " " & xCant & " " & CDbl(PrecioX)
                            VR = Main.OCXFiscal.agregaitem(productoX, xCant, CDbl(PrecioX))
                           
                    Next
                    'Aqui iria el detalle de la boleta
                  
                   ' MsgBox Lp_CuantoPaga
                   
                   ' If Val(lbTconDesc.Caption) > 0 Then
                            'Subtotal
                            
                            'Mid(InsFiscalSTotal, 11 - Len(Trim(lbTconDesc.Caption)), Len(Trim(lbTconDesc.Caption))) = Trim(lbTconDesc.Caption)
                            'Instruccion = "19" & InsFiscalSTotal
                            'Call Main.evaluaCommand(Instruccion)
                           '
                            'Descuento
                    '        vr = OCXFiscal.agregadescuento("DESCUENTO CLIENTE", CDbl(lbTconDesc.Caption))
                            'Mid(InsFiscalDescuento, 10 - Len(Trim(Str(Val(lbTconDesc.Caption) - Val(LBTotal.Caption)))), Len(Trim(Str(Val(lbTconDesc.Caption) - Val(LBTotal.Caption))))) = Trim(Str(Val(lbTconDesc.Caption) - Val(LBTotal.Caption)))
                            'Instruccion = "1731" & InsFiscalDescuento & "        Descuento Cliente     "
                            'Call Main.evaluaCommand(Instruccion)        '123456789012345678901234567890"
                            
                  '  End If
                   
                                       
                    
                  '  cuantopaso = 0
                  '  For i = 1 To Me.LvPagos.ListItems.Count
                  '      cuantopaso = cuantopaso + CDbl(Me.LvPagos.ListItems(i).SubItems(2))
                        VR = Main.OCXFiscal.agregapago(0, CDbl(Me.LBTotal))
                  '  Next
            
                    'If CDbl(Me.TxtTotal) > cuantopaso Then
                    '    vr = OCXFiscal.agregapago(0, (CDbl(Me.TxtTotal) - cuantopaso) + 100)
                    'End If
                    VR = Main.OCXFiscal.cierraboleta(1)
                    VR = Main.OCXFiscal.fini()
            Exit Sub
            
errorIF:
    MsgBox "Error :" & Err.Description

End Sub
