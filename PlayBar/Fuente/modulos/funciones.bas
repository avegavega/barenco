Attribute VB_Name = "funciones"
Public objExcel As Excel.Application
Public Sub Aplicar_skin(ByVal Formulario As Form)
    Acceso.Skin1.LoadSkin App.Path & "\st.skn"
    Acceso.Skin1.ApplySkin Formulario.hwnd
End Sub
Public Sub LLenarCombo(Cbo As ComboBox, Campo As String, Indice As String, Tabla As String, Optional ByVal Condicion As String)
    Dim RsCombo As Recordset
    If Condicion = Empty Then
        Sql = "SELECT " & Campo & "," & Indice & "  FROM " & Tabla
    Else
        Sql = "SELECT " & Campo & "," & Indice & "  FROM " & Tabla & " WHERE " & Condicion
    End If
    Call Consulta(RsCombo, Sql)
    If RsCombo.RecordCount > 0 Then
        RsCombo.MoveFirst
        Cbo.Clear
        Do While Not RsCombo.EOF
            Cbo.AddItem RsCombo.Fields(0)
            Cbo.ItemData(Cbo.ListCount - 1) = RsCombo.Fields(1)
            RsCombo.MoveNext
        Loop
    End If
End Sub
Public Sub LLenar_Grilla(Record_Set As ADODB.Recordset, Name_Formulario As Form, Name_ListView As ListView, Con_ChekBox As Boolean, Full_Row_Select As Boolean, Grid_line As Boolean, Mensaje_Grilla_Vacia As Boolean)
On Error GoTo Genera_Error
Dim Contador_i As Integer, Tipo_Dato As Variant, Cantidad_Decimales As Integer, Tipo_Formato As String, mDato As String
Name_Formulario.MousePointer = 11
Name_ListView.ListItems.Clear
Name_ListView.CheckBoxes = Con_ChekBox
If Record_Set.RecordCount > 0 Then
    Record_Set.MoveFirst
    While Not Record_Set.EOF
        For Contador_i = 1 To Record_Set.Fields.Count
            'DEFINE TIPO DE DATO Y FORMATO
            Tipo_Dato = Mid(Name_ListView.ColumnHeaders(Contador_i).Tag, 1, 1)
            Cantidad_Decimales = 0
            If Name_ListView.ColumnHeaders(Contador_i).Tag <> Empty Then Cantidad_Decimales = Mid(Name_ListView.ColumnHeaders(Contador_i).Tag, 4, 1)
            
            If Tipo_Dato = "T" Then
                mDato = ""
                Tipo_Formato = ">"
            End If
            If Tipo_Dato = "N" Then
                mDato = 0
                Tipo_Formato = "#,###0"
            End If
            If Tipo_Dato = "F" Then
                mDato = ""
                Tipo_Formato = "DD-MM-YYYY"
            End If
            If Tipo_Dato = "" Then
                mDato = ""
                Tipo_Formato = ">"
            End If
            
            If Cantidad_Decimales > 0 Then Tipo_Formato = Tipo_Formato & "." & Mid("0000000000", 1, Cantidad_Decimales)
            If Cantidad_Decimales = 9 Then Tipo_Formato = "##0"
            If Not IsNull(Record_Set.Fields(Contador_i - 1)) Then
                mDato = Format(Record_Set.Fields(Contador_i - 1), Tipo_Formato)
            End If
            
            If Contador_i = 1 Then
                Set itmx = Name_ListView.ListItems.Add(, , mDato)
            Else
                itmx.SubItems(Contador_i - 1) = mDato
            End If
        Next Contador_i
        Record_Set.MoveNext
    Wend
Else
    If Mensaje_Grilla_Vacia Then MsgBox "NO EXISTEN REGISTROS PARA VISUALIZAR !!!", vbCritical, mSis
End If
Name_ListView.View = lvwReport
Name_ListView.LabelEdit = lvwManual
Name_ListView.FullRowSelect = Full_Row_Select
Name_ListView.Gridlines = Grid_line
Name_Formulario.MousePointer = 0
Name_Formulario.Refresh
Exit Sub

Genera_Error:
Name_Formulario.MousePointer = 0
MsgBox "SE GENERO EL SIGUIENTE ERROR:" & Chr(13) & _
       UCase(Err.Description) & Chr(13) & _
       "NUMERO: " & Err.Number, vbCritical, mSis
End Sub
Public Function Inicio_Excel() As Boolean
    Dim j As Integer
    Set objExcel = New Excel.Application
    objExcel.Visible = True
    objExcel.SheetsInNewWorkbook = 1
    objExcel.Workbooks.Add
End Function

Public Function Formato_Excel(Num_Campos As Integer, Nombre_Campos() As String) As Boolean
    With objExcel.ActiveSheet
            .Range(.Cells(3, 1), .Cells(3, Num_Campos + 1)).Borders.LineStyle = xlContinuous
            .Range(.Cells(3, 1), .Cells(3, Num_Campos + 1)).Font.Bold = True
        For IG_I = 0 To Num_Campos Step 1
            .Cells(3, IG_I + 1) = Nombre_Campos(IG_I)
        Next IG_I
    End With
End Function

Public Function SoloNumeros(CodAscii As Integer)
    If IsNumeric(Chr(CodAscii)) Or CodAscii = 8 Then 'pas
    Else
        If CodAscii = 13 Then
            SendKeys "{TAB}" 'envia un tab
        Else
            CodAscii = 0 'No acepta el caracter ingresado
        End If
    End If
    SoloNumeros = CodAscii
End Function
Function Consulta(RecSet As ADODB.Recordset, CSql As String)
On Error GoTo falloconexion
            Connection_String = "PROVIDER=MSDASQL;dsn=exactfood;uid=tools_db;pwd=eunice;server=" & S_Servidor '   ' le agrega el path de la base de datos al Connectionstring
            Set cn = New ADODB.Connection '  ' Nuevo objeto Connection
            cn.CursorLocation = adUseClient
            cn.Open Connection_String '  'Abre la conexi�n
            Set RecSet = New ADODB.Recordset '  'Nuevo recordset
            Debug.Print CSql
            RecSet.Open CSql, cn, adOpenStatic, adLockOptimistic '  ' abre
            Exit Function
falloconexion:
MsgBox "fallo la conexion" & vbNewLine & "N� error" & Err.Number & vbNewLine & Err.Description
End Function
Function BuscaIndice(Cbo As ComboBox, Idt As Integer) As Integer
    If Cbo.ListCount > 0 Then
        For i = 0 To Cbo.ListCount - 1
            If Cbo.ItemData(i) = Idt Then
                BuscaIndice = i
                Exit Function
            End If
        Next
    End If
    BuscaIndice = -1
End Function
Public Function AceptaSoloNumeros(CodAscii As Integer)
    If IsNumeric(Chr(CodAscii)) Or CodAscii = 8 Then 'pas
    Else
        If CodAscii = 13 Then
            SendKeys "{TAB}" 'envia un tab
        ElseIf CodAscii = 46 Then 'pase
        Else
            CodAscii = 0 'No acepta el caracter ingresado
        End If
    End If
    AceptaSoloNumeros = CodAscii
End Function

Public Sub ExportarNuevo(LV As ListView, tit() As String, Formulario As Form, Progreso As XP_ProgressBar)
On Error GoTo Genera_Error

Dim Tipo_Dato As String, Cantidad_Decimales As Integer, Tipo_Formato As String, mDato As String

If LV.ListItems.Count = 0 Then Exit Sub
Dim j As Integer
Dim V As Integer
Dim H As Integer
Dim mFec As Date
Call Inicio_Excel
Formulario.MousePointer = 11

objExcel.Visible = False
With objExcel.ActiveSheet
    .Range(.Cells(1, 1), .Cells(3, 1)).Font.Bold = True
    .Range(.Cells(1, 1), .Cells(1, 1)).Font.Size = 14
    .Range(.Cells(2, 1), .Cells(3, 1)).Font.Size = 12
    .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Font.Bold = True
    .Range(.Cells(5, 1), .Cells(5, LV.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
    .Cells(1, 1) = UCase(tit(0))
    .Cells(2, 1) = UCase(tit(1))
    .Cells(3, 1) = UCase(tit(2))
   ' MsgBox UBound(tit)
End With
For j = 1 To LV.ColumnHeaders.Count
    objExcel.ActiveSheet.Cells(5, j) = LV.ColumnHeaders(j).Text
    objExcel.ActiveSheet.Columns(j).ColumnWidth = (LV.ColumnHeaders(j).Width) / 100
Next j
Progreso.Min = 1
If LV.ListItems.Count = 1 Then Progreso.Max = 10 Else Progreso.Max = LV.ListItems.Count

For V = 1 To LV.ListItems.Count
    LbProgreso = Format(V / Progreso.Max * 100, "###") & "%"
    Progreso.Value = V
    H = 1
    If Mid(LV.ColumnHeaders(H).Tag, 1, 1) = "F" Then
        If Len(LV.ListItems(V)) = 0 Then
            'ignoramos si no tiene valor la fecha
        Else
            mFec = LV.ListItems(V)
            objExcel.ActiveSheet.Cells(V + 5, H) = mFec
        End If
    ElseIf Mid(LV.ColumnHeaders(H).Tag, 1, 1) = "N" Then
        If Val(LV.ListItems(V)) > 0 Then objExcel.ActiveSheet.Cells(V + 5, H) = CDbl(LV.ListItems(V))
    Else
        objExcel.ActiveSheet.Cells(V + 5, H) = LV.ListItems(V)
    End If
    For H = 1 To LV.ColumnHeaders.Count - 1
        If Mid(LV.ColumnHeaders(H + 1).Tag, 1, 1) = "F" Then
            If Len(LV.ListItems(V).SubItems(H)) > 0 Then
              '  If Not IsDate(Len(LV.ListItems(V).SubItems(H))) Then
                    'ignoramos si no tiene valor la fecha
              '  Else
                    On Error Resume Next
                    mFec = LV.ListItems(V).SubItems(H)
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = mFec
              '  End If
            End If
        ElseIf Mid(LV.ColumnHeaders(H + 1).Tag, 1, 1) = "N" Then
            If LV.ListItems(V).SubItems(H) = "" Then
                objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
            Else
                If Not IsNumeric(LV.ListItems(V).SubItems(H)) Then
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
                Else
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = CDbl(LV.ListItems(V).SubItems(H))
                End If
            End If
        Else
            objExcel.ActiveSheet.Cells(V + 5, H + 1) = LV.ListItems(V).SubItems(H)
        End If
    Next H
Next V
'FORMATO COLUMNAS
For H = 1 To LV.ColumnHeaders.Count
    Tipo_Dato = Mid(LV.ColumnHeaders(H).Tag, 1, 1)
    If Len(LV.ColumnHeaders(H).Tag) = 4 Then Cantidad_Decimales = Mid(LV.ColumnHeaders(H).Tag, 4, 1)
    If Tipo_Dato = "N" Then
        Tipo_Formato = "#,##0"
        If Cantidad_Decimales > 0 Then Tipo_Formato = Tipo_Formato & "." & Mid("0000000000", 1, Cantidad_Decimales)
        If Cantidad_Decimales = 9 Then Tipo_Formato = "###0"
        With objExcel.ActiveSheet
            .Range(.Cells(5, H), .Cells(V + 8, H)).NumberFormat = Tipo_Formato
        End With
    End If
Next H

objExcel.Visible = True
Formulario.MousePointer = 0

Exit Sub

Genera_Error:
Formulario.MousePointer = 0
MsgBox "SE GENERO EL SIGUIENTE ERROR:" & Chr(13) & _
       UCase(Err.Description) & Chr(13) & _
       "NUMERO: " & Err.Number, vbCritical, mSis

End Sub


