VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form DImpresoras 
   BackColor       =   &H00000040&
   Caption         =   "Designar Impresoras"
   ClientHeight    =   6720
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   11490
   Icon            =   "DImpresoras.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   11490
   Begin VB.Frame Frame3 
      Caption         =   "1er Nivel"
      Height          =   5295
      Left            =   7680
      TabIndex        =   26
      Top             =   480
      Width           =   3135
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   10
         Left            =   720
         TabIndex        =   34
         Top             =   4680
         Width           =   1572
      End
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   9
         Left            =   600
         TabIndex        =   33
         Top             =   3360
         Width           =   1572
      End
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   8
         Left            =   600
         TabIndex        =   32
         Top             =   2160
         Width           =   1572
      End
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   7
         Left            =   600
         TabIndex        =   31
         Top             =   960
         Width           =   1572
      End
      Begin VB.Label Label13 
         Caption         =   "Label13"
         DataField       =   "ImpresoraInternos"
         DataSource      =   "AdoConfigImp"
         Height          =   255
         Left            =   720
         TabIndex        =   38
         Top             =   4320
         Width           =   1815
      End
      Begin VB.Label Label12 
         Caption         =   "Label12"
         DataField       =   "ImpresoraCafe"
         DataSource      =   "AdoConfigImp"
         Height          =   255
         Left            =   600
         TabIndex        =   37
         Top             =   3000
         Width           =   1935
      End
      Begin VB.Label Label11 
         Caption         =   "Label11"
         DataField       =   "ImpresoraBar"
         DataSource      =   "AdoConfigImp"
         Height          =   255
         Left            =   600
         TabIndex        =   36
         Top             =   1800
         Width           =   1935
      End
      Begin VB.Label Label10 
         Caption         =   "Label10"
         DataField       =   "ImpresoraCocina"
         DataSource      =   "AdoConfigImp"
         Height          =   255
         Left            =   600
         TabIndex        =   35
         Top             =   600
         Width           =   1935
      End
      Begin VB.Label Label9 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Internos"
         Height          =   1095
         Left            =   120
         TabIndex        =   30
         Top             =   4080
         Width           =   2535
      End
      Begin VB.Label Label8 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cafeteria"
         Height          =   1095
         Left            =   120
         TabIndex        =   29
         Top             =   2760
         Width           =   2535
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Bar"
         Height          =   1095
         Left            =   120
         TabIndex        =   28
         Top             =   1560
         Width           =   2535
      End
      Begin VB.Label Label6 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cocina"
         Height          =   1095
         Left            =   120
         TabIndex        =   27
         Top             =   360
         Width           =   2535
      End
   End
   Begin MSAdodcLib.Adodc AdoConfigImp 
      Height          =   330
      Left            =   120
      Top             =   120
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Impresoras"
      Caption         =   "ConfImpresoras"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      Caption         =   "Seleccione Impresoras"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6495
      Left            =   360
      TabIndex        =   0
      Top             =   120
      Width           =   6975
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   6
         Left            =   5280
         TabIndex        =   25
         Top             =   6000
         Width           =   1572
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H000080FF&
         Caption         =   "Impresoras Instaladas en el Sistema"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1932
         Left            =   480
         TabIndex        =   4
         Top             =   360
         Visible         =   0   'False
         Width           =   5652
         Begin VB.CommandButton CmdConfirma 
            Caption         =   "Cancelar"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   1
            Left            =   3000
            TabIndex        =   22
            Top             =   1320
            Width           =   1572
         End
         Begin VB.CommandButton CmdConfirma 
            Caption         =   "Aceptar"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   0
            Left            =   960
            TabIndex        =   21
            Top             =   1320
            Width           =   1572
         End
         Begin VB.ComboBox CboImpresoras 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   384
            ItemData        =   "DImpresoras.frx":030A
            Left            =   240
            List            =   "DImpresoras.frx":030C
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   600
            Width           =   5172
         End
      End
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   5
         Left            =   5280
         TabIndex        =   20
         Top             =   5280
         Width           =   1572
      End
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   4
         Left            =   5280
         TabIndex        =   19
         Top             =   4440
         Width           =   1572
      End
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   3
         Left            =   5280
         TabIndex        =   13
         Top             =   3600
         Width           =   1572
      End
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   2
         Left            =   5280
         TabIndex        =   9
         Top             =   2640
         Visible         =   0   'False
         Width           =   1572
      End
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   1
         Left            =   5280
         TabIndex        =   8
         Top             =   1680
         Visible         =   0   'False
         Width           =   1572
      End
      Begin VB.CommandButton CmdCambiarImp 
         Caption         =   "Seleccionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Index           =   0
         Left            =   5280
         TabIndex        =   3
         Top             =   720
         Visible         =   0   'False
         Width           =   1572
      End
      Begin VB.Label Label5 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label5"
         DataField       =   "PrintCafe"
         DataSource      =   "AdoConfigImp"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   24
         Top             =   6000
         Width           =   5055
      End
      Begin VB.Label Label1 
         Caption         =   "Impresora para Cafeteria"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   375
         Index           =   6
         Left            =   120
         TabIndex        =   23
         Top             =   5640
         Width           =   3735
      End
      Begin VB.Label Label4 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label4"
         DataField       =   "PrintInternos"
         DataSource      =   "AdoConfigImp"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   18
         Top             =   5280
         Width           =   5055
      End
      Begin VB.Label Label3 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label3"
         DataField       =   "PrintBar"
         DataSource      =   "AdoConfigImp"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   17
         Top             =   4440
         Width           =   5055
      End
      Begin VB.Label Label1 
         Caption         =   "Impresora para Vales Internos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   375
         Index           =   5
         Left            =   120
         TabIndex        =   16
         Top             =   4920
         Width           =   3735
      End
      Begin VB.Label Label1 
         Caption         =   "Impresora para Comandas ""2"""
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   375
         Index           =   4
         Left            =   120
         TabIndex        =   15
         Top             =   4080
         Width           =   3735
      End
      Begin VB.Label Label2 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label2"
         DataField       =   "PrintCocina"
         DataSource      =   "AdoConfigImp"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   120
         TabIndex        =   14
         Top             =   3600
         Width           =   5052
      End
      Begin VB.Label Label1 
         Caption         =   "Impresora para Comandas ""1"""
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   372
         Index           =   3
         Left            =   120
         TabIndex        =   12
         Top             =   3240
         Width           =   3732
      End
      Begin VB.Label LBinformes 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label2"
         DataSource      =   "AdoConfigImp"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   120
         TabIndex        =   11
         Top             =   2640
         Visible         =   0   'False
         Width           =   5052
      End
      Begin VB.Label Label1 
         Caption         =   "Impresora para Informeres Varios"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   372
         Index           =   2
         Left            =   120
         TabIndex        =   10
         Top             =   2280
         Width           =   3732
      End
      Begin VB.Label LBInfoTurno 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label2"
         DataSource      =   "AdoConfigImp"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   120
         TabIndex        =   7
         Top             =   1680
         Visible         =   0   'False
         Width           =   5052
      End
      Begin VB.Label Label1 
         Caption         =   "Impresora para rendición de Turno"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   372
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   1320
         Width           =   3732
      End
      Begin VB.Label LBImpVales 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label3"
         DataSource      =   "AdoConfigImp"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   372
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Visible         =   0   'False
         Width           =   5052
      End
      Begin VB.Label Label1 
         Caption         =   "Impresora para Ticket o Boletas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   372
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   3732
      End
   End
End
Attribute VB_Name = "DImpresoras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim queImp As Integer
Private Sub CmdCambiarImp_Click(Index As Integer)
    For i = 0 To 10
        CmdCambiarImp(i).Enabled = False
    Next
    queImp = Index
    Frame2.Visible = True
End Sub

Private Sub CmdConfirma_Click(Index As Integer)
    Item = CboImpresoras.ListIndex
    If Index = 0 Then
        If Item > -1 Then
           ' If queImp = 0 Then AdoConfigImp.Recordset.Fields(13) = CboImpresoras.List(Item)
           ' If queImp = 1 Then AdoConfigImp.Recordset.Fields(14) = CboImpresoras.List(Item)
           ' If queImp = 2 Then AdoConfigImp.Recordset.Fields(15) = CboImpresoras.List(Item)
            
            If queImp = 3 Then AdoConfigImp.Recordset.Fields(2) = CboImpresoras.List(Item)
            If queImp = 4 Then AdoConfigImp.Recordset.Fields(1) = CboImpresoras.List(Item)
            If queImp = 5 Then AdoConfigImp.Recordset.Fields(3) = CboImpresoras.List(Item)
            If queImp = 6 Then AdoConfigImp.Recordset.Fields(0) = CboImpresoras.List(Item)
            
            If queImp = 7 Then AdoConfigImp.Recordset.Fields(6) = CboImpresoras.List(Item)
            If queImp = 8 Then AdoConfigImp.Recordset.Fields(5) = CboImpresoras.List(Item)
            If queImp = 9 Then AdoConfigImp.Recordset.Fields(4) = CboImpresoras.List(Item)
            If queImp = 10 Then AdoConfigImp.Recordset.Fields(7) = CboImpresoras.List(Item)
            
            AdoConfigImp.Recordset.MoveFirst
        
        End If
    End If
    For i = 0 To 10
        CmdCambiarImp(i).Enabled = True
    Next
    Frame2.Visible = False
End Sub

Private Sub Form_Load()
Dim X As Printer
For Each X In Printers
     CboImpresoras.AddItem X.DeviceName
Next
DImpresoras.Height = 7100
DImpresoras.Width = 8010
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Configuracion.Visible = True
    Configuracion.WindowState = 2
End Sub
