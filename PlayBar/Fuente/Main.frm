VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Object = "{531C1877-C9DA-4778-97E9-91EA75A2F898}#1.0#0"; "ocxsam350.ocx"
Begin VB.Form Main 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00000040&
   Caption         =   "Sistema de Ventas With MySql Data Base"
   ClientHeight    =   10095
   ClientLeft      =   2715
   ClientTop       =   690
   ClientWidth     =   15405
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Main.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MouseIcon       =   "Main.frx":0442
   MousePointer    =   99  'Custom
   Moveable        =   0   'False
   ScaleHeight     =   10095
   ScaleWidth      =   15405
   WindowState     =   2  'Maximized
   Begin OCXSAM350Lib.Ocxsam350 OCXFiscal 
      Height          =   270
      Left            =   7770
      TabIndex        =   76
      Top             =   9195
      Width           =   270
      _Version        =   65536
      _ExtentX        =   476
      _ExtentY        =   476
      _StockProps     =   0
   End
   Begin VB.ListBox ListaTemporal 
      Height          =   300
      Left            =   0
      TabIndex        =   73
      Top             =   10440
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.Timer TmrRobot 
      Enabled         =   0   'False
      Interval        =   5000
      Left            =   0
      Top             =   4800
   End
   Begin MSCommLib.MSComm Tf6 
      Left            =   7560
      Top             =   9960
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   -1  'True
      RThreshold      =   1
      RTSEnable       =   -1  'True
      BaudRate        =   19200
      SThreshold      =   1
   End
   Begin VB.CommandButton Command3 
      Height          =   735
      Left            =   0
      Picture         =   "Main.frx":074C
      Style           =   1  'Graphical
      TabIndex        =   63
      Top             =   7920
      Width           =   255
   End
   Begin VB.CommandButton Command2 
      Height          =   735
      Left            =   0
      Picture         =   "Main.frx":0CD6
      Style           =   1  'Graphical
      TabIndex        =   62
      Top             =   7080
      Width           =   252
   End
   Begin VB.CommandButton CmdComando 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Index           =   5
      Left            =   6360
      TabIndex        =   55
      ToolTipText     =   "Porcentaje de Ocupacion"
      Top             =   9360
      Width           =   1335
   End
   Begin VB.CommandButton CmdComando 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Index           =   4
      Left            =   4920
      Picture         =   "Main.frx":1260
      Style           =   1  'Graphical
      TabIndex        =   49
      ToolTipText     =   "Vistazo rapido del estado de las mesas"
      Top             =   9360
      Width           =   1452
   End
   Begin VB.FileListBox File1 
      Appearance      =   0  'Flat
      Height          =   270
      Left            =   3000
      TabIndex        =   46
      Top             =   5040
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.PictureBox Picture4 
      BackColor       =   &H00000040&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5652
      Left            =   0
      ScaleHeight     =   5595
      ScaleWidth      =   4995
      TabIndex        =   27
      Top             =   0
      Width           =   5052
      Begin VB.CommandButton cmdAlternativa 
         Caption         =   "Alt"
         Height          =   495
         Left            =   4440
         TabIndex        =   75
         Top             =   3120
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton cmdZoom 
         Caption         =   "zoom"
         Height          =   495
         Left            =   4350
         TabIndex        =   74
         Top             =   3840
         Width           =   735
      End
      Begin VB.CommandButton CmdOut 
         Caption         =   "Out"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4440
         TabIndex        =   69
         Top             =   1800
         Width           =   495
      End
      Begin VB.CommandButton CmdCentroProduccion 
         BackColor       =   &H8000000A&
         Height          =   495
         Left            =   4440
         Picture         =   "Main.frx":1A86
         Style           =   1  'Graphical
         TabIndex        =   64
         Top             =   2400
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.ListBox List2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         ItemData        =   "Main.frx":1B88
         Left            =   4440
         List            =   "Main.frx":1B8A
         TabIndex        =   60
         ToolTipText     =   "Garzon - Cantidad de Mesas"
         Top             =   720
         Width           =   500
      End
      Begin VB.PictureBox Picture5 
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         Height          =   480
         Index           =   0
         Left            =   4440
         Picture         =   "Main.frx":1B8C
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   59
         Top             =   120
         Width           =   480
      End
      Begin VB.CommandButton CmdSaca 
         BackColor       =   &H80000000&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3360
         Picture         =   "Main.frx":1FCE
         Style           =   1  'Graphical
         TabIndex        =   38
         ToolTipText     =   "Elimina Item seleccionado"
         Top             =   240
         Width           =   852
      End
      Begin VB.PictureBox Picture3 
         BackColor       =   &H00000040&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   0
         OLEDropMode     =   1  'Manual
         Picture         =   "Main.frx":2410
         ScaleHeight     =   1155
         ScaleWidth      =   4875
         TabIndex        =   28
         Top             =   4440
         Width           =   4932
         Begin VB.TextBox TxtNpersonas 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   720
            TabIndex        =   31
            Top             =   720
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.PictureBox Picture5 
            DrawStyle       =   4  'Dash-Dot-Dot
            FillStyle       =   3  'Vertical Line
            FontTransparent =   0   'False
            Height          =   330
            Index           =   1
            Left            =   960
            Picture         =   "Main.frx":2D70
            ScaleHeight     =   270
            ScaleWidth      =   495
            TabIndex        =   61
            ToolTipText     =   "Listado de garzones"
            Top             =   120
            Width           =   555
         End
         Begin VB.CommandButton CmdBusqueda 
            Height          =   355
            Left            =   2040
            Picture         =   "Main.frx":3BC2
            Style           =   1  'Graphical
            TabIndex        =   54
            Top             =   720
            Width           =   385
         End
         Begin VB.TextBox TxtCodigo 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   2520
            TabIndex        =   34
            Text            =   "0"
            Top             =   720
            Width           =   720
         End
         Begin VB.VScrollBar ScrollCant 
            Height          =   372
            Left            =   1800
            Max             =   1
            Min             =   500
            TabIndex        =   45
            Top             =   720
            Value           =   1
            Width           =   135
         End
         Begin VB.TextBox TxtMesaSel 
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   27.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   675
            IMEMode         =   3  'DISABLE
            Left            =   0
            MaxLength       =   3
            TabIndex        =   29
            Text            =   "0"
            Top             =   360
            Width           =   975
         End
         Begin VB.TextBox TxtCantidad 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Left            =   1320
            MaxLength       =   2
            TabIndex        =   33
            Top             =   720
            Width           =   495
         End
         Begin VB.TextBox txtGarzonSel 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1800
            MaxLength       =   25
            TabIndex        =   30
            Top             =   120
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.Label lbTconDesc 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   252
            Left            =   3480
            TabIndex        =   66
            Top             =   360
            Width           =   1332
         End
         Begin VB.Label LBTotal 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   20.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   432
            Left            =   3360
            TabIndex        =   35
            Top             =   600
            Width           =   1452
         End
         Begin VB.Label Label6 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "TOTAL"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1092
            Index           =   2
            Left            =   3360
            TabIndex        =   36
            Top             =   0
            Width           =   1572
         End
         Begin VB.Label Label6 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "N.Pers."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000006&
            Height          =   252
            Index           =   1
            Left            =   600
            TabIndex        =   65
            Top             =   480
            Visible         =   0   'False
            Width           =   732
         End
         Begin VB.Label Label6 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Codigo"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   10.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000006&
            Height          =   252
            Index           =   4
            Left            =   2400
            TabIndex        =   52
            Top             =   480
            Width           =   852
         End
         Begin VB.Label Label6 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "ME&SA"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Index           =   0
            Left            =   0
            TabIndex        =   37
            Top             =   0
            Width           =   975
         End
         Begin VB.Label Label6 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Cant"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000006&
            Height          =   252
            Index           =   3
            Left            =   1440
            TabIndex        =   32
            Top             =   480
            Width           =   492
         End
         Begin VB.Label lbgarcia 
            BackColor       =   &H80000007&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label11"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0000FFFF&
            Height          =   300
            Left            =   1560
            TabIndex        =   71
            Top             =   120
            Width           =   2172
         End
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   3735
         Index           =   0
         IntegralHeight  =   0   'False
         Left            =   0
         Style           =   1  'Checkbox
         TabIndex        =   39
         Top             =   600
         Width           =   4332
      End
      Begin VB.Label Label9 
         BackColor       =   &H8000000D&
         BackStyle       =   0  'Transparent
         Caption         =   "Archivo de Ayuda"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   360
         Left            =   2640
         MouseIcon       =   "Main.frx":3F75
         TabIndex        =   58
         Top             =   0
         Width           =   1212
      End
      Begin VB.Label Label1 
         BackColor       =   &H00000040&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Consumo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   375
         Index           =   1
         Left            =   0
         TabIndex        =   41
         Top             =   0
         Width           =   4695
      End
      Begin VB.Label Label7 
         BackColor       =   &H8000000C&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "        Cant       Precio         Descripci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   252
         Index           =   0
         Left            =   0
         TabIndex        =   40
         Top             =   360
         Width           =   4332
      End
   End
   Begin MSAdodcLib.Adodc AdoEgresos 
      Height          =   330
      Left            =   5400
      Top             =   6840
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Egresos"
      Caption         =   "Egresos"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc AdoPedidos 
      Height          =   330
      Left            =   4440
      Top             =   5400
      Visible         =   0   'False
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   582
      ConnectMode     =   3
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   2
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   100
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Pedidos"
      Caption         =   "Pedido"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc AdoSoloImpresoras 
      Height          =   330
      Left            =   1080
      Top             =   9840
      Visible         =   0   'False
      Width           =   1560
      _ExtentX        =   2752
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Impresoras"
      Caption         =   "Impresoras"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CheckBox Dscto 
      Caption         =   "Descto"
      DataField       =   "Descuento"
      DataSource      =   "AdoConfiguracion"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   192
      Left            =   2400
      TabIndex        =   17
      Top             =   10800
      Visible         =   0   'False
      Width           =   972
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   0
      Top             =   6240
   End
   Begin VB.CommandButton CmdComando 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Index           =   2
      Left            =   2640
      Picture         =   "Main.frx":43B7
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "Actualizar estado de Mesas"
      Top             =   9360
      Width           =   975
   End
   Begin VB.CommandButton CmdComando 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Index           =   1
      Left            =   1560
      Picture         =   "Main.frx":4972
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   9360
      Width           =   1095
   End
   Begin VB.CommandButton CmdComando 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Index           =   0
      Left            =   480
      Picture         =   "Main.frx":502E
      Style           =   1  'Graphical
      TabIndex        =   14
      ToolTipText     =   "Ticket, Dscto, cambio de mesa, Unir mesas"
      Top             =   9360
      Width           =   1095
   End
   Begin VB.CheckBox Garzones 
      Caption         =   "Garzones"
      DataField       =   "CuadroGarzones"
      DataSource      =   "AdoConfiguracion"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   192
      Left            =   3720
      TabIndex        =   13
      Top             =   10080
      Visible         =   0   'False
      Width           =   972
   End
   Begin MSAdodcLib.Adodc AdoGarzon 
      Height          =   330
      Left            =   2880
      Top             =   6360
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "select * from personal order by codigo"
      Caption         =   "Garzones"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc AdoAllProductos 
      Height          =   330
      Left            =   2640
      Top             =   6000
      Visible         =   0   'False
      Width           =   2280
      _ExtentX        =   4022
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   2
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   $"Main.frx":5717
      Caption         =   "Productos"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoConRubros 
      Height          =   330
      Left            =   480
      Top             =   6600
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      ConnectMode     =   12
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   1
      LockType        =   4
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "par_rubros_venta"
      Caption         =   "Rubros"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc AdoConfiguracion 
      Height          =   330
      Left            =   720
      Top             =   6960
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "Config"
      Caption         =   "Configuracion"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.PictureBox PictRubros 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5652
      Left            =   5040
      ScaleHeight     =   5595
      ScaleWidth      =   3075
      TabIndex        =   1
      Top             =   0
      Width           =   3135
      Begin VB.CommandButton CMDrubros 
         Appearance      =   0  'Flat
         Caption         =   "Rubros"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   0
         Left            =   0
         MaskColor       =   &H8000000E&
         TabIndex        =   2
         Top             =   0
         Width           =   3075
      End
   End
   Begin VB.PictureBox Picture2 
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   10215
      Left            =   8280
      ScaleHeight     =   10155
      ScaleWidth      =   6915
      TabIndex        =   8
      Top             =   0
      Width           =   6975
      Begin VB.CommandButton CmdSubir 
         Height          =   495
         Left            =   6240
         Picture         =   "Main.frx":57C0
         Style           =   1  'Graphical
         TabIndex        =   68
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton CmdAbajo 
         Height          =   495
         Left            =   5520
         Picture         =   "Main.frx":5ACA
         Style           =   1  'Graphical
         TabIndex        =   67
         Top             =   9480
         Width           =   495
      End
      Begin VB.OptionButton OptClick 
         BackColor       =   &H00000080&
         Caption         =   "Carga Con Un Click"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   312
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   9720
         Value           =   -1  'True
         Width           =   1935
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid LTxRubro 
         Height          =   8775
         Index           =   0
         Left            =   0
         TabIndex        =   10
         Top             =   600
         Width           =   6855
         _ExtentX        =   12091
         _ExtentY        =   15478
         _Version        =   393216
         BackColor       =   -2147483644
         Rows            =   1
         Cols            =   5
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   400
         AllowBigSelection=   0   'False
         FocusRect       =   2
         HighLight       =   2
         GridLinesFixed  =   1
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   5
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   4
      End
      Begin VB.Label Label11 
         BackColor       =   &H00000040&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   375
         Left            =   2760
         TabIndex        =   72
         Top             =   0
         Width           =   1335
      End
      Begin VB.Label Label7 
         BackColor       =   &H8000000C&
         Caption         =   "Codigo             Descripci�n                         Precio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   210
         Index           =   1
         Left            =   0
         TabIndex        =   44
         Top             =   360
         Width           =   6855
      End
      Begin VB.Label Label5 
         BackColor       =   &H00000040&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Descripcion"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   375
         Index           =   0
         Left            =   0
         TabIndex        =   9
         Top             =   0
         Width           =   6855
      End
   End
   Begin VB.CommandButton CmdComando 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Index           =   3
      Left            =   3600
      Picture         =   "Main.frx":5DD4
      Style           =   1  'Graphical
      TabIndex        =   47
      ToolTipText     =   "Re-Imprime el ultimo ticket emitido"
      Top             =   9360
      Width           =   1335
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H80000012&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3300
      Left            =   360
      ScaleHeight     =   3240
      ScaleWidth      =   7635
      TabIndex        =   0
      Top             =   5760
      Width           =   7695
      Begin MSAdodcLib.Adodc AdoTurnos 
         Height          =   330
         Left            =   480
         Top             =   120
         Visible         =   0   'False
         Width           =   1920
         _ExtentX        =   3387
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   2
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   3
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "DSN=exactfood"
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "exactfood"
         OtherAttributes =   ""
         UserName        =   "tools_db"
         Password        =   "eunice"
         RecordSource    =   "turnos"
         Caption         =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.Timer TimerInactivo 
         Interval        =   1
         Left            =   3000
         Top             =   1560
      End
      Begin MSAdodcLib.Adodc AdoQuitados 
         Height          =   330
         Left            =   4920
         Top             =   1440
         Visible         =   0   'False
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   2
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   3
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "DSN=exactfood"
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "exactfood"
         OtherAttributes =   ""
         UserName        =   "tools_db"
         Password        =   "eunice"
         RecordSource    =   "Quitados"
         Caption         =   "Quitados"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc AdoDescuentos 
         Height          =   330
         Left            =   5160
         Top             =   360
         Visible         =   0   'False
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   2
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   3
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "DSN=exactfood"
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "exactfood"
         OtherAttributes =   ""
         UserName        =   "tools_db"
         Password        =   "eunice"
         RecordSource    =   "Descuento"
         Caption         =   "Descuentos"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.Label LbAutorizador 
         Caption         =   "Fecha descuento"
         DataField       =   "Fecha"
         DataSource      =   "AdoDescuentos"
         Height          =   255
         Left            =   5160
         TabIndex        =   57
         Top             =   120
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label Label8 
         Caption         =   "Label8"
         DataField       =   "CodigoProducto"
         DataSource      =   "AdoQuitados"
         Height          =   255
         Left            =   1680
         TabIndex        =   56
         Top             =   120
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Label2"
         DataSource      =   "AdoVales"
         Height          =   255
         Left            =   3960
         TabIndex        =   53
         Top             =   960
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label LBmesa 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   0
         Left            =   0
         TabIndex        =   3
         Top             =   0
         Width           =   735
      End
   End
   Begin VB.Label Label10 
      Caption         =   "Label10"
      DataField       =   "PrintCafe"
      DataSource      =   "AdoSoloImpresoras"
      Height          =   255
      Left            =   2520
      TabIndex        =   70
      Top             =   9840
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label NCol 
      Caption         =   "0"
      DataField       =   "ColumnasTicket"
      DataSource      =   "AdoConfiguracion"
      Height          =   255
      Left            =   6360
      TabIndex        =   51
      Top             =   10080
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Label LBlevelControl 
      Caption         =   "2"
      DataField       =   "NivelSistema"
      DataSource      =   "AdoConfiguracion"
      Height          =   375
      Left            =   7080
      TabIndex        =   50
      Top             =   10080
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Label LBruta 
      Caption         =   "Label2"
      DataField       =   "Directorio"
      DataSource      =   "AdoConfiguracion"
      Height          =   750
      Left            =   3600
      TabIndex        =   48
      Top             =   9360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label LBCard 
      Caption         =   "TarjetaDes"
      DataField       =   "DescuentoCard"
      DataSource      =   "AdoConfiguracion"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Left            =   1800
      TabIndex        =   43
      Top             =   9360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label LBegreso 
      Caption         =   "Egreso"
      DataField       =   "Concepto"
      DataSource      =   "AdoEgresos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Left            =   6960
      TabIndex        =   42
      Top             =   9240
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label LBfoxTicket 
      Caption         =   "npedidoTicket"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6000
      TabIndex        =   26
      Top             =   6720
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label LBBfactura 
      Caption         =   "Factura"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8040
      TabIndex        =   25
      Top             =   10320
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label LBnguia 
      Caption         =   "Guia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6480
      TabIndex        =   24
      Top             =   7560
      Width           =   855
   End
   Begin VB.Label LBBboleta 
      Caption         =   "Boleta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6120
      TabIndex        =   23
      Top             =   6480
      Width           =   855
   End
   Begin VB.Label LbBticketSalida 
      Caption         =   "Ticket"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   252
      Left            =   6240
      TabIndex        =   22
      Top             =   5160
      Width           =   852
   End
   Begin VB.Label LBBpedido 
      Caption         =   "Pedido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6600
      TabIndex        =   21
      Top             =   4920
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label LBprinter 
      Caption         =   "Print Vale"
      DataField       =   "PrintVales"
      DataSource      =   "AdoConfiguracion"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3480
      TabIndex        =   20
      Top             =   10800
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label LBpedido 
      Caption         =   "Pedido"
      DataField       =   "NPedido"
      DataSource      =   "AdoPedidos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4560
      TabIndex        =   19
      Top             =   6240
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label LBcodMesa 
      Caption         =   "CodMesa"
      DataField       =   "CodMesa"
      DataSource      =   "AdoEstadoM"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6480
      TabIndex        =   18
      Top             =   6120
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label LBnGarzon 
      Caption         =   "Garzon"
      DataField       =   "Nombre"
      DataSource      =   "AdoGarzon"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Left            =   2760
      TabIndex        =   12
      Top             =   9360
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "Label4"
      DataField       =   "Nombre"
      DataSource      =   "AdoTurnos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4800
      TabIndex        =   7
      Top             =   10080
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      DataField       =   "Descripcion"
      DataSource      =   "AdoAllProductos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5640
      TabIndex        =   6
      Top             =   10080
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label lbmesas 
      Caption         =   "Nmesas"
      DataField       =   "NumeroMesas"
      DataSource      =   "AdoConfiguracion"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Top             =   10800
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label LBrubros 
      Caption         =   "Rubros"
      DataField       =   "rub_nombre"
      DataSource      =   "adoConRubros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1440
      TabIndex        =   4
      Top             =   10800
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Menu manager 
      Caption         =   "&Gerencia"
      Begin VB.Menu edprecios 
         Caption         =   "Editar lista Precios"
      End
      Begin VB.Menu sep2 
         Caption         =   "-"
      End
      Begin VB.Menu fin 
         Caption         =   "&Salir"
      End
   End
   Begin VB.Menu manProductos 
      Caption         =   "&Productos"
      Begin VB.Menu masterProd 
         Caption         =   "M&aestro de Productos"
      End
      Begin VB.Menu recetas 
         Caption         =   "Maestro de &Recetas"
         Visible         =   0   'False
      End
      Begin VB.Menu introInsumo 
         Caption         =   "Ingreso de Mercader�as"
         Visible         =   0   'False
      End
      Begin VB.Menu mproveedores 
         Caption         =   "Maestro Proveedores"
         Visible         =   0   'False
      End
      Begin VB.Menu ManInformes 
         Caption         =   "&Informes de Ventas"
         Visible         =   0   'False
      End
      Begin VB.Menu Mpersonal 
         Caption         =   "M&aestro de Personal"
      End
      Begin VB.Menu mnP 
         Caption         =   "Productos"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu manconfig 
      Caption         =   "&Configurar"
      Begin VB.Menu config 
         Caption         =   "Configurar &Sistema"
      End
      Begin VB.Menu menucorrelativos 
         Caption         =   "&Correlativos"
      End
      Begin VB.Menu extranjeras 
         Caption         =   "Monedas Extranjeras"
      End
   End
   Begin VB.Menu mainturno 
      Caption         =   "Datos del Turno"
      Begin VB.Menu TurnoTurno 
         Caption         =   "Turno"
      End
      Begin VB.Menu TurnoNombre 
         Caption         =   "Nombre"
      End
      Begin VB.Menu TurnoFecha 
         Caption         =   "Fecha"
      End
      Begin VB.Menu TurnoInicio 
         Caption         =   "Inicio"
      End
      Begin VB.Menu TurnoEfeInicial 
         Caption         =   "Efectivo Inicial"
      End
      Begin VB.Menu comenta 
         Caption         =   "Comentario"
      End
      Begin VB.Menu InformesPrinter 
         Caption         =   "Impresora Informes"
      End
      Begin VB.Menu fiscalprint 
         Caption         =   "Impresora Fiscal"
      End
      Begin VB.Menu ImpVales 
         Caption         =   "Impresora Vales"
      End
      Begin VB.Menu ImpCocina 
         Caption         =   "Imprime Cocina"
      End
      Begin VB.Menu ImpCafeteria 
         Caption         =   "Impresora Cafeteria"
      End
      Begin VB.Menu ImpBarra 
         Caption         =   "Imprime Barra"
      End
      Begin VB.Menu ImpTotal 
         Caption         =   "Centro Produccion"
      End
      Begin VB.Menu CargaMouse 
         Caption         =   "Carga con Mouse"
      End
      Begin VB.Menu MnAutDesc 
         Caption         =   "AutoDescuento"
      End
      Begin VB.Menu sep 
         Caption         =   "-"
      End
      Begin VB.Menu NboletaFiscal 
         Caption         =   "Boleta Fiscal"
      End
      Begin VB.Menu sep10 
         Caption         =   "-"
      End
      Begin VB.Menu MnIdEquipo 
         Caption         =   "IdEquipp"
      End
   End
   Begin VB.Menu cmds 
      Caption         =   "C&omandos"
      Begin VB.Menu test 
         Caption         =   "Marca Mesa"
         Shortcut        =   {F12}
      End
      Begin VB.Menu cobramesa 
         Caption         =   "CobraMesa"
         Shortcut        =   {F11}
      End
      Begin VB.Menu veturno 
         Caption         =   "Turno"
         Shortcut        =   {F9}
      End
      Begin VB.Menu refresca 
         Caption         =   "Actualizar"
         Shortcut        =   {F8}
      End
      Begin VB.Menu enviar 
         Caption         =   "Enviar a  centros de Producci�n"
         Shortcut        =   {F5}
      End
      Begin VB.Menu QuitaItem 
         Caption         =   "Sacar Item seleccionado"
         Shortcut        =   {F6}
      End
      Begin VB.Menu Recorre 
         Caption         =   "Buscar Producto"
         Shortcut        =   ^B
      End
   End
   Begin VB.Menu claves 
      Caption         =   "Claves"
      Visible         =   0   'False
      Begin VB.Menu clavegeneral 
         Caption         =   "General"
      End
      Begin VB.Menu clavecaja 
         Caption         =   "caja"
      End
      Begin VB.Menu claveadicion 
         Caption         =   "adicion"
      End
      Begin VB.Menu clavesuper1 
         Caption         =   "Supervisor1"
      End
      Begin VB.Menu clavesuper2 
         Caption         =   "Supervisor2"
      End
      Begin VB.Menu clavesuper3 
         Caption         =   "Supervisor3"
      End
      Begin VB.Menu claveIDsuper1 
         Caption         =   "IdSuper1"
      End
      Begin VB.Menu ClaveIdSuper2 
         Caption         =   "IdSuper2"
      End
      Begin VB.Menu ClaveIdSuper3 
         Caption         =   "IdSuper3"
      End
      Begin VB.Menu ClaveDesc1 
         Caption         =   "ClaveDesc1"
      End
      Begin VB.Menu NombreDesc1 
         Caption         =   "NombreDesc1"
      End
      Begin VB.Menu ClaveDesc2 
         Caption         =   "ClaveDesc2"
      End
      Begin VB.Menu NombreDesc2 
         Caption         =   "NombreDesc2"
      End
      Begin VB.Menu ClaveDesc3 
         Caption         =   "ClaveDesc3"
      End
      Begin VB.Menu NombreDesc3 
         Caption         =   "NombreDesc3"
      End
   End
   Begin VB.Menu mnIfiscal 
      Caption         =   "Impresora Fiscal"
      Begin VB.Menu mnAbrePeriodo 
         Caption         =   "Forzar Abrir Periodo"
         Shortcut        =   ^A
      End
      Begin VB.Menu mnInformeX 
         Caption         =   "Forzar Informe X"
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnInformeZ 
         Caption         =   "Forzar Informe Z"
         Shortcut        =   {F2}
      End
   End
   Begin VB.Menu mnInformes 
      Caption         =   "Informes"
      Begin VB.Menu mnVeInformes 
         Caption         =   "Ver Informes"
      End
   End
   Begin VB.Menu mnCompras 
      Caption         =   "Compras"
      Begin VB.Menu mnmaestrocompras 
         Caption         =   "Maestro"
      End
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
(ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, _
ByVal lpParameters As String, ByVal lpDirectory As String, _
ByVal nShowCmd As Long) As Long

Dim tDato As Boolean

'Option Explicit
Dim X           As Integer
Dim portNum     As Integer
Dim Ack         As Integer
Dim error_26h   As Boolean
Dim xMesa As Integer

Dim pt32 As POINTAPI
Dim ptx As Long
Dim pty As Long
Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Private Type POINTAPI
    Xf As Long
    Yf As Long
End Type
Dim Contador1 As Integer


Public Sub ActualizarSoloUna(SoloUna As Integer)
    Dim cr As Integer, rb As Integer, sl As Integer
    Dim mesa As Integer
    Dim Cpedido As Double
    Dim Citmes As Integer
    Dim mcan, mpre, mdet, mcod, mitem As String
    Dim StrMesa As String
    Dim ComoEsta As Variant
    Dim IxI
   
    IxI = SoloUna
        RubroDef1(IxI) = 0
        RubroDef2(IxI) = 0
   
    File1.Path = PathMesas
        ' Recorro solo una mesa
        StrMesa = "StatusTable" & IxI & ".tab"
        File1.Pattern = StrMesa
        
        If File1.ListCount > 0 Then ' la mesa esta en el sistema
            X = FreeFile
            Open PathMesas & StrMesa For Input As X
                For j = 1 To 9
                    
                    Line Input #X, ComoEsta ' extraigo el estado
                    If j = 2 Then
                        GarzonMesa(IxI) = ComoEsta
                    End If
                    If j = 4 Then
                        Cpedido = ComoEsta
                    End If
                    If j = 5 Then
                        MesaStatus(IxI) = Val(ComoEsta)
                    End If
                    If j = 6 Then
                        Citmes = ComoEsta
                    End If
                    If j = 8 Then
                        MesaDescuento(IxI) = ComoEsta
                    End If
                    If j = 9 Then
                        Ccomenzales(IxI) = ComoEsta
                    End If
                Next
            Close #X
            
            If MesaStatus(IxI) = 2 Or MesaStatus(IxI) = 3 Then ' La mesa fue encontrada ocupada
                LBmesa(IxI - 1).BackColor = IIf(MesaStatus(IxI) = 2, MainColores(2), MainColores(3))
                MesaUpedido(IxI) = Cpedido
                'If List1(i - 1).ListCount > 0 Then  ' Limpio la lista de productos
                '    For sl = List1(i - 1).ListCount To 0 Step -1
                '        List1(i - 1).RemoveItem sl
                '    Next
                'End If
                List1(IxI - 1).Clear
                mesa = IxI
                'Q = FreeFile
                StrMesa = PathMesas & "mesa" & IxI & ".ped"
                Y = FreeFile
                LargoR = Len(rPedido)
                Open StrMesa For Random As Y Len = LargoR
                For act = 1 To LOF(Y) / LargoR
                    Get #Y, , rPedido
                    ProductoCodigo = Trim(rPedido.Codigo)
                    ProductoCodigo = ProductoCodigo & String(5 - Len(ProductoCodigo), "_")
                    IdentItemP = Trim(rPedido.Bebestible)
                    IdentItemP = String(3 - Len(IdentItemP), " ") & IdentItemP
                    Detalle = rPedido.Detalle
                    nuevaSuma = Trim(rPedido.PrecioU)
                   ' LbTotal.Caption = Val(LbTotal.Caption) + (Val(nuevaSuma) * Val(rPedido.Cantidad))
                    nuevoItem = rPedido.Cantidad & " - " & String(6 - Len(Str(nuevaSuma)), " ") & nuevaSuma & " - " & Detalle
                    Horita = rPedido.Hora
                    nuevoItem = nuevoItem & String(35 - Len(nuevoItem), " ") & Horita & "_____" & ProductoCodigo & rPedido.Rubro & IdentItemP
                    List1(mesa - 1).AddItem nuevoItem
                    IdentItemP = Val(IdentItemP)
                    If IdentItemP = 1 Then
                          RubroDef1(mesa) = RubroDef1(IxI) + (nuevaSuma * Val(rPedido.Cantidad))
                    ElseIf IdentItemP = 2 Then
                          RubroDef2(mesa) = RubroDef2(IxI) + (nuevaSuma * Val(rPedido.Cantidad))
                    End If
                    List1(IxI - 1).ListIndex = List1(IxI - 1).ListCount - 1
                                    
                Next
                Close #Y ' Datos extraidos
                'LBmesa_Click (mesa - 1)
                
                
                
            ElseIf MesaStatus(IxI) = 1 Then
                List1(IxI - 1).Clear
                LBmesa(IxI - 1).BackColor = MainColores(1)
            End If
        Else ' La mesa no esta, por ende esta libre
            MesaStatus(IxI) = 1
      '      LBmesa(IxI - 1).BackColor = MainColores(1)
            If List1(IxI - 1).ListCount > 0 Then
                For sl = List1(IxI - 1).ListCount To 0 Step -1
                    List1(IxI - 1).RemoveItem sl
                Next
            End If
           ' GarzonMesa(i) = ""
           'Ccomenzales(i) = 0
        End If
    
End Sub



Private Sub CmdAbajo_Click()
    Dim myIndice As Integer
    myIndice = Val(Label5(0).Caption) - 1
    LTxRubro(myIndice).TopRow = LTxRubro(myIndice).TopRow + 2
End Sub

Private Sub cmdAlternativa_Click()
    Dim MyHora As String
    estaM = Val(TxtMesaSel.Text) - 1  ' N� mesa -1 para indices
    If MesaStatus(estaM + 1) = 3 Then
        Beep
        Exit Sub
    End If
    
        ''  COMPRUEBA EL ESTADO PRIMERO ANTES DE ACCIONAR
    ''
    ElE = False
    ComprobarEstado (estaM + 1)
    If ElE = True Then Exit Sub
    
    
    If estaM > -1 Then
        If List1(estaM).ListCount > 0 Then
            Desde = 36
            restaItem = List1(estaM).List(List1(estaM).ListIndex)
            MyHora = Mid(restaItem, Desde, 9)
            Producto = Mid(restaItem, 15, 20)
            Sql = "SELECT cod,descripcion,precioventa,rubro,dirigida " & _
                  "FROM productos " & _
                  "WHERE tpr_id=4 AND "
                  
            
            
            
            
            
            
        Else
            Exit Sub
        End If
    End If
End Sub

Private Sub CmdBusqueda_Click()
    ElMainActivo = False
    BuscaProducto.Show 1
End Sub
Private Sub CmdCentroProduccion_Click()
    Call EnviaCproduccion
End Sub
Private Sub CmdComando_Click(Index As Integer)
    Dim MActual As Integer, todaviaNoPago As Boolean
    Dim NumTicket As Double, LaBoleta As String
    Dim b As Integer
    Dim TotMesa As Long
    Dim FueParcial As Boolean
    ElE = False
    FueParcial = False
    
    todaviaNoPago = False
    If Index = 5 Then Exit Sub
    MActual = Val(TxtMesaSel.Text)
    'MActual = Val(AccionMesa.LBIden.Caption)
    If Val(AccionMesa.LBIden.Caption) = 0 Then MActual = Val(TxtMesaSel.Text)
    If MActual <> MesaSelectItem Then
        AccionMesa.LBIden.Caption = Val(TxtMesaSel.Text)
        AccionMesa.LBTotal.Caption = LBTotal.Caption
        AccionMesa.CboOtros.Clear
        For i = 1 To 20 ' Cargo los tipos de documentos
            If OpcionesDePago(i) = "" Then Exit For
            AccionMesa.CboOtros.AddItem (OpcionesDePago(i))
        Next
        AccionMesa.CboOtros.ListIndex = 0
    End If
    If LBlevelControl.Caption = 2 And Val(TxtMesaSel.Text) <> 0 Then
        If List1(MActual - 1).ListCount > 0 Then MesaStatus(MActual) = 2
    End If
    
    
    ''  COMPRUEBA EL ESTADO PRIMERO ANTES DE ACCIONAR
    ''
    ElE = False
    ComprobarEstado (MActual)
    If ElE = True Then Exit Sub
        
    
    
    
    
    
    ''*********************************************************************
    ''**********  A  C  C  I  O  N      M  E  S  A  ````````````````````
    ''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    If Index = 0 Then     ''' Boton ACCION MESA
        If LBlevelControl.Caption = 2 Then '' nivel de control parcial
            If MesaStatus(MActual) = 2 Then
                'FrmFondo.Enabled = False
                '   ' aqui separar nueva lista
                mselec = Main.List1(MActual - 1).ListCount
                tselec = 0
                List1(MesaSelectItem).Clear
                For K = 0 To mselec - 1
                    If List1(MActual - 1).Selected(K) = True Then
                        List1(MesaSelectItem).AddItem List1(MActual - 1).List(K)
                        tselec = tselec + 1
                    End If
                Next '
                If Main.List1(MesaSelectItem).ListCount > 0 Then ''Si hay items marcados
                    itemselec = 0
                    For K = 0 To Main.List1(MesaSelectItem).ListCount - 1
                        selecItem = Main.List1(MesaSelectItem).List(K)                     ''  Se agregan productos a la mesa
                        CantRetira = Val(Left(selecItem, 3))
                        valorX = Val(Mid(selecItem, 6, 6)) * CantRetira
                        itemselec = itemselec + valorX
                    Next
                    AccionMesa.LBIden.Caption = MesaSelectItem
                    AccionMesa.LBTotal.Caption = itemselec
                    AccionMesa.CmdMesa(1).Enabled = False '' Inhabilita
                    AccionMesa.CmdMesa(2).Enabled = False '' Descuento, unir mesa
                    AccionMesa.CmdMesa(3).Enabled = False '' Cambio de mesa
                    FueParcial = True
                End If
                'AccionMesa.Visible = True
                If Main.List1(MesaSelectItem).ListCount > 0 Then
                    For K = Main.List1(MesaSelectItem).ListCount - 1 To 0 Step -1
                        Main.List1(MesaSelectItem).RemoveItem K
                    Next
                End If
                'FrmFondo.Enabled = True
            End If
        End If
        
        ' usado actualmente
        ' ESTO ES LO QUE ESTOY USANDO ACTUALMENTE
        If LBlevelControl.Caption = 1 Then '' Nivel de control Total
            
            If MesaStatus(MActual) = 2 Then
                    '************************
             
                'cazar esta informacion debe avisar al resto
                'que la mesa esta ocupada
                'FrmFondo.Enabled = False
                '   ' aqui separar nueva lista
                mselec = Main.List1(MActual - 1).ListCount
                tselec = 0
                List1(MesaSelectItem).Clear
                For K = 0 To mselec - 1   ' aQUI REVISO SI HAY ITEM MARCADOS
                    If List1(MActual - 1).Selected(K) = True Then
                        List1(MesaSelectItem).AddItem List1(MActual - 1).List(K)
                        tselec = tselec + 1
                    End If
                    selecItem = Main.List1(MActual - 1).List(K)
                    xCantRetira = Val(Left(selecItem, 3))
                    xvalorX = Val(Mid(selecItem, 6, 6)) * Val(xCantRetira)
                    xitemselec = xitemselec + Val(xvalorX)
                
                Next '
                AccionMesa.LBTotal.Caption = xitemselec
                
                'MsgBox mselec
                If Main.List1(MesaSelectItem).ListCount > 0 Then ''Si hay items marcados
                    itemselec = 0 ' HAY ITEM MARCADOS
                    
                    For K = 0 To Main.List1(MesaSelectItem).ListCount - 1
                        selecItem = Main.List1(MesaSelectItem).List(K)
                        ''  Se agregan productos a la mesa
                        
                        CantRetira = Val(Left(selecItem, 3))
                        valorX = Val(Mid(selecItem, 6, 6)) * CantRetira
                        itemselec = itemselec + valorX
                    Next
                   ' Pago.CmdFpago(Pago.CmdFpago.Count - 2).Enabled = False
                   ' Pago.CmdFpago(Pago.CmdFpago.Count - 1).Enabled = False
                   ' For K = 0 To Pago.CmdFpago.Count - 2
                   '     Pago.CmdFpago(K).Enabled = False
                   ' Next
                    AccionMesa.LBIden.Caption = MesaSelectItem
                    AccionMesa.LBTotal.Caption = itemselec
                    AccionMesa.CmdMesa(1).Enabled = False '' Inhabilita
                    AccionMesa.CmdMesa(2).Enabled = False '' Descuento, unir mesa
                    AccionMesa.CmdMesa(3).Enabled = False '' Cambio de mesa
                 
                   
                    FueParcial = True
                    'AccionMesa.Normal.Visible = False
                   
                    todaviaNoPago = False
                Else
                    todaviaNoPago = True
                End If
                
                ElMainActivo = False
                AccionMesa.Show 1
                
                
                LBmesa_Click (MActual - 1)
                
                
                
                If FueParcial = True Then
                    'MsgBox "HACIENDO MOVIMEINTOS CUANDO NO CORRESPONDE"
                    Y = FreeFile
                    Dim Sts2Temporal(9) As Variant
                    Open PathMesas & "StatusTable" & MActual & ".tab" For Input As Y
                        For i = 1 To 9
                            Line Input #Y, Sts2Temporal(i)
                        Next
                    Close #Y
                    Sts2Temporal(6) = List1(MActual - 1).ListCount
                    Sts2Temporal(7) = LBTotal.Caption
                    X = FreeFile
                    Open PathMesas & "StatusTable" & MActual & ".tab" For Output As X
                        For i = 1 To 9
                            Print #X, Sts2Temporal(i)
                        Next
                    Close #X
                    TxtMesaSel.Text = 0
                End If
                FueParcial = False
                 'AccionMesa.Visible = True
                'If Main.List1(MesaSelectItem).ListCount > 0 Then
                '    For K = Main.List1(MesaSelectItem).ListCount - 1 To 0 Step -1
                '        Main.List1(MesaSelectItem).RemoveItem K
                '    Next
                'End If
                
                If MesaStatus(MesaSelectItem) = 3 Then
                    Index = 3
                End If
                
            End If
        
            If MesaStatus(MActual) = 3 Then
                If todaviaNoPago = True Then Exit Sub
                
                '************************
                'Y = FreeFile
                'Open PathMesas & "Tmp\" & TxtMesaSel.Text & ".tmp" For Output As Y
                '    Print #Y, "Si-" & IdEquipo & "-" & Now
                '    Print #Y, "3"
                'Close #Y ' Esta info es para el robot, al
                'cazar esta informacion debe avisar al resto
                'que la mesa esta ocupada
                'FrmFondo.Enabled = False
                StrMesa = "StatusTable" & MActual & ".tab"
                X = FreeFile
                Open PathMesas & StrMesa For Input As X
                    For j = 1 To 13
                        
                        Line Input #X, ComoEsta ' extraigo el estado
                        If j = 4 Then
                            NumTicket = ComoEsta
                        End If
                        If j = 10 Then
                            docPago = ComoEsta
                        End If
                        If j = 11 Then
                            LaBoleta = ComoEsta
                        End If
                        If j = 12 Then
                            Saldillo = Val(ComoEsta)
                        End If
                        If j = 13 Then
                            Boletilla = ComoEsta
                        End If
                    Next
                Close #X
                
                '*************************************
                'nuevo pago con boleta fiscal'
                 X = FreeFile
                Pago.ListaFpagos.Clear
                Open LaRutadeArchivos & docPago For Input As X
                    Do While Not EOF(X)
                        Line Input #X, s
                        Pago.ListaFpagos.AddItem s
                    Loop
                    
                Close #X
                Pago.LBpedido.Caption = NumTicket
                Pago.LBFinal.Caption = IIf(Saldillo > 0, Me.LBTotal.Caption - Saldillo, Me.LBTotal.Caption)
                Pago.LBGARZON = lbgarcia.Caption
                Pago.LBmesa.Caption = TxtMesaSel.Text
                altito = 5500
                altito = altito - (10 * (Pago.ListaFpagos.ListCount + 1))
                altoboton = altito / (Pago.ListaFpagos.ListCount + 1)
                Pago.CmdFpago(0).Height = altoboton
                Pago.CmdFpago(0).Top = 240
                
                 
                For i = 1 To Pago.ListaFpagos.ListCount
                
                    Load Pago.CmdFpago(i)
                    'Pago.CmdFpago(i).Top = Pago.CmdFpago(i).Top + 100
                    Pago.CmdFpago(i).Visible = True
                    Pago.CmdFpago(i).Height = altoboton
                    Pago.CmdFpago(i).Top = Pago.CmdFpago(i - 1).Top + altoboton + 10
                Next
                
                For i = 0 To Pago.ListaFpagos.ListCount - 1
                    Pago.CmdFpago(i).Caption = Pago.ListaFpagos.List(i)
                    Pago.CmdFpago(i).TabIndex = i
                Next
                Pago.CmdFpago(Pago.ListaFpagos.ListCount).Caption = "Cerra Ventana"
                With Pago
                    
                    For i = 0 To .ListaFpagos.ListCount - 1
                        .AdoFpagos.Recordset.MoveFirst
                        Do While Not .AdoFpagos.Recordset.EOF
                            If .CmdFpago(i).Caption = .AdoFpagos.Recordset.Fields(0) Then
                                If .AdoFpagos.Recordset.Fields(1) Then .CmdFpago(i).Caption = .CmdFpago(i).Caption & "."
                                If .AdoFpagos.Recordset.Fields(2) = 0 Then .CmdFpago(i).Caption = .CmdFpago(i).Caption & "_"
                            End If
                            .AdoFpagos.Recordset.MoveNext
                        Loop
                    Next
                    .LBPagoNDoc.Caption = IIf(docPago = "Boleta", LaBoleta, 0)
                    .LbDoc.Caption = docPago
                End With
                
                
                With Pago.MshMonedas
                   'Set .DataSource = Me.ad
                    .Cols = 3
                    .Row = 0: .Col = 0
                    .Text = "Moneda": .ColWidth(0) = 950: .Col = 1
                    .Text = "Nacional": .ColWidth(1) = 700
                    .ColWidth(2) = 700
                    For hh = 1 To .Rows - 1
                        .Row = hh: .Col = 1
                        vme = .Text: .Col = 2
                        .Text = Format(Pago.LBFinal.Caption / vme, "##,##0.00")
                    Next
                End With
                
                ElMainActivo = False
                Pago.Show 1
                For i = 1 To Pago.ListaFpagos.ListCount
                    Unload Pago.CmdFpago(i)
                Next
                
                '**************************
                ' Fin nuevo pago con boleta fiscl
                '**************************
                
                'Pago.LBFinal.Caption = LbTotal.Caption
                
                'Pago.LBgarzon = txtGarzonSel.Text
                'Pago.LBpedido = NumTicket
                'Pago.LBmesa = MActual
                'Pago.CmdFpago(0).TabIndex = 0
                'Pago.Visible = True
                'Pago.Shape1(0).Visible = False
                'Pago.LBMonto(2).Visible = False
                'Pago.LBPagoNDoc.Visible = False
                'Pago.LBPagoNDoc.Caption = LaBoleta
                
                    
                'Pago.AdoMonedas.Recordset.MoveFirst
                    
                'With Pago.MshMonedas
                    'Set .DataSource = Me.ad
                 '   .Cols = 3
                 '   .Row = 0: .Col = 0
                 '   .Text = "Moneda": .ColWidth(0) = 950: .Col = 1
                 '   .Text = "Nacional": .ColWidth(1) = 800
                 '   .ColWidth(2) = 700
                 '   For hh = 1 To .Rows - 1
                 '       .Row = hh: .Col = 1
                 '       vme = .Text: .Col = 2
                 '       .Text = Format(Pago.LBFinal.Caption / vme, "##,##0.00")
                 '   Next
                'E 'nd With
                    
                
                'Pago.Show 1
                'For K = 0 To Pago.CmdFpago.Count - 2
                '    Pago.CmdFpago(K).Enabled = False
                'Next  ' inhabilita pago
                'Pago.TxtMonto.SetFocus
            End If
        End If
        
    End If
    If Index = 1 Then
        'FrmFondo.Enabled = False
        'TurnoActual.Enabled = True
        ElMainActivo = False
        TurnoActual.Show 1
    End If
   ' Pago.CmdFpago(Pago.CmdFpago.Count - 2).Enabled = True
   ' Pago.CmdFpago(Pago.CmdFpago.Count - 1).Enabled = True
   ' AccionMesa.CmdMesa(0).Enabled = True
   ' AccionMesa.CmdMesa(1).Enabled = True
   ' AccionMesa.CmdMesa(2).Enabled = True
    If Index = 2 Then
        Actualiza = 2
        ActualizarEstadoMesas
    End If
    If Index = 3 Then RequeteImprime  '' Reimprime el vale
    If Index = 4 Then  ''  """ Vistazo de
        'FrmFondo.Enabled = False
        totalstatusmesas = 0 '"" Estado de mesas
        With StatusMesas.MSHFlexGrid1
            .ColAlignment = 1
            .Rows = lbmesas.Caption + 1
            .Row = 0
            .Col = 0
            .Text = "Mesa"
            .ColWidth(0) = 700
            .Col = 1
            .Text = "Estado"
            .ColWidth(1) = 1400
            .Col = 2
            .Text = "Total"
            .ColWidth(2) = 950
            .Col = 3
            .Text = "Garzon"
            .ColWidth(3) = 2000
            For b = 1 To lbmesas.Caption
                .Col = 0
                .Row = b
                .Text = b
                .Col = 1
                If MesaStatus(b) = 1 Then .Text = "Libre"
                If MesaStatus(b) = 2 Then .Text = "Ocupada"
                If MesaStatus(b) = 3 Then .Text = "Al Cobro"
                .Col = 2
                If MesaStatus(b) = 2 Or MesaStatus(b) = 3 Then
                    StatusMesas.LBocupadas.Caption = StatusMesas.LBocupadas.Caption + 1
                    TotMesa = 0
                    For K = 0 To List1(b - 1).ListCount - 1
                        restaItem = List1(b - 1).List(K)                   ''  Se agregan productos a la mesa
                        CantRetira = Val(Left(restaItem, 3))
                        valorX = Val(Mid(restaItem, 6, 6)) * CantRetira
                        TotMesa = TotMesa + valorX
                    Next
                    If MesaDescuento(b) <> "0" Then
                        If Mid(MesaDescuento(b), 1, 1) = "$" Then
                            TotMesa = TotMesa - Val(Mid(MesaDescuento(b), 2))
                        ElseIf IsNumeric(MesaDescuento(b)) Then
                            midesc = (TotMesa / 100) * Val(MesaDescuento(b))
                            midesc = Int((midesc / 100) * 10) * 10
                            TotMesa = TotMesa - midesc
                        End If
                    End If
                    .Text = TotMesa
                    StatusMesas.LbValor.Caption = StatusMesas.LbValor.Caption + TotMesa
                End If
                If MesaStatus(b) = 1 Then .Text = "0"
                .Col = 3
                .Text = GarzonMesa(b)
            Next
        End With
        StatusMesas.LbLibres.Caption = lbmesas.Caption - StatusMesas.LBocupadas.Caption
        StatusMesas.Label5.Caption = StatusMesas.Label5.Caption & " " & CmdComando(5).Caption
        StatusMesas.Show 1
    End If
End Sub
Public Sub AgregaItems(Index As Integer)
    Dim nuevoItem As String
    Dim nuevaSuma As Double
    Dim Detalle As String
    Dim UPedido As Double
    Dim ProductoCodigo As Variant
    Dim IdentItemP As Variant
    Dim Horita As String * 9
    Dim PedMesa As String
    Dim DetalleAlt As String
    If HappyHours = CMDrubros(Index).Caption Then
        If HorarioIniHappy <> "Todo" Then
            If Time <= HorarioIniHappy Then
                'If Time >= HorarioFinHappy Then
                    MsgBox "A�n no es hora del Happy...", vbOKOnly + vbExclamation, "Happy Hours"
                    Exit Sub
                'End If
            ElseIf Time > HorarioFinHappy Then
                    MsgBox "Disculpa, el happy ya termin� por hoy...", vbOKOnly + vbExclamation, "Happy Hours"
                    Exit Sub
            End If '' Verifica los horarios para Happy
        End If
    End If
    
    
    Me.txtGarzonSel.Text = lbgarcia.Caption
    If Val(TxtNpersonas.Text) < 1 Then TxtNpersonas.Text = "1"
    UPedido = 0
    Me.txtGarzonSel.Text = lbgarcia.Caption
    If Val(TxtMesaSel.Text) = 0 Or Val(TxtMesaSel.Text) > lbmesas.Caption Then
        mesafirst = MsgBox("Por favor,  Seleccione una mesa ... ", vbExclamation + vbOKOnly, "Aviso")
        Exit Sub
    End If
    
    
    ElE = False
    ComprobarEstado (TxtMesaSel.Text)
    If ElE = True Then Exit Sub
    
    If LTxRubro(Index).Rows > 0 Then
        mesaactiva = Val(TxtMesaSel.Text) - 1
        If MesaStatus(mesaactiva + 1) = 3 Then
            Beep
            Exit Sub
        End If
        LBmesa(mesaactiva).BackColor = MainColores(2)
        xadd = 0
        If Garzones.Value = 1 Then
            If List1(mesaactiva).ListCount > 0 Then
                txtGarzonSel.Text = lbgarcia.Caption
                xadd = 1
            End If
        End If
        If Val(TxtCantidad.Text) = 0 Then TxtCantidad.Text = "1"
        LTxRubro(Index).Col = 0
        ProductoCodigo = LTxRubro(Index).Text
        
       
        
        
        
        ProductoCodigo = Trim(Str(ProductoCodigo))
        ProductoCodigo = ProductoCodigo & String(5 - Len(ProductoCodigo), "_")
        LTxRubro(Index).Col = 3
        IdentItemP = Str(Val(LTxRubro(Index).Text))
        IdentItemP = String(3 - Len(IdentItemP), " ") & IdentItemP
        LTxRubro(Index).Col = 1
        Detalle = Mid(LTxRubro(Index).Text, 1, 20)
        DetalleAlt = LTxRubro(Index).Text
        
        
        LTxRubro(Index).Col = 2
        nuevaSuma = Val(LTxRubro(Index).Text)
        LBTotal.Caption = Val(LBTotal.Caption) + (nuevaSuma * Val(TxtCantidad.Text))
        nuevoItem = String(3 - Len(TxtCantidad), " ") & Val(TxtCantidad.Text) & " - " & String(6 - Len(Str(nuevaSuma)), " ") & nuevaSuma & " - " & Detalle
        Horita = Time
        If Len(Trim(Distinto)) = 1 Then Distinto = "00" & Trim(Distinto)
        If Len(Trim(Distinto)) = 2 Then Distinto = "0" & Trim(Distinto)
        Mid(Horita, 7, 3) = Distinto
        
        Call InsertaAlternativa(Val(ProductoCodigo), DetalleAlt, Horita)
        
        Distinto = Val(Distinto) + 1
        If Val(Distinto) = 999 Then Distinto = "1"
        nuevoItem = nuevoItem & String(35 - Len(nuevoItem), " ") & Horita & "_____" & ProductoCodigo & Mid(Label5(0).Caption, 1, 2) & IdentItemP
        List1(mesaactiva).AddItem nuevoItem
        IdentItemP = Val(IdentItemP)
        If IdentItemP = 1 Then
              RubroDef1(mesaactiva + 1) = RubroDef1(mesaactiva + 1) + (nuevaSuma * Val(TxtCantidad.Text))
        ElseIf IdentItemP = 2 Then
              RubroDef2(mesaactiva + 1) = RubroDef2(mesaactiva + 1) + (nuevaSuma * Val(TxtCantidad.Text))
        End If
        List1(mesaactiva).ListIndex = List1(mesaactiva).ListCount - 1
        
        If MesaStatus(mesaactiva + 1) = 1 Then
            MesaDescuento(mesaactiva + 1) = 0
            lbTconDesc.Caption = 0
            'UPedido = LBBpedido.Caption
            ' Esta desocupada
            Q = FreeFile
            Open PathMesas & "last.pd" For Input As Q
                Line Input #Q, s
                UPedido = Val(s)
            Close #Q
            
            Q = FreeFile
            Open PathMesas & "last.pd" For Output As Q
                Print #Q, UPedido + 1
            Close #Q
            
            MesaStatus(mesaactiva + 1) = 2
            Ccomenzales(mesaactiva + 1) = TxtNpersonas.Text
            X = FreeFile  'La mesa esta libre
            Open PathMesas & "StatusTable" & mesaactiva + 1 & ".tab" For Output As #X
                Print #X, "Si-" & IdEquipo & "-" & Now
                Print #X, lbgarcia.Caption   'GarzonMesa(mesaactiva + 1)
                Print #X, Time
                Print #X, UPedido
                Print #X, 2
                Print #X, List1(mesaactiva).ListCount 'N de Items
                Print #X, Val(LBTotal.Caption)
                Print #X, "0"
                Print #X, "2"
            Close #X
            
            '***************************************
           ' Y = FreeFile
           ' Open PathMesas & "Tmp\" & TxtMesaSel.Text & ".tmp" For Output As Y
           '     Print #Y, "Si-" & IdEquipo & "-" & Now
           '     Print #Y, "2"
           ' Close #Y ' Esta info es para el robot, al
            'cazar esta informacion debe avisar al resto
            'que la mesa esta ocupada
            
        ElseIf MesaStatus(mesaactiva + 1) = 2 Then   ' Si la mesa esta
            X = FreeFile
            Dim uuuPedido As String ' Ocupada
            Dim uuuDscto  As String
            Open PathMesas & "StatusTable" & mesaactiva + 1 & ".tab" For Input As #X
                For i = 1 To 9
                    Line Input #X, s
                    If i = 4 Then uuuPedido = s ' Extraigo Id Pedido
                    If i = 8 Then uuuDscto = s
                Next
            Close #X
            Q = FreeFile
               
            UPedido = uuuPedido
            Me.txtGarzonSel.Text = lbgarcia.Caption
            GarzonMesa(mesaactiva) = lbgarcia.Caption
            Open PathMesas & "StatusTable" & mesaactiva + 1 & ".tab" For Output As #Q
                Print #Q, "Si-" & IdEquipo & "-" & Now
                Print #Q, lbgarcia.Caption ''GarzonMesa(mesaactiva + 1)
                Print #Q, Time
                Print #Q, UPedido
                Print #Q, 2
                Print #Q, List1(mesaactiva).ListCount 'N de Items
                Print #Q, Val(LBTotal.Caption)
                Print #Q, uuuDscto
                Print #Q, "2"
            Close #Q
            Y = FreeFile
            '***************************************
            'Open PathMesas & "Tmp\" & TxtMesaSel.Text & ".tmp" For Output As Y
            '    Print #Y, "Si-" & IdEquipo & "-" & Now
            '    Print #Y, "2"
            'Close #Y ' Esta info es para el robot, al
            'cazar esta informacion debe avisar al resto
            'que la mesa esta ocupada

                
        End If
        'Label11.Caption = UPedido
        With rPedido
            X = FreeFile
            PedMesa = PathMesas & "Mesa" & mesaactiva + 1 & ".ped"
            LargoR = Len(rPedido)
            Open PedMesa For Random As #X Len = LargoR
            If LOF(X) <> 0 Then
                Close #X
                Kill PedMesa
                X = FreeFile
                Open PedMesa For Random As #X Len = LargoR
            End If
            For ind = 1 To List1(mesaactiva).ListCount
                restaItem = List1(mesaactiva).List(ind - 1)
                .Npedido = UPedido
                .Hora = Mid(restaItem, Desde, 9)
                .Nmesa = TxtMesaSel.Text
                .Codigo = Str(Val(Mid(restaItem, 50, 5)))
                .Cantidad = Left(restaItem, 3)
                .Detalle = Mid(restaItem, 15, 20)
                .PrecioU = Mid(restaItem, 6, 6)
                .SubTotal = Str(.Cantidad * .PrecioU)
                .Bebestible = Right(restaItem, 1)
                .Rubro = Mid(restaItem, 55, 2)
                .NComenzales = TxtNpersonas.Text
                Put #X, , rPedido
            Next
            Close #X
        End With ' Fin archivo detalle pedido
        
        LBmesa_Click (mesaactiva)
    End If
End Sub
Private Sub InsertaAlternativa(ElCodigo As Double, ElProducto As String, IdUnico As String)
    Dim rs_Alternativa As Recordset
    Sql = "SELECT pro_alternativa_id AS ID,pro_alternativa_nombre as NOMBRE,pro_alternativa_p_venta AS PRECIO,pro_alternativa_cantidad AS CANTIDAD " & _
          "FROM productos_alternativas " & _
          "WHERE pro_codigo=" & ElCodigo
    Call Consulta(Rst_tmp, Sql)
    If Rst_tmp.RecordCount > 0 Then
        With Rst_tmp
            .MoveFirst
            Do While Not .EOF
                Sql = "SELECT pro_alt_id,pro_codigo,p.descripcion " & _
                      "FROM productos_alternativas_mov m ,productos p " & _
                      "WHERE m.pro_codigo=p.cod AND pro_alternativa_id=" & Rst_tmp!ID
                Call Consulta(rs_Alternativa, Sql)
                If rs_Alternativa.RecordCount > 0 Then
                    With SeleccionaAlternativa
                        rs_Alternativa.MoveFirst
                        .FrmAlt.Caption = Rst_tmp!nombre
                        .skProducto = ElProducto
                        .SkIdUnico = IdUnico
                        For i = 1 To rs_Alternativa.RecordCount
                            Load .cmdAlt(i)
                            .cmdAlt(i).Caption = rs_Alternativa!descripcion
                            .cmdAlt(i).Height = .cmdAlt(0).Height
                            .cmdAlt(i).Top = .cmdAlt(i - 1).Top + .cmdAlt(0).Height + 10
                            .cmdAlt(i).Visible = True
                            .cmdAlt(i).Tag = rs_Alternativa!pro_alt_id
                            rs_Alternativa.MoveNext
                        Next
                        .txtDisponibles = Rst_tmp!Cantidad
                        .txtSeleccionadas = 0
                        .FrmAlt.Height = .cmdAlt(0).Height * rs_Alternativa.RecordCount + (.cmdAlt(0).Height * 1)
                        .cmdSeguir.Top = .FrmAlt.Top + .FrmAlt.Height + 300
                        .FrmInfo.Top = .cmdSeguir.Top - 100
                        .Height = .FrmInfo.Top + 1600
                        .Show 1
                    End With
                End If
                
            
            .MoveNext
            Loop
        End With
    End If
    
    
End Sub


Private Sub cmdOut_Click()
    On Error GoTo NADA
        
    Logeate.Show 1
NADA:
''NADA
End Sub

Private Sub CMDrubros_Click(Index As Integer)
    Dim Indice As String * 2
    For r = 0 To CantidadRubros - 1
        LTxRubro(r).Visible = False       ''  'Sobresale boton seleccionado
        CMDrubros(r).BackColor = &H8000000F  '' De rubro
    Next
    Indice = Index + 1
    LTxRubro(Index).Visible = True
    CMDrubros(Index).BackColor = &HFFFF&
    Label5(0).Caption = Indice & "-" & CMDrubros(Index).Caption
End Sub
Private Sub CmdSaca_Click()
    Dim CantRetira As Integer
    Dim valorX As Double                    ''  Saca 1 item de items cargados a una mesa
    Dim restaItem As String
    Dim Apedido As Double
    Dim fpedido As String
    Dim Desde As Long
    Dim MyHora As String
    
    estaM = Val(TxtMesaSel.Text) - 1  ' N� mesa -1 para indices
    If MesaStatus(estaM + 1) = 3 Then
        Beep
        Exit Sub
    End If
    
        ''  COMPRUEBA EL ESTADO PRIMERO ANTES DE ACCIONAR
    ''
    ElE = False
    ComprobarEstado (estaM + 1)
    If ElE = True Then Exit Sub
    
    
    If estaM > -1 Then
        If List1(estaM).ListCount > 0 Then
            Desde = 36
            nsaca = 0
            nsaca = List1(estaM).ListIndex
            
            restaItem = List1(estaM).List(List1(estaM).ListIndex)
            MyHora = Mid(restaItem, Desde, 9)
            Producto = Mid(restaItem, 15, 20)
            If CmdCentroProduccion.Enabled = True Then
                If Right(Producto, 1) = "*" Then
                    'Main.Enabled = False
                    Autorizo = "No requiere"
                    ElMainActivo = False
                    SacarItem.Show 1
                    LBmesa_Click (estaM)
                    If PermiteSacarItem = False Then Exit Sub
                End If
            End If
            IdentItemP = Right(restaItem, 1)
            ProdCodi = Val(Right(restaItem, 7))
            CantRetira = Val(Left(restaItem, 3))
            valorX = Mid(restaItem, 6, 6) * CantRetira
            LBTotal.Caption = LBTotal.Caption - valorX
            'List1(estaM).RemoveItem List1(estaM).ListIndex ' kita item
            
            If List1(estaM).ListCount = 1 Then
                List1(estaM).Clear
            Else
                List1(estaM).RemoveItem nsaca
            End If
            
            If IdentItemP = 1 Then
                RubroDef1(estaM + 1) = RubroDef1(estaM + 1) - valorX
            ElseIf IdentItemP = 2 Then
                RubroDef2(estaM + 1) = RubroDef2(estaM + 1) - valorX
            End If
            If List1(estaM).ListCount = 0 Then
                LBmesa(estaM).BackColor = MainColores(1)
             '   MesaStatus(estaM + 1) = 1
             '   GarzonMesa(estaM + 1) = ""
             '   MesaDescuento(estaM + 1) = "0"
              '  LBmesa_Click (estaM)
                
            End If
            
            If Right(Producto, 1) = "*" Then
                
                With AdoQuitados.Recordset
                    .AddNew
                    .Fields(0) = Val(Mid(restaItem, 50, 5))
                    .Fields(1) = Producto
                    .Fields(2) = TxtMesaSel.Text
                    .Fields(3) = Time
                    .Fields(4) = TurnoTurno.Caption
                    .Fields(5) = TurnoFecha.Caption
                    .Fields(6) = Autorizo
                    .Fields(7) = CantRetira
                    .Update
                End With
            End If
            estaM = estaM + 1  'queda N� real de mesa
            
            
           
                      
            PedMesa = PathMesas & "Mesa" & estaM & ".ped"
            'fpedido = Trim(Str(Apedido))
            X = FreeFile
            Kill PedMesa
            LargoR = Len(rPedido)
            Open PedMesa For Random As #X Len = LargoR
            With rPedido
                For A = 0 To List1(estaM - 1).ListCount - 1
                    restaItem = List1(estaM - 1).List(A)
                    .Npedido = 1000   '' Archiva de nuevo la lista
                    .Hora = Mid(restaItem, Desde, 9)
                    .Nmesa = Str(estaM)
                    .Codigo = Str(Val(Mid(restaItem, 50, 5)))
                    .Cantidad = Left(restaItem, 3)
                    .Detalle = Mid(restaItem, 15, 20)
                    .PrecioU = Mid(restaItem, 6, 6)
                    .SubTotal = Str(.Cantidad * .PrecioU)
                    .Bebestible = Right(restaItem, 1)
                    .Rubro = Mid(restaItem, 55, 2)
                    Put #X, , rPedido
                Next
                Close #X
            End With
            
            If List1(estaM - 1).ListCount > 0 Then
                Y = FreeFile
                StrMesa = "Statustable" & estaM & ".tab"
                Open PathMesas & StrMesa For Output As Y
                    Print #Y, "Si-" & IdEquipo & "-" & Now
                    Print #Y, lbgarcia.Caption
                    Print #Y, Time
                    Print #Y, Apedido
                    Print #Y, 2
                    Print #Y, List1(estaM - 1).ListCount
                    Print #Y, LBTotal.Caption
                    Print #Y, MesaDescuento(estaM)
                    Print #Y, 2
                Close #Y
                'Y = FreeFile
                'Open PathMesas & "Tmp\" & TxtMesaSel.Text & ".tmp" For Output As Y
                '   Print #Y, "Si-" & IdEquipo & "-" & Now
                '   Print #Y, "2"
                'Close #Y
            Else
                'Y = FreeFile
                'Open PathMesas & "Tmp\" & TxtMesaSel.Text & ".tmp" For Output As Y
                '    Print #Y, "Si-" & IdEquipo & "-" & Now
                '    Print #Y, "1"
                'Close #Y
                MesaAcero (estaM)
            End If
            
            LBmesa_Click (estaM - 1)
        End If
    End If
    Exit Sub
rutError:
    MsgBox "Se caso un error"
End Sub
Private Sub compras_Click()
MsgBox RubroDef1(Val(TxtMesaSel.Text)) & " --- " & RubroDef2(Val(TxtMesaSel.Text))
End Sub



Private Sub CmdSubir_Click()
    Dim myIndice As Integer
    myIndice = Val(Label5(0).Caption) - 1
    If LTxRubro(myIndice).TopRow < 10 Then
        LTxRubro(myIndice).TopRow = 0
    Else
        LTxRubro(myIndice).TopRow = LTxRubro(myIndice).TopRow - 10
    End If
End Sub

Private Sub cmdZoom_Click()
    If Val(Trim(Main.TxtMesaSel)) <= 0 Then Exit Sub
    ZoomMesa.Show 1
End Sub

Private Sub cobramesa_Click()
  '  CmdComando_Click (0)
    If Val(TxtMesaSel.Text) > 0 And Val(TxtMesaSel.Text) < _
        lbmesas.Caption + 1 Then
        LBmesa_Click (TxtMesaSel.Text - 1)
        CmdComando_Click (0)
    End If
End Sub

Private Sub Command1_Click()
    Logeate.Show 1
End Sub

Private Sub Command2_Click()
    If LBmesa(lbmesas.Caption - 1).Top < 0 Then
        For K = 0 To lbmesas.Caption - 1
            LBmesa(K).Top = LBmesa(K).Top + 650
        Next
        Exit Sub
    End If
    For K = 0 To lbmesas.Caption - 1
        LBmesa(K).Top = LBmesa(K).Top - 650
    Next
End Sub

Private Sub Command3_Click()
    If LBmesa(0).Top = 0 Then Exit Sub
    For K = 0 To lbmesas.Caption - 1
        LBmesa(K).Top = LBmesa(K).Top + 650
    Next
End Sub

Private Sub config_Click()
    Configuracion.Show
End Sub


Private Sub edprecios_Click()
    EditarPrecios.Show 1
End Sub

Private Sub enviar_Click()
    CmdCentroProduccion_Click
End Sub

Private Sub extranjeras_Click()
    Monedas.Show
End Sub

Private Sub fin_Click()
    Unload FrmFondo
End Sub





Private Sub Form_Load()
    
    Dim CorrBoletaFiscal As String
    Dim alto As Double
    Dim altoboton As Integer
    Dim lin As Integer
    Dim Pv, Ph As Double
    Dim ColProd As Integer
    Dim ColProd1 As Integer
    Dim ColProd2 As Integer
    Dim QueNivel As String
    Saldillo = 0
    Boletilla = 0
    If Acceso.txtPassword.Text = Security.Text1 Then
        ' CLAVE caja
        Main.manconfig.Enabled = False
        Main.manager.Enabled = False
        Main.manProductos.Enabled = True
        ClaveSi = True
    End If
    If Acceso.txtPassword.Text = Security.Text2 Then
        ' CLAVE caja
        Main.manconfig.Enabled = False
        Main.manager.Enabled = False
        Main.manProductos.Enabled = True
        ClaveSi = True
    End If
    If Acceso.txtPassword.Text = Security.Text3 Then
        ' CLAVE caja
        Main.manconfig.Enabled = False
        Main.manager.Enabled = False
        Main.manProductos.Enabled = True
        ClaveSi = True
    End If
    If Acceso.txtPassword.Text = Security.Text4 Then
        ' CLAVE caja
        
        ClaveSi = True
    End If
    With Security
        clavegeneral.Caption = .Text4.Text
        claveadicion.Caption = .Text2.Text
        clavecaja.Caption = .Text1.Text
        clavesuper1.Caption = .Text3.Text
        claveIDsuper1.Caption = .Text7.Text
        clavesuper2.Caption = .Text5.Text
        ClaveIdSuper2.Caption = .Text8.Text
        clavesuper3.Caption = .Text6.Text
        ClaveIdSuper3.Caption = .Text9.Text
        ClaveDesc1.Caption = .Text10.Text
        NombreDesc1.Caption = .Text13.Text
        ClaveDesc2.Caption = .Text11
        NombreDesc2.Caption = .Text14
        ClaveDesc3.Caption = .Text12
        NombreDesc3.Caption = .Text15
    End With
    Ack = &HA
    portNum = 1
    NoRindes = 1
    PorParte = False
    LaRutadeArchivos = LBruta.Caption
    If Len(LaRutadeArchivos) = 0 Then LaRutadeArchivos = "C:\CAFEURO\"
    Actualiza = 1
    Desde = 36
    ColProd = 700 '500 '
    ColProd1 = 2400 '1500 '
    ColProd2 = 800 '500 '

    With AdoTurnos.Recordset
        .MoveFirst
        TurnoTurno.Caption = .Fields(0)    ''' Datos del Turno
        TurnoNombre.Caption = .Fields(1) 'I'nicioTurno.Text2.Text   ''' Actual
        TurnoFecha.Caption = .Fields(2) 'InicioTurno.Text3.Text
        TurnoInicio.Caption = .Fields(3) 'InicioTurno.Text4.Text
        TurnoEfeInicial.Caption = .Fields(5) 'InicioTurno.Text6.Text
    End With
    PermiteSacarItem = False
    Distinto = "1"
    Dim j As Integer
    For j = 1 To 150
        MesaStatus(j) = 1
    Next
    For j = 1 To 150
        MesaDescuento(j) = "0"
    Next
    For j = 1 To 99
        GarzonMesa(j) = "N"
    Next
    alto = 6000
    
    
    TxtCantidad.Text = ScrollCant.Value
   
    With Correlativos.datPrimaryRS.Recordset  ''   Actualiza Correlativos
        '.Caption = .Fields(0)
        LbBticketSalida.Caption = .Fields(1)
        LBBboleta.Caption = .Fields(2)
        LBnguia.Caption = .Fields(3)
        LBBfactura.Caption = .Fields(4)
    End With
  
    X = FreeFile
    i = 0
    Open LaRutadeArchivos & "pagodoc.opc" For Input As X
        Do While Not EOF(X)
            Line Input #X, s
            OpcionesDePago(i) = s
            i = i + 1
        Loop
    Close #X
    
    Y = FreeFile
    Open LaRutadeArchivos & "Idequipo.id" For Input As Y
        Line Input #Y, IdEquipo
    Close #Y
    
    X = FreeFile
    Open LaRutadeArchivos & "path.msa" For Input As X
        Line Input #X, PathMesas
    Close #X
    
    Y = FreeFile
    Open LaRutadeArchivos & "nivel.txt" For Input As Y
        Line Input #Y, QueNivel
    Close #Y


    Timer1.Interval = 1000
    MainColores(1) = &HFF00&
    MainColores(2) = &H808000
    MainColores(3) = &HFF&
    MainColores(4) = &H80FF&
    Dim tmesas As Integer
    
    tmesas = Val(lbmesas.Caption)
    S_Empresa = AdoConfiguracion.Recordset!empresa
    comenta.Caption = AdoConfiguracion.Recordset.Fields(19)
    
    'Designa Impresoras
    If Val(QueNivel) = 2 Then
        ImpVales.Caption = AdoSoloImpresoras.Recordset.Fields(3)
        ImpCocina.Caption = AdoSoloImpresoras.Recordset.Fields(2)
        ImpBarra.Caption = AdoSoloImpresoras.Recordset.Fields(1)
        ImpCafeteria.Caption = AdoSoloImpresoras.Recordset.Fields(0)
        InformesPrinter.Caption = AdoSoloImpresoras.Recordset.Fields(3)
    ElseIf Val(QueNivel) = 1 Then
        ImpVales.Caption = AdoSoloImpresoras.Recordset.Fields(7)
        ImpCocina.Caption = AdoSoloImpresoras.Recordset.Fields(6)
        ImpBarra.Caption = AdoSoloImpresoras.Recordset.Fields(5)
        ImpCafeteria.Caption = AdoSoloImpresoras.Recordset.Fields(4)
        InformesPrinter.Caption = AdoSoloImpresoras.Recordset.Fields(7)
    End If
    
    
    
    
    If AdoConfiguracion.Recordset.Fields(26) = 0 Then CargaMouse.Caption = "No Carga con Mouse"
    If AdoConfiguracion.Recordset.Fields(26) = 1 Then CargaMouse.Caption = "Si Carga con Mouse"
 '   PathMesas = AdoConfiguracion.Recordset.Fields("directorio")
'    MnIdEquipo.Caption = AdoConfiguracion.Recordset.Fields(28)
    
    If AdoConfiguracion.Recordset.Fields(25) = 0 Then
        ImpTotal.Caption = "Sin centro Producci�n"
        CmdCentroProduccion.Enabled = False
        enviar.Enabled = False
    End If
    If AdoConfiguracion.Recordset.Fields(25) = 1 Then
        ImpTotal.Caption = "Con centro Producci�n"
        CmdCentroProduccion.Enabled = True
        enviar.Enabled = True
    End If
    If AdoConfiguracion.Recordset.Fields(24) = 1 Then
        fiscalprint.Caption = "Si - Imprime boleta"
    ElseIf AdoConfiguracion.Recordset.Fields(24) = 0 Then
        fiscalprint.Caption = "No - Imprime boleta"
    End If
    If AdoConfiguracion.Recordset.Fields(4) = 1 Then
        MnAutDesc.Caption = "Descuento Libre"
    Else
        MnAutDesc.Caption = "Descuento Restringido"
    End If
    CantidadRubros = adoConRubros.Recordset.RecordCount
    alto = PictRubros.Height
    altoboton = alto / CantidadRubros
    If CantidadRubros > 17 Then CMDrubros(0).Font.Size = 11
    CMDrubros(0).Height = altoboton
    CMDrubros(0).Caption = adoConRubros.Recordset.Fields(1)
    For i = 1 To CantidadRubros - 1
        adoConRubros.Recordset.MoveNext
        Load CMDrubros(i)                                               '''  Crea Botones para rubros
        Load LTxRubro(i)                                                 '''  Crea Listas para cada rubro
        CMDrubros(i).Height = altoboton
        CMDrubros(i).Top = (altoboton * i)
        CMDrubros(i).Caption = adoConRubros.Recordset.Fields(1)
        CMDrubros(i).Visible = True
         LTxRubro(i).Top = LTxRubro(0).Top
         LTxRubro(i).Height = LTxRubro(0).Height
         LTxRubro(i).ColAlignment(1) = 1
         LTxRubro(i).Visible = False
    Next i
    
    
    lin = 1
    Pv = 0
    Ph = 0
    Dim Distancia As Integer, l As Integer
    Distancia = LBmesa(0).Width + 20
    MesaSelectItem = lbmesas.Caption + 1
    For l = 1 To Val(lbmesas.Caption) - 1
        If lin = 10 Then
            Pv = Pv + LBmesa(0).Height + 10
            lin = 0
         '   Ph = 7135 + Ph                               ''''   Crea iconos para mesas
            Distancia = 0
        End If
        Load LBmesa(l)
        LBmesa(l).Left = Distancia
        Distancia = Distancia + LBmesa(0).Width + 20
        LBmesa(l).Top = Pv
        LBmesa(l).Caption = l + 1
        LBmesa(l).Visible = True
        lin = lin + 1
        Load List1(l)
    Next
    Load List1(MesaSelectItem)

    Dim K As Integer, C As Integer
    'ANCHOS LTXRUBRO
    For K = 0 To CantidadRubros - 1
        With LTxRubro(K)
            .ColWidth(0) = ColProd
            .ColWidth(1) = ColProd1 + 2500
            .ColWidth(2) = ColProd2
            .ColWidth(3) = 1 '400 '1
            .ColWidth(4) = 1 '1 '400 '1
            .ColAlignment(1) = 1
            .ColAlignment(0) = 2
            .Width = ColProd + ColProd1 + ColProd2 + 2900
        End With
    Next
    
    With AdoAllProductos
        Dim Iden As String
        Dim Prec As String
        Dim Prod As String
        Dim Cod As String
        Dim Envia As String
        Dim Rub As Integer
        .Recordset.MoveFirst
        For C = 1 To .Recordset.RecordCount
            Iden = .Recordset.Fields(3) - 1 '''   Carga los productos a cada rubro
            Prec = .Recordset.Fields(2)
            Prod = .Recordset.Fields(1)
            Cod = .Recordset.Fields(0)
            Rub = .Recordset.Fields("Rubro")
         '   If Val(QueNivel) = 2 Then
                Envia = .Recordset.Fields("Dirigida")
        '    ElseIf Val(QueNivel) = 1 Then
         '       Envia = .Recordset.Fields(6)
        '    End If
            LTxRubro(Iden).Row = LTxRubro(Iden).Rows - 1
            LTxRubro(Iden).Col = 0
            LTxRubro(Iden).Text = Cod
            LTxRubro(Iden).Col = 1
            LTxRubro(Iden).Text = Prod
            LTxRubro(Iden).Col = 2
            LTxRubro(Iden).Text = Prec
            LTxRubro(Iden).Col = 3
            LTxRubro(Iden).Text = Rub
            LTxRubro(Iden).Col = 4
            LTxRubro(Iden).Text = Envia
            .Recordset.MoveNext
            LTxRubro(Iden).Rows = LTxRubro(Iden).Rows + 1
        Next
    End With
    Dim anchito As Integer, mialto As Integer, altocrow As Integer, garAlto As Integer
    anchito = ColProd + ColProd1 + ColProd2
    mialto = LTxRubro(0).Height
    altocrow = LTxRubro(0).RowHeight(0)
    For K = 0 To CantidadRubros - 1
        LTxRubro(K).Rows = LTxRubro(K).Rows - 1
        alto = LTxRubro(K).Rows * altocrow
        If alto < mialto Then
            LTxRubro(K).Height = alto + 10 '- (LTxRubro(k).Rows * 3)
            LTxRubro(K).ScrollBars = flexScrollBarNone
          '  LTxRubro(K).Width = anchito
          
            With LTxRubro(K)
                .ColWidth(0) = ColProd
                .ColWidth(1) = ColProd1 + 2500
                .ColWidth(2) = ColProd2
                .ColWidth(3) = 1
                .ColWidth(4) = 1
                .ColAlignment(1) = 1
                .ColAlignment(0) = 2
                .Width = ColProd + ColProd1 + ColProd2 + 2900
            End With
          
          
          '    With LTxRubro(K)
          '      .ColWidth(0) = ColProd - 250
          '      .ColWidth(1) = ColProd1 + 1100
          '      .ColWidth(2) = ColProd2 - 200
          '      .ColWidth(3) = 1 '400 '1
          '      .ColWidth(4) = 1 '400 '1
          '      .ColAlignment(1) = 1
          '      .ColAlignment(0) = 2
          '      .Width = ColProd + ColProd1 + ColProd2 + 900
          '  End With
         '   LTxRubro(K).ColWidth(0) = ColProd - 100
         '   LTxRubro(K).ColWidth(1) = ColProd1 + 1900
         '   LTxRubro(K).ColWidth(2) = ColProd2 '- 200
         '   LTxRubro(K).ColWidth(3) = 1 '400 '1
         '   LTxRubro(K).ColWidth(4) = 1 '400 '1
            LTxRubro(K).ColAlignment(1) = 1
        End If
    Next
    
    contGarzones = 0
    garAlto = ListaGarzones.Height
    Dim G As Integer, cadaBoton As Integer, b As Integer
    With AdoGarzon
        .Recordset.MoveFirst
        For G = 0 To .Recordset.RecordCount - 1
            If .Recordset.Fields(2) = "Garz�n" Then      '' Carga lista de Garzones
                Load ListaGarzones.CmdGarzones(contGarzones + 1)
             
                ListaGarzones.CmdGarzones(contGarzones).Caption = _
                .Recordset.Fields(0) & " - " & .Recordset.Fields(1)
                PassWordGarzon(contGarzones) = .Recordset.Fields(5)
                contGarzones = contGarzones + 1
            End If
            .Recordset.MoveNext
        Next G
    End With
    cadaBoton = garAlto / contGarzones
    With ListaGarzones
        For b = 0 To contGarzones
            .CmdGarzones(b).Height = cadaBoton
            .CmdGarzones(b).Top = cadaBoton * b
            .CmdGarzones(b).Visible = True
        Next b
        Unload .CmdGarzones(.CmdGarzones.Count - 1)
        .Height = .Height + 200
    End With
    For G = 0 To ListaGarzones.CmdGarzones.Count - 1
        NombreGar(G) = ListaGarzones.CmdGarzones(G).Caption
    Next
    Lg = ListaGarzones.CmdGarzones.Count
    NombreGar(Lg) = "N"
    Dim Q As Integer
    Q = FreeFile
    Open LaRutadeArchivos & "boletafiscal.cor" For Input As Q
        Line Input #Q, CorrBoletaFiscal
        NboletaFiscal.Caption = CorrBoletaFiscal
    Close #Q
    
       
    '' Unload ListaGarzones
    Unload Configuracion
    Unload DImpresoras
    CMDrubros(0).BackColor = &HFFFF&
    Label5(0).Caption = CMDrubros(0).Caption

    Label5(0).Caption = "1 -" & Label5(0).Caption
    ActualizarEstadoMesas
    
    Hr = FreeFile
    Dim pasito As String
    Open LaRutadeArchivos & "exactfood\RHappy.aso" For Input As Hr
        Line Input #Hr, HappyHours
        Line Input #Hr, pasito
    Close #Hr
    If pasito <> "Todo" Then
        HorarioIniHappy = Mid(pasito, 1, 5)
        HorarioFinHappy = Mid(pasito, 6)
    Else
        HorarioIniHappy = "Todo"
    End If
    

    If esCaja Then
        If fiscalprint.Caption = "Si - Imprime boleta" Then
            '�'�'Abrir periodo de ventas
           ' If Not Tf6.PortOpen Then
           '    'Asignar el numero de puerto a utilizar
           '    Tf6.CommPort = portNum
           '
           '    'Abrir puerto
           '    Tf6.PortOpen = True
           ' End If
       '
        '    Call evaluaCommand("32") 'Este comando abre el periodo
            
            'Cerrar puerto
        '    Tf6.PortOpen = False
        
        End If
    End If
    
End Sub

Private Sub informes_Click()
Form1.Show
End Sub



Private Sub introInsumo_Click()
    Main.Enabled = False
    IngresoInsumos.Show
    
End Sub
Private Sub LBmesa_Click(Index As Integer)
    'If Val(TxtMesaSel.Text) = 0 Then Exit Sub
    If Index < 0 Or Index > (Val(lbmesas.Caption) - 1) Then Exit Sub
    If LBmesa(Index).BackColor = MainColores(4) Then Exit Sub
    
    If LBmesa(Index).BackColor = MainColores(3) Then
                'hay que saber que pc emiti� el vale- en q caja
        
        Y = FreeFile
        StrMesa = PathMesas & "StatusTable" & Index + 1 & ".TAB"
        Open StrMesa For Input As Y
            Line Input #Y, s
        Close #Y
    
        
        estepc = Mid(s, 4, 4)
        If estepc <> IdEquipo Then Exit Sub
        
        
    End If
  '  ActualizarSoloUna (Index + 1)
       
    TxtMesaSel.Text = LBmesa(Index).Caption
    mesa = Val(LBmesa(Index).Caption) - 1
    For T = 0 To Val(lbmesas.Caption) - 1
        List1(T).Visible = False
    Next
    
    ExtraeDatosMesa (Index + 1)
    
    lbTconDesc.Caption = ""
    
    List1(Val(LBmesa(Index).Caption) - 1).Visible = True
    
    If MesaDescuento(Index + 1) = " " Then
        LBTotal.ForeColor = &H80000012
    End If
    
    If List1(Val(LBmesa(Index).Caption) - 1).ListCount = 0 Then
        LBTotal.Caption = "0"
        txtGarzonSel.Text = ""                      ''' Si la mesa est� desocupada
        LBmesa(mesa).BackColor = MainColores(1)
        
        'If Actualiza = 1 Then
        '    If Garzones.Value = 1 Then
        '        FrmFondo.Enabled = False
                'Main.Enabled = False
        '        ListaGarzones.Visible = True
        '    End If
        'Else
        '    Actualiza = 1
        'End If
    Else
        RubroDef1(mesa + 1) = 0
        RubroDef2(mesa + 1) = 0
        TotMesa = 0
        For K = 0 To List1(mesa).ListCount - 1
            restaItem = List1(mesa).List(K) ''  Se agregan productos a la mesa
            CantRetira = Val(Left(restaItem, 3))
            valorX = Val(Mid(restaItem, 6, 6)) * CantRetira
            If Val(Right(restaItem, 2)) = 1 Then
                RubroDef1(mesa + 1) = RubroDef1(mesa + 1) + valorX
            End If
            If Val(Right(restaItem, 2)) = 2 Then
                RubroDef2(mesa + 1) = RubroDef2(mesa + 1) + valorX
            End If
            TotMesa = TotMesa + valorX
        Next
        TxtNpersonas.Text = Ccomenzales(mesa + 1)
        LBTotal.Caption = ""
        LBTotal.Caption = TotMesa
        LBTotal.ForeColor = &H0&
        
        If MesaDescuento(Index + 1) <> "0" Then
            If Mid(MesaDescuento(Index + 1), 1, 1) = "$" Then
                TotMesa = TotMesa - Val(Mid(MesaDescuento(Index + 1), 2))
                DescuentoActual(Index + 1) = Val(Mid(MesaDescuento(Index + 1), 2))
                lbTconDesc.Caption = TotMesa + Val(Mid(MesaDescuento(Index + 1), 2))
            ElseIf IsNumeric(MesaDescuento(Index + 1)) Then
                midesc = (TotMesa / 100) * Val(MesaDescuento(Index + 1))
                midesc = Int((midesc / 100) * 10) * 10
                TotMesa = TotMesa - midesc
                DescuentoActual(Index + 1) = midesc
                lbTconDesc.Caption = TotMesa + midesc
            End If
            
            LBTotal.ForeColor = &HC00000
            
        End If
        LBTotal.Caption = TotMesa
        CmdComando(0).Enabled = True
      ' TxtCantidad.Text = ""
        'TxtCantidad.SetFocus
       ' txtGarzonSel.Text = GarzonMesa(Val(LBmesa(Index).Caption))
       ' txtGarzonSel.Text = lbgarcia.Caption
    End If
End Sub

Public Function CambiaMesa(Actual As Integer, Nueva As Integer)
    Dim StrActual As String
    Dim StrNueva As String
    Dim DatMesita(9) As Variant
    If Nueva > lbmesas.Caption Then
        Beep
        Exit Function
    End If
    For ca = 0 To List1(Actual - 1).ListCount - 1
        List1(Nueva - 1).AddItem List1(Actual - 1).List(ca)
    Next   '' Llena nueva lista
    StrActual = PathMesas & "Mesa" & Actual & ".ped"
    StrNueva = PathMesas & "Mesa" & Nueva & ".ped"
    GarzonMesa(Nueva) = GarzonMesa(Actual)
    GarzonMesa(Actual) = ""
    LBmesa(Actual - 1).BackColor = MainColores(1)
    LBmesa(Nueva - 1).BackColor = MainColores(2)
    MesaUpedido(Nueva) = MesaUpedido(Actual)
    MesaUpedido(Actual) = 0
    RubroDef1(Nueva) = RubroDef1(Actual)
    RubroDef1(Actual) = 0
    RubroDef2(Nueva) = RubroDef2(Actual)
    RubroDef2(Actual) = 0
    MesaDescuento(Nueva) = MesaDescuento(Actual)
    MesaDescuento(Actual) = 0
    Ccomenzales(Nueva) = Ccomenzales(Actual)
    Ccomenzales(Actual) = 0
    
    For V = List1(Actual - 1).ListCount - 1 To 0 Step -1
        List1(Actual - 1).RemoveItem V ''Borra lista de mesa anterior
    Next
    
    MesaStatus(Actual) = 1
    MesaStatus(Nueva) = 2
    
    Q = FreeFile ' Extraigo datos de mesa actual
    StrMesa = "StatusTable" & Actual & ".tab"
    Open PathMesas & StrMesa For Input As Q
        For i = 1 To 9
            Line Input #Q, DatMesita(i)
        Next
    Close #Q
    
    X = FreeFile 'Coloco datos en la mesa nueva
    StrMesa = "StatusTable" & Nueva & ".tab"
    Open PathMesas & StrMesa For Output As X
        DatMesita(1) = "Si-" & IdEquipo & "-" & Now
        For i = 1 To 9
            Print #X, DatMesita(i)
        Next
    Close #X
    
    MesaAcero (Actual) ' La mesa actual queda libre
 
    
    File1.Path = PathMesas

    StrNueva = "Mesa" & Nueva & ".PED"
    File1.Pattern = StrNueva
  
    If File1.ListCount <> 0 Then
        Kill PathMesas & StrNueva
    End If
    Name StrActual As PathMesas & StrNueva
    List1(Nueva - 1).Visible = True
    LBmesa_Click (Nueva - 1)
    Exit Function
RutinaDeError:
    ''   Se produce un erro pero no lo detecto
    Beep
End Function
Public Sub ActualizarEstadoMesas()
    Dim cr As Integer, rb As Integer, sl As Integer
    Dim mesa As Integer
    Dim Cpedido As Double
    Dim Citmes As Integer
    Dim mcan, mpre, mdet, mcod, mitem As String
    Dim StrMesa As String
    Dim ComoEsta As Variant
    For rb = 1 To 150
        RubroDef1(rb) = 0
        RubroDef2(rb) = 0
    Next

    File1.Path = PathMesas
    For i = 1 To Me.lbmesas      ' Recorro todas las mesas
        StrMesa = "StatusTable" & i & ".tab"
        File1.Pattern = StrMesa
        
        If File1.ListCount > 0 Then ' la mesa esta en el sistema
            X = FreeFile
            Open PathMesas & StrMesa For Input As X
                For j = 1 To 9
                    
                    Line Input #X, ComoEsta ' extraigo el estado
                    If j = 2 Then
                        GarzonMesa(i) = ComoEsta
                    End If
                    If j = 4 Then
                        Cpedido = Val(ComoEsta)
                    End If
                    If j = 5 Then
                        MesaStatus(i) = Val(ComoEsta)
                    End If
                    If j = 6 Then
                        Citmes = Val(ComoEsta)
                    End If
                    If j = 8 Then
                        MesaDescuento(i) = Val(ComoEsta)
                    End If
                    If j = 9 Then
                        Ccomenzales(mesa) = Val(ComoEsta)
                    End If
                Next
            Close #X
            
            If MesaStatus(i) = 2 Or MesaStatus(i) = 3 Then ' La mesa fue encontrada ocupada
                LBmesa(i - 1).BackColor = IIf(MesaStatus(i) = 2, MainColores(2), MainColores(3))
                MesaUpedido(i) = Cpedido
                'If List1(i - 1).ListCount > 0 Then  ' Limpio la lista de productos
                '    For sl = List1(i - 1).ListCount To 0 Step -1
                '        List1(i - 1).RemoveItem sl
                '    Next
                'End If
                List1(i - 1).Clear
                mesa = i
                'Q = FreeFile
                StrMesa = PathMesas & "mesa" & i & ".ped"
                Y = FreeFile
                LargoR = Len(rPedido)
                Open StrMesa For Random As Y Len = LargoR
                For act = 1 To LOF(Y) / LargoR
                    Get #Y, , rPedido
                    ProductoCodigo = Trim(rPedido.Codigo)
                    ProductoCodigo = ProductoCodigo & String(5 - Len(ProductoCodigo), "_")
                    IdentItemP = Trim(rPedido.Bebestible)
                    IdentItemP = String(3 - Len(IdentItemP), " ") & IdentItemP
                    Detalle = rPedido.Detalle
                    nuevaSuma = Trim(rPedido.PrecioU)
                   ' LbTotal.Caption = Val(LbTotal.Caption) + (Val(nuevaSuma) * Val(rPedido.Cantidad))
                    nuevoItem = rPedido.Cantidad & " - " & String(6 - Len(Str(nuevaSuma)), " ") & nuevaSuma & " - " & Detalle
                    Horita = rPedido.Hora
                    nuevoItem = nuevoItem & String(35 - Len(nuevoItem), " ") & Horita & "_____" & ProductoCodigo & rPedido.Rubro & IdentItemP
                    List1(mesa - 1).AddItem nuevoItem
                    IdentItemP = Val(IdentItemP)
                    If IdentItemP = 1 Then
                          RubroDef1(mesa) = RubroDef1(mesa) + (nuevaSuma * Val(rPedido.Cantidad))
                    ElseIf IdentItemP = 2 Then
                          RubroDef2(mesa) = RubroDef2(mesa) + (nuevaSuma * Val(rPedido.Cantidad))
                    End If
                    List1(mesa - 1).ListIndex = List1(mesa - 1).ListCount - 1
                                    
                Next
                Close #Y ' Datos extraidos
                LBmesa_Click (mesa - 1)
                
            End If
            If MesaStatus(i) = 1 Then
                LBmesa(i - 1).BackColor = MainColores(1)
                List1(i - 1).Clear
            End If
        Else ' La mesa no esta, por ende esta libre
            MesaStatus(i) = 1
            LBmesa(i - 1).BackColor = MainColores(1)
            If List1(i - 1).ListCount > 0 Then
                For sl = List1(i - 1).ListCount To 0 Step -1
                    List1(i - 1).RemoveItem sl
                Next
            End If
            GarzonMesa(i) = ""
            Ccomenzales(i) = 0
        End If
    Next
    
    
    FrmFondo.Enabled = True
    Actualiza = 1
    
End Sub

Public Function JuntaMesas(MActiva As Integer, AUnir As Integer)
    Dim StrActiva As String
    Dim StrAunir As String
    Dim StrMesaTable As String
    Dim Au As Double
    Dim RestaItem100 As String
    If AUnir > 0 Then
        If List1(AUnir - 1).ListCount = 0 Then
            Beep
            Exit Function
        End If
        '�  Agrego a la mesa la lista de la mesa a unir
        '   Agrego los productos de ambas mesas a una lista temporal
        ListaTemporal.Clear
        For Au = List1(AUnir - 1).ListCount - 1 To 0 Step -1
            ListaTemporal.AddItem List1(AUnir - 1).List(Au)
        Next
        For Au = List1(MActiva - 1).ListCount - 1 To 0 Step -1
            ListaTemporal.AddItem List1(MActiva - 1).List(Au)
        Next
        
        ''''    En esta rutina junto dos mesas
        StrMesaTable = "StatusTable" & MActiva & ".tab"
        X = FreeFile
        Open PathMesas & StrMesaTable For Input As X
            For j = 1 To 4
                Line Input #X, ComoEsta ' extraigo el Npedido
                If j = 4 Then
                    jtpedido = ComoEsta
                End If
            Next
        Close #X
        StrAunir = PathMesas & "Mesa" & AUnir & ".ped"
        StrActiva = PathMesas & "Mesa" & MActiva & ".ped"
        Kill StrAunir
        Kill StrActiva
        List1(MActiva - 1).Clear
        List1(AUnir - 1).Clear
        Xactiva = FreeFile
        LargoR = Len(rPedido)
        Open StrActiva For Random As #Xactiva Len = LargoR
            With rPedido
                For Aun = 0 To ListaTemporal.ListCount - 1
                    RestaItem100 = ListaTemporal.List(Aun)
                    .Npedido = nowpedido   '' Archiva de nuevo la lista
                    .Hora = Mid(RestaItem100, Desde, 9)
                    .Nmesa = Str(mesaactiva + 1)
                    .Codigo = Str(Val(Mid(RestaItem100, 50, 5)))
                    .Cantidad = Left(RestaItem100, 3)
                    .Detalle = Mid(RestaItem100, 15, 20)
                    .PrecioU = Mid(RestaItem100, 6, 6)
                    .SubTotal = Str(.Cantidad * .PrecioU)
                    .Bebestible = Right(RestaItem100, 1)
                    .Rubro = Mid(RestaItem100, 55, 2)
                    Put #Xactiva, , rPedido
                Next
                
            End With
        Close #Xactiva
        
        Qnuevo = FreeFile
        Open PathMesas & StrMesaTable For Output As Qnuevo
            Print #Qnuevo, "Si-" & IdEquipo & "-" & Now
            Print #Qnuevo, lbgarcia.Caption ' txtGarzonSel.Text
            Print #Qnuevo, Time
            Print #Qnuevo, jtpedido
            Print #Qnuevo, 2
            Print #Qnuevo, ListaTemporal.ListCount
            Print #Qnuevo, LBTotal.Caption
            Print #Qnuevo, "0"
            Print #Qnuevo, 2
        Close #Qnuevo
        ListaTemporal.Clear
        List1(AUnir - 1).Clear
        MesaAcero (AUnir)
        ExtraeDatosMesa (MActiva)
        RubroDef1(MActiva) = RubroDef1(MActiva) + RubroDef1(AUnir)
        RubroDef2(MActiva) = RubroDef2(MActiva) + RubroDef2(AUnir)
        Ccomenzales(MActiva) = Ccomenzales(MActiva) + Ccomenzales(AUnir)
        Ccomenzales(AUnir) = 0
        LBmesa(AUnir - 1).BackColor = MainColores(1)
        GarzonMesa(AUnir) = ""
        MesaStatus(AUnir) = 1
        RubroDef1(AUnir) = 0
        RubroDef2(AUnir) = 0
        MesaDescuento(AUnir) = 0
        MesaUpedido(AUnir) = 0
    End If
    'AccionMesa.Normal_Click
    Unload AccionMesa
    FrmFondo.Enabled = True
    Main.SetFocus
    'LBmesa_Click (MActiva - 1)
End Function
Function ImprimeVale(miDoc As String, ValorOtroDoc As String, SaldoOtroDoc As String)
    Dim Pdido As Long
    Dim UTotal As Long
    Dim Q As Integer
    Dim s As String
    Dim Unidad As String * 2
    Dim Detalle As String * 20
    Dim Precio As String * 6
    Dim cTotal As String * 7
    Dim Total As String * 15
    
    Dim StrMesa As String
    Dim Apagar As Double
    Dim ParaDescontar As Double
    Dim Impresora As Printer
    Dim StsTemporal(13) As String
    Dim ValeCantidad(99) As String * 2, ValeDetalle(99) As String * 25, ValeTotal(99) As String * 7, ValeUnitario(99) As String * 7
    For Each Impresora In Printers
        If Impresora.DeviceName = ImpVales.Caption Then
            Set Printer = Impresora
            Exit For
        End If
    Next
    ParaDescontar = 0
    xMesa = Val(TxtMesaSel.Text) - 1
    StrMesa = PathMesas & "Mesa" & (xMesa + 1) & ".ped"
    If AccionMesa.LBIden.Caption = lbmesas.Caption + 1 Then
        xMesa = AccionMesa.LBIden.Caption
    End If
    Q = FreeFile
    Open PathMesas & "last.pd" For Input As Q
        Line Input #Q, s
        Pdido = Val(s)
    Close #Q
    Label11.Caption = "Tket:" & Pdido
    LBpedido.Caption = Pdido
    UPedido = LBpedido.Caption
    Q = FreeFile
    
    Open PathMesas & "last.pd" For Output As Q
        Print #Q, Pdido + 1
    Close #Q
    If Val(AccionMesa.LBIden.Caption) <> MesaSelectItem Then
        With AdoPedidos.Recordset
            X = FreeFile
            LargoR = Len(rPedido)
            Open StrMesa For Random As #X Len = LargoR
      '      Pdido = Me.LBpedido.Caption
            LBpedido.Caption = LBpedido.Caption + 1
            For j = 1 To LOF(X) / LargoR
                
                Get #X, , rPedido
                .AddNew
                .Fields(0) = Pdido
                'Pdido = rPedido.Npedido
                .Fields(1) = Main.TurnoFecha.Caption ' fecha
                .Fields(2) = rPedido.Hora 'hora
                .Fields(3) = rPedido.Nmesa ' mesa
                .Fields(4) = rPedido.Codigo ' codigo
                .Fields(5) = rPedido.Cantidad ' cantidad
                .Fields(6) = rPedido.Detalle ' detalle
                .Fields(7) = Val(Trim(rPedido.PrecioU))
                .Fields(8) = Val(Trim(rPedido.SubTotal)) 'subtotal
                .Fields(9) = rPedido.Bebestible ' bebestible
                .Fields(10) = rPedido.Rubro 'rubro
                .Fields(11) = Main.TurnoTurno.Caption 'turno
                If Val(txtGarzonSel.Text) = 0 Then txtGarzonSel.Text = "No Ingres�"
                .Fields(12) = lbgarcia.Caption  'txtGarzonSel.Text
                .MoveNext
            Next
          
            Close #X
        End With
        'Kill StrMesa
    End If
    
   
        If LBlevelControl.Caption = 1 Then ' Control Caja y Adicion
        
            StrMesa = "StatusTable" & xMesa + 1 & ".tab"
            If xMesa = MesaSelectItem Then
                StrMesa = "StatusTable" & xMesa & ".tab"
            End If
            X = FreeFile
            Open PathMesas & StrMesa For Input As X
                For j = 1 To 9
                    Line Input #X, ComoEsta ' extraigo el estado
                    StsTemporal(j) = ComoEsta
                Next
            Close #X
            If xMesa <> MesaSelectItem Then
             '   yy = FreeFile
             '   Open PathMesas & "Tmp\" & TxtMesaSel.Text & ".tmp" For Output As yy
             '      Print #yy, "Si-" & IdEquipo & "-" & Now
             '      Print #yy, "3"
             '   Close #yy
                
                'Pdido = StsTemporal(4)
                StsTemporal(5) = 3
                StsTemporal(10) = miDoc
                StsTemporal(11) = IIf(miDoc = "Boleta", Str(Me.NboletaFiscal.Caption), "0")
                If Val(SaldoOtroDoc) > 0 Then
                    StsTemporal(12) = SaldoOtroDoc
                    StsTemporal(13) = Me.NboletaFiscal.Caption
                Else
                     StsTemporal(12) = "Nada"
                     StsTemporal(13) = "Nada"
                End If
            Else
                Y = FreeFile
                
                Dim Sts2Temporal(9) As Variant
                Open PathMesas & "StatusTable" & TxtMesaSel.Text & ".tab" For Input As Y
                    For i = 1 To 9
                        Line Input #Y, Sts2Temporal(i)
                    Next
                Close #Y
                Sts2Temporal(1) = "Si-" & IdEquipo & "-" & Now
                Sts2Temporal(4) = Pdido
                Sts2Temporal(6) = List1(TxtMesaSel.Text - 1).ListCount
                Sts2Temporal(7) = LBTotal.Caption
                X = FreeFile
                Open PathMesas & "StatusTable" & TxtMesaSel.Text & ".tab" For Output As X
                    For i = 1 To 9
                        Print #X, Sts2Temporal(i)
                    Next
                Close #X
            End If
            Q = FreeFile
            StsTemporal(4) = Pdido
            StsTemporal(1) = "Si-" & IdEquipo & "-" & Now
            Open PathMesas & StrMesa For Output As Q
                For j = 1 To 13
                    Print #Q, StsTemporal(j)
                Next
            Close #Q
            
            If miDoc = "Boleta" Then
                Me.NboletaFiscal.Caption = Me.NboletaFiscal.Caption + 1
                Q = FreeFile
                Open LaRutadeArchivos & "boletafiscal.cor" For Output As Q
                    Print #Q, Trim(Main.NboletaFiscal.Caption)
                Close #Q
            End If
        End If
        

    If xMesa <> MesaSelectItem Then
        If DescuentoActual(TxtMesaSel.Text) > 0 Then
            ds = FreeFile
            If Main.Dscto.Value = 0 Then
                Open LaRutadeArchivos & Trim(TxtMesaSel.Text) & ".dct" For Input As ds
                        Line Input #ds, Autorizador
                Close ds
                
            End If
            With AdoDescuentos.Recordset
                .AddNew
                .Fields(0) = TxtMesaSel.Text
                .Fields(1) = Time
                .Fields(3) = LBTotal.Caption + DescuentoActual(TxtMesaSel.Text)
                .Fields(2) = DescuentoActual(TxtMesaSel.Text)
                DescuentoActual(TxtMesaSel.Text) = 0
                .Fields(4) = TurnoTurno.Caption
                .Fields(5) = TurnoFecha.Caption
                .Fields(6) = lbgarcia.Caption
                If Dscto.Value = 0 Then
                    .Fields(7) = Autorizador
                End If
                .Update
            End With
           
                
        End If
        If LBlevelControl.Caption = 1 Then
            LBmesa(xMesa).BackColor = MainColores(3)
            MesaStatus(xMesa + 1) = 3
        End If
        If LBlevelControl.Caption = 2 Then
            LBmesa(xMesa).BackColor = MainColores(1)
            MesaStatus(xMesa + 1) = 1
        End If
        
    End If
    
    If LBlevelControl.Caption = 2 Then ' Solo adicion
        If TxtMesaSel.Text <> MesaSelectItem Then
            
            With AdoVales.Recordset
                .AddNew
                .Fields(0) = Pdido
                .Fields(1) = TxtMesaSel.Text
                .Fields(2) = AccionMesa.LBTotal.Caption
                .Fields(3) = Time
                .Fields(4) = lbgarcia.Caption
                .Fields(5) = TurnoFecha.Caption
                .Fields(6) = TurnoTurno.Caption
                LbBticketSalida.Visible = True
                .Fields(7) = LbBticketSalida.Caption
                LbBticketSalida.Visible = False
                If PorParte = False Then
                    .Fields(8) = RubroDef1(TxtMesaSel.Text)
                    .Fields(9) = RubroDef2(TxtMesaSel.Text)
                Else
                    .Fields(8) = RubroDef1(TxtMesaSel.Text)
                    .Fields(9) = RubroDef2(TxtMesaSel.Text)
                End If
                .Fields(10) = NoRindes
                .Update
            End With
            If PorParte = False Then
                 GarzonMesa(TxtMesaSel.Text) = ""
                 MesaStatus(TxtMesaSel.Text) = 1
                 MesaUpedido(TxtMesaSel.Text) = 0
                 MesaDescuento(TxtMesaSel.Text) = 0
                 DescuentoActual(TxtMesaSel.Text) = 0
                 RubroDef1(TxtMesaSel.Text) = 0
                 RubroDef2(TxtMesaSel.Text) = 0
                ' AccionMesa.LBIden.Caption = 0
                ' AccionMesa.LBTotal.Caption = 0
            End If
            PorParte = False
        End If
    End If
    cantItems = List1(xMesa).ListCount
    
    ' *j* es solamente vale interno *j*
    'If Mid(fiscalprint.Caption, 1, 2) = "No" Then
    If miDoc <> "Boleta" Or Mid(fiscalprint.Caption, 1, 2) = "No" Then
        Q = FreeFile
        
        Open "LPT1" For Output As Q
            Print #Q, Chr$(&H1B); "a"; Chr$(1);
            Print #Q, Chr$(&H1B); "!"; Chr$(50);
            Print #Q, S_Empresa
            
            Print #Q, Chr$(&H1B); "!"; Chr$(6);
            Print #Q, Chr$(&H1B); "a"; Chr$(0);
            Print #Q, ""
            Print #Q, " "
            Print #Q, "TICKET NRO " & Pdido
        'Print #Q, Space(NCol.Caption) & Mid(S_Empresa, 10)
        
       
        LbBticketSalida.Caption = LbBticketSalida.Caption + 1
        Correlativos.txtFields(1).Text = LbBticketSalida.Caption
    
        
        Print #Q, "Mesa:" & TxtMesaSel.Text & "       " & _
        Time & "       " & TurnoFecha.Caption
        Print #Q, Space(NCol.Caption) & "Garz�n:  " & lbgarcia.Caption
        Print #Q, Space(NCol.Caption + 2) & "_____________________________"
        Print #Q, ""
        Print #Q, Space(NCol.Caption - 3) & "Un   Detalle            Total"
        For X = 0 To cantItems - 1
            esteitem = List1(xMesa).List(X)
            SoloItem = Mid(esteitem, 15, 20)
            CantRetira = Val(Left(esteitem, 3))
            valorX = Val(Mid(esteitem, 6, 6))
            SubTotal = CantRetira * valorX
            Unidad = CantRetira
            Detalle = SoloItem
            Precio = Str(valorX)
            cTotal = Str(SubTotal)
            UTotal = UTotal + SubTotal
            Print #Q, Space(NCol.Caption - 3) & Unidad; " "; Detalle; " "; cTotal
            
        Next
        Apagar = AccionMesa.LBTotal.Caption
       ' If Apagar <> UTotal Then
       '     ' hay descuento
       '     ' imprimir informacion
       '     ParaDescontar = UTotal - Apagar
       '' End If
        
       ' Print #Q, " "
        If Val(lbTconDesc) > 0 Then
            Print #Q, "      Descuento $" & lbTconDesc - LBTotal
        End If
        'si es vale parcial
        Debug.Print PorParte
        Print #Q, Chr$(&H1B); "a"; Chr$(1);
        Print #Q, Chr$(&H1B); "!"; Chr$(50);
            
            
            
        If PorParte Then
            Print #Q, "TOTAL $ " & Apagar
        Else
            Print #Q, "TOTAL $ " & LBTotal
        End If
            Print #Q, Chr$(&H1B); "!"; Chr$(6);
            Print #Q, Chr$(&H1B); "a"; Chr$(0);
        Print #Q, Space(NCol.Caption) & "Docuemento de pago:"; miDoc
        Print #Q, "    Este ticket no tiene valor tributario" ' comenta.Caption
         Print #Q, Chr$(&H1D); "V"; Chr$(66); Chr$(0); 'Feeds paper & cut

        
        Close #Q
        Printer.EndDoc
        
        'abrimos archivo con datos del pago
        Q = FreeFile
        Open LaRutadeArchivos & "ticket.txt" For Input As Q
        Line Input #Q, s
        paso = CambiaLetra("Arial", 14, False, True, True)
        Printer.Print s  '' Titulo
        Line Input #Q, s
        Printer.Print s  '' liena en blanco
        Line Input #Q, s
        paso = CambiaLetra("Arial", 10, False, True, True)
        Printer.Print s '' Direccion
        Line Input #Q, s
        Printer.Print s   '' linea en blanco
        paso = CambiaLetra("Arial", 9, False, False, False)
        Line Input #Q, s
        Printer.Print s  '' mesa,hora,fecha
        Line Input #Q, s
        Printer.Print s '' garzon
        Line Input #Q, s
        Printer.Print s ''' line horizontal
        Line Input #Q, s
        Printer.Print s  '' Un Detalle Total
        paso = CambiaLetra("Courier New", 10, False, True, False)
        Do While Not EOF(Q)
            Line Input #Q, s
            If Mid(s, 1, (8 + NCol.Caption)) = Space(NCol.Caption) & "   TOTAL" Then
                paso = CambiaLetra("Times Roman", 16, False, True, False)
                Printer.Print s  '' Total
                paso = CambiaLetra("Arial", 7, False, False, False)
            Else
                Printer.Print s  '' Detalle
            End If
        Loop
        Close #Q
        Pago.Visible = False
        'Printer.NewPage
        ''Printer.EndDoc
        AccionMesa.Visible = False
        PorParte = False
    End If ' fin de vale interno
    
    '*******************************
    '* imprime boleta ahora fiscal *
    '*******************************
    'Verificar si corresponde a Boleta
    'o a otro documento (ch/restaurant ticket resta, fac, guia)
    If Mid(fiscalprint.Caption, 1, 2) = "Si" Then
    
        If AccionMesa.Interno.Value = True Then
            For Each Impresora In Printers
                If Impresora.DeviceName = InformesPrinter.Caption Then
                    Set Printer = Impresora
                    Exit For
                End If
            Next
        End If
        'Exit Sub
           '''Aqui usaremos IMPRESORA FISCAL
            If xMesa <> MesaSelectItem Then
                           
                If AccionMesa.Normal.Value = True Then
                    '"Definitavamente" & vbCr & "hay que imprimir boleta"
                    AccionMesa.Caption = "IMPRIMIENDO BOLETA...POR FAVOR ESPERE..."
        
                    portNum = 1
                    On Error GoTo port_error2
                    
                    
                    '31 Mayo 2017
                    'Aqui insertaremos codigo boleta fiscal Bixolon
                    'Barenco
                    
                    
                    EmiteBoleta
                    
                    
                    GoTo saltarIBM 'nos saltamos el codigo de  IBM
                    
                    If Not Main.Tf6.PortOpen Then
                       'Asignar el numero de puerto a utilizar
                       Main.Tf6.CommPort = portNum
                    
                       'Abrir puerto
                       Main.Tf6.PortOpen = True
                    End If
                    
                    'Optimista
                    error_26h = False
                    
                    'Abrir boleta
                    Call Main.evaluaCommand("001")
                    
                    'Si no hubo problema para abrir la boleta, continuar
                    If Not error_26h Then
                        AccionMesa.Normal.Visible = False
                        AccionMesa.Interno.Visible = False
                        For i = 0 To 4
                            AccionMesa.CmdMesa(i).Enabled = False
                        Next
                    
                        
                        Dim totalLinea As String
                        Dim PrecioX As String
                        Dim MValorLinea As String * 9
                        Dim AnchoValor As String
                        Dim LaCantidadDeMinutos As String * 9
                        Dim EmpezarEn As Integer
                        Dim Acobrar As String * 9 '  000
                        Dim MiEfectivo As String * 10
                        Dim Instruccion As String
                        Dim InsFiscalCodigo As String * 13
                        Dim InsFiscalCantidad As String * 6
                        Dim InsFiscalPrecio As String * 9
                        Dim InsFiscalSTotal As String * 10
                        Dim InsFiscalDescuento As String * 9
                        InsFiscalDescuento = "000000000"
                        InsFiscalPrecio = "000000000"
                        InsFiscalCodigo = "0000000000000"
                        InsFiscalCantidad = "000000"
                        MValorLinea = "000000000"
                        InsFiscalSTotal = "0000000000"
                        
                        'Comentarios al inicio de la boleta
                        Call evaluaCommand("111" & "Hora emisi�n:" & Time)
                        Instruccion = "111CAJERO(A):" & Main.TurnoNombre.Caption & " - Tket:" & Pdido
                        Call Main.evaluaCommand(Instruccion)
                        'Call evaluaCommand("111LINEA DE COMENTARIO ENCABEZADO 1")
                        'Instruccion = "111No INTERNO: " & Pdido
                        'Call Main.evaluaCommand(Instruccion)
                        
                        Instruccion = "111TURNO :" & Main.TurnoTurno.Caption & "  MESA:" & Main.TxtMesaSel.Text & "  G:" & lbgarcia.Caption
                        Call Main.evaluaCommand(Instruccion)
                        'Call Principal.evaluaCommand("111***         ****              **")
                              
                        'Boleta y caja
                        Call Main.evaluaCommand("12")
                    
                        'Vender un producto 1 (formato corto)
                        ''detallando el producto
                        Call Main.evaluaCommand("30")
                        For j = 0 To List1(xMesa).ListCount - 1
                            esteitem = List1(xMesa).List(j)
                            ProdCodi = Trim(Str(Val(Mid(esteitem, 50, 5))))
                            CantRetira = Trim(Str(Val(Left(esteitem, 3))))
                            productox = Mid(esteitem, 15, 20) ' detalle
                            PrecioX = Trim(Str(Val(Mid(esteitem, 6, 6))))
                            totalLinea = Trim(Str(Val(CantRetira) * Val(PrecioX)))
                            Mid(MValorLinea, 10 - Len(totalLinea), Len(totalLinea)) = totalLinea '+total
                            Mid(InsFiscalCodigo, 14 - Len(ProdCodi), Len(ProdCodi)) = ProdCodi
                            Mid(InsFiscalCantidad, 7 - Len(CantRetira), Len(CantRetira)) = CantRetira
                            Mid(InsFiscalPrecio, 10 - Len(PrecioX), Len(PrecioX)) = PrecioX
                            
                            Instruccion = "13112" ' inicio item
                            Instruccion = Instruccion & InsFiscalCodigo  '+ codigo
                            
                            Instruccion = Instruccion & InsFiscalCantidad & "000" '+cantidad
                            
                            Instruccion = Instruccion & InsFiscalPrecio '+precio
                            
                            Instruccion = Instruccion & MValorLinea 'total item
                            
                            Instruccion = Instruccion & productox '+detalle
                            
                            If Val(InsFiscalPrecio) > 0 Then Call Main.evaluaCommand(Instruccion) ' vende Item
                            'call Principal.evaluaCommand("131127801610001295000001000" & Detalle)
                                                          '13fiocccccccccccccqqqqqqqqqppppppppptttttttttxxxxxx...
                            InsFiscalPrecio = "000000000"
                            InsFiscalCodigo = "0000000000000"
                            InsFiscalCantidad = "000000"
                            MValorLinea = "000000000"
                            totalLinea = ""
                            CantRetira = ""
                            PrecioX = ""
                        Next
                        'totaliza
                        
                        '' Si hay descuento colocar subtotal
                        If Val(lbTconDesc.Caption) > 0 Then
                            'Subtotal
                            Mid(InsFiscalSTotal, 11 - Len(Trim(lbTconDesc.Caption)), Len(Trim(lbTconDesc.Caption))) = Trim(lbTconDesc.Caption)
                            Instruccion = "19" & InsFiscalSTotal
                            Call Main.evaluaCommand(Instruccion)
                            
                            'Descuento
                            Mid(InsFiscalDescuento, 10 - Len(Trim(Str(Val(lbTconDesc.Caption) - Val(LBTotal.Caption)))), Len(Trim(Str(Val(lbTconDesc.Caption) - Val(LBTotal.Caption))))) = Trim(Str(Val(lbTconDesc.Caption) - Val(LBTotal.Caption)))
                            Instruccion = "1731" & InsFiscalDescuento & "        Descuento Cliente     "
                            Call Main.evaluaCommand(Instruccion)        '123456789012345678901234567890"
                            
                        End If
                        
                        'Total
                        Mid(InsFiscalSTotal, 11 - Len(Trim(LBTotal.Caption)), Len(Trim(LBTotal.Caption))) = Trim(LBTotal.Caption)
                        'Total Boleta
                        Instruccion = "20" & InsFiscalSTotal
                        Call Main.evaluaCommand(Instruccion)
                                                   '  "200000003600"
                        'Formas de pago
                        Instruccion = "263101" & InsFiscalSTotal ' Efectivo
                        Call Main.evaluaCommand(Instruccion) 'DESCRIPCION ADICIONAL FP      ")
                        '                              fittpppppppppp
                        'Fin de pagos
                        Call Main.evaluaCommand("271000000000")
                                          '    fmmmmmmmmm
                        'Comentarios al pie de la boleta
                       'Call Main.evaluaCommand("111***     USTED FUE ATENDIDO POR    ***")
                        Call Main.evaluaCommand("111                                     ")
                        Call Main.evaluaCommand("111        DE LUNES  A  SABADO          ")
                        Call Main.evaluaCommand("111*  DESDE LAS 18:00 A LAS 22:00 HRS. *")
                        Call Main.evaluaCommand("111**       H A P P Y    H O U R S     *")
                       ' Instruccion = "111" & Main.txtGarzonSel.Text
                        'Call Main.evaluaCommand(Instruccion)
                        
                        'Cerrar boleta
                        Call Main.evaluaCommand("99")
                         'Cerrar puerto
                        Main.Tf6.PortOpen = False
                    End If
saltarIBM:
                    AccionMesa.Hide
                    AccionMesa.Caption = "Acci�n para la mesa"
                    AccionMesa.Normal.Visible = True
                    AccionMesa.Interno.Visible = True
                    For i = 0 To 4
                        AccionMesa.CmdMesa(i).Enabled = True
                    Next
                   ' Y = FreeFile
                   ' Open PathMesas & "Tmp\" & TxtMesaSel.Text & ".tmp" For Output As Y
                   ' Print #Y, "Si-" & IdEquipo & "-" & Now
                   ' Print #Y, "3"
                   ' Close #Y ' Esta info es para el robot, al
                    'cazar esta informacion debe avisar al resto
                    'que la mesa esta al cobro
                   
             
                ElseIf AccionMesa.Interno = True Then
                    If Val(SaldoOtroDoc) > 0 Then ''''AKI BOLETA POR SALDO
                            Saldillo = Val(SaldoOtroDoc)
                            AccionMesa.Caption = "Imprimiendo Boleta...Espere"
                            portNum = 1
                            'On Error GoTo port_error
                            
                            If Not Main.Tf6.PortOpen Then
                               Main.Tf6.CommPort = portNum
                               Main.Tf6.PortOpen = True
                            End If
                            error_26h = False
                            If Not error_26h Then
                                AccionMesa.Normal.Visible = False
                                AccionMesa.Interno.Visible = False
                                Dim InsFiscalSaldillo As String * 9
                                InsFiscalSaldillo = "000000000"
                                For i = 0 To 4
                                    AccionMesa.CmdMesa(i).Enabled = False
                                Next
                                Call Main.evaluaCommand("001")
                                Instruccion = "111CAJERO(A):" & Main.TurnoNombre.Caption & " - Tket:" & Pdido
                                Call Main.evaluaCommand(Instruccion)
                                Instruccion = "111TURNO:" & Main.TurnoTurno.Caption & "   MESA:" & Me.TxtMesaSel.Text & "   G:" & lbgarcia.Caption
                                Call Main.evaluaCommand(Instruccion)
                                Call Main.evaluaCommand("12")
                                Mid(InsFiscalSaldillo, 10 - Len(Trim(Str(Saldillo))), Len(Trim(Str(Saldillo)))) = Trim(Str(Saldillo))
                                Instruccion = "13112COD_SALDO0000000001000"
                                Instruccion = Instruccion & InsFiscalSaldillo & InsFiscalSaldillo & "Saldo " & miDoc
                                '             '13fiocccccccccccccqqqqqqqqqppppppppptttttttttxxxxxx...
                                Call Main.evaluaCommand(Instruccion)
                                'Call Principal.evaluaCommand("131127801610001295000001000" & Detalle)
                                Instruccion = "200" & InsFiscalSaldillo
                                Call Main.evaluaCommand(Instruccion)
                                                           '  "200000003600"
                                Instruccion = "2631010" & InsFiscalSaldillo ' Efectivo
                                Call Main.evaluaCommand(Instruccion) 'DESCRIPCION ADICIONAL FP      ")
                                Call Main.evaluaCommand("271000000000")
                                'Cerrar boleta
                                Call Main.evaluaCommand("99")
                                BoletillaMesa(TxtMesaSel.Text) = Main.NboletaFiscal.Caption
                                SaldilloMesa(TxtMesaSel.Text) = Saldillo
                                Main.NboletaFiscal.Caption = Main.NboletaFiscal.Caption + 1
                                Q = FreeFile
                                Open LaRutadeArchivos & "boletafiscal.cor" For Output As Q
                                    Print #Q, Trim(Main.NboletaFiscal.Caption)
                                Close #Q
                                Saldillo = 0
               
                            End If
                    End If  '' FIN BOLETA POR SALDO
                                   
                End If ' fIN DE LA BOLETA FISCAL
       
        End If
            ''' eSTO LO USO SI USO IMPRESORA
            ''' TERMICA NO FISCAL
            
            'For X = 0 To 99
            '    ValeCantidad(X) = ""
            '    ValeDetalle(X) = ""
            '    ValeTotal(X) = ""
            '    ValeUnitario(X) = ""
            'Next

            'For X = 0 To List1(xMesa).ListCount - 1
            '    esteitem = List1(xMesa).List(X)
            '    SoloItem = Mid(esteitem, 15, 20)
            '    CantRetira = Val(Left(esteitem, 3))
            '    valorX = Val(Mid(esteitem, 6, 6))
            '    SubTotal = CantRetira * valorX
          '
          '      Unidad = CantRetira
          '      Detalle = SoloItem
          '      Precio = Str(valorX)
          '      cTotal = Str(SubTotal)
          '      UTotal = UTotal + SubTotal
          '      ValeCantidad(X) = Unidad
          '      ValeDetalle(X) = Detalle
          '      ValeTotal(X) = cTotal
          '      ValeUnitario(X) = valorX
          '
          '  Next
          '  ''Aqui se creara un archivo texto
          '  ''para imprimir Boleta Fiscal
          '  ' La boleta impresa ser� guardada y el nombre
          '  ' ser� el N� boleta y la fecha
          '  If miDoc = "Boleta" Then
          '      Dim estaBoletaEs As String
          '      estaboleta = Val(NboletaFiscal.Caption) + 1
          '      estaBoletaEs = TurnoTurno.Caption & "_" & TurnoFecha.Caption & "_" & estaboleta & ".bol"
          '      With AdoEstadoM.Recordset
          '          Criterio = "CodMesa = " & xMesa + 1
          '          If xMesa = MesaSelectItem Then Criterio = "CodMesa = " & xMesa
          '          .MoveFirst
          '          .Find Criterio
          '          .Fields(10) = estaBoletaEs
'         '                          .Fields(9) = miDoc
'         ''       .Fields(10) = IIf(miDoc = "Boleta", Str(Main.NboletaFiscal.Caption), "0")
'
'                    .MoveFirst
          '          .Update
          '      End With
          '  Else
          '      estaBoletaEs = TurnoTurno.Caption & "_" & TurnoFecha.Caption & "_" & Pdido & ".bol"
          '  End If
          '  X = FreeFile
         '   Open LaRutadeArchivos & "\HistorialBoletas\" & estaBoletaEs For Output As X
         '       ' Se debe comprobar tambien si existe descuento
         '       If miDoc = "Boleta" Then
                
         '           Print #X, "Manuel Bulnes 489 - Temuco - Temuco"
         '           Print #X, "Giro: Restaurant"
         '           Print #X, "Soc. Comercial Euro Ltda"
         '           Print #X, "R.U.T.:77.774.580-8"
         '           Print #X, "Resol. Ex. SII N�40 del 15 de Julio del 2003"
         '           Print #X, "BOLETA AUTORIZADA POR EL S.I.I."
         '           Print #X, " "
         '           Print #X, "Fecha:" & TurnoFecha.Caption & "  Hora:" & Time()
         '           Print #X, " "
         '           Print #X, "Boleta NRO:" & estaboleta
         '       Else ' Si no es boleta hay que identificar el
         '            ' el documento y comprobar si el monto en
         '            ' otro doc. cubre el total, si no fuese as�
         '            ' lo l�gico es dar la boleta por la diferencia
         '           Print #X, "Manuel Bulnes 489 - Temuco - Temuco"
         '           Print #X, "Giro: Restaurant"
         '           Print #X, "Soc. Comercial Euro Ltda"
         '           Print #X, " "
         '           Print #X, "Fecha:" & TurnoFecha.Caption & "  Hora:" & Time()
         '           Print #X, "V A L E   I N T E R N O"
         '           Print #X, "Doc:"; miDoc
         '           Print #X, "no constituye boleta"
         '       End If
         '       Print #X, "__________________________________________________"
         '       Print #X, "Cant - Descripci�n        -Unitario-  Subtotal"
         '       For bta = 0 To Me.List1(xMesa).ListCount - 1
         '           Print #X, ValeCantidad(bta) & " " & ValeDetalle(bta) & " " & ValeUnitario(bta) & " " & ValeTotal(bta)
         '       Next
         '       Print #X, "__________________________________________________"
         '
        '        If Val(lbTconDesc.Caption) > 0 Then
        ''            Print #X, "Subtotal  $:" & lbTconDesc.Caption
        '            xciento = Int(100 / lbTconDesc.Caption * (lbTconDesc.Caption - LBTotal.Caption))
        '            Print #X, xciento & "% Descuento $ "; lbTconDesc.Caption - LBTotal.Caption
        '        Else
        '            Print #X, "Subtotal  $:" & lbTconDesc.Caption
        '        End If
        '        Print #X, " "
        '        Print #X, "T O T A L $:" & LBTotal.Caption
        '        Print #X, " "
        '        Print #X, "Ud. fue atendido por:"
        '        Print #X, Mid(txtGarzonSel.Text, 4)
        '        Print #X, " "
        '        Print #X, " Gracias por preferirnos..."
       '     Close X ' Aqui estaria listo el archivo con la boleta 99
       '
       '
            'Ahora hay que imprimir el archivo
       '     If miDoc = "Boleta" Then
       '         Q = FreeFile ' Aqui se actualiza el correlativo de las boletas
       '         Open LaRutadeArchivos & "boletafiscal.cor" For Output As Q
       '             Print #Q, estaboleta
       '             NboletaFiscal.Caption = NboletaFiscal.Caption + 1
       '         Close #Q
       '     End If
      '
      '      For Each Impresora In Printers
        '            If Impresora.DeviceName = ImpVales.Caption Then
        '                Set Printer = Impresora
        '                Exit For
        '            End If
        '    Next
        '    Printer.EndDoc
        '    Q = FreeFile
        '    Open LaRutadeArchivos & "\HistorialBoletas\" & estaBoletaEs For Input As Q
        '        paso = CambiaLetra("Courier New", 10, False, True, False)
        '        Do While Not EOF(Q)
        '            Line Input #Q, s
        '            Printer.Print s  ''Envia
        '        Loop
        '    Close #Q
        '    Printer.NewPage
     '       Printer.EndDoc
     '
     
     ''''  **************************************************************
                '  ESTO LO USO SI TENGO BOLETA PREIMPRESA
    ''' ******************************************************
     
     '    'With BoletaParcial
     '       'Dim EstosItems As Integer, EstasPaginas As Integer, Cntdor As Integer
     '       'EstosItems = List1(xMesa).ListCount
     '       'EstasPaginas = Int(EstosItems / 12)
     '       'hst = EstosItems
     '       'If (EstasPaginas * 12) < EstosItems Then
     '           'hst = EstosItems - (EstasPaginas * 12)
     '       '    EstasPaginas = EstasPaginas + 1
     '       'End If
     '       'Cntdor = 0
            'For X = 1 To EstasPaginas
    '         '   ftr = 11
    '          '  If X = EstasPaginas Then
    '           '     If (EstasPaginas * 12) = EstosItems Then
    '            '        ftr = 11
    '             '   ElseIf (EstasPaginas * 12) < EstosItems Then
    '             '       ftr = EstosItems - (EstasPaginas * 12)
    '             '   End If
    '           ' End If
    '
    '            'For l = 0 To ftr
    '             '   .Label1(l).Caption = ValeCantidad(Cntdor)
    '             '   .Label2(l).Caption = ValeDetalle(Cntdor)
                 '   .Label3(l).Caption = ValeTotal(Cntdor)
                 '
                 '   Cntdor = Cntdor + 1
                 '   If X = EstasPaginas Then
                 '       .LBfecha.Caption = Time & ";" & Me.TurnoFecha.Caption
                 '       If LbTotal.Caption < UTotal Then 'se pillo descuento
                 '           .LbDescuento.Visible = True
                 '           .LbDescuento.Caption = "Desc. $ " & UTotal - LbTotal.Caption
                 '           .LbTotal.Caption = LbTotal.Caption
                 '       Else
                 '           .LbDescuento.Caption = " "
                 '           .LbTotal.Caption = UTotal
                 '       End If
                 '   Else
                 '       .LbDescuento.Visible = False
                 '       .LBfecha.Caption = Time & ";" & Me.TurnoFecha.Caption
                 ''       .LbTotal.Caption = "Continua"
                 ''   End If
              '  Next
               ' .Label5.Caption = "Garz:" & Mid(txtGarzonSel.Text, 1, 2) & _
                '                "M:" & TxtMesaSel.Text
               ' .PrintForm
                'Unload BoletaParcial
            '    For m = 0 To 11
            '        .Label1(X).Caption = ""
            '        .Label2(X).Caption = ""
            '        .Label3(X).Caption = ""
            '    Next
            'Next
        'End With
        AccionMesa.Hide
        Exit Function
    End If ' fin boleta
    
    If LBlevelControl.Caption = 2 Then
        If MesaSelectItem <> AccionMesa.LBIden.Caption Then
            GarzonMesa(TxtMesaSel.Text) = ""
            MesaStatus(TxtMesaSel.Text) = 1
            MesaUpedido(TxtMesaSel.Text) = 0
            MesaDescuento(TxtMesaSel.Text) = 0
            RubroDef1(TxtMesaSel.Text) = 0
            RubroDef2(TxtMesaSel.Text) = 0
            List1(TxtMesaSel.Text - 1).Clear
            List1(TxtMesaSel.Text - 1).Visible = False
            List1(TxtMesaSel.Text - 1).Visible = True
            FrmFondo.Enabled = True
            TxtMesaSel.Text = "0"
        End If
    End If
    Printer.NewPage
    Printer.EndDoc
    TxtMesaSel.Text = "0"
    'Main.SetFocus
    'LBmesa_Click (Val(TxtMesaSel.Text))
    'Set ImprimeVale = "x"
    
    Exit Function
port_error2:
'    MsgBox "Error...Emite Vale", vbCritical
End Function

Public Sub EmiteBoleta()
  Dim Lp_CuantoPaga As Long, m As Integer
  'Emitimos boleta
  Dim dato As String, dato2 As String * 3
  Dim Bp_NoFiscal As Boolean
  Dim xCant As Single
  Dim Lp_TotalSugerida As Long
  Dim Lp_PropinaSugerida As Long
  Dim Sp_TotalPropina As String * 35
  Dim SP_TotalCPropina As String * 35
  Dim Bar_Producto As String * 20
  Dim Bar_Cantidad As String * 3
  Dim Bar_Precio As String * 9
  
  Bp_NoFiscal = False
  On Error GoTo errorIF
                   ' MsgBox "inciado EmiteBoleta"
                    Lp_CuantoPaga = 0
                    Lp_TotalSugerida = CDbl(LBTotal) * 1.1
                    Lp_PropinaSugerida = Lp_TotalSugerida - CDbl(LBTotal)
                    
                    RSet Sp_TotalPropina = " PROPINA SUGERIDA :" & Str(Lp_PropinaSugerida)
                    RSet SP_TotalCPropina = " TOTAL   SUGERIDO :" & Str(Lp_TotalSugerida)
                    
                    'MsgBox "Puerto imp fiscal " & IG_Puerto_Impresora_Fiscal
                    'Vr = OcxFiscal.Init(IG_Puerto_Impresora_Fiscal)
                    VR = OCXFiscal.init(1)
                    'MsgBox "iniciada la conexion con if " & Vr
                    If VR = 0 Then
                        'cn.RollbackTrans
                        MsgBox "No se pudo completar la boleta..." & vbNewLine & Err.Number & " " & Err.Description, vbInformation
                        Exit Sub
                    End If
                    
                    
                    'Vr = OCXFiscal.conflineaencabezado(1, "=MESA:" & Main.TxtMesaSel & " - UD FUE ATENDIDO POR ===")
                    'Vr = OCXFiscal.conflineaencabezado(2, "===" & lbgarcia.Caption & " ===")
                 
                    VR = OCXFiscal.conflineacola(0, Sp_TotalPropina)
                    VR = OCXFiscal.conflineacola(1, SP_TotalCPropina)
              '      MsgBox "Info" & vbNewLine & Sp_TotalPropina & vbNewLine & SP_TotalCPropina
                    VR = OCXFiscal.conflineacola(2, " ")
                    'Vr = OCXFiscal.conflineacola(0, "=MESA:" & Main.TxtMesaSel & " - UD FUE ATENDIDO POR ===")
                    'Vr = OCXFiscal.conflineacola(1, "===" & lbgarcia.Caption & " ===")
                    
                    
                    
                    VR = OCXFiscal.abrirboleta(1, 1)
                    'Vr = OcxFiscal.abrirboleta(0, 1)
                    VR = OCXFiscal.obtenernumboleta()
                    Lp_Nro_Boleta_Obtenida_IF = OCXFiscal.boleta
                    
                    
                    For j = 0 To List1(xMesa).ListCount - 1
                            
                            esteitem = List1(xMesa).List(j)
                            ProdCodi = Trim(Str(Val(Mid(esteitem, 50, 5))))
                            CantRetira = Trim(Str(Val(Left(esteitem, 3))))
                            productox = Mid(esteitem, 15, 20) ' detalle
                            PrecioX = Trim(Str(Val(Mid(esteitem, 6, 6))))
                            xCant = CantRetira
                            
                           ' MsgBox productoX & " " & xCant & " " & CDbl(PrecioX)
                            VR = OCXFiscal.agregaitem(productox, xCant, CDbl(PrecioX))
                           
                    Next
                    'Aqui iria el detalle de la boleta
                  
                   ' MsgBox Lp_CuantoPaga
                   
                    If Val(lbTconDesc.Caption) > 0 Then
                            'Subtotal
                            
                            'Mid(InsFiscalSTotal, 11 - Len(Trim(lbTconDesc.Caption)), Len(Trim(lbTconDesc.Caption))) = Trim(lbTconDesc.Caption)
                            'Instruccion = "19" & InsFiscalSTotal
                            'Call Main.evaluaCommand(Instruccion)
                           '
                            'Descuento
                            VR = OCXFiscal.agregadescuento("DESCUENTO CLIENTE", CDbl(lbTconDesc.Caption))
                            'Mid(InsFiscalDescuento, 10 - Len(Trim(Str(Val(lbTconDesc.Caption) - Val(LBTotal.Caption)))), Len(Trim(Str(Val(lbTconDesc.Caption) - Val(LBTotal.Caption))))) = Trim(Str(Val(lbTconDesc.Caption) - Val(LBTotal.Caption)))
                            'Instruccion = "1731" & InsFiscalDescuento & "        Descuento Cliente     "
                            'Call Main.evaluaCommand(Instruccion)        '123456789012345678901234567890"
                            
                    End If
                   
                                       
                    
                  '  cuantopaso = 0
                  '  For i = 1 To Me.LvPagos.ListItems.Count
                  '      cuantopaso = cuantopaso + CDbl(Me.LvPagos.ListItems(i).SubItems(2))
                        VR = OCXFiscal.agregapago(0, CDbl(Me.LBTotal))
                  '  Next
            
                    'If CDbl(Me.TxtTotal) > cuantopaso Then
                    '    vr = OCXFiscal.agregapago(0, (CDbl(Me.TxtTotal) - cuantopaso) + 100)
                    'End If
                    VR = OCXFiscal.cierraboleta(1)
                    
                    
                    VR = OCXFiscal.abrirnofiscal(0, 0)
                    
                    VR = OCXFiscal.lineanofiscal("Detalle")
                    VR = OCXFiscal.lineanofiscal("=============================")
                    
                    For j = 0 To List1(xMesa).ListCount - 1
                            
                            esteitem = List1(xMesa).List(j)
                            ProdCodi = Trim(Str(Val(Mid(esteitem, 50, 5))))
                            CantRetira = Trim(Str(Val(Left(esteitem, 3))))
                            productox = Mid(esteitem, 15, 20) ' detalle
                            PrecioX = Trim(Str(Val(Mid(esteitem, 6, 6))))
                            xCant = CantRetira
                            
                            Bar_Producto = productox
                            RSet Bar_Cantidad = CantRetira
                            RSet Bar_Precio = PrecioX
                           ' MsgBox productoX & " " & xCant & " " & CDbl(PrecioX)
                            'VR = OCXFiscal.agregaitem(productoX, xCant, CDbl(PrecioX))
                           VR = OCXFiscal.lineanofiscal(productox & "        " & xCant & "     $" & PrecioX)
                    Next
                    
                     VR = OCXFiscal.lineanofiscal("====================================")
                     VR = OCXFiscal.lineanofiscal("                   TOTAL $ " & LBTotal)
                     VR = OCXFiscal.lineanofiscal("====================================")
                     VR = OCXFiscal.lineanofiscal("      P   R   O   P   I   N   A")
                     VR = OCXFiscal.lineanofiscal("====================================")
                     VR = OCXFiscal.lineanofiscal(Sp_TotalPropina)
                     VR = OCXFiscal.lineanofiscal(SP_TotalCPropina)
                     VR = OCXFiscal.lineanofiscal("====================================")
                     VR = OCXFiscal.cierranofiscal(0)
                     VR = OCXFiscal.fini()
            Exit Sub
            
errorIF:
    MsgBox "Error :" & Err.Description
End Sub
Private Sub List1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    List1(Index).ToolTipText = _
    Mid(List1(Index).List(List1(Index).ListIndex), 50, 5)
End Sub

Private Sub Losinsumos_Click()
    Main.Enabled = False
    Me.Hide
    Insumos.Show
End Sub

Private Sub LTxRubro_Click(Index As Integer)
    If Mid(CargaMouse.Caption, 1, 2) = "No" Then Exit Sub
    
    
    
    If OptClick(0).Value = True Then AgregaItems (Index)
End Sub

Private Sub LTxRubro_DblClick(Index As Integer)
    If Mid(CargaMouse.Caption, 1, 2) = "No" Then Exit Sub
    If OptClick(0).Value = True Then AgregaItems (Index)
End Sub


Private Sub ManInformes_Click()
    MasterInfo.Show
End Sub

Private Sub masterProd_Click()
    pro_maestroproductos.Show 1
    Exit Sub


End Sub

Private Sub menucorrelativos_Click()
    Correlativos.Show
End Sub

Private Sub mnAbrePeriodo_Click() ''' Abrir Periodo
    On Error GoTo port_error         ''' De ventas
    
    'Abrir periodo de ventas
    If Not Tf6.PortOpen Then
       'Asignar el numero de puerto a utilizar
       Tf6.CommPort = portNum
    
       'Abrir puerto
       Tf6.PortOpen = True
    End If

    Call evaluaCommand("32") 'Este comando abre el periodo
    error_26h = False
    
    'Cerrar puerto
    Tf6.PortOpen = False

    Exit Sub
    
port_error:
    MsgBox "Error...", vbCritical
End Sub



Private Sub mnInformeX_Click()
    Dim PassInformes As String
    
    PassInformes = InputBox("Ingrese password para informes:", "Requiere autorizaci�n")
    
    If PassInformes <> "4268" Then Exit Sub
    On Error GoTo port_error
    
    'Informe "X"
    If Not Tf6.PortOpen Then
       'Asignar el numero de puerto a utilizar
       Tf6.CommPort = portNum
    
       'Abrir puerto
       Tf6.PortOpen = True
    End If
    
    Call evaluaCommand("01")
    
    'Cerrar puerto
    Tf6.PortOpen = False
    
    Exit Sub
    
port_error:
    MsgBox "Error...", vbCritical
End Sub

Private Sub mnInformeZ_Click()
    Dim PassInformes As String
    
    PassInformes = InputBox("Ingrese password para informes:", "Requiere autorizaci�n")
    
    If PassInformes <> "6842" Then Exit Sub
    
    
    
    On Error GoTo port_error
    
    'Informe "Z"
    If Not Tf6.PortOpen Then
       'Asignar el numero de puerto a utilizar
       Tf6.CommPort = portNum
    
       'Abrir puerto
       Tf6.PortOpen = True
    End If

    Call evaluaCommand("021")
    
    'Cerrar puerto
    Tf6.PortOpen = False
    
    Exit Sub
    
port_error:
    MsgBox "Error...", vbCritical
End Sub

Private Sub mnP_Click()

    Dim porsi As Variant
    porsi = MsgBox("Antes de modificar este archivo se recomienda cerrar" & Chr(13) & _
    "el sistema, y hacerlo al inicio de la sesi�n" & Chr(13) & _
    "� Cerrar el sistema ahora ?", vbYesNoCancel, "Advertencia y a la vez recomendaci�n")
    If porsi = 6 Then
        Unload Main
        Unload FrmFondo
        End
    End If
    If porsi = 2 Then
        Exit Sub
    End If
    If porsi = 7 Then
        Main.Visible = False
        IntroProducto.Show
        ElMainActivo = False
    End If
End Sub

Private Sub mnVeInformes_Click()
    VerInformes.Show 1
End Sub

Private Sub Mpersonal_Click()
    Personal.Show
End Sub


Private Sub mproveedores_Click()
         Proveedores.Show
End Sub



Private Sub Picture5_Click(Index As Integer)
    If Val(Me.TxtMesaSel) = 0 Then Exit Sub
    If Index = 0 Then TxtMesaSel.SetFocus
    If Index = 1 Then
       If MesaStatus(Val(TxtMesaSel.Text)) = 3 Then Exit Sub
       ElMainActivo = False
       ListaGarzones.LBmesa.Caption = TxtMesaSel.Text
       ListaGarzones.Show 1
    End If
End Sub
Private Sub QuitaItem_Click()
    CmdSaca_Click
End Sub
Private Sub recetas_Click()
    Minutas.Show
End Sub
Private Sub Recorre_Click()
    CmdBusqueda_Click
End Sub
Private Sub refresca_Click()
    CmdComando_Click (2)
End Sub
Private Sub ScrollCant_Change()
    TxtCantidad.Text = ScrollCant.Value
End Sub
Private Sub test_Click()
    TxtMesaSel.SelLength = Len(TxtMesaSel.Text)
    TxtMesaSel.Text = ""
    TxtMesaSel.SetFocus
End Sub
Private Sub Timer1_Timer()
    Dim Normal As String
    Dim MesasOcupadas As Integer
    Dim PorcentajeFree As Integer
    Dim porcentajeOcu As Integer
    Dim Cadagarzon(99) As Integer
    Dim cadagnmesas(99) As Double
    MesasOcupadas = 0

    LaMesa = Val(TxtMesaSel.Text)
    'If LBmesa(lamesa - 1).BackColor = MainColores(4) Then
    '    TxtMesaSel.Text = 0
    '    Exit Sub
    'End If
    If Val(TxtMesaSel.Text) > 0 And Val(TxtMesaSel.Text) < Val(lbmesas.Caption) + 1 Then
    
    
        If MesaStatus(LaMesa) = 2 Then
            CmdComando(0).Enabled = True   ''' Revisa el estado de la mesa actual
           ' txtGarzonSel.Text = LBGARCIA.Caption
            CmdSaca.Enabled = True
            'LBmesa(LaMesa - 1).BackColor = MainColores(2)
        End If
        If MesaStatus(LaMesa) = 3 Then
                CmdComando(0).Enabled = True
                CmdSaca.Enabled = False
             '   LBmesa(LaMesa - 1).BackColor = MainColores(3)
        End If
        If MesaStatus(LaMesa) = 1 Then
            LBmesa(LaMesa - 1).BackColor = MainColores(1)
            CmdComando(0).Enabled = False  '' Para activar el boton de accion mesa
            'LBmesa(LaMesa - 1).BackColor = MainColores(1)
            CmdSaca.Enabled = False
        End If
    Else
        CmdComando(0).Enabled = False
        CmdSaca.Enabled = False
    End If
   
    For b = 1 To lbmesas.Caption
        If MesaStatus(b) > 1 Then
            MesasOcupadas = MesasOcupadas + 1
        End If
    Next
    porcentajeOcu = Int((MesasOcupadas / lbmesas.Caption) * 100)
    PorcentajeFree = 100 - porcentajeOcu
    CmdComando(5).Caption = "Ocup. " & porcentajeOcu & "%" & " - " & "Libre " & PorcentajeFree & "%"
   
    'Estadistica de garzones
    For b = 1 To lbmesas.Caption
        If MesaStatus(b) > 1 Then
            If GarzonMesa(b) <> " " Then
                cadagnmesas(Val(Mid(GarzonMesa(b), 1, 2))) = _
                cadagnmesas(Val(Mid(GarzonMesa(b), 1, 2))) + 1
            End If
        End If
    Next
    List2.Clear
    For b = 1 To 99
        If cadagnmesas(b) > 0 Then
            List2.AddItem ("G" & b & "-" & cadagnmesas(b) & "m")
        End If
    Next
                
    
End Sub


Private Sub TimerInactivo_Timer()
    If ElMainActivo = False Then Exit Sub
        
    ptx = pt32.Xf
    pty = pt32.Yf
    Call GetCursorPos(pt32)
    
    
    If pt32.Xf = ptx And pt32.Yf = pty Then
        Contador1 = Contador1 + 1
        
        If Contador1 = 900 Then ' Cinco segundo de inactividad
            Me.TmrRobot.Enabled = False
            Me.Timer1.Enabled = False
            cmdOut_Click
            Contador1 = 0
        End If
    Else
        Contador1 = 0
    End If
End Sub

Private Sub TmrRobot_Timer()
    Label9.Caption = Time
    Dim pasito(5) As String
    Dim InfoTmp As String
    Dim TmpOcupado As String * 2
    Dim TmpIdEkipo As String * 4
    Dim TmpTiempo As Variant
    Dim TmpSegundos As Variant
    Dim TmpEstado As Variant
  
    For rcr = 1 To Val(lbmesas.Caption)
        Xestado = FreeFile
        On Error GoTo SALEMERGENCIA
        Open PathMesas & "StatusTable" & rcr & ".TAB" For Input As Xestado
            
            For ccc = 1 To 5
                Line Input #Xestado, pasito(ccc)
            Next
        Close #Xestado
        
        InfoTmp = pasito(1)
        TmpOcupado = Mid(InfoTmp, 1, 2)
        TmpIdEkipo = Mid(InfoTmp, 4, 4)
        TmpTiempo = Mid(InfoTmp, 9)
        TmpEstado = pasito(5)
        
        TmpSegundos = DateDiff("s", TmpTiempo, Now)
        
        
          If TmpSegundos > 9 Then
             ' If TmpIdEkipo <> IdEquipo Then
                If TmpEstado = 1 Then
                    MesaStatus(rcr) = 1
                    LBmesa(rcr - 1).BackColor = MainColores(1)
                    List1(rcr - 1).Clear
                End If
                
                If TmpEstado = 2 Then
                    MesaStatus(rcr) = 2
                    LBmesa(rcr - 1).BackColor = MainColores(2)
                End If
                If TmpEstado = 3 Then
                    MesaStatus(rcr) = 3
                    LBmesa(rcr - 1).BackColor = MainColores(3)
                End If
            
             ' ActualizarSoloUna (i)
          Else
              LBmesa(rcr - 1).BackColor = MainColores(4)
          End If
              
      Next
SALEMERGENCIA:
''       VUELVE
    
        
End Sub

Private Sub TurnoEfeInicial_Click()
    Dim nuevoEfe As Variant
    nuevoEfe = InputBox("Ingrese monto efectivo Incial ", "Efectivo Inicial", "50000")
    If Val(nuevoEfe) > 0 Then
        TurnoEfeInicial.Caption = nuevoEfe
        InicioTurno.Text6.Text = nuevoEfe
        InicioTurno.AdoTurnos.Recordset.Fields(5) = nuevoEfe
        InicioTurno.AdoTurnos.Recordset.Update
    End If
End Sub


Private Sub TxtCantidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        mgar = Val(TxtCantidad.Text)
        If Val(mgar) < 0 Or Val(mgar) > 100 Then
            MsgBox "Cantidad no v�lida ... ", vbExclamation
        Else
            TxtCodigo.Text = ""
            TxtCodigo.SetFocus
            'SendKeys "{tab}"
        End If
    End If
    If KeyAscii <> 8 Then
        tDato = IsNumeric(Chr(KeyAscii))
        If Not tDato Then KeyAscii = 0
    End If
End Sub

Private Sub TxtCodigo_GotFocus()
    TxtCodigo.SelLength = Len(TxtCodigo.Text)
End Sub

Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        bcod = Val(TxtCodigo.Text)
        If Val(bcod) < 1 Then
            MsgBox "C�digo no v�lido ...", vbExclamation
        Else
            
            ElE = False
            ComprobarEstado (TxtMesaSel.Text)
            If ElE = True Then Exit Sub
            For r = 0 To CantidadRubros - 1
                LTxRubro(r).Visible = False       ''  'Sobresale boton seleccionado
                CMDrubros(r).BackColor = &H8000000F  '' De rubro
            Next
            
            EncuentraCodigo (Val(TxtCodigo.Text))
            TxtCantidad.SetFocus
            TxtCantidad.Text = "1"
            'TxtCantidad.SelLength = Len(TxtCantidad.Text)
        End If
    End If
    If KeyAscii <> 8 Then
        tDato = IsNumeric(Chr(KeyAscii))
        If Not tDato Then KeyAscii = 0
    End If
End Sub

Private Sub txtGarzonSel_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        mgar = Val(txtGarzonSel.Text)
        If mgar <= contGarzones And mgar > 0 Then
            txtGarzonSel.Text = ListaGarzones.CmdGarzones(mgar - 1).Caption
            GarzonMesa(Val(TxtMesaSel.Text)) = txtGarzonSel.Text
            'SendKeys "{tab}"
            
            'TxtCantidad.SelLength = Len(TxtCantidad.Text)
            'TxtCantidad.SetFocus
            TxtNpersonas.SelLength = Len(TxtNpersonas.Text)
            TxtNpersonas.SetFocus
        Else
            MsgBox "Garzon no V�lido", vbExclamation
        End If
    End If
    If KeyAscii <> 8 Then
        tDato = IsNumeric(Chr(KeyAscii))
        If Not tDato Then KeyAscii = 0
    End If

End Sub
Private Sub TxtMesaSel_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If Val(TxtMesaSel.Text) < 1 Or Val(TxtMesaSel.Text) > lbmesas.Caption Then
            Exit Sub
        End If
        LBmesa_Click (Val(TxtMesaSel.Text) - 1)
       'If Garzones.Value = 0 Then
       '     txtGarzonSel.SetFocus
       ' End If
    End If
    If KeyAscii <> 8 Then
        tDato = IsNumeric(Chr(KeyAscii))
        If Not tDato Then KeyAscii = 0
    End If
End Sub

Public Function FiltroCampo(rstTemp As ADODB.Recordset, _
   strField As String, strFilter As String) As ADODB.Recordset
   rstTemp.Filter = strField & " = '" & strFilter & "'"
   Set FiltroCampo = rstTemp
End Function

Public Function CambiaLetra(Letra As String, Porte As Long, Raya As Boolean, Negrita As Boolean, Cursiva As Boolean)
    Printer.FontName = Letra
    Printer.FontSize = Porte
    Printer.FontUnderline = Raya
    Printer.FontBold = Negrita
    Printer.FontItalic = Cursiva
End Function


Public Sub EncuentraCodigo(Ingreso As Integer)
    Dim Indice As String * 2
    For b = 0 To CMDrubros.Count - 1
        For j = 0 To LTxRubro(b).Rows - 1
            LTxRubro(b).Col = 0
            LTxRubro(b).Row = j
            If Ingreso = LTxRubro(b).Text Then
                Indice = b + 1
                LTxRubro(b).Visible = True
                CMDrubros(b).BackColor = &HFFFF&
                Label5(0).Caption = Indice & "-" & CMDrubros(b).Caption
                AgregaItems (b)
                GoTo encontrado
            End If
        Next
    Next
encontrado:
End Sub
Public Function BuscaEnvio(Ingreso As Integer) As Integer
    For b = 0 To CMDrubros.Count - 1
        For j = 0 To LTxRubro(b).Rows - 1
            LTxRubro(b).Col = 0
            LTxRubro(b).Row = j
            If Ingreso = LTxRubro(b).Text Then
                LTxRubro(b).Col = 1
                Producto_Comanda = LTxRubro(b).Text
            
                LTxRubro(b).Col = 4
                
                Resultado = LTxRubro(b).Text
                BuscaEnvio = Resultado
                GoTo yupi
            End If
        Next
    Next
yupi:
End Function
Public Sub RequeteImprime()
    FrmFondo.Enabled = True
    Main.Enabled = True
    Exit Sub
    Dim Q As Integer
    Q = FreeFile
    Open LaRutadeArchivos & "ticket.txt" For Input As Q
    Line Input #Q, s
    paso = CambiaLetra("Arial", 14, False, True, True)
    Printer.Print s  '' Titulo
    Line Input #Q, s
    Printer.Print s  '' liena en blanco
    Line Input #Q, s
    paso = CambiaLetra("Arial", 8, False, False, True)
    Printer.Print s '' Direccion
    Line Input #Q, s
    Printer.Print s   '' linea en blanco
    paso = CambiaLetra("Arial", 8, False, False, False)
    Line Input #Q, s
    Printer.Print s  '' mesa,hora,fecha
    Line Input #Q, s
    Printer.Print s '' garzon
    Line Input #Q, s
    Printer.Print s ''' line horizontal
    Line Input #Q, s
    Printer.Print s  '' Un Detalle Total
    paso = CambiaLetra("Courier New", 10, False, True, False)
    Do While Not EOF(Q)
        Line Input #Q, s
        If Mid(s, 1, (8 + NCol.Caption)) = Space(NCol.Caption) & "   TOTAL" Then
            paso = CambiaLetra("Times Roman", 16, False, True, False)
            Printer.Print s  '' Total
            paso = CambiaLetra("Arial", 8, False, False, False)
        Else
            Printer.Print s  '' Detalle
        End If
    Loop
    Close #Q
    Printer.EndDoc
    FrmFondo.Enabled = True
    Main.Enabled = True
End Sub

Private Sub TxtNpersonas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        mgar = Val(TxtNpersonas.Text)
        If mgar > 0 Then
            'txtGarzonSel.Text = ListaGarzones.CmdGarzones(mgar - 1).Caption
            'GarzonMesa(Val(TxtMesaSel.Text)) = txtGarzonSel.Text
            
            TxtCantidad.SelLength = Len(TxtCantidad.Text)
            TxtCantidad.SetFocus

        Else
            MsgBox "N�mero de comenzales no corresponde ... ", vbExclamation
        End If
    End If
    If KeyAscii <> 8 Then
        tDato = IsNumeric(Chr(KeyAscii))
        If Not tDato Then KeyAscii = 0
    End If
End Sub

Private Sub veturno_Click()
    CmdComando_Click (1)
End Sub
Function EnviaCproduccion()
    ''' Primero Actualizar mesa
  '  Me.CmdCentroProduccion.Enabled = False
    LBmesa_Click (Val(TxtMesaSel.Text) - 1)
     Dim Sql2 As String, RstTmp2 As Recordset
    Dim AccAlimentos As Boolean  ''  Envia e imprime' comandas en
    Dim AccBebestibles As Boolean ' centros de produccion
    Dim AccCafes As Boolean
    Dim Folio As Double
    Dim Desde As Long
    Dim EsteDetalle As String * 20
    Dim ParaCentros(1 To 99) As String * 70
    Dim Impresora As Printer
    Dim bCodigo As Integer
    Dim adonde As Integer
    Dim Comanda_Barra As Boolean
    Dim Comanda_Cocina As Boolean
    Dim Comanda_Plancha As Boolean
    'AccAlimentos = False: AccBebestibles = False: AccCafes = False
    Comanda_Barra = False: Comanda_Cocina = False: Comanda_Plancha = False
    Contador = 1
    estaM = Val(TxtMesaSel.Text) - 1
    If MesaStatus(estaM + 1) = 3 Or MesaStatus(estaM + 1) = 4 Then
        Beep
        Me.CmdCentroProduccion.Enabled = True
        Exit Function
        
    End If
  '  If txtGarzonSel.Text = "" Or txtGarzonSel.Text = "No ingreso" Or txtGarzonSel.Text = "N" Then
  '      MsgBox "Debe ingresar garz�n...", vbExclamation + vbOKOnly
  '      Exit Sub: End If
  
      ''  COMPRUEBA EL ESTADO PRIMERO ANTES DE ACCIONAR
    ''
    ElE = False
    ComprobarEstado (estaM + 1)
    If ElE = True Then Exit Function
    
    If estaM > -1 Then
        If List1(estaM).ListCount > 0 Then
            
            For i = 0 To List1(estaM).ListCount - 1
                
                restaItem = List1(estaM).List(i)
                
                EsteDetalle = Mid(restaItem, 15, 20)
                If Mid(EsteDetalle, 20, 1) <> "*" Then
                    bCodigo = Val(Mid(restaItem, 50, 5))
                    adonde = BuscaEnvio(bCodigo)
                    
                    If adonde = 1 Then Comanda_Barra = True
                    If adonde = 2 Then Comanda_Cocina = True
                    If adonde = 3 Then Comanda_Plancha = True
                    
                    
                   ' If adonde = 2 Or adonde = 3 Then AccAlimentos = True
                   ' If adonde = 1 Or adonde = 3 Then AccBebestibles = True
                   ' If adonde = 4 Or adonde = 3 Then AccCafes = True
                    ParaCentros(Contador) = Trim(Str(adonde)) & "/" _
                    & Left(restaItem, 3) & " " & Producto_Comanda
                    Desde = 36
                    'MENSAJES DE LA IMPRESION
                    Sql2 = "SELECT m.men_detalle " & _
                            "FROM tmp_mensajes t, ped_mensajes m " & _
                            "WHERE tmp_hora_unica='" & Mid(restaItem, Desde, 9) & "' AND tmp_mesa=" & Val(Main.TxtMesaSel) & " AND " & _
                            "t.men_id=m.men_id"
                    Call Consulta(RstTmp2, Sql2)
                    If RstTmp2.RecordCount > 0 Then
                        RstTmp2.MoveFirst
                        Do While Not RstTmp2.EOF
                            Contador = Contador + 1
                            ParaCentros(Contador) = Trim(Str(adonde)) & "/" _
                            & "     >" & LCase(RstTmp2!men_detalle)
                            RstTmp2.MoveNext
                        Loop
                    End If
                    
                     'AQUI AGREGAMOS LAS ALTERNATIVAS
                    Sql2 = "SELECT a.pro_codigo,p.descripcion " & _
                       "FROM alternativas_mov m,productos_alternativas_mov a,productos p " & _
                       "WHERE m.id_unico='" & Mid(restaItem, Desde, 9) & "' AND m.pro_alt_id=a.pro_alt_id AND a.pro_codigo=p.cod "
                   
                   
                    Call Consulta(RstTmp2, Sql2)
                    If RstTmp2.RecordCount > 0 Then
                        RstTmp2.MoveFirst
                        Do While Not RstTmp2.EOF
                            Contador = Contador + 1
                            ParaCentros(Contador) = Trim(Str(adonde)) & "/" _
                            & "     CON> " & RstTmp2!descripcion
                            RstTmp2.MoveNext
                        Loop
                    End If
                    
                    
                    
                    
                    Debug.Print ParaCentros(Contador)
                    Contador = Contador + 1
                End If
            Next
            Contador = Contador - 1
            
            If Comanda_Barra = False And Comanda_Cocina = False And Comanda_Plancha = False Then Exit Function
            
            
            'If AccAlimentos = False And AccBebestibles = False And AccCafes = False Then Exit Sub ' Nada que imprimir
            
            Y = FreeFile
            Open PathMesas & "StatusTable" & estaM + 1 & ".tab" For Input As Y
                For i = 1 To 4
                    Line Input #Y, s
                Next
            Close #Y
            Folio = Val(s)
            Desde = 36
            
            
            
            
            On Error Resume Next
            If Comanda_Barra = True Then
                X = FreeFile  '' Comanda BARRA
                Open "LPT1" For Output As X
                    Print #X, Chr$(&H1B); "a"; Chr$(1);
                    Print #X, Chr$(&H1B); "!"; Chr$(50);
                    Print #X, "** COMANDA ** "
                    Print #X, "** BARRA **"
                

                
                    
                    Print #X, Chr$(&H1B); "!"; Chr$(6);
                    Print #X, Chr$(&H1B); "a"; Chr$(0);
    
                    Print #X, "MESA:" & estaM + 1 & "  Hora: " & Time
                    Print #X, "Garz�n:"; lbgarcia.Caption
                    Print #X, " "
                    For i = 1 To 99
                        If Mid(ParaCentros(i), 1, 1) = "1" Then
                            Print #X, Mid(ParaCentros(i), 3)
                        End If
                    Next
                    Print #X, " "
                    Print #X, " "
                    Print #X, "Turno:" & TurnoTurno.Caption & "  - Fecha:" & TurnoFecha.Caption
                    Print #X, " <    _______ O _______   >"
                    Print #X, ""
                    Print #X, ""
                     Print #X, Chr$(&H1D); "V"; Chr$(66); Chr$(0); 'Feeds paper & cut

                    
                Close X
            End If
            If Comanda_Cocina = True Then
                Q = FreeFile  '' Comanda Barra
                
                Open "\\192.168.0.10\PS-COCINA" For Output As Q
                    Print #Q, Chr$(&H1B); "a"; Chr$(1);
                    Print #Q, Chr$(&H1B); "!"; Chr$(50);
                    Print #Q, "** COMANDA **"
                    Print #Q, "**C O C I N A **"
                    Print #Q, Chr$(&H1B); "!"; Chr$(6);
                    Print #Q, Chr$(&H1B); "a"; Chr$(0);
                     
                    ' Open LaRutadeArchivos & "CCOCINA.pdd" For Output As Q
                     
                     Print #Q, "MESA:" & estaM + 1 & "  Hora: " & Time
                     Print #Q, "Garz�n:"; lbgarcia.Caption
                     Print #Q, " "
                     For i = 1 To 99
                         If Mid(ParaCentros(i), 1, 1) = "2" Then
                             Print #Q, Mid(ParaCentros(i), 3)
                             Debug.Print Mid(ParaCentros(i), 3)
                         End If
                     Next
                     Print #Q, " "
                     Print #Q, " "
                     Print #Q, "Turno:" & TurnoTurno.Caption & " - Fecha:" & TurnoFecha.Caption
                     Print #Q, " >  _______ o _______"
                     Print #Q, ""
                     Print #Q, ""
                      Print #Q, Chr$(&H1D); "V"; Chr$(66); Chr$(0); 'Feeds paper & cut

                Close Q
                
            End If
            If Comanda_Plancha = True Then
                Q = FreeFile  '' Comanda CAFETERIA
                Open LaRutadeArchivos & "CPLANCHA.pdd" For Output As Q
                    Print #Q, "**C O M A N D A    P L A N C H A **"
                    Print #Q, "MESA:" & estaM + 1 & "  Hora: " & Time
                    Print #Q, "Garz�n:"; lbgarcia.Caption
                    Print #Q, " "
                    For i = 1 To 99
                        If Mid(ParaCentros(i), 1, 1) = "3" Then
                            Print #Q, Mid(ParaCentros(i), 3)
                        End If
                    Next
                    Print #Q, " "
                    Print #Q, " "
                    Print #Q, "Turno:" & TurnoTurno.Caption & " - Fecha:" & TurnoFecha.Caption
                    Print #Q, " >  _______ o _______"
                
                Close Q
                
            End If
            
            
            PedMesa = PathMesas & "Mesa" & estaM + 1 & ".ped"
            fpedido = Trim(Str(Folio))
            X = FreeFile
            Kill PedMesa
            LargoR = Len(rPedido)
            Open PedMesa For Random As #X Len = LargoR
            With rPedido
                For A = 0 To List1(estaM).ListCount - 1
                    restaItem = List1(estaM).List(A)
                    .Npedido = fpedido   '' Archiva de nuevo la lista
                    .Hora = Mid(restaItem, Desde, 9)
                    .Nmesa = Str(estaM + 1)
                    .Codigo = Str(Val(Mid(restaItem, 50, 5)))
                    .Cantidad = Left(restaItem, 3)
                    .Detalle = Mid(restaItem, 15, 19) & "*"
                    .PrecioU = Mid(restaItem, 6, 6)
                    .SubTotal = Str(.Cantidad * .PrecioU)
                    .Bebestible = Right(restaItem, 1)
                    .Rubro = Mid(restaItem, 55, 2)
                    Put #X, , rPedido
                    Mid(restaItem, 34, 1) = "*"
                    List1(estaM).List(A) = restaItem
                Next
                Close #X
            End With
            
        End If
    End If
    Me.CmdCentroProduccion.Enabled = True
    

End Function


Public Function sendCommand(msg As String) As String

    Dim Comando       As String
    Dim Respuesta     As String
    Dim carIn         As String
    Dim sigueLeyendo  As Boolean
    
    'Armar comando final
    Comando = Chr$(&H87) & msg & Chr$(&H88)
    
    'Enviar comando a la impresora
    Tf6.Output = Comando

    Respuesta = ""

    sigueLeyendo = True

    While sigueLeyendo And (InStr(Respuesta, Chr$(&HD)) = 0)
      DoEvents

      carIn = Tf6.Input

      If InStr(carIn, Chr$(Ack)) > 0 Then
         sigueLeyendo = False
      Else
         Respuesta = Respuesta & carIn
      End If

    Wend

    'Retornar respuesta
    sendCommand = Respuesta

End Function
Public Sub evaluaCommand(cmd As String)

    Dim respCmd  As String
    
    respCmd = sendCommand(cmd)
    
    If Len(respCmd) > 1 Then
       'Evaluar respuesta al comando enviado
'       MsgBox Left$(respCmd, 3)
       Select Case Left$(respCmd, 3)
            
           Case "043"     'Falta papel o tapa abierta
                While Left$(respCmd, 3) = "043"
                    MsgBox "Compruebe que la impresora Fiscal tenga" & vbCr & "papel y que la tapa este cerrada", vbCritical
                                        
                    'Reenvia comando
                    respCmd = sendCommand(cmd)
                Wend
           Case "060"     'Periodo fiscal no abierto, avisar
                MsgBox "Comando Invalido en periodo de No Venta" & vbCr & "Respuesta : " & respCmd, vbCritical
           Case "067"     'Periodo fiscal exceda las 26 horas
                MsgBox "Periodo fiscal excede las 26 horas" & vbCr & "Respuesta : " & respCmd, vbCritical
                MsgBox "Debe emitir informe 'Z'", vbInformation
                error_26h = True
           Case "080"     'Error de impresion, comnado repetir + comando del error
                While Left$(respCmd, 3) = "080"
                    MsgBox "Error de impresion" & vbCr & "Respuesta : " & respCmd, vbCritical
                                        
                    'Comando repetir
                    respCmd = sendCommand("18")
                    
                    'Reenvia comando
                    respCmd = sendCommand(cmd)
                Wend
           
           Case "082"     'Error de impresion, comando repetir
                While Left$(respCmd, 3) = "082"
                    MsgBox "Error de impresion" & vbCr & "Respuesta : " & respCmd, vbCritical
                                        
                    'Comando repetir
                    respCmd = sendCommand("18")
                Wend
       End Select
    End If
End Sub
Public Sub MesaAcero(Mesita As Integer)
    Qcero = FreeFile  ''  Coloca mesa en 0
   
    StrMesa = "StatusTable" & Mesita & ".tab"
    
    Open PathMesas & StrMesa For Output As Qcero
        Print #Qcero, "Si-" & IdEquipo & "-" & Now
        Print #Qcero, lbgarcia.Caption
        Print #Qcero, Time
        Print #Qcero, 0
        Print #Qcero, 1
        Print #Qcero, 0
        Print #Qcero, 0
        Print #Qcero, "0"
        Print #Qcero, 0
    Close #Qcero
    MesaDescuento(Mesita) = ""
    DescuentoActual(Mesita) = 0
    
   
End Sub
Public Sub ExtraeDatosMesa(DatMesa As Integer)
    Dim cr As Integer, rb As Integer, sl As Integer
    Dim mesa As Integer
    Dim Cpedido As Double
    Dim Citmes As Integer
    Dim mcan, mpre, mdet, mcod, mitem As String
    Dim StrMesa As String
    Dim ComoEsta As Variant
    For rb = 1 To 150
        RubroDef1(rb) = 0
        RubroDef2(rb) = 0
    Next

    File1.Path = PathMesas
    'For i = 1 To Me.LBmesas      ' Recorro todas las mesas
        StrMesa = "StatusTable" & DatMesa & ".tab"
        File1.Pattern = StrMesa
        
        If File1.ListCount > 0 Then ' la mesa esta en el sistema
            X = FreeFile
            Open PathMesas & StrMesa For Input As X
                For j = 1 To 9
                    
                    Line Input #X, ComoEsta ' extraigo el estado
                    If j = 2 Then
                        GarzonMesa(DatMesa) = ComoEsta
                        lbgarcia.Caption = ComoEsta
                    End If
                    If j = 4 Then
                        Cpedido = ComoEsta
                    End If
                    If j = 5 Then
                        MesaStatus(DatMesa) = ComoEsta
                    End If
                    If j = 6 Then
                        Citmes = ComoEsta
                    End If
                    If j = 8 Then
                        MesaDescuento(DatMesa) = ComoEsta
                    End If
                    If j = 9 Then
                        Ccomenzales(DatMesa) = ComoEsta
                    End If
                Next
            Close #X
            
            If MesaStatus(DatMesa) = 2 Or MesaStatus(DatMesa) = 3 Then ' La mesa fue encontrada ocupada
                LBmesa(DatMesa - 1).BackColor = IIf(MesaStatus(DatMesa) = 2, MainColores(2), MainColores(3))
                MesaUpedido(DatMesa) = Cpedido
                'If List1(i - 1).ListCount > 0 Then  ' Limpio la lista de productos
                '    For sl = List1(i - 1).ListCount To 0 Step -1
                '        List1(i - 1).RemoveItem sl
                '    Next
                'End If
                List1(DatMesa - 1).Clear
                mesa = DatMesa
                'Q = FreeFile
                StrMesa = PathMesas & "mesa" & mesa & ".ped"
                Y = FreeFile
                LargoR = Len(rPedido)
                Open StrMesa For Random As Y Len = LargoR
                For act = 1 To LOF(Y) / LargoR
                    Get #Y, , rPedido
                    ProductoCodigo = Trim(rPedido.Codigo)
                    ProductoCodigo = ProductoCodigo & String(5 - Len(ProductoCodigo), "_")
                    IdentItemP = Trim(rPedido.Bebestible)
                    IdentItemP = String(3 - Len(IdentItemP), " ") & IdentItemP
                    Detalle = rPedido.Detalle
                    nuevaSuma = Trim(rPedido.PrecioU)
                    LBTotal.Caption = Val(LBTotal.Caption) + (Val(nuevaSuma) * Val(rPedido.Cantidad))
                    nuevoItem = rPedido.Cantidad & " - " & String(6 - Len(Str(nuevaSuma)), " ") & nuevaSuma & " - " & Detalle
                    Horita = rPedido.Hora
                    nuevoItem = nuevoItem & String(35 - Len(nuevoItem), " ") & Horita & "_____" & ProductoCodigo & rPedido.Rubro & IdentItemP
                    List1(mesa - 1).AddItem nuevoItem
                    IdentItemP = Val(IdentItemP)
                    If IdentItemP = 1 Then
                          RubroDef1(mesa) = RubroDef1(mesa) + (nuevaSuma * Val(rPedido.Cantidad))
                    ElseIf IdentItemP = 2 Then
                          RubroDef2(mesa) = RubroDef2(mesa) + (nuevaSuma * Val(rPedido.Cantidad))
                    End If
                    List1(mesa - 1).ListIndex = List1(mesa - 1).ListCount - 1
                                    
                Next
                Close #Y ' Datos extraidos
          '      LBmesa_Click (mesa - 1)
                
            Else
                List1(DatMesa - 1).Clear
                LBmesa(DatMesa - 1).BackColor = MainColores(1)
                MesaDescuento(DatMesa) = 0
                
            End If
        Else ' La mesa no esta, por ende esta libre
            MesaStatus(DatMesa) = 1
            LBmesa(DatMesa - 1).BackColor = MainColores(1)
            If List1(DatMesa - 1).ListCount > 0 Then
                For sl = List1(DatMesa - 1).ListCount To 0 Step -1
                    List1(DatMesa - 1).RemoveItem sl
                Next
            End If
            GarzonMesa(DatMesa) = ""
            Ccomenzales(DatMesa) = 0 'hjlkk�1233
            
        End If
   ' Next
    
    
    FrmFondo.Enabled = True
    Actualiza = 1

End Sub


Public Sub ComprobarEstado(Mesina As Integer)
    
    Dim Estados(5) As String
    Xestado = FreeFile
        On Error GoTo selemergencia:
        Open PathMesas & "StatusTable" & Mesina & ".TAB" For Input As Xestado
            
            For ccc = 1 To 5
                Line Input #Xestado, Estados(ccc)
                
                
            Next
        Close #Xestado
        
        InfoTmpst = Estados(1)
        TmpOcupadoST = Mid(InfoTmpst, 1, 2)
        TmpIdEkipoST = Mid(InfoTmpst, 4, 4)
        TmpTiempoST = Mid(InfoTmpst, 9)
        TmpEstadoST = Estados(5)
        
        TmpSegundosST = DateDiff("s", TmpTiempoST, Now)
        
        
          If TmpSegundosST > 19 Then
             ' If TmpIdEkipo <> IdEquipo Then
                If TmpEstadoST = 1 Then
                    MesaStatus(Mesina) = 1
                    LBmesa(Mesina - 1).BackColor = MainColores(1)
                    List1(Mesina - 1).Clear
                    MesaDescuento(Mesina) = "0"
                End If
                
                If TmpEstadoST = 2 Then
                    MesaStatus(Mesina) = 2
                    LBmesa(Mesina - 1).BackColor = MainColores(2)
                End If
                If TmpEstadoST = 3 Then
                    MesaStatus(Mesina) = 3
                    LBmesa(Mesina - 1).BackColor = MainColores(3)
                End If
            
             ' ActualizarSoloUna (i)
          Else
                If IdEquipo <> TmpIdEkipoST Then
                    
                    ElE = True
                    MsgBox "La mesa " & Mesina & " esta siendo ocupada en otro terminal, espere unos segundos..."
                    Me.TxtMesaSel.Text = 0
                    LBmesa(Mesina - 1).BackColor = MainColores(4)
                End If
          End If
selemergencia:
          ''naaa
    
End Sub
