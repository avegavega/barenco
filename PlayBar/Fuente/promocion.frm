VERSION 5.00
Begin VB.Form promocion 
   BackColor       =   &H8000000C&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Promoción"
   ClientHeight    =   3735
   ClientLeft      =   -120
   ClientTop       =   30
   ClientWidth     =   4545
   DrawMode        =   15  'Merge Pen Not
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3735
   ScaleWidth      =   4545
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton CmdCerrar 
      Caption         =   "&Cerrar"
      Height          =   375
      Left            =   3000
      TabIndex        =   6
      Top             =   3240
      Width           =   1335
   End
   Begin VB.CommandButton CmdActualizar 
      Caption         =   "&Actualizar"
      Height          =   375
      Left            =   240
      TabIndex        =   5
      Top             =   3240
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Configurar promocion"
      Height          =   3015
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   3975
      Begin VB.TextBox txtPromo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   360
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   4
         Top             =   1680
         Width           =   3255
      End
      Begin VB.TextBox txtValor 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   960
         TabIndex        =   3
         Text            =   "1500"
         Top             =   720
         Width           =   2175
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Comentario de promoción"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   1320
         Width           =   3735
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Desde este Valor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   3495
      End
   End
End
Attribute VB_Name = "promocion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Q As Integer, Stxt As String

Private Sub CmdActualizar_Click()
    Q = FreeFile
    Open LaRutadeArchivos & "promocion.txt" For Output As Q
        Print #Q, TxtValor.Text
        Print #Q, txtPromo.Text
    Close #Q
End Sub

Private Sub CmdCerrar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Q = FreeFile
    Open LaRutadeArchivos & "promocion.txt" For Input As Q
        Line Input #Q, Stxt
        TxtValor.Text = Stxt
        Line Input #Q, Stxt
        txtPromo.Text = Stxt
    Close #Q
End Sub
