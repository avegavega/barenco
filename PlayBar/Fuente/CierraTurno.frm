VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form CierreTurno 
   BackColor       =   &H00400000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cierre de Turno"
   ClientHeight    =   5850
   ClientLeft      =   30
   ClientTop       =   330
   ClientWidth     =   7950
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5850
   ScaleWidth      =   7950
   StartUpPosition =   2  'CenterScreen
   Begin MSAdodcLib.Adodc AdoInsumos 
      Height          =   330
      Left            =   480
      Top             =   4920
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   ""
      Caption         =   "insumos"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc AdoInfoVentas 
      Height          =   330
      Left            =   480
      Top             =   4560
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "InfoVentas"
      Caption         =   "InfoVentas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton CmdCancela 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   374
      Left            =   5040
      TabIndex        =   23
      Top             =   5160
      Width           =   1584
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H80000007&
      Caption         =   "Datos del Turno"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   3641
      Left            =   4560
      TabIndex        =   10
      Top             =   240
      Width           =   2673
      Begin VB.Label LBfecha 
         BackColor       =   &H00000000&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label8"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   374
         Left            =   1210
         TabIndex        =   22
         Top             =   484
         Width           =   1221
      End
      Begin VB.Label Label3 
         BackColor       =   &H00000000&
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Index           =   1
         Left            =   1560
         TabIndex        =   21
         Top             =   240
         Width           =   975
      End
      Begin VB.Label LbFin 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label12"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   374
         Left            =   121
         TabIndex        =   20
         Top             =   3146
         Width           =   1221
      End
      Begin VB.Label LbEfectivo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label11"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   374
         Left            =   121
         TabIndex        =   19
         Top             =   2420
         Width           =   1221
      End
      Begin VB.Label LbInicio 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label10"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   374
         Left            =   121
         TabIndex        =   18
         Top             =   1694
         Width           =   1221
      End
      Begin VB.Label LBresponsable 
         BackColor       =   &H00000000&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label9"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   374
         Left            =   121
         TabIndex        =   17
         Top             =   1089
         Width           =   2310
      End
      Begin VB.Label LbIdTurno 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label8"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   374
         Left            =   121
         TabIndex        =   16
         Top             =   484
         Width           =   979
      End
      Begin VB.Label Label7 
         BackColor       =   &H00000000&
         Caption         =   "Hora T�rmino"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   253
         Left            =   121
         TabIndex        =   15
         Top             =   2904
         Width           =   2431
      End
      Begin VB.Label Label6 
         BackColor       =   &H00000000&
         Caption         =   "Efectivo Inicial"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   253
         Left            =   121
         TabIndex        =   14
         Top             =   2178
         Width           =   2189
      End
      Begin VB.Label Label5 
         BackColor       =   &H00000000&
         Caption         =   "Hora de Inicio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   253
         Left            =   121
         TabIndex        =   13
         Top             =   1452
         Width           =   1584
      End
      Begin VB.Label Label4 
         BackColor       =   &H00000000&
         Caption         =   "Responsable"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   253
         Left            =   121
         TabIndex        =   12
         Top             =   847
         Width           =   1584
      End
      Begin VB.Label Label3 
         BackColor       =   &H00000000&
         Caption         =   "Id del Turno"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Opciones del cierre"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4246
      Index           =   0
      Left            =   720
      TabIndex        =   1
      Top             =   242
      Width           =   2794
      Begin VB.CheckBox Check9 
         Caption         =   "Resumen de Caja"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   253
         Left            =   363
         TabIndex        =   24
         Top             =   726
         Value           =   1  'Checked
         Width           =   1826
      End
      Begin VB.CheckBox Check8 
         Caption         =   "Ventas al Personal"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   253
         Left            =   363
         TabIndex        =   9
         Top             =   3751
         Visible         =   0   'False
         Width           =   2189
      End
      Begin VB.CheckBox Check7 
         Caption         =   "Costo de productos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   374
         Left            =   363
         TabIndex        =   8
         Top             =   3267
         Value           =   1  'Checked
         Width           =   2068
      End
      Begin VB.CheckBox Check6 
         Caption         =   "Platos Quitados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   253
         Left            =   363
         TabIndex        =   7
         Top             =   2904
         Value           =   1  'Checked
         Width           =   1947
      End
      Begin VB.CheckBox Check5 
         Caption         =   "Descuentos Efectuados"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   374
         Left            =   363
         TabIndex        =   6
         Top             =   2420
         Value           =   1  'Checked
         Width           =   2310
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Ventas por Garz�n y ventas por mesa"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Left            =   363
         TabIndex        =   5
         Top             =   1694
         Value           =   1  'Checked
         Width           =   2068
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Ventas por Rubro"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   374
         Left            =   363
         TabIndex        =   4
         Top             =   1331
         Value           =   1  'Checked
         Width           =   1947
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Tickets emitidos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   374
         Left            =   363
         TabIndex        =   2
         Top             =   968
         Value           =   1  'Checked
         Width           =   1947
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H0000FFFF&
         FillColor       =   &H000000C0&
         FillStyle       =   0  'Solid
         Height          =   132
         Index           =   8
         Left            =   121
         Shape           =   2  'Oval
         Top             =   3751
         Visible         =   0   'False
         Width           =   132
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H0000FFFF&
         FillColor       =   &H000000C0&
         FillStyle       =   0  'Solid
         Height          =   132
         Index           =   7
         Left            =   121
         Shape           =   2  'Oval
         Top             =   2541
         Width           =   132
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H0000FFFF&
         FillColor       =   &H000000C0&
         FillStyle       =   0  'Solid
         Height          =   132
         Index           =   6
         Left            =   121
         Shape           =   2  'Oval
         Top             =   2904
         Width           =   132
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H0000FFFF&
         FillColor       =   &H000000C0&
         FillStyle       =   0  'Solid
         Height          =   132
         Index           =   5
         Left            =   121
         Shape           =   2  'Oval
         Top             =   3388
         Width           =   132
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H0000FFFF&
         FillColor       =   &H000000C0&
         FillStyle       =   0  'Solid
         Height          =   132
         Index           =   4
         Left            =   121
         Shape           =   2  'Oval
         Top             =   1089
         Width           =   132
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H0000FFFF&
         FillColor       =   &H000000C0&
         FillStyle       =   0  'Solid
         Height          =   132
         Index           =   3
         Left            =   121
         Shape           =   2  'Oval
         Top             =   1452
         Width           =   132
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H0000FFFF&
         FillColor       =   &H000000C0&
         FillStyle       =   0  'Solid
         Height          =   132
         Index           =   2
         Left            =   121
         Shape           =   2  'Oval
         Top             =   1815
         Width           =   132
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H0000FFFF&
         FillColor       =   &H000000C0&
         FillStyle       =   0  'Solid
         Height          =   132
         Index           =   0
         Left            =   121
         Shape           =   2  'Oval
         Top             =   726
         Width           =   132
      End
      Begin VB.Label Label1 
         Caption         =   "Items que se imprimen:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.5
            Charset         =   0
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   253
         Left            =   121
         TabIndex        =   3
         Top             =   363
         Width           =   2310
      End
   End
   Begin VB.CommandButton CmdConfirma 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4560
      Picture         =   "CierraTurno.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   4200
      Width           =   2655
   End
   Begin VB.Label LbPrinter 
      Caption         =   "impresora"
      Height          =   255
      Left            =   480
      TabIndex        =   26
      Top             =   5520
      Width           =   3015
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      DataField       =   "Fecha"
      DataSource      =   "AdoInfoVentas"
      Height          =   253
      Left            =   2662
      TabIndex        =   25
      Top             =   4961
      Visible         =   0   'False
      Width           =   616
   End
End
Attribute VB_Name = "CierreTurno"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdCancela_Click()
    Unload Me  '' Cancela el cierre del turno
End Sub

Private Sub CmdConfirma_Click()
    Dim Turno As Long
    Dim TVentas As String
    Dim TNrendibles As Double
    Dim TAcumulado As Double
    Dim TAlimentos As Double
    Dim TBebestibles As Double
    
    Dim X As Integer
    Dim E As Integer
    Dim T As Integer
    Dim s As String
    Dim TextoDet As String * 12
    Dim TextoValor As String * 8
    Dim Impresora As Printer
    'TurnoActual
    'TurnoActual.Enabled = False
    'TurnoActual.Visible = False
    CierreTurno.Enabled = False
    X = FreeFile
    Informes.Visible = False
    Load Informes
    Informes.Enabled = False
    Turno = Main.TurnoTurno.Caption
    Dia = Mid(LBfecha.Caption, 1, 2)
    Mes = Mid(LBfecha.Caption, 4, 2)
    ano = Mid(LBfecha.Caption, 7, 4)
    If Main.LBlevelControl.Caption = 2 Then
        With Informes.MSHvalesEmitidos
            .Row = .Rows - 1
            .Col = 2
            TVentas = .Text
            .Col = 6
            TAlimentos = .Text
            .Col = 7
            TBebestibles = .Text
        End With
    Else
        TVentas = Mid(Informes.LbTIngresos.Caption, 8)
        TVentas = Trim(TVentas)
        TNrendibles = Informes.LBtotNoRinde.Caption
        TAlimentos = Informes.LBalimentos.Caption
        TBebestibles = Informes.LBbebestibles.Caption
    End If
    
    Informes.NuevoTXTFile (LaRutadeArchivos & "StatusBox.txt")
    Informes.Nuevo2XTFile (LaRutadeArchivos & "TicketEmitidos.txt")
    Informes.Nuevo3XTFile (LaRutadeArchivos & "VtaRubro.txt")
    Informes.Nuevo4XTFile (LaRutadeArchivos & "AcumGarzon.txt")
    Informes.Nuevo5XTFile (LaRutadeArchivos & "Quitados.txt")
    Informes.DescuentosEfectuados (LaRutadeArchivos & "Dsctos.txt")
    If Main.LBlevelControl.Caption = 1 Then
        Informes.Nuevo2XTFile (LaRutadeArchivos & "TicketEmitidos.txt")
    End If
    If Main.LBlevelControl.Caption = 2 Then
        Informes.Nuevo6XTFile (LaRutadeArchivos & "TicketEmitidos.txt")
    End If
    Unload Informes
    GoTo informeZ
    Open "LPT1" For Output As X
    Print #X, "CIERRE DE TURNO"
    Print #X, "  "
    Print #X, " "
    TextoDet = "Fecha"  ''' Datos del turno
    TextoValor = LBfecha.Caption
    Print #X, TextoDet; Tab; TextoValor
    TextoDet = "Id del Turno"
    TextoValor = LbIdTurno.Caption
    Print #X, TextoDet; Tab; TextoValor
    TextoDet = "Responsable"
    TextoValor = LBresponsable.Caption
    Print #X, TextoDet; Tab; TextoValor
    TextoDet = "Efectivo Inicial"
    TextoValor = LbEfectivo.Caption
    Print #X, TextoDet; Tab; TextoValor
    TextoDet = "Hora de Inicio"
    TextoValor = LbInicio.Caption
    Print #X, TextoDet; Tab; TextoValor
    TextoDet = "Hora de Fin"
    TextoValor = LbFin.Caption
    Print #X, TextoDet; Tab; TextoValor
    Print #X, "_______________________"
    Print #X, "  "
    Print #X, "  "
    E = FreeFile
    Open LaRutadeArchivos & "StatusBox.txt" For Input As E
    Do While Not EOF(E)
        Line Input #E, s
        Print #X, s
    Loop
    Print #X, "_______________________"
    Close #E
        If Check5.Value = 1 Then
        E = FreeFile
        Print #X, " "
        Print #X, " "
        Open LaRutadeArchivos & "Dsctos.txt" For Input As E
        Do While Not EOF(E)
            Line Input #E, s
            Print #X, s
        Loop
        Print #X, "_______________________"
        Close #E
    End If
    If Check1.Value = 1 Then
        E = FreeFile
        Print #X, " "
        Print #X, " "
        Open LaRutadeArchivos & "TicketEmitidos.txt" For Input As E
        Do While Not EOF(E)
            Line Input #E, s
            Print #X, s
        Loop
        Print #X, "_______________________"
        Close #E
    End If
    If Check2.Value = 1 Then
        E = FreeFile
        Print #X, " "
        Print #X, " "
        Open LaRutadeArchivos & "VtaRubro.txt" For Input As E
        Do While Not EOF(E)
            Line Input #E, s
            Print #X, s
        Loop
        Print #X, "_______________________"
        Close #E
    End If
    If Check3.Value = 1 Then
        E = FreeFile
        Print #X, " "
        Print #X, " "
        Open LaRutadeArchivos & "AcumGarzon.txt" For Input As E
        Do While Not EOF(E)
            Line Input #E, s
            Print #X, s
        Loop
        Print #X, "_______________________"
        Close #E
    End If
  '  If Check4.Value = 1 Then
  '      E = FreeFile
  '      Print #X, " "
   '     Print #X, " "
  '      Open LaRutadeArchivos & "AcumMesas.txt" For Input As E
  '      Do While Not EOF(E)
  '          Line Input #E, s
  '          Print #X, s
   '     Loop
  '      Print #X, "_______________________"
   '     Close #E
  '  End If

    If Check6.Value = 1 Then
        E = FreeFile
        Print #X, " "
        Print #X, " "
        Open LaRutadeArchivos & "Quitados.txt" For Input As E
        Do While Not EOF(E)
            Line Input #E, s
            Print #X, s
        Loop
        Print #X, "_______________________"
        Close #E
    End If
    If Check7.Value = 1 Then
        ' costo de insumos
    End If
       ''
    Print #X, " "
    Print #X, " "
    Print #X, " *** DETALLES ***"
    Print #X, " "
    Print #X, "Efectivo      :________________"
    Print #X, " "
    Print #X, "Cheque        :________________"
    Print #X, " "
    Print #X, "T. Credito    :________________"
    Print #X, " "
    Print #X, " "
    Print #X, "T O T A L     :___________________"
    Print #X, " "
    Print #X, "-----------------------"
    Print #X, "Fin del Archivo..."
    Print #X, "_______________________"
    Print #X, Chr$(&H1D); "V"; Chr$(66); Chr$(0); 'Feeds paper & cut

    Close #X
    If "X" = "A" Then
        For Each Impresora In Printers
            If Impresora.DeviceName = Me.LBprinter.Caption Then
                Set Printer = Impresora
                Exit For
            End If
        Next
        
        X = FreeFile
        Open LaRutadeArchivos & "InfoVentas.Txt" For Input As X
        Line Input #X, s
        paso = Informes.TipoLetra("Arial", 12, True)
        Printer.Print s
        paso = Informes.TipoLetra("Courier New", 8, False)
        Do While Not EOF(X)
            Line Input #X, s
            Printer.Print s
        Loop
        Printer.EndDoc
        Close #X
    End If
informeZ:
    sie = MsgBox(" Termin� de imprimir ... ", vbQuestion + vbYesNo, "Cerrando Turno...")
    If sie = 7 Then  '' No se termin� de imprimir
        CierreTurno.Enabled = True
        CierreTurno.SetFocus
    Else
        Dim PassInformes As String
        'Do While PassInformes <> "6842"
        '    PassInformes = InputBox("Ingrese password para informes:", "Requiere autorizaci�n")
        'Loop
        
       ' Main.mnInformeZ
        ''If PassInformes <> "6842" Then Exit Sub

       ' With AdoInfoVentas.Recordset
       '     .MoveLast
       '     .AddNew
       '     .Fields(0) = Main.TurnoFecha.Caption
       '     .Fields(1) = Turno
       '     .Fields(2) = TVentas
       '     .Fields(3) = TNrendibles
      '      .Fields(4) = TAlimentos
      '      .Fields(5) = TBebestibles
      '      .MoveFirst
      '  End With
        Sql = "UPDATE turnos SET entrega='" & Time & "',efectivoinicial=0"
        Call Consulta(Rst_tmp, Sql)
        
        'With InicioTurno.AdoTurnos.Recordset
        '    .Fields("Entrega") = Time
        '    .Fields("efectivoinicial") = 0
        '    .Update
        'End With
        If Mid(Main.fiscalprint.Caption, 1, 2) = "Si" Then
        
                With Main.OCXFiscal
                    Vr = .init(IG_Puerto_Impresora_Fiscal)
                    If Vr = 0 Then
                        
                        MsgBox "No se pudo completar la boleta...", vbInformation
                        Exit Sub
                    End If
                   
                    
                    Vr = .cierrejornada()
                    Vr = .fini()
                End With

        
        
            'If Not Main.Tf6.PortOpen Then
            '   'Asignar el numero de puerto a utilizar
            '    Main.Tf6.CommPort = 1
            '   'Abrir puerto
            '    Main.Tf6.PortOpen = True
            'End If
            'Call Main.evaluaCommand("021")
            'Cerrar puerto
            'Main.Tf6.PortOpen = False
        End If
        
        Unload InicioTurno
        Unload Me
        Unload FrmFondo
        Unload CierreTurno
        Acceso.cmdOk.Enabled = False
        Unload FrmFondo
    End If
    
  '  CierreTurno.SetFocus
End Sub

Private Sub Form_Load()
    Dim Impresora As Printer
    
    With Main
        LbIdTurno.Caption = .TurnoTurno.Caption
        LBfecha.Caption = .TurnoFecha.Caption
        LBresponsable.Caption = .TurnoNombre.Caption
        LbInicio.Caption = .TurnoInicio.Caption
        LbEfectivo.Caption = .TurnoEfeInicial.Caption
        LbFin.Caption = Time
        LBprinter.Caption = .InformesPrinter.Caption
    End With
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
ElMainActivo = True
End Sub
