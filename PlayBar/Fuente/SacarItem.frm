VERSION 5.00
Begin VB.Form SacarItem 
   BackColor       =   &H00000080&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Sistema de Seguridad y Control"
   ClientHeight    =   3045
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4410
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3045
   ScaleWidth      =   4410
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox TxtClave 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      IMEMode         =   3  'DISABLE
      Left            =   480
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   1560
      Width           =   3492
   End
   Begin VB.CommandButton CmdOk 
      Caption         =   "C&onfirmar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      Left            =   240
      TabIndex        =   1
      Top             =   2280
      Width           =   1692
   End
   Begin VB.CommandButton CmdCancelar 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      Left            =   2400
      TabIndex        =   2
      Top             =   2280
      Width           =   1692
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Clave Supervisora para sacar producto enviado a centro de producción."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   3975
   End
End
Attribute VB_Name = "SacarItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CmdCancelar_Click()
    PermiteSacarItem = False
    ElMainActivo = True
    Unload Me
    Main.Enabled = True
End Sub

Private Sub cmdOK_Click()
    Autorizo = "No requiere"
    With Main
        Select Case TxtClave.Text
            Case .clavesuper1.Caption
                Autorizador = .claveIDsuper1.Caption
            Case .clavesuper2.Caption
                Autorizador = .ClaveIdSuper2.Caption
            Case .clavesuper3.Caption
                Autorizador = .ClaveIdSuper3.Caption
            Case Else
                Beep
                PermiteSacarItem = False
                MsgBox " ¡¡ ACCION INDEBIDA, CLAVE NO ENCONTRADA ... !!", vbExclamation + vbOKOnly, "ATENCION, CUIDADO, ATENCION"
                Main.Enabled = True
                ElMainActivo = True
                Unload Me
                Exit Sub
        End Select
        PermiteSacarItem = True
        Autorizo = Autorizador
        'Retirando
        
    End With
   ' Main.Enabled = True
   ElMainActivo = True
    Unload Me
    
End Sub

Static Sub Retirando()
    Dim CantRetira As Integer
    Dim valorX As Double                    ''  Saca 1 item de items cargados a una mesa
    Dim restaItem As String
    Dim Apedido As Double
    Dim fpedido As String
    Dim Desde As Long
    Dim MyHora As String
    Dim estaM As Integer
    Dim Producto As String
    Dim IdentItemP As String
    Dim ProdCodi As Integer
    Dim Criterio As String
    Dim myPedido As Double
    Dim PedMesa As String
    Dim X As Integer, A As Integer
    estaM = Val(Main.TxtMesaSel.Text) - 1
    If MesaStatus(estaM + 1) = 3 Then
        Beep
        Exit Sub
    End If
    If estaM > -1 Then
        If Main.List1(estaM).ListCount > 0 Then
            Desde = 36
            restaItem = Main.List1(estaM).List(Main.List1(estaM).ListIndex)
            MyHora = Mid(restaItem, Desde, 9)
            Producto = Mid(restaItem, 15, 20)
            If Right(Producto, 1) = "*" Then
                If PermiteSacarItem = False Then Exit Sub
            End If
            IdentItemP = Right(restaItem, 1)
            ProdCodi = Val(Right(restaItem, 7))
            CantRetira = Val(Left(restaItem, 3))
            valorX = Mid(restaItem, 6, 6) * CantRetira
            Main.LbTotal.Caption = Main.LbTotal.Caption - valorX
            Main.List1(estaM).RemoveItem Main.List1(estaM).ListIndex
            If IdentItemP = 1 Then
                RubroDef1(estaM + 1) = RubroDef1(estaM + 1) - valorX
            ElseIf IdentItemP = 2 Then
                RubroDef2(estaM + 1) = RubroDef2(estaM + 1) - valorX
            End If
            If Main.List1(estaM).ListCount = 0 Then
                Main.LBmesa(estaM).BackColor = MainColores(1)
                MesaStatus(estaM + 1) = 1
                GarzonMesa(estaM) = ""
                MesaDescuento(estaM + 1) = 0
            End If
            If Mid(Producto, 20, 1) = "*" Then
                MsgBox "Grabar"
                With Main.AdoQuitados.Recordset
                    .AddNew
                    .Fields(0) = Val(Mid(restaItem, 50, 5))
                    .Fields(1) = Producto
                    .Fields(2) = Main.TxtMesaSel.Text
                    .Fields(3) = Time
                    .Fields(4) = Main.TurnoTurno.Caption
                    .Fields(5) = Main.TurnoFecha.Caption
                    .Fields(6) = Autorizador
                    .Fields(7) = CantRetira
                    .Update
                End With
                estaM = estaM + 1
            Else
                MsgBox "no hay para que grabar"
            End If
            
           '
           'aqui debo actualizar mesa
           '
           PedMesa = PathMesas & "Mesa" & estaM & ".ped"
            fpedido = Trim(Str(Apedido))
            X = FreeFile
            Kill PedMesa
            LargoR = Len(rPedido)
            Open PedMesa For Random As #X Len = LargoR
            With rPedido
                For A = 0 To Main.List1(estaM - 1).ListCount - 1
                    restaItem = Main.List1(estaM - 1).List(A)
                    .Npedido = fpedido   '' Archiva de nuevo la lista
                    .Hora = Mid(restaItem, Desde, 9)
                    .Nmesa = Str(estaM)
                    .Codigo = Str(Val(Mid(restaItem, 50, 5)))
                    .Cantidad = Left(restaItem, 3)
                    .Detalle = Mid(restaItem, 15, 20)
                    .PrecioU = Mid(restaItem, 6, 6)
                    .SubTotal = Str(.Cantidad * .PrecioU)
                    .Bebestible = Right(restaItem, 1)
                    .Rubro = Mid(restaItem, 55, 2)
                    Put #X, , rPedido
                Next
                Close #X
            End With
            
        End If
        
    End If
    PermiteSacarItem = False
    ElMainActivo = True
    Exit Sub
rutError:
    MsgBox "Se caso un error"
End Sub

Private Sub TxtClave_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cmdOK_Click
    'SendKeys "{tab}"
End Sub
