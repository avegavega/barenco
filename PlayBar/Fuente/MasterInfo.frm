VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form MasterInfo 
   BackColor       =   &H00000000&
   Caption         =   "Maestro de Informes"
   ClientHeight    =   5640
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7425
   LinkTopic       =   "Form1"
   ScaleHeight     =   5640
   ScaleWidth      =   7425
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command1 
      Caption         =   "Imprimir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      TabIndex        =   25
      Top             =   7920
      Width           =   2295
   End
   Begin TabDlg.SSTab Tabulador 
      Height          =   5775
      Left            =   360
      TabIndex        =   12
      Top             =   2040
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   10186
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      BackColor       =   -2147483647
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Rendiciones"
      TabPicture(0)   =   "MasterInfo.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "MshInfo"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Estad�sticas"
      TabPicture(1)   =   "MasterInfo.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "MSHxRubros"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame4"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame5"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "Exclusivo SII (Impresora Fiscal)"
      TabPicture(2)   =   "MasterInfo.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      Begin VB.Frame Frame5 
         BackColor       =   &H8000000B&
         Caption         =   "Productos por garzon"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5175
         Left            =   6960
         TabIndex        =   18
         Top             =   480
         Width           =   4095
         Begin VB.ComboBox Combo2 
            Height          =   315
            Left            =   120
            TabIndex        =   24
            Text            =   "Combo2"
            Top             =   1080
            Visible         =   0   'False
            Width           =   2655
         End
         Begin VB.CommandButton CmdAhora 
            Caption         =   "Ahora"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2880
            TabIndex        =   22
            Top             =   720
            Width           =   1095
         End
         Begin VB.ComboBox Combo1 
            Height          =   315
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   21
            Text            =   "Selecciones Producto"
            Top             =   720
            Width           =   2655
         End
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MshProdGarzon 
            Height          =   3615
            Left            =   360
            TabIndex        =   19
            Top             =   1320
            Width           =   3495
            _ExtentX        =   6165
            _ExtentY        =   6376
            _Version        =   393216
            FixedCols       =   0
            HighLight       =   2
            FillStyle       =   1
            ScrollBars      =   2
            SelectionMode   =   1
            _NumberOfBands  =   1
            _Band(0).Cols   =   2
         End
         Begin VB.Label Label4 
            Caption         =   "Seeci�n especial"
            Height          =   255
            Left            =   960
            TabIndex        =   23
            Top             =   360
            Width           =   2535
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Ventas por Garzon"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5175
         Left            =   3960
         TabIndex        =   16
         Top             =   480
         Width           =   2895
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHxGarzon 
            Height          =   4335
            Left            =   120
            TabIndex        =   17
            Top             =   480
            Width           =   2655
            _ExtentX        =   4683
            _ExtentY        =   7646
            _Version        =   393216
            FixedCols       =   0
            HighLight       =   2
            FillStyle       =   1
            ScrollBars      =   1
            SelectionMode   =   1
            _NumberOfBands  =   1
            _Band(0).Cols   =   2
         End
      End
      Begin VB.Frame MSHxRubros 
         Caption         =   "Ventas por producto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5175
         Left            =   240
         TabIndex        =   14
         Top             =   480
         Width           =   3615
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid MshProductos 
            Height          =   4335
            Left            =   240
            TabIndex        =   15
            Top             =   480
            Width           =   3135
            _ExtentX        =   5530
            _ExtentY        =   7646
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            HighLight       =   2
            FillStyle       =   1
            ScrollBars      =   2
            SelectionMode   =   1
            _NumberOfBands  =   1
            _Band(0).Cols   =   3
         End
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MshInfo 
         Height          =   5052
         Left            =   -74520
         TabIndex        =   13
         Top             =   600
         Width           =   8652
         _ExtentX        =   15266
         _ExtentY        =   8916
         _Version        =   393216
         Rows            =   3
         Cols            =   6
         FixedCols       =   0
         FocusRect       =   2
         HighLight       =   2
         FillStyle       =   1
         ScrollBars      =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   6
      End
   End
   Begin MSAdodcLib.Adodc AdoInfoVentas 
      Height          =   330
      Left            =   7080
      Top             =   360
      Visible         =   0   'False
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=exactfood"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "exactfood"
      OtherAttributes =   ""
      UserName        =   "tools_db"
      Password        =   "eunice"
      RecordSource    =   "InfoVentas"
      Caption         =   "InfoVentas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00000080&
      Caption         =   "Datos para Informe"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2055
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   11655
      Begin VB.CommandButton CmdGenera 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   5280
         Picture         =   "MasterInfo.frx":0054
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   840
         Width           =   2415
      End
      Begin VB.Frame Frame3 
         Caption         =   "Turnos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   3120
         TabIndex        =   6
         Top             =   480
         Width           =   1935
         Begin VB.CheckBox ChkTurno 
            Caption         =   "Todos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   9
            Top             =   600
            Width           =   1095
         End
         Begin VB.TextBox TxtTurno 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1200
            TabIndex        =   8
            Text            =   "1"
            Top             =   240
            Width           =   495
         End
         Begin VB.Label Lbsolo 
            Caption         =   "S�lo turno"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Rango de Fechas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   2655
         Begin VB.TextBox TxtFin 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1080
            TabIndex        =   5
            Top             =   720
            Width           =   1455
         End
         Begin VB.TextBox TxtInicio 
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "MM-dd-yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   13322
               SubFormatType   =   0
            EndProperty
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   1080
            TabIndex        =   4
            Top             =   360
            Width           =   1455
         End
         Begin VB.Label Label2 
            Caption         =   "Hasta"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   3
            Top             =   840
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Desde"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   2
            Top             =   360
            Width           =   735
         End
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   $"MasterInfo.frx":3942
         ForeColor       =   &H0000FFFF&
         Height          =   855
         Left            =   8160
         TabIndex        =   20
         Top             =   600
         Width           =   2775
      End
   End
   Begin VB.Label Label3 
      Caption         =   "0"
      DataField       =   "Turno"
      DataSource      =   "AdoInfoVentas"
      Height          =   255
      Left            =   6840
      TabIndex        =   10
      Top             =   1320
      Visible         =   0   'False
      Width           =   255
   End
End
Attribute VB_Name = "MasterInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim EsteTurno As String
Dim EstaFecha As String
Dim PorGarzon As ADODB.Recordset
Dim ProductoGarzon As ADODB.Recordset

    
Private Sub ChkTurno_Click()
    If ChkTurno.Value = 0 Then
        TxtTurno.Enabled = True
        Lbsolo.Enabled = True
    Else
        TxtTurno.Enabled = False
        Lbsolo.Enabled = False
    End If
End Sub
Private Sub CmdAhora_Click()
    Dim PedBuscar As String
    Dim SoloFecha As ADODB.Recordset
    Dim BuscGarzon As ADODB.Recordset
    Dim ProdBuscas As String
    Dim xFec As String
    Dim X As Integer, K As Integer, F As Integer, Q As Integer
    Dim MYFecha As Date
    Dim Nped As String * 10
    Dim NCant As String * 4
    Dim porsi As String
    Dim GarzonVendio(99) As String
    Dim Si As Boolean
    Si = False
    porsi = "0"
    If Combo1.ListIndex = 0 Then
        paso = MsgBox(" No hay producto selecionado ... ", vbExclamation + vbOKOnly)
        Exit Sub
    End If
    Label4.Caption = "Por favor espere ..."
    For vd = 0 To 99
        GarzonVendio(vd) = ""
    Next
    
    X = FreeFile
 
    d = CDate(TxtFin.Text) - CDate(TxtInicio.Text)
    MYFecha = CDate(TxtInicio.Text)
    ProdBuscas = Combo2.List(Combo1.ListIndex)
    FileName = "RegNped.lis"
    Open FileName For Output As X
    For ciclo = 0 To d
        Set SoloFecha = Nothing
        xFec = MYFecha
        Set SoloFecha = CampoFiltrado(Main.AdoPedidos.Recordset, "Fecha", xFec, "CodProducto", ProdBuscas)
        xx = SoloFecha.RecordCount
        MYFecha = MYFecha + 1
        If xx > 0 Then
            With SoloFecha     '' Creando lista de
                .MoveFirst      '' Cantidad y pedido
                For zz = 1 To .RecordCount
                    Nped = .Fields(0)
                    NCant = .Fields(5)
                    Print #X, NCant & "-" & Nped
                    .MoveNext
                Next
            End With
        End If
    Next
    Close #X
    
    'Archivo de pedidos
    ' Cantidad y N�pedido
    '
    FileName = "RegNped.lis"
    Q = FreeFile
    Open FileName For Input As Q
    
    
    
    Do While Not EOF(Q)
        Line Input #Q, PedBuscar ' Cantidad y Npedido
   
        ProdBuscas = Trim(Mid(PedBuscar, 6, 10))
        xFec = "0"   ' ' 'Buscando el garzon que corresponde al pedido
        Set BuscGarzon = Nothing
        Set BuscGarzon = CampoFiltrado(Pago.AdoTickets.Recordset, "Fecha", xFec, "Nticket", ProdBuscas)
        
        If Mid(BuscGarzon.Fields(4), 1, 1) = "N" Then
            ngarzon = Lg
        Else
            ngarzon = Val(Mid(BuscGarzon.Fields(4), 1, 2)) - 1
        End If
        GarzonVendio(ngarzon) = Val(GarzonVendio(ngarzon)) + Val(Mid(PedBuscar, 1, 4))
    Loop
    Close #Q
    Close #X
   MsgBox Lg
    MshProdGarzon.Rows = 2
    For dd = 0 To Lg
            With MshProdGarzon
                .Row = dd + 1: .Col = 0
                .Text = NombreGar(dd)
                .Col = 1
                .Text = GarzonVendio(dd)
                .Rows = .Rows + 1
            End With
    Next
    Label4.Caption = "Listo..."
    
    
    
    
End Sub

Private Sub CmdGenera_Click()
    Dim MiDetalle As String * 20
    Dim FecDesde As Date
    Dim FecHasta As Date
    Dim FecX As String
    Dim ElTurno As Integer
    Dim MiTurno As String
    Dim PorProducto As ADODB.Recordset
    Dim PorCodigo As ADODB.Recordset
    Dim ProductoVendido(8000) As String * 20
    Dim CantidadProductoVendido(8000) As String * 10
    Dim RubroProductoVendido(8000) As String * 2
    For aa = 0 To 8000
        ProductoVendido(aa) = " "
        CantidadProductoVendido(aa) = " "
    Next
'    esta
    If IsDate(TxtInicio.Text) And IsDate(TxtFin.Text) Then
        FecDesde = CDate(TxtInicio.Text)
        FecHasta = CDate(TxtFin.Text)
        FecDesdeX = FecDesde
        FecHastaX = FecHasta
    Else
        MsgBox "Fechas Ingresadas no v�lidas ... ", vbExclamation
        Exit Sub
    End If
    If ChkTurno.Value = 1 Then
        ElTurno = 0
    Else
        ElTurno = TxtTurno.Text
    End If
    MiTurno = ElTurno
    With AdoInfoVentas.Recordset
        .MoveFirst
        paso = .RecordCount
        ps = 1
        MshInfo.Row = 1
        For s = 0 To .RecordCount - 1
            If CDate(.Fields(0)) >= FecDesde And _
            CDate(.Fields(0)) <= FecHasta Then
                If ChkTurno.Value = 1 Then
                    MshInfo.Row = ps
                    MshInfo.Col = 0
                    MshInfo.Text = .Fields(0)
                    MshInfo.Col = 1
                    MshInfo.Text = .Fields(1)
                    MshInfo.Col = 2
                    MshInfo.Text = .Fields(2)
                    MshInfo.Col = 4
                    MshInfo.Text = .Fields(4)
                    MshInfo.Col = 5
                    MshInfo.Text = .Fields(5)
                    ps = ps + 1
                    MshInfo.Rows = ps + 1
                End If
                If ChkTurno.Value = 0 Then
                    If .Fields(1) = ElTurno Then
                        MshInfo.Row = ps
                        MshInfo.Col = 0
                        MshInfo.Text = .Fields(0)
                        MshInfo.Col = 1
                        MshInfo.Text = .Fields(1)
                        MshInfo.Col = 2
                        MshInfo.Text = .Fields(2)
                        MshInfo.Col = 4
                        MshInfo.Text = .Fields(4)
                        MshInfo.Col = 5
                        MshInfo.Text = .Fields(5)
                        ps = ps + 1
                        MshInfo.Rows = ps + 1
                    End If
                End If
                
            End If
            .MoveNext
        Next
    End With
    
    With MshInfo
        .Col = 2
        For s = 1 To .Rows - 1
            .Row = s
            SumaTotal = SumaTotal + Val(.Text)
        Next
        .Row = .Rows - 1
        .Text = SumaTotal
    End With
    
    ''  ESTADISTICAS
    ' Por producto
    ' CICLO DE COMPROBACION
    d = FecHasta - FecDesde
    MSHProductos.Rows = 2
    For ciclo = 0 To d
        FecX = FecDesde + ciclo
        Set PorProducto = Nothing
        Set PorProducto = CampoFiltrado(Main.AdoPedidos.Recordset, "Turno", MiTurno, "Fecha", FecX)
        ll = PorProducto.RecordCount
        If ll > 0 Then
            PorProducto.MoveFirst
            ultimovacio = 0
            For xx = 1 To PorProducto.RecordCount
                If PorProducto.Fields(5) > 0 Then
                    MiDetalle = PorProducto.Fields(6)
                    micantidad = PorProducto.Fields(5)
                    mirubro = PorProducto.Fields(10)
                    For ss = 0 To 8000
                        If ProductoVendido(ss) = Mid(MiDetalle, 1, 20) Then
                            CantidadProductoVendido(ss) = Val(CantidadProductoVendido(ss)) + micantidad
                            Exit For
                        End If
                        If ss = 8000 Then ' aqui busca primer vacio
                            For yy = 0 To 8000
                                If Mid(ProductoVendido(yy), 1, 1) = " " Then
                                   ' MsgBox "ulsa" & " " & yy
                                    ultimovacio = yy
                                    Exit For
                                End If
                            Next
                            ProductoVendido(ultimovacio) = MiDetalle
                            CantidadProductoVendido(ultimovacio) = micantidad
                            RubroProductoVendido(ultimovacio) = mirubro
                         '   MsgBox ProductoVendido(ultimovacio)
                        End If
                    Next
                    
                End If
                PorProducto.MoveNext
            Next
        End If
    Next
    For zz = 0 To 8000
        If Mid(ProductoVendido(zz), 1, 1) = " " Then
            Exit For
        End If
        MSHProductos.Col = 0
        MSHProductos.Row = zz + 1
        MSHProductos.Text = ProductoVendido(zz)
        MSHProductos.Col = 1
        MSHProductos.Text = CantidadProductoVendido(zz)
        MSHProductos.Col = 2
        MSHProductos.Text = RubroProductoVendido(zz)
        MSHProductos.Rows = MSHProductos.Rows + 1
    Next
    MSHProductos.Rows = MSHProductos.Rows - 1
    MSHProductos.Col = 2
    MSHProductos.Sort = flexSortGenericAscending
    ' hasta aqui ventas por producto
        
    'Aqui comienza separacion vent
   
   
   
End Sub

Private Sub Command1_Click()
    Dim Narchivo As Integer
    Dim miProductos As String * 20
    Dim misVendidos As String
    Dim rrrr As String
    Narchivo = FreeFile
    Open "c:\cafeuro\listaproductos" For Output As Narchivo
        Print #Narchivo, "Desde: "; TxtInicio.Text
        Print #Narchivo, "Hasta: "; TxtFin.Text
        Print #Narchivo, "----------------"
        For i = 0 To MSHProductos.Rows - 1
            MSHProductos.Row = i
            MSHProductos.Col = 0
            miProductos = MSHProductos.Text
            MSHProductos.Col = 1
            misVendidos = MSHProductos.Text
                  
            Print #Narchivo, miProductos; misVendidos
        Next
    Close #Narchivo
    Narchivo = FreeFile
    For Each Impresora In Printers
        If Impresora.DeviceName = Main.InformesPrinter.Caption Then
            Set Printer = Impresora
            Exit For
        End If
    Next
    paso = Main.CambiaLetra("Courier", 12, False, True, False)
    Open "c:\cafeuro\listaproductos" For Input As Narchivo
        Do While Not EOF(Narchivo)
            Line Input #Narchivo, rrrr
            Printer.Print rrrr
        Loop
    Close #Narchivo
    Printer.NewPage
    Printer.EndDoc
        
    
    
End Sub

Private Sub Form_Load()
   
    With Main.AdoAllProductos
        .Recordset.MoveFirst
        For C = 1 To .Recordset.RecordCount
            Combo1.AddItem (.Recordset.Fields(1))
            Combo2.AddItem (.Recordset.Fields(0))
            .Recordset.MoveNext
        Next
    End With
    With MSHProductos
        .ColWidth(0) = 1800
        .ColWidth(1) = 900
        .ColWidth(2) = 10
        .Col = 0: .Row = 0
        .Text = "Producto": .Col = 1
        .Text = "Vendidos"
        
        
    End With
    
    With MshInfo
        .Row = 0
        .Col = 0: .Text = "Fecha"
        .Col = 1: .Text = "Turno"
        .Col = 2: .Text = "Ventas"
        .Col = 3: .Text = "Acumulado"
        .Col = 4: .Text = "Alimentos"
        .Col = 5: .Text = "Bebestibles"
    End With
    
    
    With MSHxGarzon
        .Row = 0
        .Col = 0: .Text = "Garzon"
        .Col = 1: .Text = "Total"
    End With
    With MshProdGarzon
        .Row = 0
        .Col = 0: .Text = "Garzon"
        .ColWidth(0) = 1900
        .Col = 1: .Text = "Vendidos"
        .ColAlignment = 0
    End With
    TxtInicio.Text = Date
    TxtFin.Text = Date
End Sub
Public Function CampoFiltrado(rstTemp As ADODB.Recordset, _
   strField As String, strFilter As String, strCampo As String, strFiltro As String) As ADODB.Recordset
   Dim Cadena1 As String
   Dim Cadena2 As String
   Dim CadenaD As String
   If Val(strFilter) > 0 Then
        Cadena1 = strField & "=" & strFilter
   End If
   Cadena2 = strCampo & " = " & strFiltro
   If Val(strFilter) > 0 Then
        CadenaD = Cadena1 & " And " & Cadena2
   Else
        CadenaD = Cadena2
   End If
   rstTemp.Filter = 0
   rstTemp.Filter = CadenaD
   Set CampoFiltrado = rstTemp
End Function



