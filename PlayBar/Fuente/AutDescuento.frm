VERSION 5.00
Begin VB.Form AutDescuento 
   Caption         =   "Solicite Autorizacion"
   ClientHeight    =   2565
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   2565
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton CmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      Left            =   2400
      TabIndex        =   2
      Top             =   1920
      Width           =   1692
   End
   Begin VB.CommandButton CmdOk 
      Caption         =   "C&onfirmar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      Left            =   240
      TabIndex        =   1
      Top             =   1920
      Width           =   1692
   End
   Begin VB.TextBox TxtClave 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      IMEMode         =   3  'DISABLE
      Left            =   600
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   1080
      Width           =   3492
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Ingrese clave para autorizar descuento"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   360
      TabIndex        =   4
      Top             =   120
      Width           =   3975
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   495
      Left            =   600
      TabIndex        =   3
      Top             =   240
      Width           =   3495
   End
End
Attribute VB_Name = "AutDescuento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOK_Click()
    With Main
        Select Case TxtClave.Text
            Case .ClaveDesc1.Caption
                Autorizador = .NombreDesc1.Caption
            Case .ClaveDesc2.Caption
                Autorizador = .NombreDesc2.Caption
            Case .ClaveDesc3.Caption
                Autorizador = .NombreDesc3.Caption
            Case Else
                Beep
                permiteDescuento = False
                MsgBox " �� ERROR, CLAVE NO ENCONTRADA ... !!", vbExclamation + vbOKOnly, "ATENCION, CUIDADO, ATENCION"
                Unload Me
                Exit Sub
        End Select
        permiteDescuento = True
        
        Unload Me
        'Retirando
        
    End With
End Sub



Private Sub TxtClave_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then cmdOK_Click
End Sub
