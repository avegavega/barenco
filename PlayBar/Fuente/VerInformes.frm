VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Begin VB.Form VerInformes 
   Caption         =   "Informes"
   ClientHeight    =   8385
   ClientLeft      =   3105
   ClientTop       =   1305
   ClientWidth     =   16320
   LinkTopic       =   "Form1"
   ScaleHeight     =   8385
   ScaleWidth      =   16320
   Begin VB.Frame FraProgreso 
      Caption         =   "Recopilando  y comparando informacion"
      Height          =   795
      Left            =   1725
      TabIndex        =   30
      Top             =   4995
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   31
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   1440
      OleObjectBlob   =   "VerInformes.frx":0000
      Top             =   0
   End
   Begin VB.Frame Frame 
      Height          =   6690
      Index           =   2
      Left            =   120
      TabIndex        =   12
      Top             =   1440
      Width           =   14892
      Begin TabDlg.SSTab stabInformes 
         Height          =   6120
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   14655
         _ExtentX        =   25850
         _ExtentY        =   10795
         _Version        =   393216
         TabHeight       =   520
         TabCaption(0)   =   "General"
         TabPicture(0)   =   "VerInformes.frx":0234
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Line"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "GridInf"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Gridpagos"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "LvTotales"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "LvFpagos"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "CmdExcelTotales"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "CmdExcelFpago"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         TabCaption(1)   =   "Detalle Tickets"
         TabPicture(1)   =   "VerInformes.frx":0250
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Gridtickets"
         Tab(1).Control(1)=   "LvTicket"
         Tab(1).Control(2)=   "CmdExcelTicket"
         Tab(1).ControlCount=   3
         TabCaption(2)   =   "Estadisticas"
         TabPicture(2)   =   "VerInformes.frx":026C
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "GridProductos"
         Tab(2).Control(1)=   "GridMesas"
         Tab(2).Control(2)=   "GridGarzones"
         Tab(2).Control(3)=   "GridRubros"
         Tab(2).Control(4)=   "Frame2"
         Tab(2).Control(5)=   "LvLineas"
         Tab(2).Control(6)=   "LvProductos"
         Tab(2).Control(7)=   "CmdExcelLineas"
         Tab(2).Control(8)=   "CmdExcelPruductos"
         Tab(2).ControlCount=   9
         Begin VB.CommandButton CmdExcelFpago 
            Caption         =   "Excel"
            Height          =   300
            Left            =   9315
            TabIndex        =   37
            Top             =   4215
            Width           =   1665
         End
         Begin VB.CommandButton CmdExcelTotales 
            Caption         =   "Excel"
            Height          =   300
            Left            =   1995
            TabIndex        =   36
            Top             =   4185
            Width           =   1665
         End
         Begin VB.CommandButton CmdExcelPruductos 
            Caption         =   "Excel"
            Height          =   255
            Left            =   -63540
            TabIndex        =   35
            Top             =   5595
            Width           =   1170
         End
         Begin VB.CommandButton CmdExcelLineas 
            Caption         =   "Excel"
            Height          =   270
            Left            =   -68295
            TabIndex        =   34
            Top             =   5640
            Width           =   1305
         End
         Begin MSComctlLib.ListView LvProductos 
            Height          =   3765
            Left            =   -65640
            TabIndex        =   33
            Top             =   1785
            Width           =   5070
            _ExtentX        =   8943
            _ExtentY        =   6641
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            HotTracking     =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "T1000"
               Text            =   "Producto"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N109"
               Text            =   "Cantidad"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "N109"
               Text            =   "Rubro"
               Object.Width           =   0
            EndProperty
         End
         Begin MSComctlLib.ListView LvLineas 
            Height          =   4590
            Left            =   -69225
            TabIndex        =   32
            Top             =   930
            Width           =   3240
            _ExtentX        =   5715
            _ExtentY        =   8096
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            HotTracking     =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Forma de Pago"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Activo"
               Object.Width           =   0
            EndProperty
         End
         Begin VB.CommandButton CmdExcelTicket 
            Caption         =   "Excel"
            Height          =   300
            Left            =   -74850
            TabIndex        =   29
            Top             =   5400
            Width           =   1665
         End
         Begin MSComctlLib.ListView LvTicket 
            Height          =   4740
            Left            =   -74850
            TabIndex        =   28
            Top             =   600
            Width           =   14265
            _ExtentX        =   25162
            _ExtentY        =   8361
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            HotTracking     =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Nro Ticket"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N109"
               Text            =   "Mesa"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Object.Tag             =   "N100"
               Text            =   "Venta"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "T1000"
               Text            =   "Forma de Pago"
               Object.Width           =   4586
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Tag             =   "T1000"
               Text            =   "Garzon"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "T1000"
               Text            =   "Hora"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Object.Tag             =   "T1000"
               Text            =   "Fecha Emision"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Object.Tag             =   "T1000"
               Text            =   "Documento"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Object.Tag             =   "N109"
               Text            =   "Turno"
               Object.Width           =   2540
            EndProperty
         End
         Begin MSComctlLib.ListView LvFpagos 
            Height          =   3180
            Left            =   6855
            TabIndex        =   27
            Top             =   915
            Width           =   6225
            _ExtentX        =   10980
            _ExtentY        =   5609
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            HotTracking     =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Forma de Pago"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Activo"
               Object.Width           =   0
            EndProperty
         End
         Begin MSComctlLib.ListView LvTotales 
            Height          =   3225
            Left            =   495
            TabIndex        =   26
            Top             =   900
            Width           =   5745
            _ExtentX        =   10134
            _ExtentY        =   5689
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            HotTracking     =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Turno"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Activo"
               Object.Width           =   0
            EndProperty
         End
         Begin VB.Frame Frame2 
            Caption         =   "Filtro"
            Height          =   852
            Left            =   -65640
            TabIndex        =   21
            Top             =   600
            Width           =   5052
            Begin VB.CommandButton cmdBuscar 
               Caption         =   "Buscar"
               Height          =   252
               Left            =   3000
               TabIndex        =   25
               Top             =   480
               Width           =   732
            End
            Begin VB.CommandButton cmdTodos 
               Caption         =   "Todos"
               Height          =   252
               Left            =   3960
               TabIndex        =   24
               Top             =   480
               Width           =   972
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
               Height          =   252
               Left            =   120
               OleObjectBlob   =   "VerInformes.frx":0288
               TabIndex        =   23
               Top             =   480
               Width           =   732
            End
            Begin VB.TextBox txtProducto 
               Height          =   288
               Left            =   840
               TabIndex        =   22
               Top             =   480
               Width           =   2052
            End
         End
         Begin MSDataGridLib.DataGrid GridRubros 
            Height          =   4812
            Left            =   -69240
            TabIndex        =   19
            Top             =   720
            Width           =   3252
            _ExtentX        =   5741
            _ExtentY        =   8493
            _Version        =   393216
            AllowUpdate     =   0   'False
            HeadLines       =   1
            RowHeight       =   15
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Caption         =   "Lineas"
            ColumnCount     =   2
            BeginProperty Column00 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column01 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            SplitCount      =   1
            BeginProperty Split0 
               BeginProperty Column00 
               EndProperty
               BeginProperty Column01 
               EndProperty
            EndProperty
         End
         Begin MSDataGridLib.DataGrid GridGarzones 
            Height          =   4812
            Left            =   -74880
            TabIndex        =   17
            Top             =   720
            Width           =   3132
            _ExtentX        =   5530
            _ExtentY        =   8493
            _Version        =   393216
            AllowUpdate     =   0   'False
            Appearance      =   0
            HeadLines       =   1
            RowHeight       =   15
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Caption         =   "Garzones"
            ColumnCount     =   2
            BeginProperty Column00 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column01 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            SplitCount      =   1
            BeginProperty Split0 
               BeginProperty Column00 
               EndProperty
               BeginProperty Column01 
               EndProperty
            EndProperty
         End
         Begin MSDataGridLib.DataGrid Gridtickets 
            Height          =   4692
            Left            =   -74640
            TabIndex        =   16
            Top             =   600
            Width           =   13452
            _ExtentX        =   23733
            _ExtentY        =   8281
            _Version        =   393216
            AllowUpdate     =   0   'False
            HeadLines       =   1
            RowHeight       =   15
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnCount     =   2
            BeginProperty Column00 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column01 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            SplitCount      =   1
            BeginProperty Split0 
               BeginProperty Column00 
               EndProperty
               BeginProperty Column01 
               EndProperty
            EndProperty
         End
         Begin MSDataGridLib.DataGrid Gridpagos 
            Height          =   3492
            Left            =   6840
            TabIndex        =   15
            Top             =   600
            Width           =   6252
            _ExtentX        =   11033
            _ExtentY        =   6165
            _Version        =   393216
            AllowUpdate     =   0   'False
            HeadLines       =   1
            RowHeight       =   22
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Caption         =   "Formas de Pago"
            ColumnCount     =   2
            BeginProperty Column00 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column01 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            SplitCount      =   1
            BeginProperty Split0 
               BeginProperty Column00 
               EndProperty
               BeginProperty Column01 
               EndProperty
            EndProperty
         End
         Begin MSDataGridLib.DataGrid GridInf 
            Height          =   3492
            Left            =   480
            TabIndex        =   14
            Top             =   600
            Width           =   5772
            _ExtentX        =   10186
            _ExtentY        =   6165
            _Version        =   393216
            AllowUpdate     =   0   'False
            HeadLines       =   1
            RowHeight       =   19
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Caption         =   "Totales"
            ColumnCount     =   2
            BeginProperty Column00 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column01 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            SplitCount      =   1
            BeginProperty Split0 
               BeginProperty Column00 
               EndProperty
               BeginProperty Column01 
               EndProperty
            EndProperty
         End
         Begin MSDataGridLib.DataGrid GridMesas 
            Height          =   4812
            Left            =   -71520
            TabIndex        =   18
            Top             =   720
            Width           =   2172
            _ExtentX        =   3836
            _ExtentY        =   8493
            _Version        =   393216
            AllowUpdate     =   0   'False
            HeadLines       =   1
            RowHeight       =   15
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Caption         =   "Mesas"
            ColumnCount     =   2
            BeginProperty Column00 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column01 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            SplitCount      =   1
            BeginProperty Split0 
               BeginProperty Column00 
               EndProperty
               BeginProperty Column01 
               EndProperty
            EndProperty
         End
         Begin MSDataGridLib.DataGrid GridProductos 
            Height          =   3972
            Left            =   -65640
            TabIndex        =   20
            Top             =   1560
            Width           =   5052
            _ExtentX        =   8916
            _ExtentY        =   7011
            _Version        =   393216
            AllowUpdate     =   0   'False
            HeadLines       =   1
            RowHeight       =   15
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Caption         =   "Productos"
            ColumnCount     =   2
            BeginProperty Column00 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column01 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   0
               EndProperty
            EndProperty
            SplitCount      =   1
            BeginProperty Split0 
               BeginProperty Column00 
               EndProperty
               BeginProperty Column01 
               EndProperty
            EndProperty
         End
         Begin VB.Line Line 
            X1              =   5400
            X2              =   6840
            Y1              =   2160
            Y2              =   2160
         End
      End
   End
   Begin VB.Frame Frame 
      Height          =   975
      Index           =   1
      Left            =   11040
      TabIndex        =   10
      Top             =   360
      Width           =   3972
      Begin VB.CommandButton cmdGenera 
         Caption         =   "Generar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   492
         Left            =   1200
         TabIndex        =   11
         Top             =   360
         Width           =   2172
      End
   End
   Begin VB.Frame Frame 
      Height          =   975
      Index           =   0
      Left            =   9360
      TabIndex        =   7
      Top             =   360
      Width           =   1695
      Begin VB.ComboBox ComTurno 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   480
         Width           =   1335
      End
      Begin VB.Label Turno 
         Caption         =   "Turno"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fechas"
      Height          =   975
      Left            =   6000
      TabIndex        =   3
      Top             =   360
      Width           =   3375
      Begin MSComCtl2.DTPicker DatInicio 
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   480
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         _Version        =   393216
         Format          =   91488257
         CurrentDate     =   40288
      End
      Begin MSComCtl2.DTPicker DtFin 
         Height          =   375
         Left            =   1920
         TabIndex        =   6
         Top             =   480
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         _Version        =   393216
         Format          =   91488257
         CurrentDate     =   40288
      End
      Begin VB.Label al 
         Caption         =   "al"
         Height          =   255
         Left            =   1560
         TabIndex        =   4
         Top             =   480
         Width           =   375
      End
   End
   Begin VB.Frame OpcionInforme 
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   5895
      Begin VB.ComboBox ComInforme 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   480
         Visible         =   0   'False
         Width           =   4455
      End
      Begin VB.Label Label1 
         Caption         =   "Seleccione Informe"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   3975
      End
   End
End
Attribute VB_Name = "VerInformes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Rs_Inf As Recordset
Dim Rs_Fpagos As Recordset
Dim Rs_Reporte As Recordset
Dim Rs_Tickets As Recordset
Dim S_sql_fpagos As String
Dim S_filtro_fecha As String
Dim S_sql_ticket As String
Dim Rs_Garzones As Recordset
Dim Rs_Mesas As Recordset
Dim Rs_Lineas As Recordset
Dim Rs_Productos As Recordset
Dim Condicion As String, S_turno As String

Private Sub cmdBuscar_Click()
    If Len(txtProducto) = 0 Then Exit Sub
    
'    Rs_Productos.Filter = "descripcion Like '%" & txtProducto & "%'"
    Call CargaProductos(Condicion & " AND descripcion Like '%" & txtProducto & "%'")
End Sub

Private Sub CmdExcelFpago_Click()
    Dim tit(2) As String
    If Me.LvFpagos.ListItems.Count = 0 Then Exit Sub
    CmdExcelFpago.Caption = "Generando..."
    DoEvents
    FraProgreso.Visible = True
    tit(0) = "Ticket emitidos"
    tit(1) = DatInicio
    tit(2) = Me.DtFin
    ExportarNuevo Me.LvFpagos, tit, Me, BarraProgreso
    FraProgreso.Visible = False
    CmdExcelFpago.Caption = "Excel"
End Sub

Private Sub CmdExcelLineas_Click()
    Dim tit(2) As String
    If LvLineas.ListItems.Count = 0 Then Exit Sub
    CmdExcelLineas.Caption = "Generando..."
    DoEvents
    FraProgreso.Visible = True
    tit(0) = "Ticket emitidos"
    tit(1) = DatInicio
    tit(2) = Me.DtFin
    ExportarNuevo LvLineas, tit, Me, BarraProgreso
    FraProgreso.Visible = False
    CmdExcelLineas.Caption = "Excel"
End Sub

Private Sub CmdExcelPruductos_Click()
    Dim tit(2) As String
    If LvProductos.ListItems.Count = 0 Then Exit Sub
    CmdExcelPruductos.Caption = "Generando..."
    DoEvents
    FraProgreso.Visible = True
    tit(0) = "Ticket emitidos"
    tit(1) = DatInicio
    tit(2) = Me.DtFin
    ExportarNuevo LvProductos, tit, Me, BarraProgreso
    FraProgreso.Visible = False
    CmdExcelPruductos.Caption = "Excel"
End Sub

Private Sub CmdExcelTicket_Click()
    Dim tit(2) As String
    If LvTicket.ListItems.Count = 0 Then Exit Sub
    CmdExcelTicket.Caption = "Generando..."
    DoEvents
    FraProgreso.Visible = True
    tit(0) = "Ticket emitidos"
    tit(1) = DatInicio
    tit(2) = Me.DtFin
    ExportarNuevo LvTicket, tit, Me, BarraProgreso
    FraProgreso.Visible = False
    CmdExcelTicket.Caption = "Excel"
End Sub

Private Sub CmdExcelTotales_Click()
    Dim tit(2) As String
    If LvTotales.ListItems.Count = 0 Then Exit Sub
    CmdExcelTotales.Caption = "Generando..."
    DoEvents
    FraProgreso.Visible = True
    tit(0) = "Ticket emitidos"
    tit(1) = DatInicio
    tit(2) = Me.DtFin
    ExportarNuevo LvTotales, tit, Me, BarraProgreso
    FraProgreso.Visible = False
    CmdExcelTotales.Caption = "Excel"
End Sub

Private Sub CmdGenera_Click()
    Dim id_informe As Integer
    Dim Sql_Inf As String
    
    Dim FechaInicio As String
    Dim FechaFin As String
    Me.cmdGenera.Enabled = False
    DoEvents
    FechaInicio = Format(DatInicio.Value, "YYYY-MM-DD 00:00:00")
    FechaFin = Format(DtFin.Value, "YYYY-MM-DD 00:00:00")
    If ComTurno.Text = "Todos" Then
        S_turno = " "
    Else
        S_turno = " AND Turno = " & ComTurno.Text & " "
    End If
    Condicion = " WHERE fecha BETWEEN '" & FechaInicio & "' AND '" & FechaFin & "' " & S_turno
    id_informe = 1 'ComInforme.ItemData(ComInforme.ListIndex)
    Sql = "SELECT con_consulta,con_orden,con_consulta_pago,con_orden_pago  FROM consultas_informes WHERE inf_id=" & id_informe & " AND con_activo='SI'"
    Call Consulta(Rs_Inf, Sql)
    If Rs_Inf.RecordCount > 0 Then
        Sql_Inf = Rs_Inf!con_consulta & Condicion & Rs_Inf!con_orden
        Call Consulta(Rs_Reporte, Sql_Inf)
        Set GridInf.DataSource = Rs_Reporte 'Totales
        GridInf.Columns(1).NumberFormat = "##,###"
        GridInf.Columns(1).Alignment = dbgRight
        
        LLenar_Grilla Rs_Reporte, Me, Me.LvTotales, False, True, True, False
        
        If Not IsNull(Rs_Inf!con_consulta_pago) Then
            S_sql_fpagos = Rs_Inf!con_consulta_pago & Condicion & Rs_Inf!con_orden_pago
            Call Consulta(Rs_Fpagos, S_sql_fpagos)
            Set Gridpagos.DataSource = Rs_Fpagos ' Formas de pago
            Gridpagos.Columns(1).NumberFormat = "##,###"
            Gridpagos.Columns(1).Alignment = dbgRight
            Gridpagos.Columns(1).Width = 2000
            LLenar_Grilla Rs_Fpagos, Me, Me.LvFpagos, False, True, True, False
        End If
        
        
        S_sql_ticket = "SELECT Nticket as No_Ticket,NMesa as Mesa,valor as Venta,fpago as Forma_Pago,Garzon,Hora,date_FORMAT(fecha,'%d-%m-%Y') as Fecha_Emision,Documento as Doc_Venta,Turno " & _
                       "FROM tickets " & Condicion
        Call Consulta(Rs_Tickets, S_sql_ticket)
        Set Gridtickets.DataSource = Rs_Tickets ' Detalle de tickets
        LLenar_Grilla Rs_Tickets, Me, Me.LvTicket, False, True, True, False
        
        Gridtickets.Columns(2).Alignment = dbgRight
        Gridtickets.Columns(2).NumberFormat = "##,##0"
            
        
        Sql = "SELECT Garzon,Sum(valor) as Venta FROM tickets " & Condicion & " GROUP BY Garzon ORDER BY garzon"
        Call Consulta(Rs_Garzones, Sql)
        Rs_Garzones.Sort = "Venta"
        Set GridGarzones.DataSource = Rs_Garzones
        GridGarzones.Columns(1).Alignment = dbgRight
        GridGarzones.Columns(1).NumberFormat = "##,##0"
        GridGarzones.Columns(1).Width = 900
        
         Sql = "SELECT NMesa as Nro_Mesa,Sum(valor) as Venta FROM tickets " & Condicion & " GROUP BY Garzon ORDER BY NMesa"
        Call Consulta(Rs_Mesas, Sql)
        Rs_Mesas.Sort = "Venta"
        Set GridMesas.DataSource = Rs_Mesas
        GridMesas.Columns(1).Alignment = dbgRight
        GridMesas.Columns(1).NumberFormat = "##,##0"
        GridMesas.Columns(1).Width = 700
        
        Sql = "SELECT rub_nombre,sum(PrecioTotal) as Venta,p.MyRub FROM par_rubros_venta r,pedidos p " & Condicion & " AND r.rub_id=p.myrub GROUP BY rub_nombre "
     
        Call Consulta(Rs_Lineas, Sql)
        Rs_Lineas.Sort = "Venta"
        Set GridRubros.DataSource = Rs_Lineas
        GridRubros.Columns(1).Alignment = dbgRight
        GridRubros.Columns(1).NumberFormat = "##,##0"
        GridRubros.Columns(1).Width = 900
        GridRubros.Columns(2).Width = 1
        LLenar_Grilla Rs_Lineas, Me, Me.LvLineas, False, True, True, False
        
        
        
       CargaProductos (Condicion & "")
        
    End If
    Me.cmdGenera.Enabled = True
End Sub
Private Sub CargaProductos(MasCondicion As String)
        Sql = "SELECT descripcion,Sum(Cantidad) as Cant, sum(PrecioTotal) as Venta,MyRub FROM pedidos,productos p " & MasCondicion & " AND CodProducto=Cod GROUP BY descripcion "
        Call Consulta(Rs_Productos, Sql)
        If Rs_Productos.RecordCount > 0 Then
            Rs_Productos.Sort = "Venta"
            Set GridProductos.DataSource = Rs_Productos
            GridProductos.Columns(1).Alignment = dbgRight
            GridProductos.Columns(2).Alignment = dbgRight
            GridProductos.Columns(2).NumberFormat = "##,##0"
            GridProductos.Columns(0).Width = 3000
            GridProductos.Columns(1).Width = 600
            GridProductos.Columns(2).Width = 900
            GridProductos.Columns(3).Width = 1
        End If
        LLenar_Grilla Rs_Productos, Me, LvProductos, False, True, True, False
End Sub
Private Sub Form_Load()
    Aplicar_skin Me
    DatInicio.Value = Date
    DtFin.Value = Date
    Sql = "SELECT inf_id,inf_nombre FROM tipo_informes WHERE inf_activo='SI' ORDER BY inf_id"
    Call Consulta(Rst_tmp, Sql)
    If Rst_tmp.RecordCount > 0 Then
        Rst_tmp.MoveFirst
        Do While Not Rst_tmp.EOF
            ComInforme.AddItem Rst_tmp!inf_nombre
            ComInforme.ItemData(ComInforme.NewIndex) = Rst_tmp!inf_id
            Rst_tmp.MoveNext
        Loop
    End If
    
    Sql = "SELECT DISTINCTROW  Turno  FROM tickets"
    Call Consulta(Rst_tmp, Sql)
    If Rst_tmp.RecordCount > 0 Then
        Rst_tmp.MoveFirst
        Do While Not Rst_tmp.EOF
            ComTurno.AddItem Rst_tmp!Turno
            ComTurno.List(ComTurno.NewIndex) = Rst_tmp!Turno
            Rst_tmp.MoveNext
        Loop
    End If
    ComTurno.AddItem "Todos"
    ComTurno.ListIndex = ComTurno.ListCount - 1
    
End Sub


Private Sub GridRubros_DblClick()
    GridRubros.Col = 2
    Call CargaProductos(Condicion & " AND MyRub =" & GridRubros.Text)
End Sub

Private Sub txtProducto_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
