VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form BuscaProducto 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00008000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Busqueda Rapida"
   ClientHeight    =   6435
   ClientLeft      =   2760
   ClientTop       =   3375
   ClientWidth     =   7455
   ControlBox      =   0   'False
   DrawMode        =   1  'Blackness
   DrawStyle       =   6  'Inside Solid
   FillStyle       =   6  'Cross
   Icon            =   "BuscaProducto.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6435
   ScaleWidth      =   7455
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   480
      OleObjectBlob   =   "BuscaProducto.frx":0442
      Top             =   5760
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "&Buscar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4560
      TabIndex        =   1
      Top             =   720
      Width           =   1332
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   1440
      TabIndex        =   0
      Top             =   630
      Width           =   2775
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00008000&
      ForeColor       =   &H80000008&
      Height          =   2295
      Left            =   600
      ScaleHeight     =   2265
      ScaleWidth      =   6465
      TabIndex        =   4
      Top             =   960
      Width           =   6495
      Begin VB.CommandButton Command3 
         Caption         =   "LIMPIAR"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   4680
         TabIndex        =   33
         Top             =   1200
         Width           =   1335
      End
      Begin VB.CommandButton Command2 
         Caption         =   " "
         Height          =   375
         Left            =   1680
         TabIndex        =   32
         Top             =   1800
         Width           =   3255
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "M"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   26
         Left            =   3960
         TabIndex        =   31
         Top             =   1200
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "N"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   25
         Left            =   3360
         TabIndex        =   30
         Top             =   1200
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "B"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   24
         Left            =   2760
         TabIndex        =   29
         Top             =   1200
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "V"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   23
         Left            =   2160
         TabIndex        =   28
         Top             =   1200
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "C"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   22
         Left            =   1560
         TabIndex        =   27
         Top             =   1200
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "X"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   21
         Left            =   960
         TabIndex        =   26
         Top             =   1200
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "Z"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   20
         Left            =   360
         TabIndex        =   25
         Top             =   1200
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "P"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   19
         Left            =   5520
         TabIndex        =   24
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   18
         Left            =   4920
         TabIndex        =   23
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "I"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   17
         Left            =   4320
         TabIndex        =   22
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "U"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   16
         Left            =   3720
         TabIndex        =   21
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "Y"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   15
         Left            =   3120
         TabIndex        =   20
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "T"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   14
         Left            =   2520
         TabIndex        =   19
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "R"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   13
         Left            =   1920
         TabIndex        =   18
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "E"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   12
         Left            =   1320
         TabIndex        =   17
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "W"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   11
         Left            =   720
         TabIndex        =   16
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "Q"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   10
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "�"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   9
         Left            =   5640
         TabIndex        =   14
         Top             =   720
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "L"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   8
         Left            =   5040
         TabIndex        =   13
         Top             =   720
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "K"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   7
         Left            =   4440
         TabIndex        =   12
         Top             =   720
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "J"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   6
         Left            =   3840
         TabIndex        =   11
         Top             =   720
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "H"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   5
         Left            =   3240
         TabIndex        =   10
         Top             =   720
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "G"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   4
         Left            =   2640
         TabIndex        =   9
         Top             =   720
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "F"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   3
         Left            =   2040
         TabIndex        =   8
         Top             =   720
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "D"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   1440
         TabIndex        =   7
         Top             =   720
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "S"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   840
         TabIndex        =   6
         Top             =   720
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "A"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   240
         TabIndex        =   5
         Top             =   720
         Width           =   615
      End
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHresultado 
      Height          =   2295
      Left            =   600
      TabIndex        =   3
      Top             =   3240
      Visible         =   0   'False
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   4048
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      RowHeightMin    =   330
      ScrollBars      =   2
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   4
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5520
      TabIndex        =   2
      Top             =   5760
      Width           =   1575
   End
End
Attribute VB_Name = "BuscaProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Declare Function SetWindowRgn Lib "user32" (ByVal hwnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long
Private Declare Function CreateEllipticRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long

Option Explicit

Private Sub CancelButton_Click()
    FrmFondo.Enabled = True
    Unload Me
End Sub

Private Sub Command1_Click(Index As Integer)
    Text1.Text = Text1.Text & Command1(Index).Caption
    Text1.SetFocus
End Sub

Private Sub Command2_Click()
    Text1.Text = Text1.Text & " "
End Sub

Private Sub Command3_Click()
    Text1.Text = ""
End Sub

Private Sub Form_Load()
    'FrmFondo.Enabled = False
     Dim Xs As Long, Ys As Long
    Xs = Me.Width / Screen.TwipsPerPixelX
    Ys = Me.Height / Screen.TwipsPerPixelY
    SetWindowRgn hwnd, CreateEllipticRgn(0, 0, Xs, Ys), True
    Aplicar_skin Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'FrmFondo.Enabled = True
    ElMainActivo = True
End Sub

Private Sub MSHresultado_Click()
    Dim ElCodigo As Integer
    ElCodigo = 0
    With MSHresultado
        .Col = 0
        ElCodigo = Val(.Text)
    End With
    If ElCodigo > 0 Then
        Main.EncuentraCodigo (ElCodigo)
        Unload Me
    End If
End Sub

Private Sub OKButton_Click()
    Dim b As Integer, j As Integer
    Dim LaPos As Long
    Dim CadBusc As String
    Dim Cuenta As Integer
    Dim BusCod As Integer
    Dim BusProd As String
    Dim BusValor As Long
    Dim BusRubro As String
    MSHresultado.Clear
    With MSHresultado
        .Row = 0
        .Col = 0
        .Text = "Codigo"
        .ColWidth(0) = 750
        .Col = 1
        .Text = "Producto"
        .ColWidth(1) = 2900
        .Col = 2
        .Text = "Precio"
        .ColWidth(2) = "750"
        .Col = 3
        .Text = "Rubro"
        .ColWidth(3) = "1700"
    End With
    Cuenta = 1
    CadBusc = Text1.Text
    MSHresultado.Visible = True
    For b = 0 To Main.CMDrubros.Count - 1
        For j = 0 To Main.LTxRubro(b).Rows - 1
            Main.LTxRubro(b).Col = 0
            Main.LTxRubro(b).Row = j
            BusCod = Main.LTxRubro(b).Text
            Main.LTxRubro(b).Col = 1
            BusProd = Main.LTxRubro(b).Text
            Main.LTxRubro(b).Col = 2
            BusValor = Main.LTxRubro(b).Text
            Main.LTxRubro(b).Col = 3
            BusRubro = Main.CMDrubros(b).Caption
            LaPos = InStr(1, BusProd, CadBusc, vbTextCompare)
            If LaPos > 0 Then
                With MSHresultado
                    .Row = Cuenta
                    Cuenta = Cuenta + 1
                    .Col = 0
                    .Text = BusCod
                    .Col = 1
                    .Text = BusProd
                    .Col = 2
                    .Text = BusValor
                    .Col = 3
                    .Text = BusRubro
                    .Rows = Cuenta + 1
                End With
            End If
        Next
    Next
encontrado:
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then OKButton_Click
End Sub
