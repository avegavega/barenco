VERSION 5.00
Begin VB.Form TurnoActual 
   BackColor       =   &H000040C0&
   BorderStyle     =   0  'None
   Caption         =   "Informes del Turno"
   ClientHeight    =   4245
   ClientLeft      =   810
   ClientTop       =   2805
   ClientWidth     =   5010
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4245
   ScaleWidth      =   5010
   Begin VB.CommandButton CmdTurno 
      BackColor       =   &H0080FF80&
      Caption         =   "C&ancelar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   2
      Left            =   1320
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   3240
      Width           =   2412
   End
   Begin VB.CommandButton CmdTurno 
      BackColor       =   &H0080FF80&
      Caption         =   "&Cerrar Turno"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   1
      Left            =   840
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2280
      Width           =   3372
   End
   Begin VB.CommandButton CmdTurno 
      BackColor       =   &H00FFFF80&
      Caption         =   "&Hoja de Informes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   720
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1440
      Width           =   3612
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00000080&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Acci�n del Turno"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   495
      Left            =   1000
      TabIndex        =   3
      Top             =   600
      Width           =   3015
   End
   Begin VB.Shape Shape2 
      BorderStyle     =   4  'Dash-Dot
      BorderWidth     =   2
      DrawMode        =   15  'Merge Pen Not
      FillStyle       =   5  'Downward Diagonal
      Height          =   3975
      Left            =   120
      Shape           =   2  'Oval
      Top             =   120
      Width           =   4815
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H8000000D&
      BackStyle       =   1  'Opaque
      BorderStyle     =   0  'Transparent
      BorderWidth     =   2
      Height          =   1215
      Left            =   0
      Top             =   0
      Width           =   5055
   End
End
Attribute VB_Name = "TurnoActual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Declare Function SetWindowRgn Lib "user32" (ByVal hwnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long
Private Declare Function CreateEllipticRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long

Private Sub CmdTurno_Click(Index As Integer)
    If Index = 0 Then
        Informes.Show 1
        'FrmFondo.Enabled = False
        TurnoActual.Visible = False
    End If
    If Index = 1 Then
        CierreTurno.Show 1
    End If
    If Index = 2 Then
        '.FrmFondo.Enabled = True
        TurnoActual.Visible = False
        ElMainActivo = True
        Main.Show
    End If
End Sub
Private Sub CmdTurno_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    For i = 0 To 2
        CmdTurno(i).BackColor = &H80FF80
        CmdTurno(i).FontSize = 18
        CmdTurno(Index).FontBold = False
    Next
    CmdTurno(Index).BackColor = &HFFFF80
    CmdTurno(Index).FontSize = 20
    CmdTurno(Index).FontBold = True
End Sub

Private Sub Form_Load()
Dim Xs As Long, Ys As Long
Xs = Me.Width / Screen.TwipsPerPixelX
Ys = Me.Height / Screen.TwipsPerPixelY
SetWindowRgn hwnd, CreateEllipticRgn(0, 0, Xs, Ys), True4040

End Sub
