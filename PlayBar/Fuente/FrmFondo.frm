VERSION 5.00
Begin VB.MDIForm FrmFondo 
   Appearance      =   0  'Flat
   BackColor       =   &H80000006&
   Caption         =   "Caferia Premium"
   ClientHeight    =   3255
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5310
   Icon            =   "FrmFondo.frx":0000
   LinkTopic       =   "MDIForm1"
   MouseIcon       =   "FrmFondo.frx":0442
   Moveable        =   0   'False
   ScrollBars      =   0   'False
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
End
Attribute VB_Name = "FrmFondo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub MDIForm_Load()
    Me.Caption = App.ProductName
    Unload Acceso
    Unload DImpresoras
    Main.Show
End Sub

Private Sub MDIForm_Terminate()
'seguro = MsgBox("� Est� de seguro que desea salir del sistema ? ...", vbExclamation + vbOKCancel)
'  If seguro = 1 Then End
    Unload ListaGarzones
   Load FrmFondo
    FrmFondo.Show
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
    Unload ListaGarzones
    Unload AccionMesa
    Unload FormasDePago
    Unload Pago
    Acceso.Show
End Sub
