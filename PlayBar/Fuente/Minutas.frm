VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Minutas 
   Caption         =   "Receta de Productos"
   ClientHeight    =   7875
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11760
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7875
   ScaleWidth      =   11760
   WindowState     =   2  'Maximized
   Begin VB.CommandButton CmdUnload 
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9000
      TabIndex        =   23
      Top             =   6600
      Width           =   1695
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C000&
      Caption         =   "Insumos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6015
      Index           =   1
      Left            =   8040
      TabIndex        =   15
      Top             =   360
      Width           =   3735
      Begin VB.ComboBox ComboInsumos 
         Height          =   315
         ItemData        =   "Minutas.frx":0000
         Left            =   600
         List            =   "Minutas.frx":0002
         TabIndex        =   21
         Text            =   "Busca Insumo"
         Top             =   960
         Width           =   2535
      End
      Begin VB.ComboBox ComboNoVeo 
         Height          =   315
         Left            =   480
         TabIndex        =   19
         Text            =   "Combo1"
         Top             =   600
         Width           =   2535
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHinsumos 
         Height          =   3495
         Left            =   120
         TabIndex        =   17
         Top             =   2280
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   6165
         _Version        =   393216
         Cols            =   5
         FixedCols       =   0
         HighLight       =   2
         ScrollBars      =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   5
      End
      Begin VB.CommandButton CmdAgregaInsumo 
         Caption         =   "<-- Agrega insumo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   600
         TabIndex        =   16
         Top             =   1320
         Width           =   2535
      End
      Begin MSAdodcLib.Adodc AdoInsumos 
         Height          =   330
         Left            =   120
         Top             =   360
         Visible         =   0   'False
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   3
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "DSN=Euro"
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "Euro"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   "Select codigo,insumo,umedida,precio, familia from losinsumos order by codigo"
         Caption         =   "Adodc1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.Label Label4 
         Caption         =   "Label4"
         DataField       =   "insumo"
         DataSource      =   "AdoInsumos"
         Height          =   135
         Left            =   1800
         TabIndex        =   20
         Top             =   480
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "(Tambi�n puede hacer doble click en la lista)"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   2040
         Width           =   3375
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00000000&
      Caption         =   "Minuta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5535
      Index           =   0
      Left            =   3120
      TabIndex        =   3
      Top             =   120
      Width           =   4935
      Begin VB.ListBox ListaMinuta 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2700
         ItemData        =   "Minutas.frx":0004
         Left            =   120
         List            =   "Minutas.frx":0006
         TabIndex        =   13
         Top             =   2280
         Width           =   4695
      End
      Begin VB.CommandButton CmdGrabaMinuta 
         BackColor       =   &H00004040&
         Height          =   495
         Left            =   2640
         Picture         =   "Minutas.frx":0008
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Graba minuta"
         Top             =   1440
         Width           =   1455
      End
      Begin VB.CommandButton CmdSacaInsumo 
         BackColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   360
         Picture         =   "Minutas.frx":044A
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Saca insumo seleccionado"
         Top             =   1560
         Width           =   495
      End
      Begin VB.Label LbTminuta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Valor de esta minuta:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   22
         Top             =   5040
         Width           =   3855
      End
      Begin VB.Label Label2 
         Caption         =   "Cod.    Insumo       Um Cant Valor"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   2040
         Width           =   4695
      End
      Begin VB.Label LbProducto 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   810
         Left            =   1320
         TabIndex        =   6
         Top             =   600
         Width           =   3375
      End
      Begin VB.Label LbCodigo 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   120
         TabIndex        =   5
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0C000&
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo              Producto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   3495
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C000&
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7095
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   3015
      Begin VB.ComboBox CmboInvisible 
         Height          =   315
         Left            =   360
         TabIndex        =   11
         Text            =   "invisible"
         Top             =   480
         Visible         =   0   'False
         Width           =   2175
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHProductos 
         Height          =   4215
         Left            =   120
         TabIndex        =   9
         Top             =   1680
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   7435
         _Version        =   393216
         Rows            =   3
         FixedRows       =   0
         FixedCols       =   0
         HighLight       =   2
         FillStyle       =   1
         ScrollBars      =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Baltic"
            Size            =   8.25
            Charset         =   186
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.ComboBox CmboProductos 
         Height          =   315
         ItemData        =   "Minutas.frx":088C
         Left            =   360
         List            =   "Minutas.frx":088E
         TabIndex        =   2
         Text            =   "Busca producto"
         Top             =   720
         Width           =   2175
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Seleccionar  ----->"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   600
         TabIndex        =   1
         Top             =   1080
         Width           =   1935
      End
      Begin VB.Label Label10 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00808000&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Haga doble click en el producto o bien b�squelo escribiendo las primeras letras y luego presione ""Seleccionar-->""."
         ForeColor       =   &H00FFFFFF&
         Height          =   855
         Index           =   0
         Left            =   240
         TabIndex        =   10
         Top             =   6120
         Width           =   2655
      End
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   $"Minutas.frx":0890
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1455
      Index           =   1
      Left            =   4080
      TabIndex        =   12
      Top             =   5880
      Width           =   3735
   End
End
Attribute VB_Name = "Minutas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Respusta As Double
Dim ElInsumo As String * 20
Dim CodInsumo As String * 5
Dim UMInsumo As String * 2
Dim CantInsumo As String * 5
Dim CostoInsumo As String * 7
Dim FamiliaInsumo As String * 15

Private Sub CmdAgregaInsumo_Click()
    Dim PrecioInsumo As Double, Respuesta As Variant
    
    If ComboInsumos.ListIndex > -1 Then
        With MSHinsumos
            For Respuesta = 0 To .Rows - 2
                .Row = Respuesta + 1: .Col = 0
                If Val(ComboNoVeo.List(ComboInsumos.ListIndex)) = _
                   Val(.Text) Then
                    CodInsumo = .Text: .Col = 1
                    ElInsumo = .Text: .Col = 2
                    UMInsumo = .Text: .Col = 3
                    PrecioInsumo = .Text: .Col = 4
                    FamiliaInsumo = .Text
                    Exit For
                End If
            Next
                
            Respuesta = InputBox(ElInsumo & Chr(13) & _
                            "Ingrese la cantidad en " & UMInsumo)
            If Val(Respuesta) > 0 Then
                CantInsumo = Respuesta
                CostoInsumo = Val(Respuesta) * PrecioInsumo
                ListaMinuta.AddItem (CodInsumo & " " & ElInsumo & " " & UMInsumo & " " & CantInsumo & " " & CostoInsumo & " " & FamiliaInsumo)
            End If
            CalculaMinuta
        End With
    End If
        
End Sub

Private Sub CmdGrabaMinuta_Click()
    If ListaMinuta.ListCount = 0 Then Exit Sub
    GrabaMinuta
End Sub

Private Sub CmdSacaInsumo_Click()
    If ListaMinuta.ListIndex > -1 Then
        ListaMinuta.RemoveItem (ListaMinuta.ListIndex)
        ListaMinuta.Refresh
        CalculaMinuta
    End If
End Sub

Private Sub CmdUnload_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
    If CmboProductos.ListIndex > -1 Then
        ListaMinuta.Clear
        LbCodigo.Caption = CmboInvisible.List(CmboProductos.ListIndex)
        LbProducto.Caption = CmboProductos.List(CmboProductos.ListIndex)
        MuestraMinuta (LbCodigo.Caption)
    End If
End Sub

Private Sub Command12_Click()
    Dim NuevaFam As String * 15 '' Agrega familia
    F = FreeFile
    NuevaFam = InputBox("Nombre de la nueva familia", "Nueva Familia")
    If Len(Trim(NuevaFam)) > 0 Then
        If ListaFamily.ListCount = 1 And Len(ListaFamily.List(0)) = 0 Then
            ListaFamily.List(0) = NuevaFam
        Else
            ListaFamily.AddItem (NuevaFam)
        End If
    End If
    LargoR = Len(rFamilia)
    Kill LaRutadeArchivos & "familia.txt"
    Open LaRutadeArchivos & "familia.txt" For Random As #F Len = LargoR
    For i = 0 To ListaFamily.ListCount - 1
        rFamilia.NomFamilia = Mid(ListaFamily.List(i), 1, 15)
        Put #F, , rFamilia
    Next
    Close #F
        
        
End Sub



Private Sub Form_Load()
    Contador = 0
    With MSHProductos
        .ColWidth(0) = 700
        .ColWidth(1) = 2400
        .Col = 1
        For i = 0 To Main.CMDrubros.Count - 1
            .Row = Contador: .Text = UCase(Main.CMDrubros(i).Caption)
            '.Rows = .Rows + 1
            For l = 0 To Main.LTxRubro(i).Rows - 1
                .Col = 0: .Row = Contador + 1: Main.LTxRubro(i).Row = l
                Main.LTxRubro(i).Col = 0
                .Text = Main.LTxRubro(i).Text
                CmboInvisible.AddItem (Main.LTxRubro(i).Text)
                Main.LTxRubro(i).Col = 1: .Col = 1
                .Text = Main.LTxRubro(i).Text
                CmboProductos.AddItem (Main.LTxRubro(i).Text)
                Contador = Contador + 1
                .Rows = .Rows + 1
            Next
            Contador = Contador + 2
            .Rows = .Rows + 2
        Next
    End With
    With MSHinsumos
        .ColWidth(0) = 500
        .ColWidth(1) = 1700
        .ColWidth(2) = 380
        .ColWidth(3) = 550
        .Row = 0: .Col = 0
        .Text = "Cod.": .Col = 1
        .Text = "Insumo": .Col = 2
        .Text = "UM": .Col = 3
        .Text = "Precio": .Row = 1: .Col = 0
        AdoInsumos.Recordset.MoveFirst
        For i = 1 To AdoInsumos.Recordset.RecordCount
            .Text = AdoInsumos.Recordset.Fields(0)
            .Col = 1: .Text = AdoInsumos.Recordset.Fields(1)
            .Col = 2: .Text = AdoInsumos.Recordset.Fields(2)
            .Col = 3: .Text = AdoInsumos.Recordset.Fields(3)
            .Col = 4: .Text = AdoInsumos.Recordset.Fields(4)
            ComboInsumos.AddItem (AdoInsumos.Recordset.Fields(1))
            ComboNoVeo.AddItem (AdoInsumos.Recordset.Fields(0))
            AdoInsumos.Recordset.MoveNext
            .Rows = .Rows + 1
            .Col = 0: .Row = .Row + 1
        Next
        
    End With
    
End Sub


Private Sub MSHinsumos_DblClick()
    Dim Respuesta As Variant
    Dim PrecioInsumo As Double
    With MSHinsumos
        .Col = 0
        CodInsumo = .Text: .Col = 1
        ElInsumo = .Text: .Col = 2
        UMInsumo = .Text: .Col = 3
        PrecioInsumo = .Text: .Col = 4
        FamiliaInsumo = .Text
        If Val(CodInsumo) = 0 Then Exit Sub
        Respuesta = InputBox(ElInsumo & Chr(13) & _
                        "Ingrese la cantidad en " & UMInsumo)
        If Val(Respuesta) > 0 Then
            CantInsumo = Respuesta
            CostoInsumo = Val(Respuesta) * PrecioInsumo
            ListaMinuta.AddItem (CodInsumo & " " & ElInsumo & " " & UMInsumo & " " & CantInsumo & " " & CostoInsumo & " " & FamiliaInsumo)
        End If
        CalculaMinuta
    End With
End Sub

Private Sub MSHProductos_DblClick()
    ListaMinuta.Clear
    With MSHProductos
        .Col = 0
        If Val(.Text) > 0 Then
            LbCodigo.Caption = .Text
            .Col = 1
            LbProducto.Caption = .Text
            MuestraMinuta (LbCodigo.Caption)
        End If
    End With
End Sub

Public Sub CalculaMinuta()
    Dim TotalMinuta As Double, i As Integer
    For i = 0 To ListaMinuta.ListCount - 1
        TotalMinuta = TotalMinuta + Val(Mid(ListaMinuta.List(i), 36, 7))
    Next
    LbTminuta.Caption = "Total de esta Minuta    $" & Format(TotalMinuta, "##,###.##")
End Sub

Public Sub GrabaMinuta()
    Dim LaMinuta As MinutaForma, ind As Integer
    Dim rp As Integer, MinRuta As String
    
    If Val(LbCodigo.Caption) = 0 Then Exit Sub
    rp = MsgBox("Grabar minuta de " & LbProducto.Caption, vbQuestion + vbOKCancel)
    If rp = 1 Then
        MinRuta = LaRutadeArchivos & "Minutas\" & LbCodigo.Caption & ".min"
        With LaMinuta
            X = FreeFile
            LargoR = Len(LaMinuta)
            Open MinRuta For Random As #X Len = LargoR
            If LOF(X) <> 0 Then
                Close #X
                Kill MinRuta
                X = FreeFile
                Open MinRuta For Random As #X Len = LargoR
            End If
            For ind = 0 To ListaMinuta.ListCount - 1
                CodInsumo = Mid(ListaMinuta.List(ind), 1, 5)
                ElInsumo = Mid(ListaMinuta.List(ind), 7, 20)
                UMInsumo = Mid(ListaMinuta.List(ind), 28, 2)
                CantInsumo = Mid(ListaMinuta.List(ind), 31, 5)
                CostoInsumo = Mid(ListaMinuta.List(ind), 36, 7)
                FamiliaInsumo = Right(ListaMinuta.List(ind), 15)
                .MinCodInsumo = CodInsumo
                .MinElInsumo = ElInsumo
                .MinUMInsumo = UMInsumo
                .MinCantInsumo = CantInsumo
                .MinCostoInsumo = CostoInsumo
                .MinFamiliaInsumo = FamiliaInsumo
                Put #X, , LaMinuta
            Next
            Close #X
            MsgBox "La minuta ha sido guardada ... ", vbInformation
            ListaMinuta.Clear
        End With
    End If
End Sub
Public Sub MuestraMinuta(MinCodigo As Integer)
    Dim LaMinuta As MinutaForma, ind As Integer
    Dim rp As Integer, MinRuta As String
    
    MinRuta = LaRutadeArchivos & "Minutas\" & MinCodigo & ".min"
    With LaMinuta
        X = FreeFile
        LargoR = Len(LaMinuta)
        Open MinRuta For Random As #X Len = LargoR
        If LOF(X) = 0 Then
            Close #X
            Exit Sub
        End If
        ListaMinuta.Clear
        For ind = 1 To LOF(X) / LargoR
            Get #X, , LaMinuta
            ListaMinuta.AddItem (.MinCodInsumo & " " & .MinElInsumo & " " & _
                                .MinUMInsumo & " " & .MinCantInsumo & " " & .MinCostoInsumo & " " & .MinFamiliaInsumo)
        Next
        Close #X
    End With
    CalculaMinuta
End Sub
