VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form Supervisor 
   BackColor       =   &H00008000&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Anular Ticket emitido"
   ClientHeight    =   2925
   ClientLeft      =   30
   ClientTop       =   225
   ClientWidth     =   4230
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2925
   ScaleWidth      =   4230
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdCancelar 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      Left            =   2400
      TabIndex        =   2
      Top             =   2280
      Width           =   1692
   End
   Begin VB.CommandButton CmdOk 
      Caption         =   "C&onfirmar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      Left            =   120
      TabIndex        =   1
      Top             =   2280
      Width           =   1692
   End
   Begin VB.TextBox TxtClave 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   492
      IMEMode         =   3  'DISABLE
      Left            =   360
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   1560
      Width           =   3492
   End
   Begin MSAdodcLib.Adodc AdoEliminados 
      Height          =   330
      Left            =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   2040
      _ExtentX        =   3598
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   3
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "DSN=ventaseuro"
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "ventaseuro"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "TEliminados"
      Caption         =   "eliminados"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label5 
      Caption         =   "Label5"
      DataField       =   "Supervisor"
      DataSource      =   "AdoEliminados"
      Height          =   255
      Left            =   2040
      TabIndex        =   8
      Top             =   0
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Valor    $"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   375
      Left            =   240
      TabIndex        =   7
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Ticket N�"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   375
      Left            =   240
      TabIndex        =   6
      Top             =   720
      Width           =   1215
   End
   Begin VB.Label LbValor 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   1560
      TabIndex        =   5
      Top             =   1080
      Width           =   1935
   End
   Begin VB.Label LbN 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   1560
      TabIndex        =   4
      Top             =   720
      Width           =   2055
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Clave Supervisora"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   16.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   600
      TabIndex        =   3
      Top             =   120
      Width           =   3012
   End
End
Attribute VB_Name = "Supervisor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdCancelar_Click()
    Informes.Enabled = True
    Unload Me
End Sub

Private Sub cmdOK_Click()
    Dim svTicket As Long
    Dim Permiso As String
    Dim Myticket As String
    Dim TickPedido As ADODB.Recordset
    
    If TxtClave <> Security.Text3.Text And _
    TxtClave <> Security.Text5.Text And _
    TxtClave <> Security.Text6.Text Then
        paso = MsgBox("Clave supervisora incorrecta" & Chr(13) & "   ��    I N T R U S O    !!", vbCritical)
        Informes.Enabled = True
        Unload Me
        Exit Sub
    End If
    If TxtClave = Security.Text3.Text Then Permiso = Security.Text7.Text
    If TxtClave = Security.Text5.Text Then Permiso = Security.Text8.Text
    If TxtClave = Security.Text6.Text Then Permiso = Security.Text9.Text
    svTicket = LbN.Caption
    Myticket = svTicket
    If Main.LBlevelControl.Caption = 1 Then
       
       With Pago.AdoTickets.Recordset
            .MoveFirst
            Criterio = "NTicket = " & svTicket
            .Find Criterio
            If .EOF Then
               ' MsgBox "fin de archivo"
            Else
                '.Fields(2) = 0 'Valor
                .Fields(3) = "Nulo_"
                .Fields(4) = "Ticket Anulado"
                .Fields(7) = "Nulo" 'Tipo documento
                .Fields(8) = 0 ' N�documento
                
                .Fields(10) = 0 'comestible
                .Fields(11) = 0  ' bebestible
                .Fields(12) = 0 'Personas
                .MoveFirst
            End If
        End With
    
    Else
        
        'With Main.AdoVales.Recordset
        '    .MoveFirst
        '    Criterio = "NTicket = " & svTicket
        '    .Find Criterio
        '    If .EOF Then
        '       ' MsgBox "fin de archivo"
        '    Else
        '        .Fields(2) = 0
        '        .Fields(3) = "Nulo"
        '        .Fields(4) = "Ticket Anulado"
        '        .Fields(8) = 0
        '        .Fields(9) = 0
        '        .Update
        '    End If
        'End With
    End If
    With AdoEliminados.Recordset
        .AddNew
        .Fields(0) = svTicket
        .Fields(1) = Permiso
        .Fields(2) = LbValor.Caption
        .Fields(3) = Main.TurnoTurno.Caption
        .Fields(4) = Main.TurnoFecha.Caption
        .Update
    End With
    Set TickPedido = Main.FiltroCampo(Main.AdoPedidos.Recordset, "NPedido", Myticket)
    With TickPedido
        For i = 1 To .RecordCount
            .Fields(2) = "Nulo"
            .Fields(3) = 0
            .Fields(4) = 0
            .Fields(5) = 0
            .Fields(7) = 0
            .Fields(8) = 0
            .Fields(9) = 0
            .Fields(10) = 0
            .Fields(11) = 0
            .MoveNext
        Next
    End With
    Unload Informes
    
    With Main.AdoPedidos.Recordset
    
    End With
    
    FrmFondo.Enabled = True
    Unload Supervisor
End Sub
