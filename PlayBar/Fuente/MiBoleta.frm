VERSION 5.00
Begin VB.Form MiBoleta 
   Caption         =   "Configurar impresión de boleta"
   ClientHeight    =   6960
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4725
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   6960
   ScaleWidth      =   4725
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "&Cerrar"
      Height          =   375
      Left            =   3360
      TabIndex        =   14
      Top             =   6360
      Width           =   975
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   4815
      Left            =   960
      TabIndex        =   7
      Top             =   600
      Width           =   3615
      Begin VB.Label LbPtotal 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "TOTAL"
         DragMode        =   1  'Automatic
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   720
         TabIndex        =   10
         Top             =   2640
         Width           =   1815
      End
      Begin VB.Label LbPDetalle 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Detalle"
         DragMode        =   1  'Automatic
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   360
         TabIndex        =   9
         Top             =   2040
         Width           =   2895
      End
      Begin VB.Label LbPfecha 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         DragMode        =   1  'Automatic
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   720
         TabIndex        =   8
         Top             =   1560
         Width           =   1935
      End
      Begin VB.Image Image1 
         Height          =   975
         Left            =   720
         Picture         =   "MiBoleta.frx":0000
         Stretch         =   -1  'True
         Top             =   120
         Width           =   1695
      End
      Begin VB.Line Line1 
         Index           =   0
         X1              =   0
         X2              =   3600
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line2 
         Index           =   0
         X1              =   0
         X2              =   0
         Y1              =   4800
         Y2              =   0
      End
      Begin VB.Line Line3 
         Index           =   0
         X1              =   720
         X2              =   720
         Y1              =   4800
         Y2              =   0
      End
      Begin VB.Line Line3 
         Index           =   1
         X1              =   1440
         X2              =   1440
         Y1              =   4800
         Y2              =   0
      End
      Begin VB.Line Line2 
         Index           =   1
         X1              =   2160
         X2              =   2160
         Y1              =   4800
         Y2              =   0
      End
      Begin VB.Line Line3 
         Index           =   2
         X1              =   2880
         X2              =   2880
         Y1              =   4800
         Y2              =   0
      End
      Begin VB.Line Line3 
         Index           =   3
         X1              =   3600
         X2              =   3600
         Y1              =   4800
         Y2              =   0
      End
      Begin VB.Line Line1 
         Index           =   2
         X1              =   0
         X2              =   3600
         Y1              =   1920
         Y2              =   1920
      End
      Begin VB.Line Line1 
         Index           =   3
         X1              =   0
         X2              =   3600
         Y1              =   1440
         Y2              =   1440
      End
      Begin VB.Line Line1 
         Index           =   4
         X1              =   0
         X2              =   3600
         Y1              =   960
         Y2              =   960
      End
      Begin VB.Line Line1 
         Index           =   5
         X1              =   0
         X2              =   3600
         Y1              =   480
         Y2              =   480
      End
      Begin VB.Line Line1 
         Index           =   6
         X1              =   0
         X2              =   3600
         Y1              =   3840
         Y2              =   3840
      End
      Begin VB.Line Line1 
         Index           =   7
         X1              =   0
         X2              =   3600
         Y1              =   3360
         Y2              =   3360
      End
      Begin VB.Line Line1 
         Index           =   8
         X1              =   0
         X2              =   3600
         Y1              =   2880
         Y2              =   2880
      End
      Begin VB.Line Line1 
         Index           =   9
         X1              =   0
         X2              =   3600
         Y1              =   2400
         Y2              =   2400
      End
      Begin VB.Line Line1 
         Index           =   11
         X1              =   0
         X2              =   3600
         Y1              =   4800
         Y2              =   4800
      End
      Begin VB.Line Line1 
         Index           =   12
         X1              =   0
         X2              =   3600
         Y1              =   4320
         Y2              =   4320
      End
      Begin VB.Line Line1 
         Index           =   13
         X1              =   0
         X2              =   3600
         Y1              =   3840
         Y2              =   3840
      End
   End
   Begin VB.CommandButton CmdPosiciones 
      Caption         =   "Guardar Posiciones"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   6
      Top             =   6240
      Width           =   2655
   End
   Begin VB.TextBox txtCitem 
      Height          =   375
      Left            =   3360
      TabIndex        =   5
      Top             =   5640
      Width           =   855
   End
   Begin VB.TextBox TxtAncho 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1200
      TabIndex        =   3
      Top             =   0
      Width           =   375
   End
   Begin VB.TextBox TxtLargo 
      Alignment       =   1  'Right Justify
      Height          =   405
      Left            =   0
      TabIndex        =   0
      Top             =   1320
      Width           =   375
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Label7"
      Height          =   255
      Left            =   480
      TabIndex        =   13
      Top             =   4800
      Width           =   495
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Label6"
      Height          =   255
      Left            =   480
      TabIndex        =   12
      Top             =   2880
      Width           =   495
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Label5"
      Height          =   255
      Left            =   480
      TabIndex        =   11
      Top             =   960
      Width           =   495
   End
   Begin VB.Label Label2 
      Caption         =   "CANTIDAD DE ITEMS POR BOLETA"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   1
      Left            =   1320
      TabIndex        =   4
      Top             =   5520
      Width           =   2295
   End
   Begin VB.Label Label2 
      Caption         =   "A N C H O"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   2
      Top             =   0
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "L A R G O"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   165
   End
End
Attribute VB_Name = "MiBoleta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdPosiciones_Click()
    Dim X As Integer
    X = FreeFile
    Open LaRutadeArchivos & "Boleta.Pos" For Output As X
        Print #X, TxtAncho.Text
        Print #X, TxtLargo.Text
        Print #X, LbPfecha.Left / 567
        Print #X, (LbPfecha.Top / 567) * Label5.Caption
        Print #X, (LbPDetalle.Left / 567)
        Print #X, (LbPDetalle.Top / 567) * Label5.Caption
        Print #X, (LbPtotal.Left / 567)
        Print #X, (LbPtotal.Top / 567) * Label5.Caption
        Print #X, txtCitem.Text
    Close #X
End Sub


Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Dim Q As Integer, Ps As String
    Q = FreeFile
    Open LaRutadeArchivos & "Boleta.Pos" For Input As Q
        Line Input #Q, Ps '
        TxtAncho.Text = Ps
        Line Input #Q, Ps
        TxtLargo.Text = Ps
        Line Input #Q, Ps
        LbPfecha.Left = Val(Ps) * 567
        Line Input #Q, Ps
        LbPfecha.Top = Val(Ps) * 567
        Line Input #Q, Ps
        LbPDetalle.Left = Val(Ps) * 567
        Line Input #Q, Ps
        LbPDetalle.Top = Val(Ps) * 567
        Line Input #Q, Ps
        LbPtotal.Left = Val(Ps) * 567
        Line Input #Q, Ps
        LbPtotal.Top = Val(Ps) * 567
        Line Input #Q, Ps
        txtCitem.Text = Ps
    Close #Q
End Sub

Private Sub Frame1_DragDrop(Source As Control, X As Single, Y As Single)
    Source.Left = X
    Source.Top = Y
End Sub

Private Sub TxtLargo_Change()
    If Val(TxtLargo.Text) > 0 Then
        Label5.Caption = TxtLargo.Text / 10
        Label6.Caption = Label5.Caption * 5
        Label7.Caption = Label5.Caption * 9
    End If
End Sub
