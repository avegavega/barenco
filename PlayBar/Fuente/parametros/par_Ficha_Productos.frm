VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form par_Ficha_Productos 
   Caption         =   "FICHA PRODUCTO"
   ClientHeight    =   9885
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   14475
   LinkTopic       =   "Form1"
   ScaleHeight     =   9885
   ScaleWidth      =   14475
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   300
      Left            =   0
      Top             =   720
   End
   Begin VB.CommandButton cmdCierra 
      Caption         =   "&Cerrar Ventana"
      Height          =   405
      Left            =   13200
      TabIndex        =   9
      Top             =   8760
      Width           =   1470
   End
   Begin VB.Frame Frame 
      Caption         =   "FICHA"
      Height          =   9105
      Index           =   0
      Left            =   375
      TabIndex        =   8
      Top             =   240
      Width           =   14700
      Begin VB.Frame Frame 
         Caption         =   "Alternativas"
         Height          =   2730
         Index           =   2
         Left            =   450
         TabIndex        =   37
         Top             =   4575
         Width           =   4770
         Begin VB.CommandButton cmdEditarAlternativa 
            Caption         =   "&Editar"
            Height          =   285
            Left            =   3630
            TabIndex        =   40
            Top             =   2295
            Width           =   840
         End
         Begin VB.CommandButton cmdNewAlternativa 
            Caption         =   "&Nuevo"
            Height          =   285
            Left            =   2745
            TabIndex        =   39
            Top             =   2295
            Width           =   840
         End
         Begin MSComctlLib.ListView lvProdAlternativas 
            Height          =   1890
            Left            =   165
            TabIndex        =   38
            Top             =   330
            Width           =   4335
            _ExtentX        =   7646
            _ExtentY        =   3334
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Id"
               Object.Width           =   706
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Nombre Alternativa"
               Object.Width           =   4762
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Precio"
               Object.Width           =   1411
            EndProperty
         End
      End
      Begin VB.ComboBox comActivo 
         Height          =   315
         ItemData        =   "par_Ficha_Productos.frx":0000
         Left            =   1920
         List            =   "par_Ficha_Productos.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   35
         Top             =   3960
         Width           =   3285
      End
      Begin VB.CommandButton cmdGraba 
         Caption         =   "&Grabar Producto"
         Height          =   510
         Left            =   360
         TabIndex        =   7
         Top             =   8400
         Width           =   2535
      End
      Begin VB.TextBox txtPrecioVenta 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000009&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   6
         Top             =   3525
         Width           =   3300
      End
      Begin VB.TextBox txtDescripcion 
         Height          =   285
         Left            =   1920
         TabIndex        =   5
         Top             =   3090
         Width           =   3300
      End
      Begin TabDlg.SSTab SSTabInfo 
         Height          =   7665
         Left            =   5685
         TabIndex        =   17
         Top             =   600
         Width           =   8685
         _ExtentX        =   15319
         _ExtentY        =   13520
         _Version        =   393216
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "Receta"
         TabPicture(0)   =   "par_Ficha_Productos.frx":0016
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Frame(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Historial"
         TabPicture(1)   =   "par_Ficha_Productos.frx":0032
         Tab(1).ControlEnabled=   0   'False
         Tab(1).ControlCount=   0
         Begin VB.Frame Frame 
            Height          =   6825
            Index           =   1
            Left            =   120
            TabIndex        =   20
            Top             =   570
            Width           =   8400
            Begin ACTIVESKINLibCtl.SkinLabel SkRF2 
               Height          =   255
               Left            =   360
               OleObjectBlob   =   "par_Ficha_Productos.frx":004E
               TabIndex        =   34
               Top             =   6480
               Visible         =   0   'False
               Width           =   3015
            End
            Begin VB.CommandButton cmdOkReceta 
               Caption         =   "Ok"
               Height          =   285
               Left            =   7920
               TabIndex        =   32
               Top             =   600
               Width           =   375
            End
            Begin MSComctlLib.ListView LvReceta 
               Height          =   5415
               Left            =   360
               TabIndex        =   31
               Top             =   960
               Width           =   7935
               _ExtentX        =   13996
               _ExtentY        =   9551
               View            =   3
               LabelWrap       =   -1  'True
               HideSelection   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   1
               NumItems        =   6
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "Nro"
                  Object.Width           =   0
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Text            =   "Codigo"
                  Object.Width           =   1852
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Text            =   "Descripcion"
                  Object.Width           =   5733
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   3
                  Text            =   "Precio Un."
                  Object.Width           =   1887
               EndProperty
               BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   4
                  Text            =   "Cant."
                  Object.Width           =   1887
               EndProperty
               BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   5
                  Text            =   "SubTotal"
                  Object.Width           =   1887
               EndProperty
            End
            Begin VB.TextBox txtRecetaSubTotal 
               Alignment       =   1  'Right Justify
               BackColor       =   &H8000000F&
               Height          =   285
               Left            =   6840
               Locked          =   -1  'True
               TabIndex        =   30
               TabStop         =   0   'False
               Top             =   600
               Width           =   1095
            End
            Begin VB.TextBox txtRecetaCantidad 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   5760
               TabIndex        =   27
               Top             =   600
               Width           =   1095
            End
            Begin VB.TextBox TxtRecetaPrecioU 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   4680
               Locked          =   -1  'True
               TabIndex        =   29
               TabStop         =   0   'False
               Top             =   600
               Width           =   1095
            End
            Begin VB.TextBox txtRecetaDescripcion 
               BackColor       =   &H8000000F&
               Height          =   285
               Left            =   1440
               Locked          =   -1  'True
               TabIndex        =   28
               TabStop         =   0   'False
               Top             =   600
               Width           =   3300
            End
            Begin VB.TextBox TxtRecetaCodigo 
               Height          =   285
               Left            =   360
               TabIndex        =   26
               Top             =   600
               Width           =   1095
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
               Height          =   255
               Left            =   1440
               OleObjectBlob   =   "par_Ficha_Productos.frx":00EC
               TabIndex        =   22
               Top             =   360
               Width           =   2175
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
               Height          =   255
               Left            =   360
               OleObjectBlob   =   "par_Ficha_Productos.frx":0160
               TabIndex        =   21
               Top             =   360
               Width           =   735
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
               Height          =   255
               Left            =   5880
               OleObjectBlob   =   "par_Ficha_Productos.frx":01CA
               TabIndex        =   23
               Top             =   360
               Width           =   975
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
               Height          =   255
               Left            =   5640
               OleObjectBlob   =   "par_Ficha_Productos.frx":0238
               TabIndex        =   24
               Top             =   600
               Width           =   1215
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
               Height          =   255
               Left            =   7200
               OleObjectBlob   =   "par_Ficha_Productos.frx":02AE
               TabIndex        =   25
               Top             =   360
               Width           =   735
            End
            Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
               Height          =   255
               Left            =   4800
               OleObjectBlob   =   "par_Ficha_Productos.frx":031C
               TabIndex        =   33
               Top             =   360
               Width           =   975
            End
         End
      End
      Begin VB.ComboBox comCentro 
         Height          =   315
         Left            =   1920
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   2625
         Width           =   3285
      End
      Begin VB.ComboBox ComDeptos 
         Height          =   315
         Left            =   1920
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   1395
         Width           =   3285
      End
      Begin VB.ComboBox ComLineas 
         Height          =   315
         Left            =   1920
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1800
         Width           =   3285
      End
      Begin VB.ComboBox ComTpr 
         Height          =   315
         Left            =   1920
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   1020
         Width           =   3285
      End
      Begin VB.ComboBox ComRubro 
         Height          =   315
         Left            =   1920
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   2205
         Width           =   3285
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   210
         Index           =   1
         Left            =   315
         OleObjectBlob   =   "par_Ficha_Productos.frx":038E
         TabIndex        =   11
         Top             =   645
         Width           =   1470
      End
      Begin VB.TextBox txtCodigo 
         Height          =   285
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Text            =   "(autom�tico)"
         Top             =   630
         Width           =   1470
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   0
         Left            =   435
         OleObjectBlob   =   "par_Ficha_Productos.frx":03F8
         TabIndex        =   12
         Top             =   1440
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   2
         Left            =   450
         OleObjectBlob   =   "par_Ficha_Productos.frx":046E
         TabIndex        =   13
         Top             =   1830
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   3
         Left            =   450
         OleObjectBlob   =   "par_Ficha_Productos.frx":04F0
         TabIndex        =   14
         Top             =   1050
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   4
         Left            =   450
         OleObjectBlob   =   "par_Ficha_Productos.frx":0568
         TabIndex        =   15
         Top             =   2235
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   5
         Left            =   435
         OleObjectBlob   =   "par_Ficha_Productos.frx":05E4
         TabIndex        =   16
         Top             =   2640
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   6
         Left            =   435
         OleObjectBlob   =   "par_Ficha_Productos.frx":0654
         TabIndex        =   18
         Top             =   3105
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   7
         Left            =   450
         OleObjectBlob   =   "par_Ficha_Productos.frx":06C8
         TabIndex        =   19
         Top             =   3525
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   255
         Index           =   8
         Left            =   480
         OleObjectBlob   =   "par_Ficha_Productos.frx":0744
         TabIndex        =   36
         Top             =   3975
         Width           =   1335
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   75
      OleObjectBlob   =   "par_Ficha_Productos.frx":07AE
      Top             =   6195
   End
End
Attribute VB_Name = "par_Ficha_Productos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCierra_Click()


    Unload Me
End Sub

Private Sub cmdEditarAlternativa_Click()
    If lvProdAlternativas.SelectedItem Is Nothing Then Exit Sub
    IM_Id_Alternativa = lvProdAlternativas.SelectedItem.Text
    BM_NuevaAlternativa = False
    OpcionesAlternativas.Show 1
    CargaAlternativa
End Sub

Private Sub cmdGraba_Click()
    If Len(txtDescripcion) < 3 Then
        MsgBox "La descripcion es demasiado corta", vbOKOnly + vbInformation
        txtDescripcion.SetFocus
        Exit Sub
    End If
    If ComTpr.ItemData(ComTpr.ListIndex) = 1 Or ComTpr.ItemData(ComTpr.ListIndex) = 2 Then
        If Val(txtPrecioVenta) < 0 Then
            MsgBox "Debe ingresar un precio de venta v�lido"
            txtPrecioVenta.SetFocus
            Exit Sub
        End If
    End If
        
    If Nuevo_Visor Then
        Sql = "INSERT INTO productos (Descripcion,PrecioVenta,Rubro,Dirigida,dep_id,lin_id,tpr_id,pro_activo) " & _
              "VALUES('" & txtDescripcion & "'," & Val(txtPrecioVenta) & "," & ComRubro.ItemData(ComRubro.ListIndex) & "," & _
              comCentro.ItemData(comCentro.ListIndex) & "," & ComDeptos.ItemData(ComDeptos.ListIndex) & "," & ComLineas.ItemData(ComLineas.ListIndex) & "," & ComTpr.ItemData(ComTpr.ListIndex) & ",'" & ComActivo.Text & "')"
    Else
        Sql = "UPDATE productos SET Descripcion='" & txtDescripcion & "',PrecioVenta=" & txtPrecioVenta & ",Rubro=" & ComRubro.ItemData(ComRubro.ListIndex) & "," & _
              "Dirigida=" & comCentro.ItemData(comCentro.ListIndex) & ",dep_id=" & ComDeptos.ItemData(ComDeptos.ListIndex) & "," & _
              "lin_id=" & ComLineas.ItemData(ComLineas.ListIndex) & ",tpr_id=" & ComTpr.ItemData(ComTpr.ListIndex) & ",pro_activo='" & ComActivo.Text & "' " & _
              " WHERE cod=" & txtCodigo
    End If
          
          
    Call Consulta(Rst_tmp, Sql)
    
    Unload Me
    
    
    
End Sub

Private Sub cmdNewAlternativa_Click()
    If Val(txtCodigo) = 0 Then Exit Sub
    BM_NuevaAlternativa = True
    OpcionesAlternativas.Show 1
    CargaAlternativa
End Sub

Private Sub cmdOkReceta_Click()
    Set itmx = LvReceta.ListItems.Add(, , LvReceta.ListItems.Count + 1)
    With LvReceta.ListItems
        .Item((LvReceta.ListItems.Count)).SubItems(1) = TxtRecetaCodigo
        .Item((LvReceta.ListItems.Count)).SubItems(2) = txtRecetaDescripcion
        .Item((LvReceta.ListItems.Count)).SubItems(3) = TxtRecetaPrecioU
        .Item((LvReceta.ListItems.Count)).SubItems(4) = txtRecetaCantidad
        .Item((LvReceta.ListItems.Count)).SubItems(5) = txtRecetaSubTotal
    End With
End Sub

Private Sub ComTpr_Click()
    If ComTpr.ItemData(ComTpr.ListIndex) = 1 Or ComTpr.ItemData(ComTpr.ListIndex) = 4 Then SSTabInfo.TabEnabled(0) = True Else SSTabInfo.TabEnabled(0) = False
    If ComTpr.ItemData(ComTpr.ListIndex) = 1 Or ComTpr.ItemData(ComTpr.ListIndex) = 2 Then
        
        txtPrecioVenta.Enabled = True
        txtPrecioVenta.BackColor = &H80000005

    Else
        
        txtPrecioVenta.Enabled = False
        txtPrecioVenta.BackColor = &H8000000F
        
    End If
End Sub

Private Sub Form_Load()
    ComActivo.ListIndex = 0
    CargaDatos
    If Nuevo_Visor Then
        ComDeptos.ListIndex = 0
        ComLineas.ListIndex = 0
        ComTpr.ListIndex = 0
        ComRubro.ListIndex = 0
        comCentro.ListIndex = 0
        
        
    Else
        txtCodigo = ID_Visor
        
        
        Sql = "SELECT cod,descripcion,precioventa,rubro,dirigida,dep_id,lin_id,tpr_id,pro_activo " & _
                    "FROM productos p " & _
                    "WHERE cod=" & txtCodigo
                    
        Call Consulta(Rst_tmp, Sql)
        
        If Rst_tmp.RecordCount > 0 Then
            With Rst_tmp
                txtCodigo = !Cod
                txtDescripcion = !descripcion
                txtPrecioVenta = !precioventa
                ComRubro.ListIndex = BuscaIndice(ComRubro, !Rubro)
                ComLineas.ListIndex = BuscaIndice(ComLineas, !lin_id)
                ComDeptos.ListIndex = BuscaIndice(ComDeptos, !dep_id)
                ComTpr.ListIndex = BuscaIndice(ComTpr, !tpr_id)
                comCentro.ListIndex = BuscaIndice(comCentro, !dirigida)
                ComActivo.ListIndex = IIf(!pro_Activo = "SI", 0, 1)
            End With
            CargaAlternativa
        End If
        
    End If
End Sub
Private Sub CargaAlternativa()
    Sql = "SELECT pro_alternativa_id,pro_alternativa_nombre,pro_alternativa_p_venta " & _
         "FROM productos_alternativas " & _
         "WHERE  pro_alternativa_activo='SI' and pro_codigo=" & txtCodigo
    Call Consulta(Rst_tmp, Sql)
    LLenar_Grilla Rst_tmp, Me, lvProdAlternativas, False, True, True, False
End Sub



Private Sub CargaDatos()
    


    
    LLenarCombo ComDeptos, "dep_nombre", "dep_id", "par_departamentos", "dep_activo='SI'"
    LLenarCombo ComLineas, "lin_nombre", "lin_id", "par_lineas", "lin_activo='SI'"
    LLenarCombo ComTpr, "trp_nombre", "trp_id", "par_tipos_productos", "trp_activo='SI'"
    LLenarCombo ComRubro, "rub_nombre", "rub_id", "par_rubros_venta", "rub_activo='SI'"
    LLenarCombo ComRubro, "rub_nombre", "rub_id", "par_rubros_venta", "rub_activo='SI'"
    LLenarCombo comCentro, "cen_nombre", "cen_id", "centros_produccion", "cen_activo='SI'"
    
    Aplicar_skin Me
End Sub

Private Sub Timer1_Timer()
    ComTpr.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub txtCodigoAlternativa_GotFocus()
    SkAF2.Visible = True
    txtCodigoAlternativa.SetFocus
End Sub

Private Sub txtCodigoAlternativa_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        Tipo_Producto_Busqueda = 4
        BusquedaProducto.Show 1
        txtCodigoAlternativa = Codigo_Devuelto
       ' Call BuscaCodigo(Val(txtCodigoAlternativa))
        txtCantAlternativa.SetFocus
    End If
End Sub

Private Sub txtCodigoAlternativa_LostFocus()
    SkAF2.Visible = False
End Sub

Private Sub txtCodigoAlternativa_Validate(Cancel As Boolean)
    'Call BuscaCodigo(Val(txtCodigoAlternativa))
End Sub

Private Sub txtDescripcion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub CmdOkAlternativa_Click()
    If Len(txtDescipcionAlternativa) = 0 Or Len(txtCodigoAlternativa) = 0 Then Exit Sub
    For i = 1 To LvAlternativas.ListItems.Count
        If Val(LvAlternativas.ListItems(i).Text) = Val(txtCodigoAlternativa) Then
            MsgBox "Esta ALTERNATIVA ya se encuentra en la lista...", vbOKOnly + vbInformation
            Exit Sub
        End If
    Next
    
    
    
    Set itmx = LvAlternativas.ListItems.Add(, , txtCodigoAlternativa)
    With LvAlternativas.ListItems
        .Item((LvAlternativas.ListItems.Count)).SubItems(1) = txtDescipcionAlternativa
        .Item((LvAlternativas.ListItems.Count)).SubItems(2) = txtCantAlternativa
    End With


End Sub

Private Sub txtPrecioVenta_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub txtRecetaCantidad_Change()
    txtRecetaSubTotal = Val(txtRecetaCantidad) * Val(TxtRecetaPrecioU)
    
End Sub

Private Sub TxtRecetaCodigo_GotFocus()
    SkRF2.Visible = True
    TxtRecetaCodigo.SetFocus
End Sub

Private Sub TxtRecetaCodigo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        Tipo_Producto_Busqueda = 3
        BusquedaProducto.Show 1
        TxtRecetaCodigo = Codigo_Devuelto
        TxtRecetaCodigo.SetFocus
    End If
End Sub

Private Sub TxtRecetaCodigo_LostFocus()
    SkRF2.Visible = False
End Sub

Private Sub TxtRecetaCodigo_Validate(Cancel As Boolean)
    Sql = "SELECT descripcion " & _
          "FROM productos " & _
          "WHERE cod=" & Val(TxtRecetaCodigo)
    Call Consulta(Rst_tmp, Sql)
    If Rst_tmp.RecordCount > 0 Then
        txtRecetaDescripcion = Rst_tmp!descripcion
        Cancel = False
    Else
        MsgBox "Codigo no encontrado ... ", vbOKOnly + vbInformation
        Cancel = True
    End If
End Sub
