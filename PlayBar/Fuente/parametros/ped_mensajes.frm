VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form ped_Mensajes 
   Caption         =   "Mensajes Lineas de Venta"
   ClientHeight    =   5700
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   8130
   LinkTopic       =   "Form1"
   ScaleHeight     =   5700
   ScaleWidth      =   8130
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   660
      Top             =   5910
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   30
      OleObjectBlob   =   "ped_mensajes.frx":0000
      Top             =   6090
   End
   Begin VB.Frame Mensajes 
      Caption         =   "Mensajes Lineas de Venta"
      Height          =   5115
      Left            =   330
      TabIndex        =   0
      Top             =   285
      Width           =   7305
      Begin ACTIVESKINLibCtl.SkinLabel SkID 
         Height          =   375
         Left            =   45
         OleObjectBlob   =   "ped_mensajes.frx":0234
         TabIndex        =   12
         Top             =   1560
         Width           =   540
      End
      Begin VB.CommandButton cmdSalir 
         Caption         =   "&Salir"
         Height          =   330
         Left            =   6015
         TabIndex        =   8
         Top             =   4455
         Width           =   1155
      End
      Begin VB.CommandButton cmdGraba 
         Caption         =   "&Grabar"
         Height          =   330
         Left            =   105
         TabIndex        =   7
         Top             =   4710
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.Frame Frame 
         Height          =   3285
         Left            =   615
         TabIndex        =   3
         Top             =   1005
         Width           =   6570
         Begin VB.ComboBox comActivo 
            Height          =   315
            ItemData        =   "ped_mensajes.frx":0294
            Left            =   5025
            List            =   "ped_mensajes.frx":029E
            Style           =   2  'Dropdown List
            TabIndex        =   9
            Top             =   540
            Width           =   1050
         End
         Begin VB.CommandButton cmdAgregar 
            Caption         =   "Ok"
            Height          =   315
            Left            =   6060
            TabIndex        =   6
            Top             =   510
            Width           =   390
         End
         Begin VB.TextBox txtMensaje 
            Height          =   315
            Left            =   120
            TabIndex        =   5
            Top             =   540
            Width           =   4920
         End
         Begin MSComctlLib.ListView LvMensajes 
            Height          =   2205
            Left            =   90
            TabIndex        =   4
            Top             =   885
            Width           =   6390
            _ExtentX        =   11271
            _ExtentY        =   3889
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Linea"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Mensaje"
               Object.Width           =   6174
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Activo"
               Object.Width           =   14111
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Id mensaje"
               Object.Width           =   2540
            EndProperty
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
            Height          =   285
            Index           =   1
            Left            =   105
            OleObjectBlob   =   "ped_mensajes.frx":02AA
            TabIndex        =   10
            Top             =   300
            Width           =   1230
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
            Height          =   285
            Index           =   2
            Left            =   5025
            OleObjectBlob   =   "ped_mensajes.frx":0316
            TabIndex        =   11
            Top             =   300
            Width           =   1230
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel 
         Height          =   285
         Index           =   0
         Left            =   675
         OleObjectBlob   =   "ped_mensajes.frx":0380
         TabIndex        =   2
         Top             =   600
         Width           =   1230
      End
      Begin VB.ComboBox ComLineaVenta 
         Height          =   315
         Left            =   2265
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   615
         Width           =   3420
      End
   End
End
Attribute VB_Name = "ped_Mensajes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdAgregar_Click()
    If ComLineaVenta.ListIndex = -1 Or Len(txtMensaje) = 0 Then Exit Sub
    '�Set itmx = LvMensajes.ListItems.Add(, , ComLineaVenta.Text)
    '�With LvMensajes.ListItems
    '    .Item((LvMensajes.ListItems.Count)).SubItems(1) = txtMensaje
   ' End With
    If SkID > 0 Then
        Sql = "UPDATE ped_mensajes SET men_detalle='" & txtMensaje & "',men_activo='" & ComActivo.Text & "' " & _
             "WHERE men_id=" & SkID
    Else
        Sql = "INSERT INTO ped_mensajes " & _
                "(rub_id,men_detalle) VALUES(" & ComLineaVenta.ItemData(ComLineaVenta.ListIndex) & ",'" & txtMensaje & "')"
    
    End If
    Call Consulta(Rst_tmp, Sql)
    txtMensaje = ""
    SkID = 0
    CargaMensajes
    txtMensaje.SetFocus
End Sub

Private Sub cmdGraba_Click()
    Dim RubroID As Integer
    If ComLineaVenta.ListIndex = -1 Then Exit Sub
    RubroID = ComLineaVenta.ItemData(ComLineaVenta.ListIndex)
    Sql = "DELETE FROM ped_mensajes " & _
          "WHERE rub_id=" & RubroID
    Call Consulta(Rst_tmp, Sql)
    Sql = "INSERT INTO ped_mensajes " & _
          "(rub_id,men_detalle) VALUES("
          
    For i = 1 To LvMensajes.ListItems.Count
        Sql = Sql & RubroID & ",'" & LvMensajes.ListItems(i).SubItems(1) & "'),("
    Next
    
    Sql = Mid(Sql, 1, Len(Sql) - 2)
    Call Consulta(Rst_tmp, Sql)
    
    
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub




Private Sub ComLineaVenta_Click()
    CargaMensajes
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    ComActivo.ListIndex = 0
    LLenarCombo ComLineaVenta, "rub_nombre", "rub_id", "par_rubros_venta", "rub_activo='SI'"
    
    
End Sub
Private Sub LvMensajes_DblClick()
    If LvMensajes.SelectedItem Is Nothing Then Exit Sub
    With LvMensajes
        SkID = .SelectedItem.SubItems(3)
        txtMensaje = .SelectedItem.SubItems(1)
        ComActivo.ListIndex = IIf(.SelectedItem.SubItems(2) = "SI", 0, 1)
        .ListItems.Remove .SelectedItem.Index
    End With
End Sub

Private Sub Timer1_Timer()
    ComLineaVenta.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub CargaMensajes()
    If ComLineaVenta.ListCount = 0 Then
        LvMensajes.ListItems.Clear
        Exit Sub
    End If
    Sql = "SELECT r.rub_nombre,men_detalle,m.men_activo,m.men_id " & _
          "FROM ped_mensajes m, par_rubros_venta r " & _
          "WHERE m.men_activo='SI' AND m.rub_id=r.rub_id AND m.rub_id=" & ComLineaVenta.ItemData(ComLineaVenta.ListIndex)
    Call Consulta(Rst_tmp, Sql)
    LLenar_Grilla Rst_tmp, Me, LvMensajes, False, True, True, False
    
    
End Sub

Private Sub txtMensaje_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
