VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form par_visor 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   5580
   ClientLeft      =   45
   ClientTop       =   405
   ClientWidth     =   8505
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5580
   ScaleWidth      =   8505
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer 
      Interval        =   30
      Left            =   30
      Top             =   5295
   End
   Begin VB.Frame Frame 
      Height          =   4965
      Index           =   0
      Left            =   255
      TabIndex        =   0
      Top             =   360
      Width           =   8085
      Begin VB.Frame Frame 
         Caption         =   "Acci�n"
         Height          =   870
         Index           =   1
         Left            =   435
         TabIndex        =   2
         Top             =   3765
         Width           =   6915
         Begin VB.CommandButton cmdEdita 
            Caption         =   "&Editar"
            Height          =   405
            Left            =   2535
            TabIndex        =   5
            Top             =   330
            Width           =   1470
         End
         Begin VB.CommandButton cmdNuevo 
            Caption         =   "&Nuevo"
            Height          =   405
            Left            =   930
            TabIndex        =   4
            Top             =   330
            Width           =   1470
         End
         Begin VB.CommandButton cmdCierra 
            Caption         =   "&Cerrar Ventana"
            Height          =   405
            Left            =   5235
            TabIndex        =   3
            Top             =   330
            Width           =   1470
         End
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHVisor 
         Height          =   2970
         Left            =   435
         TabIndex        =   1
         Top             =   435
         Width           =   6930
         _ExtentX        =   12224
         _ExtentY        =   5239
         _Version        =   393216
         FixedCols       =   0
         ScrollBars      =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   7695
      OleObjectBlob   =   "par_visor.frx":0000
      Top             =   4785
   End
End
Attribute VB_Name = "par_visor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCierra_Click()
    Unload Me
End Sub

Private Sub cmdEdita_Click()
    If MSHVisor.Row > 0 Then
        ID_Visor = MSHVisor.TextMatrix(MSHVisor.Row, 0)
        Nuevo_Visor = False
        Me.Enabled = False
        If Me.Caption = "DEPARTAMENTOS" Then
            par_departamentos.Caption = Me.Caption
            par_departamentos.Show 1
        ElseIf par_visor.Caption = "LINEAS" Then
            par_Lineas_venta.Caption = Me.Caption
            par_Lineas_venta.Show 1
        ElseIf par_visor.Caption = "RUBROS" Then
            Par_Rubros.Caption = Me.Caption
            Par_Rubros.Show 1
        End If
        
        Me.Enabled = True
        CargaVisor
    End If
End Sub

Private Sub cmdNuevo_Click()
    Nuevo_Visor = True
    Me.Enabled = False
    If par_visor.Caption = "DEPARTAMENTOS" Then
        par_departamentos.Caption = Me.Caption
        par_departamentos.Show 1
    ElseIf par_visor.Caption = "LINEAS" Then
        par_Lineas_venta.Caption = Me.Caption
        par_Lineas_venta.Show 1
    ElseIf par_visor.Caption = "RUBROS" Then
        Par_Rubros.Caption = Me.Caption
        Par_Rubros.Show 1
    End If
    Me.Enabled = True
    CargaVisor
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    CargaVisor
        
End Sub
Private Sub CargaVisor()
    Call Consulta(Rst_tmp, Sql_Visor)
    Set MSHVisor.DataSource = Rst_tmp
    If Rst_tmp.RecordCount > 0 Then
        MSHVisor.ColWidth(1) = 4000
    End If
End Sub

Private Sub Timer_Timer()
    MSHVisor.SetFocus
    Timer.Enabled = False
End Sub
