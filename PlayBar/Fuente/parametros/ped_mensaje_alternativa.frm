VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form ped_mensaje_alternativa 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Mensajes y Alternativas"
   ClientHeight    =   5460
   ClientLeft      =   45
   ClientTop       =   405
   ClientWidth     =   10995
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5460
   ScaleWidth      =   10995
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdGraba 
      Caption         =   "&Grabar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   5730
      TabIndex        =   1
      Top             =   4755
      Width           =   2175
   End
   Begin ACTIVESKINLibCtl.SkinLabel LbHoraUnica 
      Height          =   555
      Left            =   5475
      OleObjectBlob   =   "ped_mensaje_alternativa.frx":0000
      TabIndex        =   9
      Top             =   4695
      Width           =   1365
   End
   Begin VB.Frame Frame 
      Caption         =   "Mensajes Disponibles"
      Height          =   4305
      Index           =   1
      Left            =   195
      TabIndex        =   7
      Top             =   1050
      Width           =   5265
      Begin MSComctlLib.ListView LvMensajesDisponibles 
         Height          =   3885
         Left            =   180
         TabIndex        =   8
         Top             =   330
         Width           =   4935
         _ExtentX        =   8705
         _ExtentY        =   6853
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FlatScrollBar   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Cod. Mensaje"
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "Mensaje"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "sacar"
            Object.Width           =   2
         EndProperty
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Mensajes Seleccionados"
      Height          =   3480
      Index           =   3
      Left            =   5655
      TabIndex        =   5
      Top             =   1065
      Width           =   5175
      Begin MSComctlLib.ListView LvMensajesSeleccionados 
         Height          =   3000
         Left            =   195
         TabIndex        =   6
         Top             =   300
         Width           =   4785
         _ExtentX        =   8440
         _ExtentY        =   5292
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FlatScrollBar   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Cod. Mensaje"
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T3000"
            Text            =   "Mensaje"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Grabado"
            Object.Width           =   2
         EndProperty
      End
   End
   Begin VB.Timer Timer 
      Interval        =   50
      Left            =   1110
      Top             =   5985
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   225
      OleObjectBlob   =   "ped_mensaje_alternativa.frx":005E
      Top             =   6015
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Cerrar Ventana"
      Height          =   480
      Left            =   9360
      TabIndex        =   2
      Top             =   4815
      Width           =   1440
   End
   Begin VB.Frame Frame 
      Caption         =   "PRODUCTO"
      Height          =   840
      Index           =   0
      Left            =   135
      TabIndex        =   0
      Top             =   120
      Width           =   10740
      Begin ACTIVESKINLibCtl.SkinLabel SkProducto 
         Height          =   360
         Left            =   285
         OleObjectBlob   =   "ped_mensaje_alternativa.frx":0292
         TabIndex        =   3
         Top             =   300
         Width           =   8925
      End
   End
   Begin VB.Label LbRubro 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   2340
      TabIndex        =   4
      Top             =   5760
      Width           =   1260
   End
End
Attribute VB_Name = "ped_mensaje_alternativa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdGraba_Click()
    Dim b_Graba As Boolean
    
    Sql = "DELETE FROM tmp_mensajes WHERE tmp_hora_unica='" & Me.LbHoraUnica & "' AND tmp_mesa=" & Main.TxtMesaSel
    Call Consulta(Rst_tmp, Sql)
    b_Graba = False
    If LvMensajesSeleccionados.ListItems.Count > 0 Then
        Sql = "INSERT INTO tmp_mensajes (tmp_hora_unica,tmp_mesa,tmp_turno,tmp_fecha,men_id,men_hora) VALUES "
        For i = 1 To LvMensajesSeleccionados.ListItems.Count
           '     If LvMensajesSeleccionados.ListItems(i).SubItems(2) = "SI" Then
                    '
           '     Else
                    b_Graba = True
                    Sql = Sql & "('" & LbHoraUnica & "'," & Main.TxtMesaSel & "," & Main.TurnoTurno.Caption & ",'" & Format(Main.TurnoFecha.Caption, "YYYY-MM-DD") & "'," & Val(LvMensajesSeleccionados.ListItems(i).Text) & ",'" & Time & "'),"
           '     End If
        Next
        Sql = Mid(Sql, 1, Len(Sql) - 1)
        
    End If
    If b_Graba Then Call Consulta(Rst_tmp, Sql)
    Unload Me
    
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Me.LbHoraUnica = S_Hora_Unica
    Me.skProducto = S_Men_Producto
    
    Sql = "SELECT t.men_id,m.men_detalle,'SI' " & _
          "FROM tmp_mensajes t,ped_mensajes m " & _
          "WHERE t.men_id=m.men_id " & _
          "AND tmp_hora_unica='" & LbHoraUnica & "' " & _
          "AND tmp_mesa=" & Val(Main.TxtMesaSel)
          
          
    Call Consulta(Rst_tmp, Sql)
    
    If Rst_tmp.RecordCount > 0 Then LLenar_Grilla Rst_tmp, Me, LvMensajesSeleccionados, False, True, True, False
        
    Sql = "SELECT men_id,men_detalle " & _
          "FROM ped_mensajes " & _
          "WHERE men_activo='SI' AND rub_id=" & I_Men_Rubro
    Call Consulta(Rst_tmp, Sql)
    
    If Rst_tmp.RecordCount > 0 Then
        LLenar_Grilla Rst_tmp, Me, LvMensajesDisponibles, False, True, True, False
        If LvMensajesSeleccionados.ListItems.Count > 0 Then
            For i = 1 To LvMensajesDisponibles.ListItems.Count
                For l = 1 To LvMensajesSeleccionados.ListItems.Count
                    If Val(LvMensajesSeleccionados.ListItems(l).Text) = _
                       Val(LvMensajesDisponibles.ListItems(i).Text) Then
                        LvMensajesDisponibles.ListItems(i).SubItems(2) = "SI"
                        Exit For
                    End If
                Next l
                
                
            Next i
            For i = LvMensajesDisponibles.ListItems.Count To 1 Step -1
                If LvMensajesDisponibles.ListItems(i).SubItems(2) = "SI" Then LvMensajesDisponibles.ListItems.Remove i
            Next
        End If
    End If
    
End Sub

Private Sub LvMensajesDisponibles_Click()
    Dim ID_Men As Integer
    Dim Str_Men As String
    If LvMensajesDisponibles.SelectedItem Is Nothing Then Exit Sub
    ID_Men = Val(LvMensajesDisponibles.SelectedItem.Text)
    Str_Men = LvMensajesDisponibles.SelectedItem.SubItems(1)
    
    
    For i = 1 To LvMensajesSeleccionados.ListItems.Count
        If Val(LvMensajesSeleccionados.ListItems(i).Text) = i Then
            Exit Sub
        End If
    Next
    LvMensajesSeleccionados.ListItems.Add , , ID_Men
    LvMensajesSeleccionados.ListItems(LvMensajesSeleccionados.ListItems.Count).SubItems(1) = Str_Men
    
    LvMensajesDisponibles.ListItems.Remove (Me.LvMensajesDisponibles.SelectedItem.Index)
    
    
    
End Sub

Private Sub LvMensajesSeleccionados_Click()
    Dim ID_Men As Integer
    Dim Str_Men As String
    If LvMensajesSeleccionados.SelectedItem Is Nothing Then Exit Sub
    ID_Men = Val(LvMensajesSeleccionados.SelectedItem.Text)
    Str_Men = LvMensajesSeleccionados.SelectedItem.SubItems(1)
    
    
    For i = 1 To LvMensajesDisponibles.ListItems.Count
        If Val(LvMensajesDisponibles.ListItems(i).Text) = i Then
            Exit Sub
        End If
    Next
   LvMensajesDisponibles.ListItems.Add , , ID_Men
    LvMensajesDisponibles.ListItems(LvMensajesDisponibles.ListItems.Count).SubItems(1) = Str_Men
    
    LvMensajesSeleccionados.ListItems.Remove (Me.LvMensajesSeleccionados.SelectedItem.Index)
    
    
End Sub

Private Sub Timer_Timer()
    cmdGraba.SetFocus
    Timer.Enabled = False
    
End Sub
